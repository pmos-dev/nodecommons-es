import * as child_process from 'child_process';

import { Observable, Subject } from 'rxjs';

import { TPropertyObject } from 'tscommons-es-core';

export type TStdOutErr<T> = {
		stdout: T;
		stderr: T;
};

export function commonsExecAsString(
		command: string,
		params: string[],
		timeout?: number,
		cwd?: string,
		env?: TPropertyObject<string>
): Promise<TStdOutErr<string>> {
	return new Promise((resolve: (stdouterr: TStdOutErr<string>) => void, reject: (reason: Error) => void) => {
		child_process.exec(
				[ command, ...params ].join(' '),
				{
						timeout: timeout,
						cwd: cwd,
						env: env
				},
				(error: child_process.ExecException|null, stdout: string, stderr: string): void => {
					if (error) reject(error);
					resolve({
							stdout: stdout,
							stderr: stderr
					});
				}
		);
	});
}

export function commonsExecAsBuffer(
		command: string,
		params: string[],
		timeout?: number,
		cwd?: string,
		env?: TPropertyObject<string>
): Promise<TStdOutErr<Buffer>> {
	return new Promise((resolve: (stdouterr: TStdOutErr<Buffer>) => void, reject: (reason: Error) => void) => {
		child_process.exec(
				[ command, ...params ].join(' '),
				{
						encoding: 'buffer',
						timeout: timeout,
						cwd: cwd,
						env: env
				},
				(error: child_process.ExecException|null, stdout: Buffer, stderr: Buffer): void => {
					if (error) reject(error);
					resolve({
							stdout: stdout,
							stderr: stderr
					});
				}
		);
	});
}

export function commonsExecAsObservables(
		command: string,
		params: string[],
		timeout?: number,
		cwd?: string,
		env?: TPropertyObject<string>
): [ Promise<[ number, NodeJS.Signals ]>, TStdOutErr<Observable<Buffer>>, child_process.ChildProcess ] {
	const stdOutSubject: Subject<Buffer> = new Subject<Buffer>();
	const stdErrSubject: Subject<Buffer> = new Subject<Buffer>();

	const running: child_process.ChildProcess = child_process.spawn(
			command,
			params,
			{
					cwd: cwd,
					timeout: timeout,
					env: env
			}
	);

	running.stdout!.on(
			'data',
			(data: Buffer): void => {
				stdOutSubject.next(data);
			}
	);

	running.stderr!.on(
			'data',
			(data: Buffer): void => {
				stdErrSubject.next(data);
			}
	);

	const promise: Promise<[ number, NodeJS.Signals ]> = new Promise<[ number, NodeJS.Signals ]>((resolve: (outcome: [ number, NodeJS.Signals ]) => void, reject: (e: Error) => void): void => {
		running.on(
				'close',
				(code: number, signal: NodeJS.Signals): void => {
					stdOutSubject.complete();
					stdErrSubject.complete();

					resolve([ code, signal ]);
				}
		);
		
		running.on(
				'error',
				(e: Error): void => {
					stdOutSubject.complete();
					stdErrSubject.complete();

					reject(e);
				}
		);

		running.stdout!.on(
				'error',
				(e: Error): void => {
					stdOutSubject.complete();
					stdErrSubject.complete();

					reject(e);
				}
		);
		
		running.stderr!.on(
				'error',
				(e: Error): void => {
					stdOutSubject.complete();
					stdErrSubject.complete();

					reject(e);
				}
		);
	});

	return [
			promise,
			{
					stdout: stdOutSubject.asObservable(),
					stderr: stdErrSubject.asObservable()
			},
			running
	];
}
