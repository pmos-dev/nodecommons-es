import * as child_process from 'child_process';

import { CommonsConfig } from 'tscommons-es-config';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import {
		commonsFileMkdirRecursive,
		commonsFileWriteTextFile,
		commonsFileRm
} from 'nodecommons-es-file';
import { CommonsConfigFile } from 'nodecommons-es-config';

export function commonsSystemdInstall(
		name: string,
		command: string,
		description: string,
		config?: CommonsConfig | CommonsConfigFile,
		configPath?: string,
		configFilename?: string,
		group?: string,
		autoJoinGroup: boolean = false,
		restart: string = 'always',
		restartSec: number = 5,
		autoEnable: boolean = true,
		autoStart: boolean = false
): void {
	if (config) {
		if (config instanceof CommonsConfigFile) {
			if (config.getFilepath() !== undefined) {
				commonsOutputDebug(`Saving config into file: ${config.getFilepath()!}`);

				const paths: string[] = config.getFilepath()!.split('/');
				paths.pop();	// filename
				
				commonsFileMkdirRecursive(paths.join('/'));
				
				config.save();
			} else {
				if (!configPath) configPath = '/etc';
				if (!configFilename) configFilename = `${name}.json`;
				
				const configFilePath: string = `${configPath}/${configFilename}`;
				commonsOutputDebug(`Saving config into file: ${configFilePath}`);
				
				const paths: string[] = configFilePath.split('/');
				paths.pop();

				commonsFileMkdirRecursive(paths.join('/'));
				
				config.saveAs(configFilePath);
			}
		} else {
			if (!configPath) configPath = '/etc';
			if (!configFilename) configFilename = `${name}.json`;
			
			const configFilePath: string = `${configPath}/${configFilename}`;
			
			const dest: CommonsConfigFile = new CommonsConfigFile(configFilePath);
			dest.cloneConfig(config);
			
			return commonsSystemdInstall(
					name,
					command,
					description,
					dest,
					undefined,
					undefined,
					group,
					autoJoinGroup,
					restart,
					restartSec,
					autoEnable,
					autoStart
			);
		}
	}
	
	const systemdFile: string = `[Unit]
Description=${description}

[Service]
Type=simple
ExecStart=${command}
Restart=${restart}
RestartSec=${restartSec}
SyslogIdentifier=${name}
${group ? `Group=${group}\n` : ''}
[Install]
WantedBy=multi-user.target`;

	commonsFileWriteTextFile(`/etc/systemd/system/${name}.service`, systemdFile);
	
	const commands: string[] = [];
	
	if (group) {
		const groupCmd: string[] = [
				`getent group ${group} >/dev/null || groupadd ${group}`
		];
		
		if (autoJoinGroup) {
			groupCmd.push(
					`gpasswd -a $SUDO_USER ${group}`
			);
		}
		
		commands.push(groupCmd.join(';'));
	}

	if (autoEnable) {
		commands.push(
				`systemctl enable ${name}.service`
		);
	}
	
	if (autoStart) {
		commands.push(
				`systemctl start ${name}.service`
		);
	}

	if (commands.length > 0) {
		child_process.spawnSync(
				'bash',
				[
						'-c',
						commands.join(';')
				],
				{
						stdio: [ process.stdin, process.stdout, process.stderr ]
				}
		);
	}
}

export function commonsSystemdUninstall(
		name: string,
		group?: string
): void {
	const commands: string[] = [
			`systemctl stop ${name}.service`,
			`systemctl disable ${name}.service`
	];
	
	if (group) {
		commands.push(...[
				`gpasswd -d $SUDO_USER ${name}`,
				`groupdel ${name}`
		]);
	}

	child_process.spawnSync(
			'bash',
			[
					'-c',
					commands.join(';')
			],
			{
					stdio: [ process.stdin, process.stdout, process.stderr ]
			}
	);
	
	commonsFileRm(`/etc/systemd/system/${name}.service`);
}
