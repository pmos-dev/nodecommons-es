export enum ECommonsExitCode {
		SUCCESS = 0,
		ERROR = 1,
		MISUSE = 2,
		UNAVAILABLE = 64,	// custom
		FOREVERRESTART = 98,	// custom
		FOREVERSTOP = 99,	// custom
		INTERRUPT = 130,
		SEGFAULT = 139
}
