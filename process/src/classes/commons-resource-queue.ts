import { commonsArrayRemove, commonsBase62GenerateLongRandomId } from 'tscommons-es-core';
import { ECommonsInvocation, commonsAsyncAbortInterval, commonsAsyncInterval } from 'tscommons-es-async';

type TResourceQueueEntry<T> = {
		ticket: string;
		params: T;
		queued: number;
};

export abstract class CommonsResourceQueue<
		ResourceT,
		ParamsT
> {
	private queuedEntries: TResourceQueueEntry<ParamsT>[] = [];
	private activeEntries: TResourceQueueEntry<ParamsT>[] = [];

	constructor(
			private id: string,
			private maxActive: number,
			private dispatchTimeout: number,
			private executionTimeout: number,
			private spinInterval: number = 500
	) {}

	public get queueLength(): number {
		return this.queuedEntries.length;
	}

	public get activeLength(): number {
		return this.activeEntries.length;
	}

	public get lineLength(): number {
		return this.queueLength + this.activeLength;
	}

	public start(): void {
		commonsAsyncInterval(
				this.spinInterval,
				ECommonsInvocation.FIXED,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (): Promise<void> => {
					this.considerTimeout();
					await this.considerDispatch();
				},
				`resourceQueueLoop-${this.id}`
		);
	}

	public stop(): void {
		commonsAsyncAbortInterval(`resourceQueueLoop-${this.id}`);
	}

	protected abstract timedout(ticket: string, params: ParamsT): void;
	protected abstract cancelled(ticket: string, params: ParamsT): void;
	protected abstract dispatched(ticket: string, params: ParamsT): Promise<void>;
	protected abstract getResource(params: ParamsT): Promise<ResourceT>;
	protected abstract freeResource(resource: ResourceT): Promise<void>;

	private considerTimeout(): void {
		if (this.queuedEntries.length === 0) return;

		const expiration: number = new Date().getTime() - this.dispatchTimeout;

		const expireds: TResourceQueueEntry<ParamsT>[] = this.queuedEntries
				.filter((entry: TResourceQueueEntry<ParamsT>): boolean => entry.queued < expiration);
		
		for (const expired of expireds) {
			this.cancel(expired.ticket, true);
			this.timedout(expired.ticket, expired.params);
		}
	}

	protected abstract go(
			resource: ResourceT,
			ticket: string,
			params: ParamsT,
			executionTimeout: number
	): Promise<void>;

	private async dispatch(dispatched: TResourceQueueEntry<ParamsT>): Promise<void> {
		await this.dispatched(
				dispatched.ticket,
				dispatched.params
		);

		const resource: ResourceT = await this.getResource(dispatched.params);

		// run in a separate promise to prevent blocking
		void (async (): Promise<void> => {
			try {
				await this.go(resource, dispatched.ticket, dispatched.params, this.executionTimeout);
			} catch (e) {
				console.error(e);
				// ignore
			} finally {
				try {
					await this.freeResource(resource);
				} catch (e) {
					console.error(e);
					// ignore
				}

				const match: TResourceQueueEntry<ParamsT>|undefined = this.activeEntries
						.find((entry: TResourceQueueEntry<ParamsT>): boolean => entry.ticket === dispatched.ticket);
				if (match) {
					commonsArrayRemove(
							this.activeEntries,
							match,
							(a: TResourceQueueEntry<ParamsT>, b: TResourceQueueEntry<ParamsT>): boolean => a.ticket === b.ticket
					);
				}
			}
		})();
	}

	private async considerDispatch(): Promise<void> {
		if (this.activeEntries.length >= this.maxActive) return;
		if (this.queuedEntries.length === 0) return;

		const space: number = Math.min(this.maxActive - this.activeEntries.length, this.queuedEntries.length);
		const dispatcheds: TResourceQueueEntry<ParamsT>[] = this.queuedEntries.splice(0, space);

		this.activeEntries.push(...dispatcheds);

		const promises: Promise<void>[] = dispatcheds
				.map((dispatched: TResourceQueueEntry<ParamsT>): Promise<void> => this.dispatch(dispatched));

		await Promise.all(promises);
	}

	public queue(params: ParamsT): string {
		const ticket: string = commonsBase62GenerateLongRandomId();

		this.queuedEntries.push({
				ticket: ticket,
				params: params,
				queued: new Date().getTime()
		} as TResourceQueueEntry<ParamsT>);

		return ticket;
	}

	public cancel(ticket: string, suppressCancelledCallback: boolean = false): boolean {
		const match: TResourceQueueEntry<ParamsT>|undefined = this.queuedEntries
				.find((entry: TResourceQueueEntry<ParamsT>): boolean => entry.ticket === ticket);
		
		if (match) {
			if (!suppressCancelledCallback) this.cancelled(match.ticket, match.params);
			
			commonsArrayRemove(
					this.queuedEntries,
					match,
					(a: TResourceQueueEntry<ParamsT>, b: TResourceQueueEntry<ParamsT>): boolean => a.ticket === b.ticket
			);
			return true;
		}

		return false;
	}
}
