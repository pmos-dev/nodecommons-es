import * as child_process from 'child_process';

import {
		commonsOutputInfo,
		commonsOutputDebug,
		commonsOutputError
} from 'nodecommons-es-cli';
import { commonsFileDistRelativePath } from 'nodecommons-es-file';

import { ECommonsExitCode } from '../enums/ecommons-exit-code';

type TForkOptions = {
		cwd?: string;
		env?: {  [ key: string ]: string };
		stdio?: ('pipe'|'ipc')[];
		detached?: boolean;
		execArgv?: string[];
};

export abstract class CommonsForever {
	// helper method for child processes
	public static ready(): void {
		if (process.send !== undefined) process.send({ message: 'ready' });
	}
	
	private child: child_process.ChildProcess|undefined;

	private firstStarted: boolean = true;
	private startTime: number = 0;

	private command: string;
	private args: string[];
	private options: TForkOptions;
	
	constructor(
			private script: string,
			private permittedExitCodes: number[] = [
					ECommonsExitCode.SUCCESS,
					ECommonsExitCode.MISUSE
			],
			private minUptime: number = 2000,
			private tsConfigPaths: boolean = false,
			args?: string[]
	) {
		if (this.permittedExitCodes.includes(ECommonsExitCode.FOREVERSTOP)) throw Error('CommonsForever cannot use FOREVERSTOP as a permitted exit code');
		if (this.permittedExitCodes.includes(ECommonsExitCode.FOREVERRESTART)) throw Error('CommonsForever cannot use FOREVERRESTART as a permitted exit code');
		
		this.command = commonsFileDistRelativePath(this.script);
		
		this.args = args === undefined ? process.argv.slice(2) : args;
		
		this.options = {
				cwd: process.cwd(),
				stdio: [ 'pipe', 'pipe', 'pipe', 'ipc' ]
		};
		if (process.platform === 'win32') this.options.detached = true;
		if (this.tsConfigPaths) this.options.execArgv = '-r ./tsconfig-paths-bootstrap.js'.split(' ');
	}
	
	public start(): void {
		commonsOutputInfo('CommonsForever process running');

		this.firstStarted = true;
		this.fork();
	}
	
	public isChildActive(): boolean {
		return this.child !== undefined;
	}
	
	public abstract handleReady(first: boolean): void;
	public abstract handleMessage(message: string): void;
	
	public fork(): void {
		if (this.child !== undefined) throw new Error('Child process already exists');
		
		commonsOutputInfo('Forking child process');
		
		this.startTime = new Date().getTime();
		
		this.child = child_process.fork(
				this.command,
				this.args,
				this.options
		);
		
		this.child.on('error', (_: Error): void => {
			if (this.child === undefined) return;
			if (!this.child.connected) return;
			
			this.child = undefined;
		});
		
		this.child.on('message', (message: { message: string }): void => {
			if (message.message === 'ready') this.handleReady(this.firstStarted);
			this.firstStarted = false;
			
			this.handleMessage(message.message);
		});
		
		this.child.on('exit', (code: number|null, signal: string|null): void => {
			this.child = undefined;
			commonsOutputDebug(`Child process exited with exit code ${code || 'null'} and signal ${signal || 'null'}`);
			
			if (code !== null && this.permittedExitCodes.includes(code)) {
				void this.shutdown(code);
				return;
			}
			if (signal === 'SIGINT') {
				void this.shutdown(ECommonsExitCode.INTERRUPT);
				return;
			}
			
			const endTime: number = new Date().getTime();
			const delta: number = endTime - this.startTime;
			if (delta < this.minUptime) {
				commonsOutputError('Minimum uptime not reached. Aborting');
				void this.shutdown(ECommonsExitCode.ERROR);
				return;
			}
			
			commonsOutputInfo('Non-permitted exit code. Restarting');
			process.nextTick(
					(): void => {
						this.fork();
					}
			);
		});
		
		if (this.child.stdout !== null) {
			this.child.stdout.on('data', (data: string): void => {
				process.stdout.write(data);
			});
		}
		if (this.child.stderr !== null) {
			this.child.stderr.on('data', (data: string): void => {
				process.stderr.write(data);
			});
		}
	}
	
	public send(message: string): Promise<void> {
		return new Promise((resolve: () => void, reject: (reason: string) => void): void => {
			if (this.child === undefined) {
				reject('No client is available');
				return;
			}
			
			this.child.send(
					{ message: message },
					(outcome: null|Error): void => {
						if (outcome === null) resolve();
						else reject(outcome.message);
					}
			);
		});
	}
	
	public terminateChild(): void {
		if (this.child !== undefined) this.child.kill('SIGTERM');
	}
	
	public async shutdown(code: number): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		process.exitCode = code;

		commonsOutputInfo('Stopping CommonsForever process');
		
		this.terminateChild();
	}
		
}
