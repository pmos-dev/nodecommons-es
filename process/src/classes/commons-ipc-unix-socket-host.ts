import * as fs from 'fs';
import * as net from 'net';
import { Server, Socket } from 'net';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { CommonsIpcUnixSocket } from './commons-ipc-unix-socket';
import { CommonsIpcUnixSocketClient } from './commons-ipc-unix-socket-client';

export class CommonsIpcUnixSocketHost extends CommonsIpcUnixSocket {
	private server: Server;
	private sockets: Socket[] = [];
	
	constructor(
			name: string,
			private chmodPermissions?: string
	) {
		super(name);
		
		this.server = net.createServer();
	}
	
	private disposeSocket(socket: Socket): void {
		const index: number = this.sockets.indexOf(socket);
		if (index > -1) this.sockets.splice(index, 1);
		
		socket.unref();
	}
	
	public async closeServer(): Promise<void> {
		return new Promise((resolve: () => void, _: (_: any) => void): void => {
			if (!this.server || !this.server.listening) {
				resolve();
				return;
			}
			
			this.server.close((): void => {
				resolve();
				this.server.unref();
			});
		});
	}
	
	public async listen(
			callback: (data: string, socket: Socket) => void
	): Promise<void> {
		commonsOutputDebug('Setting up listen for IPC server');
		
		return new Promise((resolve: () => void, reject: (reason: any) => void): void => {
			this.server.listen(
					this.getPath(),
					(): void => {
						if (this.chmodPermissions) fs.chmodSync(this.getPath(), this.chmodPermissions);
						resolve();
					}
			);
			
			this.server.on(
					'error',
					(error: NodeJS.ErrnoException): void => {
						if (error.code === 'EADDRINUSE') {
							commonsOutputDebug('Unix socket is already in use!');
							
							commonsOutputDebug('Attempting removal and recreation');
							const test: CommonsIpcUnixSocketClient = new CommonsIpcUnixSocketClient(this.getName());
							void (async (): Promise<void> => {
								try {
									// eslint-disable-next-line @typescript-eslint/no-empty-function
									await test.listen((_: string): void => {});
									test.destroy();
									
									reject(new Error('Existing Unix socket on that path being used'));
									return;
								} catch (e) {
									test.destroy();
									
									if ((e as any).code === 'ECONNREFUSED') {	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
										
										commonsOutputDebug(`Unlinking existing IPC socket path at ${this.getPath()}`);
										fs.unlinkSync(this.getPath());
										
										try {
											commonsOutputDebug('Reattempting to listen');
											this.server.listen(
													this.getPath(),
													(): void => {
														commonsOutputDebug('Recreation succeeded');
														resolve();
													}
											);
											return;
										} catch (e2) {
											reject(e2);
											return;
										}
									}
									
									reject(e);
								}
							})();
						}
						
						reject(error);
					}
			);

			this.server.on(
					'connection',
					(socket: Socket): void => {
						socket.setEncoding('utf8');
						
						this.sockets.push(socket);
						
						socket.on(
								'close',
								(_: boolean): void => {
									this.disposeSocket(socket);
								}
						);
						socket.on(
								'end',
								(): void => {
									this.disposeSocket(socket);
								}
						);
						
						socket.on(
								'data',
								(data: string): void => {
									callback(data, socket);
								}
						);
					}
			);
		});
	}

	public async send(data: string): Promise<boolean> {
		return new Promise((resolve: (_: boolean) => void, _: (_: any) => void): void => {
			let success: number = 0;
			let failure: number = 0;
			
			for (const socket of this.sockets) {
				try {
					socket.write(
							data,
							(): void => {
								success++;
								
								if ((success + failure) === this.sockets.length) resolve(failure === 0);
							}
					);
				} catch (e) {
					failure++;
								
					if ((success + failure) === this.sockets.length) resolve(failure === 0);
				}
			}
		});
	}
	
	public async shutdown(wait: boolean = false): Promise<void> {
		return new Promise((resolve: () => void, _: (_: any) => void): void => {
			for (const socket of this.sockets) socket.end();
			
			if (!wait) {
				resolve();
				return;
			}
			
			const interval = setInterval((): void => {	// easier to just untype this rather than use any
				if (this.sockets.length === 0) {
					clearInterval(interval);
					resolve();
					return;
				}
			}, 100);
		});
	}
}
