import {
		commonsAsyncTimeout,
		commonsAsyncInterval,
		commonsAsyncAbortInterval,
		ECommonsInvocation
} from 'tscommons-es-async';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { CommonsIpcUnixSocketClient } from './commons-ipc-unix-socket-client';

export class CommonsIpcUnixSocketAutoReconnectClient extends CommonsIpcUnixSocketClient {
	private connected: boolean = false;
	private intervalName: string;
	
	constructor(
			name: string,
			private maxTtl: number,
			private reconnectDelay: number = 1000,
			private noopInterval: number = 1000,
			private noopCommand: string = '_noop',
			private failureCallback?: () => void,
			private connectedCallback?: (state: boolean) => void,
			private requireInitialConnection: boolean = true
	) {
		super(name);
		
		this.intervalName = `commons_ipc_unix_socket_auto_reconnect_client_${name}`;
	}
	
	public isConnected(): boolean {
		return this.connected;
	}
	
	private setConnected(state: boolean): void {
		const existing: boolean = this.connected;
		
		this.connected = state;
		
		if (this.connectedCallback && state !== existing) this.connectedCallback(state);
	}

	private async reconnect(
			callback: (data: string) => void,
			ttl: number = -1
	): Promise<boolean> {
		if (ttl === 0) {	// allows -1 to start an infinite reconnect
			commonsOutputDebug('TTL on reconnection expired');
			return false;
		}
		
		commonsOutputDebug(`Attempting reconnection to unix domain socket. TTL is ${ttl}`);

		try {
			await super.listen(callback);
			
			this.setConnected(true);
			commonsOutputDebug('Connected successfully');
			
			return true;
		} catch (e) {
			if (ttl > 1) await commonsAsyncTimeout(this.reconnectDelay);
			return await this.reconnect(callback, ttl - 1);
		}
	}

	public async listen(
			callback: (data: string) => void
	): Promise<void> {
		if (this.requireInitialConnection) {
			if (!(await this.reconnect(callback, 1))) throw new Error('Unable to connect');
		}
		
		commonsAsyncInterval(
				this.noopInterval,
				ECommonsInvocation.IF,
				async (): Promise<void> => {
					if (!this.connected) {
						commonsOutputDebug('Attempting reconnection to unix domain socket');
						
						if (!(await this.reconnect(callback, this.maxTtl))) {
							commonsOutputDebug('Timed out attempting to connect');
							
							commonsAsyncAbortInterval(this.intervalName);
							
							if (this.failureCallback) this.failureCallback();
						}
					} else {
						try {
							await super.send(this.noopCommand);
							this.setConnected(true);
						} catch (e) {
							this.setConnected(false);
							
							commonsOutputDebug('Lost connection to unix domain socket during noop');
			
							try {
								super.destroy();
							} catch (_e2) {
								// do nothing
							}
						}
					}
				},
				this.intervalName
		);
	}
	
	public async send(data: string): Promise<void> {
		if (!this.connected) throw new Error('Not connected');
		
		try {
			await super.send(data);
		} catch (e) {
			this.setConnected(false);
			commonsOutputDebug('Lost connection to unix domain socket during send');
			
			throw e;
		}
	}
	
	public close(): void {
		commonsAsyncAbortInterval(this.intervalName);
		
		super.close();
	}
	
	public destroy(): void {
		commonsAsyncAbortInterval(this.intervalName);
		
		super.destroy();
	}
}
