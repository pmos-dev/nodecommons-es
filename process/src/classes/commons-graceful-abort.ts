import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { replaceCommonsKeyPressSigIntHandler } from 'nodecommons-es-cli';

class InternalCommonsGracefulAbort {
	private callbacks: Map<string, () => void>;
	private abort: boolean = false;
	
	constructor() {
		this.callbacks = new Map<string, () => void>();
		
		this.init();
	}

	private sigIntHandler(): void {
		if (this.abort) {
			console.log('Second SIGINT detected. Forcing hard abort.');
			process.exit(128 + 2);	// 128 + SIGINT=2
		}

		console.log('SIGINT detected. Attempting clean shutdown.');
		this.abort = true;
		
		const keys: string[] = Array.from(this.callbacks.keys());
		for (const key of keys) {
			const callback: (() => void)|undefined = this.callbacks.get(key);
			this.callbacks.delete(key);
			
			if (callback !== undefined) callback();
		}
	}
	
	private init(): void {
		process.on('SIGINT', (): void => {
			this.sigIntHandler();
		});

		// CommonsKeyPress suppresses SIGINT, so we have to set a handler inside it manually
		replaceCommonsKeyPressSigIntHandler((): void => {
			this.sigIntHandler();
		});
	}
	
	public addCallback(callback: () => void): string {
		const id: string = commonsBase62GenerateRandomId();
		this.callbacks.set(id, callback);
		
		return id;
	}
	
	public removeCallback(id: string): void {
		this.callbacks.delete(id);
	}
	
	public isAborted(): boolean {
		return this.abort;
	}
}

const internal: InternalCommonsGracefulAbort = new InternalCommonsGracefulAbort();

export function commonsGracefulAbortAddCallback(callback: () => void): string {
	return internal.addCallback(callback);
}

export function commonsGracefulAbortRemoveCallback(id: string): void {
	internal.removeCallback(id);
}
export function commonsGracefulAbortIsAborted(): boolean {
	return internal.isAborted();
}
