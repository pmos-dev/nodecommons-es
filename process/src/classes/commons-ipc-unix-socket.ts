import * as os from 'os';
import * as path from 'path';

export abstract class CommonsIpcUnixSocket {
	private path: string;
	
	constructor(
			private name: string
	) {
		this.path = path.join(os.tmpdir(), `${name}.sock`);
	}
	
	public getPath(): string {
		return this.path;
	}
	
	public getName(): string {
		return this.name;
	}
}
