// NB, this uses the /run/shm system, not direct shared memory
// However it amounts to much the same

import { commonsFileExists, commonsFileReadBinaryFileAsync, commonsFileReadTextFileAsync, commonsFileRm, commonsFileWriteBinaryFileAsync, commonsFileWriteTextFileAsync } from 'nodecommons-es-file';

export class CommonsShm {
	constructor(
			private context: string,
			private shmPath: string = '/run/shm/'
	) {
		if (/[^-a-z0-9]/i.test(context)) throw new Error('Context can only contain -a-z0-9 characters');

		if (!this.shmPath.endsWith('/')) this.shmPath = `${this.shmPath}/`;
	}

	private getFile(id: string): string {
		if (/[^-a-z0-9]/i.test(id)) throw new Error('id can only contain -a-z0-9 characters');

		const filename: string = `${this.context}_${id}.temp.shm`;
		const file: string = `${this.shmPath}${filename}`;

		return file;
	}

	public read(id: string): Promise<Buffer|undefined>{
		const file: string = this.getFile(id);

		return commonsFileReadBinaryFileAsync(file);
	}

	public readString(id: string): Promise<string|undefined>{
		const file: string = this.getFile(id);

		return commonsFileReadTextFileAsync(file);
	}

	public async readInt8s(id: string): Promise<Int8Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Int8Array(buffer.buffer);
	}

	public async readInt16s(id: string): Promise<Int16Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Int16Array(buffer.buffer);
	}

	public async readInt32s(id: string): Promise<Int32Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Int32Array(buffer.buffer);
	}

	public async readUint8s(id: string): Promise<Uint8Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Uint8Array(buffer.buffer);
	}

	public async readUint16s(id: string): Promise<Uint16Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Uint16Array(buffer.buffer);
	}

	public async readUint32s(id: string): Promise<Uint32Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Uint32Array(buffer.buffer);
	}

	public async readFloat32s(id: string): Promise<Float32Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Float32Array(buffer.buffer);
	}

	public async readFloat64s(id: string): Promise<Float64Array|undefined> {
		const buffer: Buffer|undefined = await this.read(id);
		if (!buffer) return undefined;

		return new Float64Array(buffer.buffer);
	}

	public write(
			id: string,
			data: Buffer
	): Promise<void>{
		const file: string = this.getFile(id);

		return commonsFileWriteBinaryFileAsync(file, data);
	}

	public writeString(
			id: string,
			s: string
	): Promise<void> {
		const file: string = this.getFile(id);

		return commonsFileWriteTextFileAsync(file, s);
	}

	public writeTypedArray(
			id: string,
			array: Uint8Array|Uint16Array|Uint32Array|Int8Array|Int16Array|Int32Array|Float32Array|Float64Array
	): Promise<void> {
		return this.write(id, Buffer.from(array.buffer));
	}

	public writeInt8s(
			id: string,
			array: Int8Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeInt16s(
			id: string,
			array: Int16Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeInt32s(
			id: string,
			array: Int32Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeUint8s(
			id: string,
			array: Uint8Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeUint16s(
			id: string,
			array: Uint16Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeUint32s(
			id: string,
			array: Uint32Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeFloat32s(
			id: string,
			array: Float32Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public writeFloat64s(
			id: string,
			array: Float64Array
	): Promise<void> {
		return this.writeTypedArray(id, array);
	}

	public free(id: string): void {
		const file: string = this.getFile(id);

		if (!commonsFileExists(file)) return;

		commonsFileRm(file);
	}
}
