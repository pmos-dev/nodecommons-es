import { Socket } from 'net';

import { CommonsIpcUnixSocketHost } from './commons-ipc-unix-socket-host';

export class CommonsIpcUnixSocketAutoReconnectHost extends CommonsIpcUnixSocketHost {
	constructor(
			name: string,
			chmodPermissions?: string,
			private noopCommand: string = '_noop'
	) {
		super(name, chmodPermissions);
	}
	
	public async listen(
			callback: (data: string, socket: Socket) => void
	): Promise<void> {
		await super.listen(
				(data: string, socket: Socket): void => {
					if (data === this.noopCommand) return;
					callback(data, socket);
				}
		);
	}
}
