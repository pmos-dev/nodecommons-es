import * as net from 'net';
import { Socket } from 'net';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { CommonsIpcUnixSocket } from './commons-ipc-unix-socket';

type TRemoteClosedCallback = () => void;

export class CommonsIpcUnixSocketClient extends CommonsIpcUnixSocket {
	private socket: Socket|undefined = undefined;
	private remoteClosedCallback: TRemoteClosedCallback;
	
	constructor(
			name: string
	) {
		super(name);
		
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		this.remoteClosedCallback = (): void => {};
	}
	
	public async listen(
			callback: (data: string) => void
	): Promise<void> {
		return new Promise((resolve: () => void, reject: (error: Error) => void): void => {
			this.socket = net.createConnection(
					this.getPath(),
					(): void => {
						resolve();
					}
			);
			this.socket.setEncoding('utf8');
			
			this.socket.on(
					'error',
					(error: NodeJS.ErrnoException): void => {
						if (error.code === 'EPIPE') {
							this.remoteClosedCallback();
							return;
						}
						
						reject(error);
					}
			);
	
			this.socket.on(
					'data',
					(data: string): void => {
						callback(data);
					}
			);
		});
	}

	public send(data: string): Promise<void> {
		commonsOutputDebug(`Sending data: ${data}`);
		return new Promise((resolve: () => void, reject: (error: Error) => void): void => {
			if (this.socket === undefined) {
				reject(new Error('Unix IPC socket is not open yet'));
				return;
			}
			
			try {
				this.remoteClosedCallback = (): void => {
					reject(new Error('Unix IPC socket no longer available'));
				};
				
				this.socket.write(
						data,
						(): void => {
							resolve();
						}
				);
			} catch (e) {
				reject(e as Error);
			}
		});
	}
	
	public close(): void {
		if (this.socket === undefined) return;
		this.socket.end();
	}
	
	public destroy(): void {
		if (this.socket === undefined) return;
		this.socket.destroy();
		this.socket.unref();
	}
}
