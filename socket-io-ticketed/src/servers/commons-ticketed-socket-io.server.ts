import { ServerOptions, Socket } from 'socket.io';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { ICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsAppSocketIoServer } from 'nodecommons-es-app-socket-io';

export class CommonsTicketedSocketIoServer extends CommonsAppSocketIoServer {
	private ticketSocketIdMap: Map<string, string> = new Map<string, string>();

	constructor(
			expressServer: CommonsStrictExpressServer,
			expressConfig: ICommonsExpressConfig,
			private ns: string,
			defaultEnabled: boolean = true,
			options?: Partial<ServerOptions>,
			private debug: boolean = false
	) {
		super(
				expressServer,
				expressConfig,
				defaultEnabled,
				options
		);
	}

	public register(ticket: string, socketId: string): void {
		this.ticketSocketIdMap.set(ticket, socketId);
	}

	public deregister(ticket: string): void {
		this.ticketSocketIdMap.delete(ticket);
	}

	private getSocketByTicket(ticket: string): Socket|false {
		const socketId: string|undefined = this.ticketSocketIdMap.get(ticket);
		if (!socketId) {
			if (this.debug) commonsOutputDebug(`No socketId found for ticket ${ticket}`);
			return false;
		}

		const socket: Socket|undefined = this.getSocketById(socketId);
		if (!socket) {
			if (this.debug) commonsOutputDebug(`No Socket found for ticket ${ticket}, socketId ${socketId}`);
			return false;
		}

		return socket;
	}

	public dispatched(ticket: string): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		void this.direct(
				socket,
				`${this.ns}-ticketed-dispatched`,
				{
						ticket: ticket
				}
		);
	}
	
	public cancelled(ticket: string): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		this.deregister(ticket);

		void this.direct(
				socket,
				`${this.ns}-ticketed-cancelled`,
				{
						ticket: ticket
				}
		);
	}
	
	public timedout(ticket: string): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		this.deregister(ticket);

		void this.direct(
				socket,
				`${this.ns}-ticketed-timedout`,
				{
						ticket: ticket
				}
		);
	}
	
	public error(ticket: string, message: string): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		this.deregister(ticket);

		void this.direct(
				socket,
				`${this.ns}-ticketed-error`,
				{
						ticket: ticket,
						message: message
				}
		);
	}

	public progress(
			ticket: string,
			src: 'stdout'|'stderr',
			data: string
	): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		void this.direct(
				socket,
				`${this.ns}-ticketed-progress`,
				{
						ticket: ticket,
						src: src,
						data: data
				}
		);
	}

	public succeeded(
			ticket: string,
			data: string
	): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		this.deregister(ticket);

		void this.direct(
				socket,
				`${this.ns}-ticketed-succeeded`,
				{
						ticket: ticket,
						data: data
				}
		);
	}

	public failed(
			ticket: string,
			data: string
	): void {
		const socket: Socket|false = this.getSocketByTicket(ticket);
		if (!socket) return;

		this.deregister(ticket);

		void this.direct(
				socket,
				`${this.ns}-ticketed-failed`,
				{
						ticket: ticket,
						data: data
				}
		);
	}
}
