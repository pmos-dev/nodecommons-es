import { Observable } from 'rxjs';

import {
		CommonsTicketedSocketIoService,
		ICommonsTicketedSocketIoWithAutoSetup,
		TCommonsTicketedSocketIoError,
		TCommonsTicketedSocketIoFailed,
		TCommonsTicketedSocketIoProgress,
		TCommonsTicketedSocketIoSucceeded
} from 'tscommons-es-socket-io-ticketed';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsSocketIoClientService, commonsSocketIoBuildOptions, commonsSocketIoBuildUrl } from 'nodecommons-es-socket-io';

export class CommonsTicketedGenericSocketIoClientService extends CommonsSocketIoClientService implements ICommonsTicketedSocketIoWithAutoSetup {
	private ticketedService: CommonsTicketedSocketIoService;

	public get socketIoId(): string|undefined {
		return this.ticketedService.socketIoId;
	}
	public set socketIoId(id: string|undefined) {
		this.ticketedService.socketIoId = id;
	}

	public get dispatchedObservable(): Observable<string> {
		return this.ticketedService.dispatchedObservable;
	}

	public get timedoutObservable(): Observable<string> {
		return this.ticketedService.timedoutObservable;
	}

	public get cancelledObservable(): Observable<string> {
		return this.ticketedService.cancelledObservable;
	}

	public get errorObservable(): Observable<TCommonsTicketedSocketIoError> {
		return this.ticketedService.errorObservable;
	}

	public get progressObservable(): Observable<TCommonsTicketedSocketIoProgress> {
		return this.ticketedService.progressObservable;
	}

	public get succeededObservable(): Observable<TCommonsTicketedSocketIoSucceeded> {
		return this.ticketedService.succeededObservable;
	}

	public get failedObservable(): Observable<TCommonsTicketedSocketIoFailed> {
		return this.ticketedService.failedObservable;
	}

	constructor(
			url: string,
			ns: string
	) {
		super(
				commonsSocketIoBuildUrl(url),
				true,
				commonsSocketIoBuildOptions(url)
		);

		this.ticketedService = new CommonsTicketedSocketIoService(ns);

		super.addConnectCallback(
				(): void => {
					commonsOutputDebug('SocketIoService: connected');
				}
		);
		super.addDisconnectCallback(
				(): void => {
					commonsOutputDebug('SocketIoService: disconnected');
				}
		);
	}

	protected setupOns(): void {
		this.ticketedService.setupOns(
				(
						command: string,
						callback: (data: any) => void
				): void => {
					this.on(
							command,
							callback
					);
				}
		);
	}
}
