import { TEncoded, TEncodedObject } from 'tscommons-es-core';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsHttpMethod, TCommonsHttpRequestOptions } from 'tscommons-es-http';
import {
		ICommonsTicketedSocketIo,
		TCommonsTicketedSocketIoProgress,
		TPromiseWithObservables,
		commonsTicketedSubmitAndWaitAsPromise,
		commonsTicketedSubmitAndWaitAsPromiseWithAutoSocketIoSetupAndDisconnect,
		commonsTicketedSubmitAndWaitAsPromiseWithObservables,
		commonsTicketedSubmitAndWaitAsPromiseWithObservablesWithAutoSocketIoSetupAndDisconnect
} from 'tscommons-es-socket-io-ticketed';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsSocketIoClientService } from 'nodecommons-es-socket-io';

import { CommonsTicketedGenericSocketIoClientService } from '../services/commons-ticketed-generic-socket-io-client.service';

export async function commonsTicketedNodeSubmitAndWaitAsPromise<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: CommonsSocketIoClientService & ICommonsTicketedSocketIo,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		queued?: (ticketId: string) => void,
		dispatched?: () => void,
		progress?: (data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>) => void,
		debug: boolean = true
): Promise<ResultT> {
	try {
		return await commonsTicketedSubmitAndWaitAsPromise(
				restService,
				socketIoService,
				(callback: (socketIoId: string) => void): void => {
					socketIoService.addConnectCallback((): void => {
						void (async (): Promise<void> => {
							const socketIoId: string|undefined = await socketIoService.getId();
							if (!socketIoId) throw new Error('Unable to obtain socket.io ID');
							
							callback(socketIoId);
						})();
					});
					socketIoService.connect();
				},
				verb,
				script,
				body,
				checker,
				params,
				headers,
				options,
				connectToSubmitDelay,
				queued,
				dispatched,
				progress,
				(message: string): void => {
					if (debug) commonsOutputDebug(message);
				}
		);
	} finally {
		socketIoService.disconnect();
	}
}

export async function commonsTicketedNodeGenericSubmitAndWaitAsPromise<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		socketIoUrl: string,
		socketIoNs: string,
		restService: CommonsRestClientService,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		queued?: (ticketId: string) => void,
		dispatched?: () => void,
		progress?: (data: Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>) => void,
		debug: boolean = true
): Promise<ResultT> {
	const socketIoService: CommonsTicketedGenericSocketIoClientService = new CommonsTicketedGenericSocketIoClientService(socketIoUrl, socketIoNs);

	// Can't use commonsTicketedNodeSubmitAndWaitAsPromise as that recreates the auto setup stuff. See comment in callback.

	return await commonsTicketedSubmitAndWaitAsPromiseWithAutoSocketIoSetupAndDisconnect(
			restService,
			socketIoService,
			verb,
			script,
			body,
			checker,
			params,
			headers,
			options,
			connectToSubmitDelay,
			queued,
			dispatched,
			progress,
			(message: string): void => {
				if (debug) commonsOutputDebug(message);
			}
	);
}

export function commonsTicketedNodeSubmitAndWaitAsPromiseWithProgressObservable<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		restService: CommonsRestClientService,
		socketIoService: CommonsSocketIoClientService & ICommonsTicketedSocketIo,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		debug: boolean = true
): TPromiseWithObservables<
		ResultT,
		Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
> {
	const promiseWithObservables: TPromiseWithObservables<
			ResultT,
			Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
	> = commonsTicketedSubmitAndWaitAsPromiseWithObservables(
			restService,
			socketIoService,
			(callback: (socketIoId: string) => void): void => {
				socketIoService.addConnectCallback((): void => {
					void (async (): Promise<void> => {
						const socketIoId: string|undefined = await socketIoService.getId();
						if (!socketIoId) throw new Error('Unable to obtain socket.io ID');
						
						callback(socketIoId);
					})();
				});
				socketIoService.connect();
			},
			verb,
			script,
			body,
			checker,
			params,
			headers,
			options,
			connectToSubmitDelay,
			(message: string): void => {
				if (debug) commonsOutputDebug(message);
			}
	);

	const promiseWrapper: Promise<ResultT> = (async (): Promise<ResultT> => {
		try {
			return await promiseWithObservables.promise;
		} finally {
			socketIoService.disconnect();
		}
	})();

	return {
			promise: promiseWrapper,
			queuedObservable: promiseWithObservables.queuedObservable,
			dispatchedObservable: promiseWithObservables.dispatchedObservable,
			progressObservable: promiseWithObservables.progressObservable
	};
}

export function commonsTicketedNodeGenericSubmitAndWaitAsPromiseWithProgressObservable<
		ResultT extends TEncoded = TEncoded,
		BodyT extends TEncodedObject = TEncodedObject,
		ParamsT extends TEncodedObject = TEncodedObject,
		HeadersT extends TEncodedObject = TEncodedObject
>(
		socketIoUrl: string,
		socketIoNs: string,
		restService: CommonsRestClientService,
		verb: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
		script: string,
		body: BodyT,
		checker: (data: unknown) => data is ResultT,
		params?: ParamsT|undefined,	// the params can be explicitly undefined (none) as well as optional
		headers?: HeadersT|undefined,	// the headers can be explicitly undefined (none) as well as optional
		options: TCommonsHttpRequestOptions = {},
		connectToSubmitDelay: number = 100,
		debug: boolean = true
): TPromiseWithObservables<
		ResultT,
		Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
> {
	const socketIoService: CommonsTicketedGenericSocketIoClientService = new CommonsTicketedGenericSocketIoClientService(socketIoUrl, socketIoNs);

	// Can't use commonsTicketedNodeSubmitAndWaitAsPromiseWithProgressObservable as that recreates the auto setup stuff. See comment in callback.

	const promiseWithObservable: TPromiseWithObservables<
			ResultT,
			Pick<TCommonsTicketedSocketIoProgress, 'src'|'data'>
	> = commonsTicketedSubmitAndWaitAsPromiseWithObservablesWithAutoSocketIoSetupAndDisconnect(
			restService,
			socketIoService,
			verb,
			script,
			body,
			checker,
			params,
			headers,
			options,
			connectToSubmitDelay,
			(message: string): void => {
				if (debug) commonsOutputDebug(message);
			}
	);

	return promiseWithObservable;
}
