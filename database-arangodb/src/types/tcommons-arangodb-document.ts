import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TCommonsArangodbDocument = {
		_id: string;
		_key: string;
};

export function isTCommonsArangodbDocument(test: unknown): test is TCommonsArangodbDocument {
	if (!commonsTypeHasPropertyString(test, '_id')) return false;
	if (!commonsTypeHasPropertyString(test, '_key')) return false;
	
	return true;
}
