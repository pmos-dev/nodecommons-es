import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

export type TCommonsArangodbTraversalDepth = {
		min: number;
		max: number;
};

export function isTCommonsArangodbTraversalDepth(test: unknown): test is TCommonsArangodbTraversalDepth {
	if (!commonsTypeHasPropertyNumber(test, 'min')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'max')) return false;
	
	return true;
}
