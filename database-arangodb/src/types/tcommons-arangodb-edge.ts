import { commonsTypeHasPropertyString } from 'tscommons-es-core';

import { TCommonsArangodbDocument, isTCommonsArangodbDocument } from './tcommons-arangodb-document';

export type TCommonsArangodbEdge = TCommonsArangodbDocument & {
		_from: string;
		_to: string;
};

export function isTCommonsArangodbEdge(test: unknown): test is TCommonsArangodbEdge {
	if (!isTCommonsArangodbDocument(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, '_from')) return false;
	if (!commonsTypeHasPropertyString(test, '_to')) return false;
	
	return true;
}
