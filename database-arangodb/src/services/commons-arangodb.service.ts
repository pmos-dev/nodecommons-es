import { Database } from 'arangojs';
import { DocumentCollection, EdgeCollection } from 'arangojs/collection';
import { Document, DocumentMetadata } from 'arangojs/documents';
import { ArrayCursor } from 'arangojs/cursor';
import { GeneratedAqlQuery } from 'arangojs/aql';

import { CommonsNosqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';

import { TCommonsArangodbDocument } from '../types/tcommons-arangodb-document';

export abstract class CommonsArangodbService extends CommonsNosqlDatabaseService<ICommonsCredentials> {
	private db: Database|undefined;
	
	protected async init(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		this.db = new Database(
				`http://${this.credentials.host || 'localhost'}:${this.credentials.port || 8529}`,
				this.credentials.name
		);
		
		if (this.credentials.user && this.credentials.user !== '') {
			this.db.useBasicAuth(this.credentials.user, this.credentials.password);
		}
	}
	
	protected getRawDb(): Database {
		if (!this.db) throw new Error('Database not initialised');
		return this.db;
	}
	
	public getDocumentCollection<T extends { [ key: string ]: any }>(
			name: string
	): DocumentCollection<T> {
		if (!this.db) throw new Error('The database has not been initialised yet');
		return this.db.collection(name) as DocumentCollection<T>;
	}
	
	public getEdgeCollection<T extends  { [ key: string ]: any }>(
			name: string
	): EdgeCollection<T> {
		if (!this.db) throw new Error('The database has not been initialised yet');
		
		return this.db.collection(name) as EdgeCollection<T>;
	}
	
	public async query<T extends  { [ key: string ]: any }>(
			query: GeneratedAqlQuery
	): Promise<ArrayCursor<T>> {
		if (!this.db) throw new Error('The database has not been initialised yet');
		
		return await this.db.query(query);
	}
	
	protected async listTDocumentByQuery<T extends  { [ key: string ]: any }>(query: GeneratedAqlQuery|GeneratedAqlQuery): Promise<Document<T>[]> {
		const cursor: ArrayCursor<Document<T>> = await this.query(query);

		return cursor.all();
	}
	
	protected async getFirstTDocumentByQuery<T extends  { [ key: string ]: any }>(query: GeneratedAqlQuery): Promise<Document<T>|undefined> {
		const cursor: ArrayCursor<Document<T>> = await this.query(query);

		if (!cursor.hasNext) return undefined;
		
		const next: Document<T>|undefined = await cursor.next();
		if (!next) return undefined;
		
		return next;
	}
	
	public close(): void {
		if (this.db) this.db.close();
	}
	
	public async saveDocument<T extends  { [ key: string ]: any }>(
			document: T,
			collection: string
	): Promise<T & TCommonsArangodbDocument> {
		const c: DocumentCollection<T> = this.getDocumentCollection<T>(collection);
		
		const result: DocumentMetadata & {
				new?: Document<T> | undefined;
		} = await c.save(document);
		
		if (result.new) return result.new;
		
		return {
				...document,
				_id: result._id,	// eslint-disable-line no-underscore-dangle
				_key: result._key	// eslint-disable-line no-underscore-dangle
		};
	}
	
	public async removeDocument<T extends  { [ key: string ]: any }>(
			document: T & TCommonsArangodbDocument,
			collection: string
	): Promise<boolean> {
		const c: DocumentCollection<T> = this.getDocumentCollection<T>(collection);
		
		const result: DocumentMetadata & {
				old?: Document<T> | undefined;
		} = await c.remove(document);
		
		return result.old !== undefined;
	}
}
