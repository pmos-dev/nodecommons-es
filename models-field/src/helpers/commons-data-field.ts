import {
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		CommonsFixedDate,
		CommonsFixedDuration,
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate,
		commonsTypeAttemptNumber,
		commonsTypeIsDate,
		commonsTypeIsFixedDate,
		commonsTypeIsString
} from 'tscommons-es-core';
import {
		ICommonsAdamantineField,
		ECommonsAdamantineFieldType,
		fromECommonsAdamantineFieldType,
		toECommonsAdamantineFieldType,
		ECOMMONS_ADAMANTINE_FIELD_TYPES
} from 'tscommons-es-models-field';

import {
		CommonsDatabaseTypeEnum,
		CommonsDatabaseTypeBoolean,
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeStringArray,
		CommonsDatabaseTypeText,
		TCommonsDatabaseTypes,
		ECommonsDatabaseTypeNull
} from 'nodecommons-es-database';
import { CommonsModel } from 'nodecommons-es-models';

export function commonsDataFieldEncodeDefaultValue(
		value: number|string|boolean|Date|CommonsFixedDate|CommonsFixedDuration|string[]|undefined,
		type: ECommonsAdamantineFieldType,
		multiple?: boolean
): string|undefined {
	if (value === undefined || value === null) return undefined;

	switch (type) {
		case ECommonsAdamantineFieldType.STRING:
		case ECommonsAdamantineFieldType.EMAIL:
		case ECommonsAdamantineFieldType.HEXRGB:
		case ECommonsAdamantineFieldType.URL:
		case ECommonsAdamantineFieldType.TEXT:
			return value as string;
		case ECommonsAdamantineFieldType.ENUM:
			if (multiple) {
				if (commonsTypeIsString(value)) {
					if (value === '') return undefined;
					value = [ value ];
				}
				const typecast: string[] = value as string[];
				if (typecast.length === 0) return undefined;

				return JSON.stringify(value);
			}
			return value as string;
		case ECommonsAdamantineFieldType.INT:
		case ECommonsAdamantineFieldType.SMALLINT:
		case ECommonsAdamantineFieldType.TINYINT:
		case ECommonsAdamantineFieldType.FLOAT:	// it seems that toString(10) is ok for floats too
			return (value as number).toString(10);
		case ECommonsAdamantineFieldType.BOOLEAN:
			return value === true ? 'true' : 'false';
		case ECommonsAdamantineFieldType.DATETIME:
		case ECommonsAdamantineFieldType.DATE:
		case ECommonsAdamantineFieldType.TIME:
			// the date/fixed*_ is not ideal, but no other easy way to do it

			if (commonsTypeIsDate(value)) {
				return `date_${commonsDateDateToYmdHis(value, true)}`;
			}
			if (commonsTypeIsFixedDate(value)) {
				return `fixeddate_${value.YmdHis}`;
			}
			// unknown type, cannot continue
			throw new Error('Unknown date time to encode');
		case ECommonsAdamantineFieldType.DURATION:
			// the fixed*_ is not ideal, but no other easy way to do it

			return `fixedduration_${(value as CommonsFixedDuration).millis.toString(10)}`;
	}

	throw new Error('Unknown ECommonsAdamantineFieldType');
}

export function commonsDataFieldDecodeDefaultValue(
		value: string|undefined,
		type: ECommonsAdamantineFieldType,
		multiple?: boolean
): number|string|boolean|Date|CommonsFixedDate|CommonsFixedDuration|string[]|undefined {
	if (value === undefined || value === null) return undefined;

	switch (type) {
		case ECommonsAdamantineFieldType.STRING:
		case ECommonsAdamantineFieldType.EMAIL:
		case ECommonsAdamantineFieldType.HEXRGB:
		case ECommonsAdamantineFieldType.URL:
		case ECommonsAdamantineFieldType.TEXT:
			return value;
		case ECommonsAdamantineFieldType.ENUM:
			if (multiple) {
				if (value === '') return undefined;
				return JSON.parse(value) as string[];
			}
			return value;
		case ECommonsAdamantineFieldType.INT:
		case ECommonsAdamantineFieldType.SMALLINT:
		case ECommonsAdamantineFieldType.TINYINT:
			return parseInt(value, 10);
		case ECommonsAdamantineFieldType.FLOAT:
			return parseFloat(value);
		case ECommonsAdamantineFieldType.BOOLEAN:
			return value === 'true' ? true : false;
		case ECommonsAdamantineFieldType.DATETIME:
		case ECommonsAdamantineFieldType.DATE:
		case ECommonsAdamantineFieldType.TIME: {
			if (value.startsWith('date_')) {
				const parts: string[] = value.split('_');
				if (parts.length === 2 && COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(parts[1])) {
					return commonsDateYmdHisToDate(parts[1], true);
				}
			}
			if (value.startsWith('fixeddate_')) {
				const parts: string[] = value.split('_');
				if (parts.length === 2 && COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(parts[1])) {
					return CommonsFixedDate.fromYmdHis(parts[1]);
				}
			}
			// unknown type, cannot continue
			throw new Error('Unknown date time to decode');
		}
		case ECommonsAdamantineFieldType.DURATION: {
			if (!value.startsWith('fixedduration_')) return undefined;

			const parts: string[] = value.split('_');
			if (parts.length === 2) {
				const millis: number|undefined = commonsTypeAttemptNumber(parts[1]);
				if (millis === undefined) return undefined;

				return CommonsFixedDuration.fromMillis(millis);
			}
		}
	}

	throw new Error('Unknown ECommonsAdamantineFieldType');
}

export class CommonsDataFieldHelper<M extends ICommonsAdamantineField> extends CommonsModel<M> {
	public static extendStructure(
			structure: TCommonsDatabaseTypes,
			enumId: string
	): TCommonsDatabaseTypes {
		return {
				...structure,
				type: new CommonsDatabaseTypeEnum<ECommonsAdamantineFieldType>(
						ECOMMONS_ADAMANTINE_FIELD_TYPES,
						fromECommonsAdamantineFieldType,
						toECommonsAdamantineFieldType,
						ECommonsDatabaseTypeNull.NOT_NULL,
						undefined,
						undefined,
						enumId
				),
				optional: new CommonsDatabaseTypeBoolean(ECommonsDatabaseTypeNull.NOT_NULL),
				description: new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.ALLOW_NULL),
				options: new CommonsDatabaseTypeStringArray(ECommonsDatabaseTypeNull.ALLOW_NULL),
				alpha: new CommonsDatabaseTypeBoolean(ECommonsDatabaseTypeNull.ALLOW_NULL),
				multiple: new CommonsDatabaseTypeBoolean(ECommonsDatabaseTypeNull.ALLOW_NULL),
				defaultValue: new CommonsDatabaseTypeText(ECommonsDatabaseTypeNull.ALLOW_NULL)
		};
	}
}
