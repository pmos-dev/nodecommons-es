import { ECommonsImportExportEncoding } from '../enums/ecommons-import-export-encoding';

export type TCommonsImportExportEncodings = {
		[ field: string ]: ECommonsImportExportEncoding;
};
