import { ICommonsFirstClass } from 'tscommons-es-models';

import { CommonsFirstClass } from '../classes/commons-first-class';

// Unfortunately we can't use generics for this type as the number of multiple first classes is arbitary

export type TCommonsMultiParents = {
		[ firstClassName: string ]: CommonsFirstClass<ICommonsFirstClass>;
};
