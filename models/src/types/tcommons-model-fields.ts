import { TCommonsDatabaseTypes } from 'nodecommons-es-database';

export type TCommonsModelFields = TCommonsDatabaseTypes;
