import { CommonsModelForeignKey } from '../classes/commons-model-foreign-key';

export type TCommonsModelForeignKeys = {
		[key: string]: CommonsModelForeignKey;
};
