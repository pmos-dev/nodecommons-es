import { ECommonsImportExportEncoding } from '../enums/ecommons-import-export-encoding';

export type TCommonsImportExportValues = {
		[ field: string ]: {
				encoding: ECommonsImportExportEncoding;
				value: string|number|boolean|Date;
		};
};
