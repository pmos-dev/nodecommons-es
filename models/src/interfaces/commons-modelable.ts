import { ICommonsModel } from 'tscommons-es-models';

import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

// this is a true class interface, not a data structure

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export interface CommonsModelable<_ extends ICommonsModel> {
	// duplicates of CommonsModel to help with generics
		
	getTable(): string;
	getStructure(): TCommonsModelFields;
	getForeignKeys(): TCommonsModelForeignKeys;
	getUniqueKeys(): CommonsDatabaseUniqueKey[];
	getIndexKeys(): CommonsDatabaseIndexKey[];
}
