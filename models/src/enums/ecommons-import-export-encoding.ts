import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsImportExportEncoding {
		SIMPLE = 'simple',
		RAWURL = 'rawurl',
		HEX = 'hex',
		BOOL = 'bool',
		INT = 'int',
		FLOAT = 'float',
		DATE = 'date',
		TIME = 'time',
		DATETIME = 'datetime',
		BASE62ID = 'base62id',
		BASE62LONGID = 'base62longid',
		STRINGARRAY = 'stringarray'
}

export function toECommonsImportExportEncoding(type: string): ECommonsImportExportEncoding|undefined {
	switch (type) {
		case ECommonsImportExportEncoding.SIMPLE.toString():
			return ECommonsImportExportEncoding.SIMPLE;
		case ECommonsImportExportEncoding.RAWURL.toString():
			return ECommonsImportExportEncoding.RAWURL;
		case ECommonsImportExportEncoding.HEX.toString():
			return ECommonsImportExportEncoding.HEX;
		case ECommonsImportExportEncoding.BOOL.toString():
			return ECommonsImportExportEncoding.BOOL;
		case ECommonsImportExportEncoding.INT.toString():
			return ECommonsImportExportEncoding.INT;
		case ECommonsImportExportEncoding.FLOAT.toString():
			return ECommonsImportExportEncoding.FLOAT;
		case ECommonsImportExportEncoding.DATE.toString():
			return ECommonsImportExportEncoding.DATE;
		case ECommonsImportExportEncoding.TIME.toString():
			return ECommonsImportExportEncoding.TIME;
		case ECommonsImportExportEncoding.DATETIME.toString():
			return ECommonsImportExportEncoding.DATETIME;
		case ECommonsImportExportEncoding.BASE62ID.toString():
			return ECommonsImportExportEncoding.BASE62ID;
		case ECommonsImportExportEncoding.BASE62LONGID.toString():
			return ECommonsImportExportEncoding.BASE62LONGID;
		case ECommonsImportExportEncoding.STRINGARRAY.toString():
			return ECommonsImportExportEncoding.STRINGARRAY;
	}
	return undefined;
}

export function fromECommonsImportExportEncoding(type: ECommonsImportExportEncoding): string {
	switch (type) {
		case ECommonsImportExportEncoding.SIMPLE:
			return ECommonsImportExportEncoding.SIMPLE.toString();
		case ECommonsImportExportEncoding.RAWURL:
			return ECommonsImportExportEncoding.RAWURL.toString();
		case ECommonsImportExportEncoding.HEX:
			return ECommonsImportExportEncoding.HEX.toString();
		case ECommonsImportExportEncoding.BOOL:
			return ECommonsImportExportEncoding.BOOL.toString();
		case ECommonsImportExportEncoding.INT:
			return ECommonsImportExportEncoding.INT.toString();
		case ECommonsImportExportEncoding.FLOAT:
			return ECommonsImportExportEncoding.FLOAT.toString();
		case ECommonsImportExportEncoding.DATE:
			return ECommonsImportExportEncoding.DATE.toString();
		case ECommonsImportExportEncoding.TIME:
			return ECommonsImportExportEncoding.TIME.toString();
		case ECommonsImportExportEncoding.DATETIME:
			return ECommonsImportExportEncoding.DATETIME.toString();
		case ECommonsImportExportEncoding.BASE62ID:
			return ECommonsImportExportEncoding.BASE62ID.toString();
		case ECommonsImportExportEncoding.BASE62LONGID:
			return ECommonsImportExportEncoding.BASE62LONGID.toString();
		case ECommonsImportExportEncoding.STRINGARRAY:
			return ECommonsImportExportEncoding.STRINGARRAY.toString();
	}
	
	throw new Error('Unknown ECommonsImportExportEncoding');
}

export function isECommonsImportExportEncoding(test: unknown): test is ECommonsImportExportEncoding {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsImportExportEncoding(test) !== undefined;
}

export function keyToECommonsImportExportEncoding(key: string): ECommonsImportExportEncoding {
	switch (key) {
		case 'SIMPLE':
			return ECommonsImportExportEncoding.SIMPLE;
		case 'RAWURL':
			return ECommonsImportExportEncoding.RAWURL;
		case 'HEX':
			return ECommonsImportExportEncoding.HEX;
		case 'BOOL':
			return ECommonsImportExportEncoding.BOOL;
		case 'INT':
			return ECommonsImportExportEncoding.INT;
		case 'FLOAT':
			return ECommonsImportExportEncoding.FLOAT;
		case 'DATE':
			return ECommonsImportExportEncoding.DATE;
		case 'TIME':
			return ECommonsImportExportEncoding.TIME;
		case 'DATETIME':
			return ECommonsImportExportEncoding.DATETIME;
		case 'BASE62ID':
			return ECommonsImportExportEncoding.BASE62ID;
		case 'BASE62LONGID':
			return ECommonsImportExportEncoding.BASE62LONGID;
		case 'STRINGARRAY':
			return ECommonsImportExportEncoding.STRINGARRAY;
	}
	
	throw new Error(`Unable to obtain ECommonsImportExportEncoding for key: ${key}`);
}

export const ECOMMONS_IMPORT_EXPORT_ENCODINGS: ECommonsImportExportEncoding[] = Object.keys(ECommonsImportExportEncoding)
		.map((e: string): ECommonsImportExportEncoding => keyToECommonsImportExportEncoding(e));
