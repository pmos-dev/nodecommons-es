import {
		commonsTypeIsDate,
		commonsTypeIsStringArray,
		commonsTypeAttemptBoolean,
		commonsDateYmdToDate,
		commonsDateYmdHisToDate,
		commonsDateHisToDate,
		commonsDateDateToYmd,
		commonsDateDateToHis,
		commonsDateDateToYmdHis,
		commonsEncodingHexToAscii,
		commonsEncodingAsciiToHex,
		commonsBase62IsId,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HIS,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		commonsBase62IsLongId
} from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';

import {
		CommonsDatabaseTypeInt,
		CommonsDatabaseTypeTinyInt,
		CommonsDatabaseTypeSmallInt,
		CommonsDatabaseTypeTinyIntEnum,
		CommonsDatabaseTypeFloat,
		CommonsDatabaseTypeIdName,
		CommonsDatabaseTypeEnum,
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeDate,
		CommonsDatabaseTypeTime,
		CommonsDatabaseTypeDateTime,
		CommonsDatabaseTypeText,
		CommonsDatabaseTypeEncrypted,
		CommonsDatabaseTypeBoolean,
		CommonsDatabaseTypeBase62BigId,
		CommonsDatabaseTypeStringArray,
		CommonsDatabaseTypeBase62LongId
} from 'nodecommons-es-database';

import { CommonsModel } from '../classes/commons-model';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsImportExportEncodings } from '../types/tcommons-import-export-encodings';
import { TCommonsImportExportValues } from '../types/tcommons-import-export-values';

import { ECommonsImportExportEncoding, fromECommonsImportExportEncoding, toECommonsImportExportEncoding } from '../enums/ecommons-import-export-encoding';

export abstract class CommonsImportExport {
	public static hasAttribute(
			attrib: string,
			line: string
	): boolean {
		line = line.replace(
				/"[^"]*"/g,
				'""'
		);	// strip out values to avoid false matches
		
		return new RegExp(`\\s${attrib}($|\\s)`, 'i').test(line);
	}

	public static extractField(
			attrib: string,
			line: string
	): string|number|boolean|Date|string[]|undefined {
		const array: RegExpExecArray|null = new RegExp(`\\s${attrib}=([a-z0-9]{1,16})?:"([^"]*)"`, 'i').exec(line);
		if (!array) return undefined;

		const encoding: ECommonsImportExportEncoding|undefined = toECommonsImportExportEncoding(array[1]);
		if (!encoding) throw new Error(`Unknown encoding format: ${array[1]}`);
		
		const value: string|undefined = array[2];
		
		switch (encoding) {
			case ECommonsImportExportEncoding.SIMPLE:
				return value;
			case ECommonsImportExportEncoding.RAWURL:
				return decodeURIComponent(value);
			case ECommonsImportExportEncoding.HEX:
				return commonsEncodingHexToAscii(value
						.replace(/[ ]/g, '')
				);
			case ECommonsImportExportEncoding.BOOL:
				switch (value.toLowerCase()) {
					case 'true':
						return true;
					case 'false':
						return false;
					default:
						throw new Error(`Unknown boolean state: ${value}`);
				}
			case ECommonsImportExportEncoding.INT:
				return parseInt(value, 10);
			case ECommonsImportExportEncoding.FLOAT:
				return parseFloat(value);
			case ECommonsImportExportEncoding.DATE:
				if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(value)) throw new Error(`Invalid date format ${value}`);
				return commonsDateYmdToDate(value);
			case ECommonsImportExportEncoding.TIME:
				if (!COMMONS_REGEX_PATTERN_TIME_HIS.test(value)) throw new Error(`Invalid time format ${value}`);
				return commonsDateHisToDate(value);
			case ECommonsImportExportEncoding.DATETIME:
				if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(value)) throw new Error(`Invalid datetime format ${value}`);
				return commonsDateYmdHisToDate(value);
			case ECommonsImportExportEncoding.BASE62ID:
				if (!commonsBase62IsId(value)) throw new Error(`Invalid base62id ${value}`);
				return value;
			case ECommonsImportExportEncoding.BASE62LONGID:
				if (!commonsBase62IsLongId(value)) throw new Error(`Invalid base62longid ${value}`);
				return value;
			case ECommonsImportExportEncoding.STRINGARRAY:
				const attempt: unknown = JSON.parse(commonsEncodingHexToAscii(
						value
								.replace(/[ ]/g, '')
				));
				if (!commonsTypeIsStringArray(attempt)) throw new Error(`Invalid string ${attempt}`);	// eslint-disable-line @typescript-eslint/restrict-template-expressions
				return attempt;
		}
		
		throw new Error('Unknown extraction encoding format');
	}

	//-------------------------------------------------------------------------

	public static buildField(
			attrib: string,
			encoding: ECommonsImportExportEncoding,
			value: string|number|boolean|Date|undefined
	) {
		if (value === undefined) return '';
	
		switch (encoding) {
			case ECommonsImportExportEncoding.SIMPLE:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.SIMPLE)}:"${value}"`;	// eslint-disable-line @typescript-eslint/restrict-template-expressions
			case ECommonsImportExportEncoding.RAWURL:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.RAWURL)}:"${encodeURIComponent(value.toString())}"`;
			case ECommonsImportExportEncoding.HEX: {
				const encoded: string = commonsEncodingAsciiToHex(value.toString());
				const chunks: RegExpMatchArray|null = encoded.match(/.{1,32}/g);
				const joined: string = (chunks === null || chunks.length === 0) ? '' : chunks.join(' ');
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.HEX)}:"${joined}"`;
			}
			case ECommonsImportExportEncoding.BOOL:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.BOOL)}:"${commonsTypeAttemptBoolean(value) ? 'true' : 'false'}"`;
			case ECommonsImportExportEncoding.INT:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.INT)}:"${value.toString(10)}"`;
			case ECommonsImportExportEncoding.FLOAT:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.FLOAT)}:"${value.toString(10)}"`;
			case ECommonsImportExportEncoding.DATE:
				if (!commonsTypeIsDate(value)) throw new Error('Trying to encode a non-date');
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.DATE)}:"${commonsDateDateToYmd(value)}"`;
			case ECommonsImportExportEncoding.TIME:
				if (!commonsTypeIsDate(value)) throw new Error('Trying to encode a non-date');
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.TIME)}:"${commonsDateDateToHis(value)}"`;
			case ECommonsImportExportEncoding.DATETIME:
				if (!commonsTypeIsDate(value)) throw new Error('Trying to encode a non-date');
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.DATETIME)}:"${commonsDateDateToYmdHis(value)}"`;
			case ECommonsImportExportEncoding.BASE62ID:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.BASE62ID)}:"${value}"`;	// eslint-disable-line @typescript-eslint/restrict-template-expressions
			case ECommonsImportExportEncoding.BASE62LONGID:
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.BASE62LONGID)}:"${value}"`;	// eslint-disable-line @typescript-eslint/restrict-template-expressions
			case ECommonsImportExportEncoding.STRINGARRAY: {
				if (!commonsTypeIsStringArray(value)) throw new Error('Trying to encode a non-stringarray');
				const stringified: string = JSON.stringify(value);
				const encoded: string = commonsEncodingAsciiToHex(stringified);
				const chunks: RegExpMatchArray|null = encoded.match(/.{1,32}/g);
				const joined: string = (chunks === null || chunks.length === 0) ? '' : chunks.join(' ');
				return `${attrib}=${fromECommonsImportExportEncoding(ECommonsImportExportEncoding.STRINGARRAY)}:"${joined}"`;
			}
			default:
				throw new Error(`Unknown value encoding: ${encoding}`);	// eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
	}
	
	public static writeLine(
			prefix: string,
			fields: TCommonsImportExportValues = {},
			attributes: string[] = []
	): string {
		const data: string[] = [];
		for (const field of Object.keys(fields)) {
			const encoding: ECommonsImportExportEncoding = fields[field].encoding;
			const value: string|number|boolean|Date|undefined = fields[field].value;
			
			if (value !== undefined) {
				data.push(CommonsImportExport.buildField(
						field,
						encoding,
						value
				));
			}
		}
		
		data.push(...attributes);

		if (prefix !== '') data.unshift(prefix);

		return data.join(' ');
	}
	
	//-------------------------------------------------------------------------

	protected static buildExportFieldEncodings<M extends ICommonsModel>(
			model: CommonsModel<M>,
			fields: string[]
	): TCommonsImportExportEncodings {
		const structure: TCommonsModelFields = model.getStructure();
		
		const encodings: TCommonsImportExportEncodings = {};
		for (const field of fields) {
			if (
				structure[field] instanceof CommonsDatabaseTypeInt
				|| structure[field] instanceof CommonsDatabaseTypeTinyInt
				|| structure[field] instanceof CommonsDatabaseTypeSmallInt
				|| structure[field] instanceof CommonsDatabaseTypeTinyIntEnum
			) {
				encodings[field] = ECommonsImportExportEncoding.INT;
				continue;
			}

			if (
				structure[field] instanceof CommonsDatabaseTypeFloat
			) {
				encodings[field] = ECommonsImportExportEncoding.FLOAT;
				continue;
			}
				
			if (
				structure[field] instanceof CommonsDatabaseTypeIdName
				|| structure[field] instanceof CommonsDatabaseTypeEnum
			) {
				encodings[field] = ECommonsImportExportEncoding.SIMPLE;
				continue;
			}

			if (
				structure[field] instanceof CommonsDatabaseTypeString
			) {
				encodings[field] = ECommonsImportExportEncoding.RAWURL;
				continue;
			}

			if (
				structure[field] instanceof CommonsDatabaseTypeDateTime
			) {
				encodings[field] = ECommonsImportExportEncoding.DATETIME;
				continue;
			}

			if (
				structure[field] instanceof CommonsDatabaseTypeDate
			) {
				encodings[field] = ECommonsImportExportEncoding.DATE;
				continue;
			}

			if (
				structure[field] instanceof CommonsDatabaseTypeTime
			) {
				encodings[field] = ECommonsImportExportEncoding.TIME;
				continue;
			}
			
			if (
				structure[field] instanceof CommonsDatabaseTypeText
				|| structure[field] instanceof CommonsDatabaseTypeEncrypted
			) {
				encodings[field] = ECommonsImportExportEncoding.HEX;
				continue;
			}

			if (structure[field] instanceof CommonsDatabaseTypeBoolean) {
				encodings[field] = ECommonsImportExportEncoding.BOOL;
				continue;
			}
			
			if (structure[field] instanceof CommonsDatabaseTypeBase62BigId) {
				encodings[field] = ECommonsImportExportEncoding.BASE62ID;
				continue;
			}
			
			if (structure[field] instanceof CommonsDatabaseTypeBase62LongId) {
				encodings[field] = ECommonsImportExportEncoding.BASE62LONGID;
				continue;
			}
			
			if (structure[field] instanceof CommonsDatabaseTypeStringArray) {
				encodings[field] = ECommonsImportExportEncoding.STRINGARRAY;
				continue;
			}
			
			throw new Error(`Automatic export of managed models is not set up for fields of this type yet ${field}`);
		}
		
		return encodings;
	}
}
