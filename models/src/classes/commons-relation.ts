import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsRelation } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';
import { CommonsM2MLinkTable } from './commons-m2m-link-table';

export class CommonsRelation<
		ModelI extends ICommonsRelation<AI, AI>,
		AI extends ICommonsFirstClass
> extends CommonsM2MLinkTable<ModelI, AI, AI> {
	protected relationModel: CommonsFirstClass<AI>;
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			relationModel: CommonsFirstClass<AI>,
			structure: TCommonsModelFields
	) {
		super(
				database,
				tableName,
				relationModel,
				relationModel,
				'src',
				'dest',
				structure
		);
		
		this.relationModel = relationModel;
	}

	public getModel(): CommonsFirstClass<AI> {
		return this.relationModel;
	}
	
	public override getAFirstClass(): CommonsFirstClass<AI> {
		throw new Error('getAFirstClass isn\'t application for relations. Use getModel');
	}
	
	public override getBFirstClass(): CommonsFirstClass<AI> {
		throw new Error('getBFirstClass isn\'t application for relations. Use getModel');
	}
	
	public override getAField(): string {
		throw new Error('getAField isn\'t application for relations.');
	}
	
	public override getBField(): string {
		throw new Error('getBField isn\'t application for relations.');
	}

	//-------------------------------------------------------------------------

	public override async listAsByB(_b: AI): Promise<AI[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('listAsByB isn\'t application for relations. Use listRelated');
	}
	
	public override async listBsByA(_a: AI): Promise<AI[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('listBsByA isn\'t application for relations. Use listRelated');
	}

	public async listRelated(src: AI): Promise<AI[]> {
		CommonsModel.assertIdObject(src, this.relationModel.getIdField());

		return super.listBsByA(src);
	}
	
	//-------------------------------------------------------------------------

	public override async isLinked(a: AI, b: AI): Promise<boolean> {
		const existing: AI[] = await this.listRelated(a);
		
		return existing
				.map((item: AI): number => item[this.relationModel.getIdField()] as number)
				.includes(b[this.relationModel.getIdField()] as number);
	}

	//-------------------------------------------------------------------------

	public override async link(src: AI, dest: AI, values: TPropertyObject = {}): Promise<void> {
		CommonsModel.assertIdObject(src, this.relationModel.getIdField());
		CommonsModel.assertIdObject(dest, this.relationModel.getIdField());

		if (src[this.relationModel.getIdField()] === dest[this.relationModel.getIdField()]) throw new Error('Unable to relate to the same model row');

		const isTransactionOwner: boolean = await this.database.transactionClaim();

		try {
			await super.link(src, dest, values);
			await super.link(dest, src, values);
	
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async unlink(src: AI, dest: AI, ignoreMissingLink: boolean = false): Promise<void> {
		CommonsModel.assertIdObject(src, this.relationModel.getIdField());
		CommonsModel.assertIdObject(dest, this.relationModel.getIdField());

		const isTransactionOwner: boolean = await this.database.transactionClaim();

		try {
			await super.unlink(src, dest, ignoreMissingLink);
			await super.unlink(dest, src, ignoreMissingLink);
	
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async deleteAllForA(_a: AI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('deleteAllForA isn\'t application for relations. Use deleteAllrelations');
	}

	public override async deleteAllForB(_b: AI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('deleteAllForB isn\'t application for relations. Use deleteAllrelations');
	}

	public async deleteAllRelations(row: AI): Promise<void> {
		CommonsModel.assertIdObject(row, this.relationModel.getIdField());
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();

		try {
			await super.deleteAllForA(row);
			await super.deleteAllForB(row);
	
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async updateLinkRow(_row: ModelI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('updateLinkRow isn\'t application for relations. Use updateRelation instead');
	}

	public async updateRelation(row: ModelI): Promise<void> {
		CommonsModel.assertIdObject(row, 'src');
		CommonsModel.assertIdObject(row, 'dest');

		const isTransactionOwner: boolean = await this.database.transactionClaim();

		try {
			await super.updateLinkRow(row);
			
			// fudge here
			const src: number = row.src;
			const dest: number = row.dest;
			row.dest = src;
			row.src = dest;
	
			await super.updateLinkRow(row);
	
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
}
