import {
		commonsTypeHasProperty,
		commonsTypeAssertObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsOrderable } from 'tscommons-es-models';
import { ICommonsHierarchy } from 'tscommons-es-models';
import { ECommonsMoveDirection } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSmallInt } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSerialId } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeSigned } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass, COMMONS_FIRSTCLASS_DEFAULT_ID_FIELD } from './commons-first-class';
import { CommonsSecondClass } from './commons-second-class';
import { CommonsModelForeignKey } from './commons-model-foreign-key';
import { COMMONS_ORDERED_DEFAULT_ORDERED_FIELD } from './commons-ordered';

function buildHierarchyStructure(
		structure: TCommonsModelFields,
		orderedField: string,
		parentField: string
): TCommonsModelFields {
	if (structure[orderedField] !== undefined) throw new Error('The ordered field is implicit for Ordered models and should not be explicitly stated in the structure');

	structure[orderedField] = new CommonsDatabaseTypeSmallInt(
			ECommonsDatabaseTypeSigned.UNSIGNED,
			ECommonsDatabaseTypeNull.NOT_NULL
	);
	
	if (structure[parentField] !== undefined) throw new Error('The parent field is implicit for Ordered models and should not be explicitly stated in the structure');

	structure[parentField] = new CommonsDatabaseTypeId(
			ECommonsDatabaseTypeNull.ALLOW_NULL
	);
	
	return structure;
}

function buildHierarchyForeignKeys(
		foreignKeys: TCommonsModelForeignKeys,
		idField: string,
		parentField: string,
		tableName: string
): TCommonsModelForeignKeys {
	if (foreignKeys[parentField] !== undefined) throw new Error('Do not specify the internal foreign key relationship directly for hierarchy models');

	// hack to create a psuedo instance of 'this' in a static method
	// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
	const self: CommonsFirstClass<any> = {
			getTable: (): string => tableName,
			getIdField: (): string => idField
	} as CommonsFirstClass<any>;
	
	foreignKeys[parentField] = new CommonsModelForeignKey(
			self,
			ECommonsDatabaseReferentialAction.CASCADE,
			ECommonsDatabaseReferentialAction.CASCADE,
			`${tableName}__fk_parent`
	);
	
	return foreignKeys;
}

export const COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD: string = 'parent';

export class CommonsHierarchy<
		ModelI extends ICommonsHierarchy<ParentI>,
		ParentI extends ICommonsFirstClass
> extends CommonsSecondClass<ModelI, ParentI> implements ICommonsOrderable {
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			firstClass: CommonsFirstClass<ParentI>,
			firstClassField: string,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildHierarchyStructure(structure, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD),
				firstClass,
				firstClassField,
				buildHierarchyForeignKeys(
						foreignKeys,
						COMMONS_FIRSTCLASS_DEFAULT_ID_FIELD,
						COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD,
						tableName
				),
				uniqueKeys,
				indexKeys
		);
	}
	
	public getOrderedField(): string {
		return COMMONS_ORDERED_DEFAULT_ORDERED_FIELD;
	}
	
	public getParentField(): string {
		return COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD;
	}

	//-------------------------------------------------------------------------

	protected override defineViews(): void {
		super.defineViews();

		const fields: string = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'SECONDCLASS_JOIN', field))
				.join(',');
		
		this.defineView(
				'HIERARCHY_JOIN',
				`
					SELECT ${fields},
						${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')},
						${this.derivedField('parent', this.firstClass.getIdField())} AS ${this.tempField('parentId')}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					LEFT JOIN ${this.model(this)} AS ${this.derived('parent')}
						ON ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'parent')}=${this.derivedField('parent', this.getIdField())}
				`
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();

		const fields: string = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'HIERARCHY_JOIN', field))
				.join(',');

		this.database.preprepare(
				`MODELS_HIERARCHY__${this.tableName}__GET_ROOT`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'HIERARCHY_JOIN')}
					WHERE ${this.modelViewField(this, 'HIERARCHY_JOIN', 'firstClassId')} = :firstClass
					AND ${this.modelViewField(this, 'HIERARCHY_JOIN', 'parentId')} IS NULL
					LIMIT 1
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				this.structure
		);
		
		this.database.preprepare(
				`MODELS_HIERARCHY__${this.tableName}__LIST_CHILDREN_ORDERED`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'HIERARCHY_JOIN')}
					WHERE ${this.modelViewField(this, 'HIERARCHY_JOIN', 'firstClassId')} = :firstClass
					AND ${this.modelViewField(this, 'HIERARCHY_JOIN', 'parentId')} = :parent
					ORDER BY ${this.modelViewField(this, 'HIERARCHY_JOIN', COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} ASC
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						parent: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				this.structure
		);

		this.database.preprepare(
				`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
				`
					SELECT MAX(${this.modelViewField(this, 'HIERARCHY_JOIN', COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)}) AS ${this.tempField('max')}
					FROM ${this.modelView(this, 'HIERARCHY_JOIN')}
					WHERE ${this.modelViewField(this, 'HIERARCHY_JOIN', 'firstClassId')} = :firstClass
					AND ${this.modelViewField(this, 'HIERARCHY_JOIN', 'parentId')} = :parent
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						parent: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				{
						max: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		this.database.preprepare(
				`MODELS_HIERARCHY__${this.tableName}__CURRENT_ORDERED`,
				`
					SELECT ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} AS ${this.tempField('ordered')}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, this.getIdField())} = :id
				`,
				{
						id: new CommonsDatabaseTypeSerialId()
				},
				{
						ordered: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
				}
		);

		this.database.preprepare(
				`MODELS_HIERARCHY__${this.tableName}__BY_ORDERED`,
				`
					SELECT ${this.modelViewField(this, 'HIERARCHY_JOIN', this.getIdField())} AS ${this.tempField('id')}
					FROM ${this.modelView(this, 'HIERARCHY_JOIN')}
					WHERE ${this.modelViewField(this, 'HIERARCHY_JOIN', 'firstClassId')} = :firstClass
					AND ${this.modelViewField(this, 'HIERARCHY_JOIN', 'parentId')} = :parent
					AND ${this.modelViewField(this, 'HIERARCHY_JOIN', COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} = :ordered
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						parent: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						ordered: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
				},
				{
						id: new CommonsDatabaseTypeSerialId()
				}
		);
	}

	//-------------------------------------------------------------------------

	public async getRootByFirstClass(firstClassObject: ParentI): Promise<ModelI|undefined> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		return await this.database.executeParamsSingle<ModelI>(
				`MODELS_HIERARCHY__${this.tableName}__GET_ROOT`,
				{
						firstClass: firstClassObject[this.firstClass.getIdField()] as number
				}
		);
	}

	public async listChildrenOrderedByFirstClass(firstClassObject: ParentI, parent: ModelI): Promise<ModelI[]> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
		CommonsModel.assertIdObject(parent, this.getIdField());
		
		return await this.database.executeParams<ModelI>(
				`MODELS_HIERARCHY__${this.tableName}__LIST_CHILDREN_ORDERED`,
				{
						firstClass: firstClassObject[this.firstClass.getIdField()] as number,
						parent: parent[this.getIdField()] as number
				}
		);
	}

	//---------------------------------------------------------------------

	public async move(object: ModelI, direction: ECommonsMoveDirection): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(object, this.firstClassField);
		
		if (object[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] === undefined) throw new Error('Cannot move root node');

		CommonsModel.assertId(object[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD]);
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			// this is non-ideal, but it's hard to chain secondclasses otherwise, since get_by_id isn't allowed.
			const tempObject: TPropertyObject = {};
			tempObject[this.getIdField()] = object[this.firstClassField] as number;
			const firstClassObject: ParentI = tempObject as ParentI;
			
			const parent: ModelI|undefined = await this.getByFirstClassAndId(
					firstClassObject,
					object[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] as number
			);
			if (parent === undefined) throw new Error('Unable to ascertain parent for move');

			const current: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_HIERARCHY__${this.tableName}__CURRENT_ORDERED`,
					{
							id: object[this.getIdField()] as number
					}
			);
			
			const currentMax: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: object[this.firstClassField] as number,
							parent: parent[this.getIdField()] as number
					}
			);
	
			switch (direction) {
				case ECommonsMoveDirection.UP: {
					if (current === 0) throw new Error('Cannot move above the first ordered row');
	
					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_HIERARCHY__${this.tableName}__BY_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									parent: parent[this.getIdField()] as number,
									ordered: current - 1
							}
					);

					{
						const updates: TCommonsDatabaseParams = {};
						updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(current - 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.DOWN: {
					if (current === currentMax) throw new Error('Cannot move below the last ordered row');

					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_ORIENTATEDORDERED__${this.tableName}__BY_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									parent: parent[this.getIdField()] as number,
									ordered: current + 1
							}
					);
					
					{
						const updates: TCommonsDatabaseParams = {};
						updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(current + 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.TOP: {
					if (current === 0) break;	// if already topmost

					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} = ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} + 1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)} = :parent
								AND ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} < :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(
											object[this.firstClassField] as number,
											new CommonsDatabaseTypeSerialId()
									),
									parent: new CommonsDatabaseParam(
											parent[this.getIdField()] as number,
											new CommonsDatabaseTypeSerialId()
									),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
					
					const updates: TCommonsDatabaseParams = {};
					updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(0, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
				case ECommonsMoveDirection.BOTTOM: {
					if (current === currentMax) break;	// if already last
	
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} = ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} - 1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)} = :parent
								AND ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} > :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(
											object[this.firstClassField] as number,
											new CommonsDatabaseTypeSerialId()
									),
									parent: new CommonsDatabaseParam(
											parent[this.getIdField()] as number,
											new CommonsDatabaseTypeSerialId()
									),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
					
					const updates: TCommonsDatabaseParams = {};
					updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(currentMax, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
				case ECommonsMoveDirection.LEFT: {
					if (parent[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] === null) throw new Error('Cannot move leftwards on top of root node');

					const grandparent: ModelI|undefined = await this.getByFirstClassAndId(
							firstClassObject,
							parent[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] as number
					);
					if (!grandparent) throw new Error('Unable to ascertain grandparent for move');
					
					const parentMax: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									parent: grandparent[this.getIdField()] as number
							}
					);

					const updates: TCommonsDatabaseParams = {};
					updates[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] = new CommonsDatabaseParam(grandparent[this.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
					updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(parentMax + 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
					
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)}=${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)}-1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)} = :parent
								AND ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} > :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(
											object[this.firstClassField] as number,
											new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
									),
									parent: new CommonsDatabaseParam(
											parent[this.getIdField()] as number,
											new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
									),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
					
					break;
				}
				case ECommonsMoveDirection.RIGHT: {
					if (object[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] === 0) throw new Error('Cannot move rightwards because there is no higher sibbling');
					
					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_HIERARCHY__${this.tableName}__BY_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									parent: parent[this.getIdField()] as number,
									ordered: current - 1
							}
					);
					
					let prevMax: number|undefined = await this.database.executeParamsValue<number>(
							`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									parent: prev
							},
							true
					);
					if (prevMax === undefined) prevMax = -1;
					
					const updates: TCommonsDatabaseParams = {};
					updates[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
					updates[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = new CommonsDatabaseParam(prevMax + 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
					
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)}=${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} - 1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)} = :parent
								AND ${this.modelField(this, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)} > :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(
											object[this.firstClassField] as number,
											new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
									),
									parent: new CommonsDatabaseParam(
											parent[this.getIdField()] as number,
											new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
									),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
					
					break;
				}
			}
			
			const refreshed: ModelI = await this.refresh(object);
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return refreshed;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	public async jump(object: ModelI, destination: ParentI): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertId(object[this.firstClassField]);

		CommonsModel.assertIdObject(destination, this.firstClass.getIdField());
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			await this.move(object, ECommonsMoveDirection.BOTTOM);
			
			let currentMax: number|undefined = await this.database.executeParamsValue<number>(
					`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: destination[this.firstClass.getIdField()] as number,
							parent: destination[this.getIdField()] as number
					},
					true
			);
			if (currentMax === undefined) currentMax = 0;
			else currentMax++;
			
			(object as TPropertyObject)[this.firstClassField] = destination[this.firstClass.getIdField()] as number;
			(object as TPropertyObject)[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] = destination[this.getIdField()] as number;
			(object as TPropertyObject)[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = currentMax;
			
			await this.update(object);

			const refreshed: ModelI = await this.refresh(object);
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return refreshed;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	//---------------------------------------------------------------------

	public async insertRootForFirstClass(
			firstClassObject: ParentI,
			values: TPropertyObject
	): Promise<ModelI> {
		commonsTypeAssertObject(values);
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		if (commonsTypeHasProperty(values, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)) throw new Error('Ordered fields should not be specified explicitly for Hierarchy models');
		if (commonsTypeHasProperty(values, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)) throw new Error('Parent fields should not be specified explicitly for Hierarchy models');

		// oddly, the above typecasts values as 'never', so we have to fix this manually

		const typecast: TPropertyObject = values;
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			const existing: ModelI|undefined = await this.getRootByFirstClass(firstClassObject);
			if (existing !== undefined) throw new Error('A root node already exists for this hierarchy\'s firstClass');
			
			typecast[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = 0;
			typecast[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] = undefined;
			
			const object: ModelI = await super.insertForFirstClass(firstClassObject, typecast);
		
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return object;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	public async insertForFirstClassAndParent(firstClassObject: ParentI, parent: ModelI, values: TPropertyObject): Promise<ModelI> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
		CommonsModel.assertIdObject(parent, this.getIdField());
		
		if (commonsTypeHasProperty(values, COMMONS_ORDERED_DEFAULT_ORDERED_FIELD)) throw new Error('Ordered fields should not be specified explicitly for Hierarchy models');
		if (commonsTypeHasProperty(values, COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD)) throw new Error('Parent fields should not be specified explicitly for Hierarchy models');

		// oddly, the above typecasts values as 'never', so we have to fix this manually

		const typecast: TPropertyObject = values;
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			let currentMax: number|undefined = await this.database.executeParamsValue<number>(
					`MODELS_HIERARCHY__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: firstClassObject[this.firstClass.getIdField()] as number,
							parent: parent[this.getIdField()] as number
					},
					true
			);
			if (currentMax === undefined) currentMax = 0;
			else currentMax++;
			
			typecast[COMMONS_ORDERED_DEFAULT_ORDERED_FIELD] = currentMax;
			typecast[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] = parent[this.getIdField()] as number;
			
			const object: ModelI = await super.insertForFirstClass(firstClassObject, typecast);
		
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return object;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	public override async insertForFirstClass(_firstClassObject: ParentI, _values: TPropertyObject): Promise<ModelI> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call insertForFirstClass explicitly on hierarchy objects; use insertRootForFirstClass or insertForFirstClassAndParent instead');
	}

	public async deleteRootForFirstClass(firstClassObject: ParentI, object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(object, this.firstClassField);
	
		if (object[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] !== undefined) throw new Error('This is not a root node object');
	
		await this.deleteForFirstClass(firstClassObject, object);
	}

	public async deleteForFirstClassAndParent(firstClassObject: ParentI, parent: ModelI, object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(parent, this.getIdField());
		CommonsModel.assertIdObject(object, this.firstClassField);

		if (object[COMMONS_HIERARCHY_DEFAULT_PARENT_FIELD] === undefined) throw new Error('This is a root node object and cannot be deleted; use deleteRootForFirstClass instead');
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			await this.move(object, ECommonsMoveDirection.BOTTOM);
			await this.deleteForFirstClass(firstClassObject, object);
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async deleteForFirstClass(_firstClassObject: ParentI, _values: ModelI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call deleteForFirstClass explicitly on hierarchy objects; use deleteRootForFirstClass or deleteForFirstClassAndParent instead');
	}
}
