import {
		commonsTypeHasProperty,
		commonsTypeAssertObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsOrderable } from 'tscommons-es-models';
import { ICommonsOrdered } from 'tscommons-es-models';
import { ECommonsMoveDirection } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSmallInt } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSerialId } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeSigned } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';

export const COMMONS_ORDERED_DEFAULT_ORDERED_FIELD: string = 'ordered';

function buildOrderedStructure(
		structure: TCommonsModelFields,
		orderedField: string
): TCommonsModelFields {
	if (structure[orderedField] !== undefined) throw new Error('The ordered field is implicit for Ordered models and should not be explicitly stated in the structure');

	structure[orderedField] = new CommonsDatabaseTypeSmallInt(
			ECommonsDatabaseTypeSigned.UNSIGNED,
			ECommonsDatabaseTypeNull.NOT_NULL
	);
	
	return structure;
}

export class CommonsOrdered<ModelI extends ICommonsOrdered> extends CommonsFirstClass<ModelI> implements ICommonsOrderable {
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildOrderedStructure(
						structure,
						COMMONS_ORDERED_DEFAULT_ORDERED_FIELD
				),
				foreignKeys,
				uniqueKeys,
				indexKeys
		);
	}
	
	public getOrderedField(): string {
		return COMMONS_ORDERED_DEFAULT_ORDERED_FIELD;
	}

	//-------------------------------------------------------------------------

	protected override preprepare(): void {
		super.preprepare();

		this.database.preprepare(
				`MODELS_ORDERED__${this.tableName}__LIST_ORDERED`,
				`
					SELECT ${this.fields}
					FROM ${this.model(this)}
					ORDER BY ${this.modelField(this, this.getOrderedField())} ASC
				`,
				undefined,
				this.structure
		);

		this.database.preprepare(
				`MODELS_ORDERED__${this.tableName}__MAX_ORDERED`,
				`
					SELECT MAX(${this.modelField(this, this.getOrderedField())}) AS ${this.tempField('max')}
					FROM ${this.model(this)}
				`,
				undefined,
				{
						max: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		this.database.preprepare(
				`MODELS_ORDERED__${this.tableName}__CURRENT_ORDERED`,
				`
					SELECT ${this.modelField(this, this.getOrderedField())} AS ${this.tempField('ordered')}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, this.getIdField())} = :id
				`,
				{
						id: this.structure[this.getIdField()]
				},
				{
						ordered: this.structure[this.getOrderedField()]
				}
		);

		this.database.preprepare(
				`MODELS_ORDERED__${this.tableName}__BY_ORDERED`,
				`
					SELECT ${this.modelField(this, this.getIdField())} AS ${this.tempField('id')}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, this.getOrderedField())} = :ordered
				`,
				{
						ordered: this.structure[this.getOrderedField()]
				},
				{
						id: this.structure[this.getIdField()]
				}
		);
	}

	//-------------------------------------------------------------------------

	public async listOrdered(): Promise<ModelI[]> {
		return await this.database.executeParams<ModelI>(`MODELS_ORDERED__${this.tableName}__LIST_ORDERED`);
	}

	//---------------------------------------------------------------------

	public async move(object: ModelI, direction: ECommonsMoveDirection): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			const current: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_ORDERED__${this.tableName}__CURRENT_ORDERED`,
					{
							id: object[this.getIdField()] as number
					}
			);
			
			const currentMax: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_ORDERED__${this.tableName}__MAX_ORDERED`
			);
	
			switch (direction) {
				case ECommonsMoveDirection.UP: {
					if (current === 0) throw new Error('Cannot move above the first ordered row');
	
					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_ORDERED__${this.tableName}__BY_ORDERED`,
							{
									ordered: current - 1
							}
					);

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current - 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.DOWN: {
					if (current === currentMax) throw new Error('Cannot move below the last ordered row');

					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_ORDERED__${this.tableName}__BY_ORDERED`,
							{
									ordered: current + 1
							}
					);

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current + 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.TOP: {
					if (current === 0) break;	// if already topmost
	
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, this.getOrderedField())} = ${this.modelField(this, this.getOrderedField())} + 1
								WHERE ${this.modelField(this, this.getOrderedField())} < :threshold
							`,
							{
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
	
					const updates: TCommonsDatabaseParams = {};
					updates[this.getOrderedField()] = new CommonsDatabaseParam(0, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
				case ECommonsMoveDirection.BOTTOM: {
					if (current === currentMax) break;	// if already last
	
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, this.getOrderedField())} = ${this.modelField(this, this.getOrderedField())} - 1
								WHERE ${this.modelField(this, this.getOrderedField())} > :threshold
							`,
							{
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
	
					const updates: TCommonsDatabaseParams = {};
					updates[this.getOrderedField()] = new CommonsDatabaseParam(currentMax, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
					
				default: {
					throw new Error(`Unsupported move direction: ${direction}`);
				}
			}
			
			const refreshed: ModelI = await this.refresh(object);
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return refreshed;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	//---------------------------------------------------------------------

	public override async insert(_values: TPropertyObject): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call insert directly, use insertFirstClass instead');
	}

	public override async insertFirstClass(values: TPropertyObject): Promise<ModelI> {
		commonsTypeAssertObject(values);
		if (commonsTypeHasProperty(values, this.getOrderedField())) throw new Error('Ordered fields should not be specified explicitly for Ordered models');

		// oddly, the above typecasts values as 'never', so we have to fix this manually

		const typecast: TPropertyObject = values;
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			let currentMax: number|undefined = await this.database.executeParamsValue<number>(
					`MODELS_ORDERED__${this.tableName}__MAX_ORDERED`,
					undefined,
					true
			);
			if (currentMax === undefined) currentMax = 0;
			else currentMax++;
	
			typecast[this.getOrderedField()] = currentMax;
	
			const object: ModelI = await super.insertFirstClass(typecast);
		
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return object;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async delete(object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());

		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			await this.move(object, ECommonsMoveDirection.BOTTOM);
			await super.delete(object);
		
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
}
