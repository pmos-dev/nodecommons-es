import { commonsTypeHasProperty } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsOrderable } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';
import { CommonsOrdered } from './commons-ordered';
import { CommonsOrientatedOrdered } from './commons-orientated-ordered';
import { CommonsHierarchy } from './commons-hierarchy';
import { CommonsModelForeignKey } from './commons-model-foreign-key';

function buildM2MLinkStructure(
		structure: TCommonsModelFields,
		aField: string,
		bField: string
): TCommonsModelFields {
	if (structure[aField] !== undefined) throw new Error('Link fields are implicit for M2M link table models and should not be explicitly stated in the structure');
	if (structure[bField] !== undefined) throw new Error('Link fields are implicit for M2M link table models and should not be explicitly stated in the structure');

	structure[aField] = new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL);
	structure[bField] = new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL);
	
	return structure;
}

function buildM2MLinkForeignKeys(
		aFirstClass: CommonsFirstClass<any>,	// can't use A because it is static
		bFirstClass: CommonsFirstClass<any>,	// can't use B because it is static
		aField: string,
		bField: string,
		tableName: string,
		foreignKeys: TCommonsModelForeignKeys = {}
): TCommonsModelForeignKeys {
	foreignKeys[aField] = new CommonsModelForeignKey(
			aFirstClass,
			ECommonsDatabaseReferentialAction.CASCADE,
			ECommonsDatabaseReferentialAction.CASCADE,
			`${tableName}__fk_a`
	);
	foreignKeys[bField] = new CommonsModelForeignKey(
			bFirstClass,
			ECommonsDatabaseReferentialAction.CASCADE,
			ECommonsDatabaseReferentialAction.CASCADE,
			`${tableName}__fk_b`
	);
	
	return foreignKeys;
}

function buildUniqueKeys(
		aField: string,
		bField: string,
		tableName: string,
		uniqueKeys: CommonsDatabaseUniqueKey[] = []
): CommonsDatabaseUniqueKey[] {
	uniqueKeys.push(
			new CommonsDatabaseUniqueKey(
					[ aField, bField ], `${tableName}__uk`
			)
	);
	
	return uniqueKeys;
}

export class CommonsM2MLinkTable<
		ModelI extends ICommonsM2MLink<AI, BI>,
		AI extends ICommonsFirstClass,
		BI extends ICommonsFirstClass
> extends CommonsModel<ModelI> {
	protected aFirstClass: CommonsFirstClass<AI>;
	protected bFirstClass: CommonsFirstClass<BI>;
	protected aField: string;
	protected bField: string;
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			_aFirstClass: CommonsFirstClass<AI>,
			_bFirstClass: CommonsFirstClass<BI>,
			_aField: string,
			_bField: string,
			structure: TCommonsModelFields,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildM2MLinkStructure(
						structure,
						_aField,
						_bField
				),
				buildM2MLinkForeignKeys(
						_aFirstClass,
						_bFirstClass,
						_aField,
						_bField,
						tableName,
						foreignKeys
				),
				buildUniqueKeys(
						_aField,
						_bField,
						tableName,
						uniqueKeys
				),
				indexKeys
		);
		
		this.aFirstClass = _aFirstClass;
		this.bFirstClass = _bFirstClass;
		this.aField = _aField;
		this.bField = _bField;
	}
	
	public getAFirstClass(): CommonsFirstClass<AI> {
		return this.aFirstClass;
	}
	
	public getBFirstClass(): CommonsFirstClass<BI> {
		return this.bFirstClass;
	}
	
	public getAField(): string {
		return this.aField;
	}
	
	public getBField(): string {
		return this.bField;
	}

	//-------------------------------------------------------------------------

	protected override defineViews(): void {
		super.defineViews();

		const aFields: string = Object.keys(this.aFirstClass.getStructure())
				.map((field: string): string => this.derivedField('tempAFirstClass', field))
				.join(',');
		
		const bFields: string = Object.keys(this.bFirstClass.getStructure())
				.map((field: string): string => this.derivedField('tempBFirstClass', field))
				.join(',');
		
		this.defineView(
				'M2M_JOIN_B2A',
				`
					SELECT ${aFields},
						${this.derivedField('tempBFirstClass', this.bFirstClass.getIdField())} AS ${this.tempField('remoteId')}
					FROM ${this.model(this.aFirstClass)} AS ${this.derived('tempAFirstClass')}
					INNER JOIN ${this.model(this)}
						ON ${this.modelField(this, this.aField)} = ${this.derivedField('tempAFirstClass', this.aFirstClass.getIdField())}
					INNER JOIN ${this.model(this.bFirstClass)} AS ${this.derived('tempBFirstClass')}
						ON ${this.modelField(this, this.bField)} = ${this.derivedField('tempBFirstClass', this.bFirstClass.getIdField())}
				`
		);

		this.defineView(
				'M2M_JOIN_A2B',
				`
					SELECT ${bFields},
						${this.derivedField('tempAFirstClass', this.aFirstClass.getIdField())} AS ${this.tempField('remoteId')}
					FROM ${this.model(this.bFirstClass)} AS ${this.derived('tempBFirstClass')}
					INNER JOIN ${this.model(this)}
						ON ${this.modelField(this, this.bField)} = ${this.derivedField('tempBFirstClass', this.bFirstClass.getIdField())}
					INNER JOIN ${this.model(this.aFirstClass)} AS ${this.derived('tempAFirstClass')}
						ON ${this.modelField(this, this.aField)} = ${this.derivedField('tempAFirstClass', this.aFirstClass.getIdField())}
				`
		);

		this.defineView(
				'M2M_JOIN_DIRECT',
				`
					SELECT ${this.fields},
						${this.derivedField('tempAFirstClass', this.aFirstClass.getIdField())} AS ${this.tempField('aId')},
						${this.derivedField('tempBFirstClass', this.bFirstClass.getIdField())} AS ${this.tempField('bId')}
					FROM ${this.model(this)}
					INNER JOIN ${this.model(this.aFirstClass)} AS ${this.derived('tempAFirstClass')}
						ON ${this.modelField(this, this.aField)} = ${this.derivedField('tempAFirstClass', this.aFirstClass.getIdField())}
					INNER JOIN ${this.model(this.bFirstClass)} AS ${this.derived('tempBFirstClass')}
						ON ${this.modelField(this, this.bField)} = ${this.derivedField('tempBFirstClass', this.bFirstClass.getIdField())}
				`
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();

		const aFields: string = Object.keys(this.aFirstClass.getStructure())
				.map((field: string): string => this.modelViewField(this, 'M2M_JOIN_B2A', field))
				.join(',');
	
		const bFields: string = Object.keys(this.bFirstClass.getStructure())
				.map((field: string): string => this.modelViewField(this, 'M2M_JOIN_A2B', field))
				.join(',');

		{
			const sql: string[] = [
					`
						SELECT ${aFields}
						FROM ${this.modelView(this, 'M2M_JOIN_B2A')}
						WHERE ${this.modelViewField(this, 'M2M_JOIN_B2A', 'remoteId')} = :id
					`
			];
			if (this.aFirstClass instanceof CommonsOrdered || this.aFirstClass instanceof CommonsOrientatedOrdered || this.aFirstClass instanceof CommonsHierarchy) {
				sql.push(`ORDER BY ${(this.aFirstClass as unknown as ICommonsOrderable).getOrderedField()} ASC`);
			}
			
			this.database.preprepare(
					`MODELS_M2MLINKTABLE__${this.tableName}__LIST_AS_BY_B`,
					sql.join(' '),
					{
							id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
					},
					this.aFirstClass.getStructure()
			);
		}

		{
			const sql: string[] = [
					`
						SELECT ${bFields}
						FROM ${this.modelView(this, 'M2M_JOIN_A2B')}
						WHERE ${this.modelViewField(this, 'M2M_JOIN_A2B', 'remoteId')} = :id
					`
			];
			if (this.bFirstClass instanceof CommonsOrdered || this.bFirstClass instanceof CommonsOrientatedOrdered || this.bFirstClass instanceof CommonsHierarchy) {
				sql.push(`ORDER BY ${(this.bFirstClass as unknown as ICommonsOrderable).getOrderedField()} ASC`);
			}
			
			this.database.preprepare(
					`MODELS_M2MLINKTABLE__${this.tableName}__LIST_BS_BY_A`,
					sql.join(' '),
					{
							id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
					},
					this.bFirstClass.getStructure()
			);
		}
	}

	//-------------------------------------------------------------------------

	public async listAsByB(b: BI): Promise<AI[]> {
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());
				
		return await this.database.executeParams(
				`MODELS_M2MLINKTABLE__${this.tableName}__LIST_AS_BY_B`,
				{
						id: b[this.bFirstClass.getIdField()] as number
				}
		);
	}
	
	public async listBsByA(a: AI): Promise<BI[]> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());
		
		return await this.database.executeParams(
				`MODELS_M2MLINKTABLE__${this.tableName}__LIST_BS_BY_A`,
				{
						id: a[this.aFirstClass.getIdField()] as number
				}
		);
	}
	
	//-------------------------------------------------------------------------

	public async isLinked(a: AI, b: BI): Promise<boolean> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());
		
		const conditions: TCommonsDatabaseParams = {};
		conditions[this.aField] = new CommonsDatabaseParam(a[this.aFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		conditions[this.bField] = new CommonsDatabaseParam(b[this.bFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		
		return await this.database.doesRowExistByConditions(
				this.tableName,
				conditions
		);
	}

	public async getLinkRow(a: AI, b: BI): Promise<ModelI> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());
		
		const conditions: TCommonsDatabaseParams = {};
		conditions[this.aField] = new CommonsDatabaseParam(a[this.aFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		conditions[this.bField] = new CommonsDatabaseParam(b[this.bFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
	
		const rows: ModelI[] = await this.database.selectRowsByConditions(
				this.tableName,
				this.structure,
				conditions
		);
		if (rows.length === 0) throw new Error('No such link row exists');
		if (rows.length > 1) throw new Error('More than one link row appears to exist. This should not be possible.');
		
		return rows[0];
	}
	
	//-------------------------------------------------------------------------

	public async link(a: AI, b: BI, values: TPropertyObject = {}): Promise<void> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());
		
		if (values[this.aField] !== undefined) throw new Error('The aField is implicit for M2MLinkTable models and should not be explicitly provided in the values');
		values[this.aField] = a[this.aFirstClass.getIdField()] as number;
		
		if (values[this.bField] !== undefined) throw new Error('The bField is implicit for M2MLinkTable models and should not be explicitly provided in the values');
		values[this.bField] = b[this.bFirstClass.getIdField()] as number;
		
		await this.insert(values);
	}

	public async unlink(a: AI, b: BI, ignoreMissingLink: boolean = false): Promise<void> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			const conditions: TCommonsDatabaseParams = {};
			conditions[this.aField] = new CommonsDatabaseParam(a[this.aFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
			conditions[this.bField] = new CommonsDatabaseParam(b[this.bFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		
			const count: number = await this.database.countRowsByConditions(
					this.tableName,
					conditions
			);
			if (count !== 1) {
				if (count === 0 && ignoreMissingLink) {
					if (isTransactionOwner) await this.database.transactionCommit();
					return;
				}
				
				throw new Error(`Unable to unlink because no such link exists or more than one exists: ${a[this.aFirstClass.getIdField()]} - ${b[this.bFirstClass.getIdField()]}`);	// eslint-disable-line @typescript-eslint/restrict-template-expressions
			}
			
			await this.database.deleteRowsByConditions(
					this.tableName,
					conditions
			);
			
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public async deleteAllForA(a: AI): Promise<void> {
		CommonsModel.assertIdObject(a, this.aFirstClass.getIdField());

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.aField] = new CommonsDatabaseParam(a[this.aFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));

		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions
		);
	}

	public async deleteAllForB(b: BI): Promise<void> {
		CommonsModel.assertIdObject(b, this.bFirstClass.getIdField());

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.bField] = new CommonsDatabaseParam(b[this.bFirstClass.getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));

		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions
		);
	}

	public async updateLinkRow(row: ModelI): Promise<void> {
		CommonsModel.assertIdObject(row, this.aField);
		CommonsModel.assertIdObject(row, this.bField);
		
		const params: TCommonsDatabaseParams = {};
		for (const field of Object.keys(row)) {
			if (field === this.aField || field === this.bField) continue;

			if (!commonsTypeHasProperty(this.structure, field)) throw new Error(`Parameter variable is missing in the model structure: ${field}`);
			params[field] = new CommonsDatabaseParam(row[field], this.structure[field]);
		}

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.aField] = new CommonsDatabaseParam(row[this.aField], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		conditions[this.bField] = new CommonsDatabaseParam(row[this.bField], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		
		await this.database.updateRowsByConditions(
				this.tableName,
				params,
				conditions,
				true
		);
	}
}
