import {
		commonsTypeHasProperty,
		commonsTypeAssertObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsOrientatedOrdered } from 'tscommons-es-models';
import { ICommonsOrderable } from 'tscommons-es-models';
import { ECommonsMoveDirection } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSmallInt } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSerialId } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeSigned } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';
import { CommonsSecondClass } from './commons-second-class';
import { COMMONS_ORDERED_DEFAULT_ORDERED_FIELD } from './commons-ordered';

function buildOrientatedOrderedStructure(
		structure: TCommonsModelFields,
		orderedField: string
): TCommonsModelFields {
	if (structure[orderedField] !== undefined) throw new Error('The ordered field is implicit for Ordered models and should not be explicitly stated in the structure');

	structure[orderedField] = new CommonsDatabaseTypeSmallInt(
			ECommonsDatabaseTypeSigned.UNSIGNED,
			ECommonsDatabaseTypeNull.NOT_NULL
	);
	
	return structure;
}

export class CommonsOrientatedOrdered<
		ModelI extends ICommonsOrientatedOrdered<ParentI>,
		ParentI extends ICommonsFirstClass
> extends CommonsSecondClass<ModelI, ParentI> implements ICommonsOrderable {
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			firstClass: CommonsFirstClass<ParentI>,
			firstClassField: string,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildOrientatedOrderedStructure(
						structure,
						COMMONS_ORDERED_DEFAULT_ORDERED_FIELD
				),
				firstClass,
				firstClassField,
				foreignKeys,
				uniqueKeys,
				indexKeys
		);
	}
	
	public getOrderedField(): string {
		return COMMONS_ORDERED_DEFAULT_ORDERED_FIELD;
	}

	//-------------------------------------------------------------------------

	protected override preprepare(): void {
		super.preprepare();

		const fields: string = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'SECONDCLASS_JOIN', field))
				.join(',');

		this.database.preprepare(
				`MODELS_ORIENTATEDORDERED__${this.tableName}__LIST_ORDERED`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
					ORDER BY ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getOrderedField())} ASC
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				this.structure
		);

		this.database.preprepare(
				`MODELS_ORIENTATEDORDERED__${this.tableName}__MAX_ORDERED`,
				`
					SELECT MAX(${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getOrderedField())}) AS ${this.tempField('max')}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				{
						max: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		this.database.preprepare(
				`MODELS_ORIENTATEDORDERED__${this.tableName}__CURRENT_ORDERED`,
				`
					SELECT ${this.modelField(this, this.getOrderedField())} AS ${this.tempField('ordered')}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, this.getIdField())} = :id
				`,
				{
						id: new CommonsDatabaseTypeSerialId()
				},
				{
						ordered: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
				}
		);

		this.database.preprepare(
				`MODELS_ORIENTATEDORDERED__${this.tableName}__BY_ORDERED`,
				`
					SELECT ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())} AS ${this.tempField('id')}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
					AND ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getOrderedField())} = :ordered
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						ordered: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
				},
				{
						id: new CommonsDatabaseTypeSerialId()
				}
		);
	}

	//-------------------------------------------------------------------------

	public async listOrderedByFirstClass(firstClassObject: ParentI): Promise<ModelI[]> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
		
		return await this.database.executeParams(
				`MODELS_ORIENTATEDORDERED__${this.tableName}__LIST_ORDERED`,
				{
						firstClass: firstClassObject[this.firstClass.getIdField()] as number
				}
		);
	}

	//---------------------------------------------------------------------

	public async move(object: ModelI, direction: ECommonsMoveDirection): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(object, this.firstClassField);
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			const current: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_ORIENTATEDORDERED__${this.tableName}__CURRENT_ORDERED`,
					{
							id: object[this.getIdField()] as number
					}
			);
			
			const currentMax: number = await this.database.executeParamsValueNoNone<number>(
					`MODELS_ORIENTATEDORDERED__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: object[this.firstClassField] as number
					}
			);
	
			switch (direction) {
				case ECommonsMoveDirection.UP: {
					if (current === 0) throw new Error('Cannot move above the first ordered row');
	
					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_ORIENTATEDORDERED__${this.tableName}__BY_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									ordered: current - 1
							}
					);

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current - 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.DOWN: {
					if (current === currentMax) throw new Error('Cannot move below the last ordered row');

					const prev: number = await this.database.executeParamsValueNoNone<number>(
							`MODELS_ORIENTATEDORDERED__${this.tableName}__BY_ORDERED`,
							{
									firstClass: object[this.firstClassField] as number,
									ordered: current + 1
							}
					);

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(prev, new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}

					{
						const updates: TCommonsDatabaseParams = {};
						updates[this.getOrderedField()] = new CommonsDatabaseParam(current + 1, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
						
						const conditions: TCommonsDatabaseParams = {};
						conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
						
						await this.database.updateRowsByConditions(
								this.tableName,
								updates,
								conditions,
								true
						);
					}
	
					break;
				}
				case ECommonsMoveDirection.TOP: {
					if (current === 0) break;	// if already topmost
	
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, this.getOrderedField())} = ${this.modelField(this, this.getOrderedField())} + 1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, this.getOrderedField())} < :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(object[this.firstClassField], new CommonsDatabaseTypeSerialId()),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
	
					const updates: TCommonsDatabaseParams = {};
					updates[this.getOrderedField()] = new CommonsDatabaseParam(0, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
				case ECommonsMoveDirection.BOTTOM: {
					if (current === currentMax) break;	// if already last
	
					await this.database.noneParams(
							`
								UPDATE ${this.model(this)}
								SET ${this.setField(this, this.getOrderedField())} = ${this.modelField(this, this.getOrderedField())} - 1
								WHERE ${this.modelField(this, this.firstClassField)} = :firstClass
								AND ${this.modelField(this, this.getOrderedField())} > :threshold
							`,
							{
									firstClass: new CommonsDatabaseParam(object[this.firstClassField], new CommonsDatabaseTypeSerialId()),
									threshold: new CommonsDatabaseParam(current, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL))
							}
					);
					
					const updates: TCommonsDatabaseParams = {};
					updates[this.getOrderedField()] = new CommonsDatabaseParam(currentMax, new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL));
					
					const conditions: TCommonsDatabaseParams = {};
					conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], new CommonsDatabaseTypeSerialId());
					
					await this.database.updateRowsByConditions(
							this.tableName,
							updates,
							conditions,
							true
					);
	
					break;
				}
					
				default: {
					throw new Error(`Unsupported move direction: ${direction}`);
				}
			}
			
			const refreshed: ModelI = await this.refresh(object);
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return refreshed;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	public async jump(object: ModelI, destination: ParentI): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertId(object[this.firstClassField]);

		CommonsModel.assertIdObject(destination, this.firstClass.getIdField());
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			await this.move(object, ECommonsMoveDirection.BOTTOM);
			
			let currentMax: number|undefined = await this.database.executeParamsValue<number>(
					`MODELS_ORIENTATEDORDERED__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: destination[this.firstClass.getIdField()] as number
					},
					true
			);
			if (currentMax === undefined) currentMax = 0;
			else currentMax++;
			
			(object as TPropertyObject)[this.firstClassField] = destination[this.firstClass.getIdField()] as number;
			(object as TPropertyObject)[this.getOrderedField()] = currentMax;
			
			await this.update(object);
			
			const refreshed: ModelI = await this.refresh(object);
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return refreshed;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
	
	//---------------------------------------------------------------------

	public override async insertForFirstClass(
			firstClassObject: ParentI,
			values: TPropertyObject
	): Promise<ModelI> {
		commonsTypeAssertObject(values);
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
		
		if (commonsTypeHasProperty(values, this.getOrderedField())) throw new Error('Ordered fields should not be specified explicitly for OrientatedOrdered models');

		// oddly, the above typecasts values as 'never', so we have to fix this manually
		
		const typecast: TPropertyObject = values;
		
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			let currentMax: number|undefined = await this.database.executeParamsValue<number>(
					`MODELS_ORIENTATEDORDERED__${this.tableName}__MAX_ORDERED`,
					{
							firstClass: firstClassObject[this.firstClass.getIdField()] as number
					},
					true
			);
			if (currentMax === undefined) currentMax = 0;
			else currentMax++;
			
			typecast[this.getOrderedField()] = currentMax;
			
			const object: ModelI = await super.insertForFirstClass(firstClassObject, typecast);
		
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return object;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public override async deleteForFirstClass(firstClass: ParentI, object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(firstClass, this.firstClass.getIdField());

		if (!commonsTypeHasProperty(object, this.getOrderedField())) throw new Error('Unable to find orientator ID within the object');
			
		const isTransactionOwner: boolean = await this.database.transactionClaim();
		
		try {
			await this.move(object, ECommonsMoveDirection.BOTTOM);
			await super.deleteForFirstClass(firstClass, object);
		
			if (isTransactionOwner) await this.database.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}
}
