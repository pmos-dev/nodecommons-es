import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyString,
		commonsTypeAssertObject,
		commonsArrayChunk,
		commonsArrayUnique
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsMultiParentSecondClass } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import {
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeText,
		CommonsDatabaseTypeEncrypted
} from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseOrderBy, fromECommonsDatabaseOrderBy } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';
import { TCommonsMultiParents } from '../types/tcommons-model-multi-parents';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';
import { CommonsModelForeignKey } from './commons-model-foreign-key';

function buildMultiParentSecondClassStructure(
		structure: TCommonsModelFields,
		firstClasses: TCommonsMultiParents
): TCommonsModelFields {
	for (const firstClassField of Object.keys(firstClasses)) {
		if (structure[firstClassField] !== undefined) throw new Error('The first class fields are implicit for MultiParentSecondClass models and should not be explicitly stated in the structure');

		structure[firstClassField] = new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL);
	}
	
	return structure;
}

function buildMultiParentSecondClassForeignKeys(
		foreignKeys: TCommonsModelForeignKeys,
		firstClasses: TCommonsMultiParents,
		tableName: string
): TCommonsModelForeignKeys {
	for (const firstClassField of Object.keys(firstClasses)) {
		if (foreignKeys[firstClassField] !== undefined) throw new Error('Do not specify the foreign key relationship directly for multi-parent-second-class models');
		foreignKeys[firstClassField] = new CommonsModelForeignKey(
				firstClasses[firstClassField],
				ECommonsDatabaseReferentialAction.CASCADE,
				ECommonsDatabaseReferentialAction.CASCADE,
				`${tableName}__fk_2nd_${firstClassField}`
		);
	}
	
	return foreignKeys;
}

// Unfortunately we can't use generics for this class as the number of multiple first classes is arbitary

type TFirstClassParents = {
		[ fieldName: string ]: ICommonsFirstClass;
};

export class CommonsMultiParentSecondClass<
		ModelI extends ICommonsMultiParentSecondClass
> extends CommonsFirstClass<ModelI> {
	protected firstClasses: TCommonsMultiParents;
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			_firstClasses: TCommonsMultiParents,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildMultiParentSecondClassStructure(structure, _firstClasses),
				buildMultiParentSecondClassForeignKeys(
						foreignKeys,
						_firstClasses,
						tableName
				),
				uniqueKeys,
				indexKeys
		);
		
		this.firstClasses = _firstClasses;
	}
	
	public getFirstClasses(): TCommonsMultiParents {
		return this.firstClasses;
	}

	//-------------------------------------------------------------------------

	protected override defineViews(): void {
		super.defineViews();

		// This view is really a bit pointless, as it just returns the same data as the original column, but with "firstClass_" appended on the front. However, it does give a small level of guarantee that the field is the correct one, so we might as well retain it.
		const derivedFields: string[] = [];
		const joins: string[] = [];
		for (const field of Object.keys(this.firstClasses)) {
			const firstClass: CommonsFirstClass<ICommonsFirstClass> = this.firstClasses[field];
			
			derivedFields.push(`${this.modelField(firstClass, firstClass.getIdField())} AS ${this.tempField(`firstClass_${field}`)}`);
			joins.push(`INNER JOIN ${this.model(firstClass)} ON ${this.modelField(this, field)} = ${this.modelField(firstClass, firstClass.getIdField())}`);
		}

		this.defineView(
				'MULTIPARENTSECONDCLASS_JOIN',
				`
					SELECT ${this.fields},
						${derivedFields.join(',')}
					FROM ${this.model(this)}
					${joins.join(' ')}
				`
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();

		const fields: string = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field))
				.join(',');

		const clauses: string[] = [];
		const params: TCommonsDatabaseTypes = {};
		for (const field of Object.keys(this.firstClasses)) {
			clauses.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', `firstClass_${field}`)}=:firstClass_${field}`);
			params[`firstClass_${field}`] = this.structure[field];
		}

		this.database.preprepare(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__GET_BY_FIRSTCLASS_PARENTS_AND_ID`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', this.getIdField())} = :id
					AND ${clauses.join(' AND ')}
				`,
				{
						id: this.structure[this.getIdField()],
						...params
				},
				this.structure
		);

		this.database.preprepare(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__LIST_BY_FIRSTCLASS_PARENTS`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}
					WHERE ${clauses.join(' AND ')}
				`,
				params,
				this.structure
		);

		this.database.preprepare(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__LIST_ALL_IDS_BY_FIRSTCLASS_PARENTS`,
				`
					SELECT ${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', this.getIdField())} AS ${this.tempField('idField')}
					FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}
					WHERE ${clauses.join(' AND ')}
				`,
				params,
				{
						idField: this.structure[this.getIdField()]
				}
		);
	}

	//-------------------------------------------------------------------------

	public async listByFirstClassParents(firstClassObjects: TFirstClassParents): Promise<ModelI[]> {
		const params: { [ field: string ]: number } = {};
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());
			params[`firstClass_${field}`] = firstClassObject[this.firstClasses[field].getIdField()] as number;
		}
		
		return await this.database.executeParams(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__LIST_BY_FIRSTCLASS_PARENTS`,
				params
		);
	}

	public override async getById(_id: number): Promise<ModelI|undefined> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call get_by_id directly, use getByFirstClassesAndId instead. Or if unavoidable, use getByIdOnly');
	}
	
	// Occasionally it is unavoidable to need to get an ID independently of knowing its firstClass parents.
	// e.g. when using a REST API and an M2MLinkTable, where we only have A and B, rather than their parents.
	public async getByIdOnly(id: number): Promise<ModelI|undefined> {
		return super.getById(id);
	}

	protected async internalMultiParentSecondClassGetByFirstClassAndId(
			firstClassObjects: TFirstClassParents,
			id: number
	): Promise<ModelI|undefined> {
		const params: { [ field: string ]: number } = { id: id };

		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());
			params[`firstClass_${field}`] = firstClassObject[this.firstClasses[field].getIdField()] as number;
		}

		return await this.database.executeParamsSingle(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__GET_BY_FIRSTCLASS_PARENTS_AND_ID`,
				params
		);
	}

	public async getByFirstClassesAndId(
			firstClassObjects: TFirstClassParents,
			id: number
	): Promise<ModelI|undefined> {
		CommonsModel.assertId(id);
		
		return await this.internalMultiParentSecondClassGetByFirstClassAndId(firstClassObjects, id);
	}

	// eslint-disable-next-line @typescript-eslint/require-await
	public override async bulkGetByIds(
			_ids: number[],
			_orderBy?: { [field: string]: ECommonsDatabaseOrderBy }
	): Promise<ModelI[]> {
		throw new Error('Do not call bulkGetByIds directly, use bulkGetByFirstClassesAndIds instead');
	}

	public async bulkGetByFirstClassesAndIds(
			firstClassObjects: TFirstClassParents,
			ids: number[],
			orderBy?: { [field: string]: ECommonsDatabaseOrderBy }
	): Promise<ModelI[]> {
		if (ids.length === 0) return [];
		if (ids.length > 255) {
			const slice: number[] = ids.slice(0, 255);
			const remainder: number[] = ids.slice(255);
			
			return [
					...(await this.bulkGetByFirstClassesAndIds(firstClassObjects, slice, orderBy)),
					...(await this.bulkGetByFirstClassesAndIds(firstClassObjects, remainder, orderBy))
			];
		}

		const clauses: string[] = [];
		const params: { [ field: string ]: CommonsDatabaseParam } = {};
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			clauses.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', `firstClass_${field}`)}=:firstClass_${field}`);
			
			params[`firstClass_${field}`] = new CommonsDatabaseParam(
					firstClassObject[this.firstClasses[field].getIdField()],
					this.structure[field]
			);
		}

		for (const id of ids) CommonsModel.assertId(id);

		const idsString: string = ids
				.map((id: number): string => id.toString())
				.join(',');

		const fields: string[] = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field));
		
		const query: string[] = [
				`SELECT ${fields.join(',')}`,
				`FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}`,
				`WHERE ${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', this.getIdField())} IN (${idsString})`,
				`AND ${clauses.join(' AND ')}`
		];
		
		if (orderBy) {
			const orderByClauses: string[] = [];
			for (const field of Object.keys(orderBy)) {
				const direction: ECommonsDatabaseOrderBy = orderBy[field];
				orderByClauses.push(`${this.modelField(this, field)} ${fromECommonsDatabaseOrderBy(direction)}`);
			}
			
			if (orderByClauses.length > 0) query.push(`ORDER BY ${orderByClauses.join(',')}`);
		}

		return await this.database.queryParams<ModelI>(
				query.join(' '),
				params,
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------

	public override async searchIdsForNonencryptedFields(_value: string, _fields: string[]): Promise<number[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call searchIdsForEncryptedFields directly, use searchIdsForEncryptedFieldsByFirstclasses instead');
	}

	public override async searchIdsForEncryptedFields(_value: string, _fields: string[]): Promise<number[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call searchIdsForEncryptedFields directly, use searchIdsForEncryptedFieldsByFirstclasses instead');
	}

	public override async searchFields(_value: string, _fields: string[], _orderBy?: { [field: string]: ECommonsDatabaseOrderBy }): Promise<ModelI[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call search directly, use searchByFirstclasses instead');
	}

	public async searchIdsForNonencryptedFieldsByFirstClass(
			firstClassObjects: TFirstClassParents,
			value: string,
			fields: string[]
	): Promise<number[]> {
		const clauses: string[] = [];
		const params: { [ field: string ]: CommonsDatabaseParam } = {};
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			clauses.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', `firstClass_${field}`)}=:firstClass_${field}`);

			params[`firstClass_${field}`] = new CommonsDatabaseParam(
					firstClassObject[this.firstClasses[field].getIdField()],
					this.structure[field]
			);
		}

		if (fields.length === 0) return [];

		const conditions: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) throw new Error(`Attempting to search on an encrypted field: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeString || this.structure[field] instanceof CommonsDatabaseTypeText)) throw new Error(`Attempting to search on a non-string/text field: ${field}`);

			conditions.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field)} LIKE :valuea`);
			conditions.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field)} LIKE :valueb`);
			conditions.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field)} LIKE :valuec`);
			conditions.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field)} LIKE :valued`);
		}

		return await this.database.queryParamsList<number>(
				`
					SELECT ${this.getIdField()}
					FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}
					WHERE ${clauses.join(' AND ')}
					AND (${conditions.join(' OR ')})
				`,
				{
						...params,
						valuea: new CommonsDatabaseParam(value, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valueb: new CommonsDatabaseParam(`${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valuec: new CommonsDatabaseParam(`%${value}`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valued: new CommonsDatabaseParam(`%${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL))
				},
				this.getIdField(),
				this.structure[this.getIdField()]
		);
	}

	public async searchIdsForEncryptedFieldsByFirstClass(
			firstClassObjects: TFirstClassParents,
			value: string,
			fields: string[]
	): Promise<number[]> {
		const clauses: string[] = [];
		const params: { [ field: string ]: CommonsDatabaseParam } = {};
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			clauses.push(`${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', `firstClass_${field}`)}=:firstClass_${field}`);

			params[`firstClass_${field}`] = new CommonsDatabaseParam(
					firstClassObject[this.firstClasses[field].getIdField()],
					this.structure[field]
			);
		}

		if (fields.length === 0) return [];

		const results: TCommonsDatabaseTypes = {};
		const fieldnames: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeEncrypted)) throw new Error(`Attempting to search on a non-encrypted field: ${field}`);

			results[field] = this.structure[field];
			fieldnames.push(this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', field));
		}

		results[this.getIdField()] = this.structure[this.getIdField()];

		const idQueryParams: { [ field: string ]: number } = {};
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			idQueryParams[`firstClass_${field}`] = firstClassObject[this.firstClasses[field].getIdField()] as number;
		}

		const allIds: number[] = await this.database.executeParamsList<number>(
				`MODELS_MULTIPARENTSECONDCLASS__${this.tableName}__LIST_ALL_IDS_BY_FIRSTCLASS`,
				idQueryParams
		);
		if (allIds.length === 0) return [];

		const chunks: number[][] = commonsArrayChunk<number>(allIds, 500);
		const matches: TPropertyObject = {};
		for (const ids of chunks) {
			if (ids.length === 0) continue;

			// don't need to include the firstClasses in the where clause, as it was already done in the lookup for the id numbers
			const result: TPropertyObject[] = await this.database.queryParams<TPropertyObject>(
					`
						SELECT ${fieldnames.join(',')},
							${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', this.getIdField())}
						FROM ${this.modelView(this, 'MULTIPARENTSECONDCLASS_JOIN')}
						WHERE ${this.modelViewField(this, 'MULTIPARENTSECONDCLASS_JOIN', this.getIdField())} IN (${ids.join(',')})
					`,
					params
			);

			for (const row of result) {
				for (const field of fields) {
					if (!commonsTypeHasPropertyString(row, field)) continue;
					const s: string = row[field] as string;
					
					if (!s.toLowerCase().includes(value.toLowerCase())) continue;
					
					matches[row[this.getIdField()]] = row[this.getIdField()];	// eslint-disable-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
				}
			}
		}

		return Object.values(matches);	// eslint-disable-line @typescript-eslint/no-unsafe-return
	}

	public async searchFieldsByFirstClass(
			firstClassObjects: TFirstClassParents,
			value: string,
			fields: string[],
			orderBy?: { [field: string]: ECommonsDatabaseOrderBy }
	): Promise<ModelI[]> {
		const encrypted: string[] = [];
		const regular: string[] = [];

		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform search on: ${field}`);

			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) {
				encrypted.push(field);
			} else {
				regular.push(field);
			}
		}

		const ids: number[] = [
				...(await this.searchIdsForEncryptedFieldsByFirstClass(firstClassObjects, value, encrypted)),
				...(await this.searchIdsForNonencryptedFieldsByFirstClass(firstClassObjects, value, regular))
		];

		return await this.bulkGetByFirstClassesAndIds(firstClassObjects, commonsArrayUnique<number>(ids), orderBy);
	}

	//-------------------------------------------------------------------------

	public override async insertFirstClass(_values: TPropertyObject): Promise<ModelI> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call insertFirstClass directly, use insertForFirstClasses instead');
	}

	public async insertForFirstClasses(
			firstClassObjects: TFirstClassParents,
			values: TPropertyObject
	): Promise<ModelI> {
		commonsTypeAssertObject(values);

		// the assertion typecasts value as never, so we need an explicit type closure here
		const typecast: TPropertyObject = values;
		
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			if (commonsTypeHasProperty(values, field)) throw new Error('Firstclass fields should not be specified explicitly for multi-parent-second-class models');

			typecast[field] = firstClassObject[this.firstClasses[field].getIdField()] as number;
		}
		
		return super.insertFirstClass(typecast);
	}

	public override async delete(_object: ModelI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call delete directly, use deleteForFirstClasses instead');
	}
	
	public async deleteForFirstClasses(
			firstClassObjects: TFirstClassParents,
			object: ModelI
	): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], this.structure[this.getIdField()]);
		
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			if (firstClassObject[this.firstClasses[field].getIdField()] !== object[field]) throw new Error('Firstclass orientator mismatch in the MultiParentSecondClass model data');
			
			conditions[field] = new CommonsDatabaseParam(firstClassObject[this.firstClasses[field].getIdField()], this.structure[field]);
		}
		
		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions,
				true
		);
	}

	public async deleteAllForFirstClasses(
			firstClassObjects: TFirstClassParents
	): Promise<void> {
		const conditions: TCommonsDatabaseParams = {};
		
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			conditions[field] = new CommonsDatabaseParam(firstClassObject[this.firstClasses[field].getIdField()], this.structure[field]);
		}
		
		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions,
				true
		);
	}

	public async updateForFirstClass(
			firstClassObjects: TFirstClassParents,
			object: ModelI
	): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		
		for (const field of Object.keys(this.firstClasses)) {
			const firstClassObject: ICommonsFirstClass = firstClassObjects[field];
			
			CommonsModel.assertIdObject(firstClassObject, this.firstClasses[field].getIdField());

			if (firstClassObject[this.firstClasses[field].getIdField()] !== object[field]) throw new Error('Firstclass orientator mismatch in the MultiParentSecondClass model data');
		}
		
		await this.update(object);
	}
}
