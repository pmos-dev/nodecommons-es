import {
		commonsTypeAssertNumber,
		commonsTypeAssertObject,
		commonsTypeHasPropertyNumber,
		commonsObjectMapObject,
		commonsTypeHasProperty
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { MAX_SIGNED_32BIT_INTEGER } from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';

import {
		commonsDatabaseTable,
		commonsDatabaseView,
		commonsDatabaseTableField,
		commonsDatabaseViewField,
		commonsDatabaseTempField,
		commonsDatabaseSetField,
		ICommonsCredentials
} from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { CommonsDatabaseForeignKey } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { CommonsDatabaseType } from 'nodecommons-es-database';
import { CommonsDatabaseTypeInt } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeSigned } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseEngine } from 'nodecommons-es-database';

import { CommonsModelForeignKey } from '../classes/commons-model-foreign-key';

import { CommonsModelable } from '../interfaces/commons-modelable';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

type TModelFields = {
		[field: string]: string;
};

// two dirty types to save having to import the database-sqlite package into this generic models one
type TSqliteForeignKeys = {
		[ field: string ]: CommonsDatabaseForeignKey;
};
type TSqliteMethods = {
	createTableWithKeys: (
			table: string,
			structure: TCommonsDatabaseTypes,
			ifNotExists: boolean,
			foreignKeys?: TSqliteForeignKeys,
			uniqueKeys?: CommonsDatabaseUniqueKey[]
	) => Promise<void>;
};

export class CommonsModel<ModelI extends ICommonsModel> implements CommonsModelable<ModelI> {
	public static assertId(value: any): number|never {
		const asserted: number = commonsTypeAssertNumber(value);
		
		if (asserted < 0 || asserted >= MAX_SIGNED_32BIT_INTEGER) throw new Error('Assertion fail: variable is not an ID number');

		return asserted;
	}

	public static assertIdObject(object: ICommonsModel, idField: string): ICommonsModel {
		try {
			commonsTypeAssertObject(object);
			if (!commonsTypeHasPropertyNumber(object, idField)) throw new Error(`No ${idField} field present in object data`);
			CommonsModel.assertId(object[idField]);
			
			return object;
		} catch (e) {
			throw new Error('Assertion fail: variable is not an ID object');
		}
	}

	protected fields: string;
	private views: Map<string, string> = new Map<string, string>();
	
	constructor(
			protected database: CommonsSqlDatabaseService<ICommonsCredentials>,
			protected tableName: string,
			protected structure: TCommonsModelFields,
			private foreignKeys: TCommonsModelForeignKeys = {},
			private uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			private indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		this.fields = this.modelFieldCsv(this);

		// we have to split the defineViews and preprepare into a second post-constructor method, as sometimes field parameters may not have been set
	}
	
	public init(): void {
		this.defineViews();
		this.preprepare();
	}
	
	protected modelFieldArray(model: CommonsModel<any>): TModelFields {
		return commonsObjectMapObject(
				model.structure,
				(_: CommonsDatabaseType, field: string): string => this.modelField(model, field)
		);
	}
	
	protected modelFieldCsv(model: CommonsModel<any>) {
		const modelFields: TModelFields = this.modelFieldArray(model);
		
		return Object.values(modelFields).join(',');
	}
	
	protected defineViews(): void {
		// to be overridden if desired
	}
	
	public listViews(): { [name: string]: string } {
		const obj: { [name: string]: string } = {};
		
		for (const name of this.views.keys()) {
			obj[name] = this.views.get(name)!;
		}
		
		return obj;
	}
	
	public getTable(): string {
		return this.tableName;
	}
	
	public getStructure(): TCommonsModelFields {
		return this.structure;
	}
	
	public getForeignKeys(): TCommonsModelForeignKeys {
		return this.foreignKeys;
	}

	public getUniqueKeys(): CommonsDatabaseUniqueKey[] {
		return this.uniqueKeys;
	}

	public getIndexKeys(): CommonsDatabaseIndexKey[] {
		return this.indexKeys;
	}

	public table(table: string): string {
		return commonsDatabaseTable(table, this.database.getEngine());
	}

	public view(table: string, view: string): string {
		return commonsDatabaseView(table, view, this.database.getEngine());
	}

	public modelView(model: CommonsModel<any>, name: string): string {
		return this.view(model.getTable(), name);
	}
	
	public tableField(table: string, field: string): string {
		return commonsDatabaseTableField(table, field, this.database.getEngine());
	}
	
	public viewField(table: string, name: string, field: string): string {
		return commonsDatabaseViewField(table, name, field, this.database.getEngine());
	}

	public derived(name: string): string {
		return this.table(name);
	}
	
	public derivedField(name: string, field: string): string {
		return this.tableField(name, field);
	}

	public model(model: CommonsModel<any>): string {
		return this.table(model.getTable());
	}

	public modelField(model: CommonsModel<any>, field: string): string {
		return this.tableField(model.getTable(), field);
	}
	
	public modelViewField(model: CommonsModel<any>, name: string, field: string): string {
		return this.viewField(model.getTable(), name, field);
	}

	public tempField(field: string): string {
		return commonsDatabaseTempField(field, this.database.getEngine());
	}

	public setField(model: CommonsModel<any>, field: string): string {
		return commonsDatabaseSetField(model.getTable(), field, this.database.getEngine());
	}

	protected modelFieldsCsv(model: CommonsModel<any>): string {
		return Object.keys(model.getStructure())
				.map((field: string): string => this.modelField(model, field))
				.join(',');
	}

	protected preprepare(): void {
		this.database.preprepare(
				`MODELS__${this.tableName}__LIST_ALL`,
				`
					SELECT ${this.modelFieldsCsv(this)}
					FROM ${this.table(this.tableName)}
				`,
				undefined,
				this.getStructure()
		);
	}

	protected defineView(name: string, sql: string): void {
		if (this.views.has(name)) throw new Error(`A view definition within the model with that name already exists: ${name}`);
		
		this.views.set(name, sql);
	}

	public async createTable(ifNotExists: boolean = false): Promise<void> {
		const alreadyExists: boolean = await this.database.doesTableExist(this.tableName);
		
		switch (this.database.getEngine()) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES: {
				await this.database.createTable(
						this.tableName,
						this.structure,
						ifNotExists
				);
		
				if (!alreadyExists) {
					for (const linkField of Object.keys(this.foreignKeys)) {
						const foreign: CommonsModelForeignKey = this.foreignKeys[linkField];
			
						await this.database.createForeignKey(
								this.tableName,
								foreign.getTable(),
								linkField,
								foreign.getField(),
								foreign.getOnDelete(),
								foreign.getOnUpdate(),
								foreign.getConstraintName()
						);
					}
			
					for (const unique of this.uniqueKeys) {
						await this.database.createUniqueKey(
								this.tableName,
								unique.getFields(),
								unique.getConstraintName()
						);
					}
			
					for (const index of this.indexKeys) {
						await this.database.createIndexKey(
								this.tableName,
								index.getFields(),
								index.getConstraintName()
						);
					}
				}
				
				break;
			}
			case ECommonsDatabaseEngine.SQLITE: {
				// SQLite doesn't support retrospective foreign keys, so have to be created at the time of table creation

				// this allows us a dirty type checking skip to save having to import the database-sqlite package into this generic models one
				const typecast: TSqliteMethods = (this.database as unknown as TSqliteMethods);
				await typecast.createTableWithKeys(
						this.tableName,
						this.structure,
						ifNotExists,
						this.foreignKeys,
						this.uniqueKeys
				);
				
				break;
			}
			default:
				throw new Error('Unknown database type for model create table');
		}
		
		if (!alreadyExists) {
			for (const name of this.views.keys()) {
				const sql: string = this.views.get(name)!;
				await this.database.createView(this.tableName, name, sql);
			}
		}
	}
	
	public async dropTable(ifExists: boolean = false): Promise<void> {
		for (const name of this.views.keys()) {
			await this.database.dropView(this.tableName, name);	// views are silently dropped if they don't exist
		}
		
		await this.database.dropTable(
				this.tableName,
				this.structure,
				ifExists
		);
	}
	
	public async listAll(): Promise<ModelI[]> {
		return await this.database.executeParams<ModelI>(`MODELS__${this.tableName}__LIST_ALL`);
	}

	public async countAll(): Promise<number> {
		const count: number|undefined = await this.database.queryParamsValue(
				`
					SELECT COUNT(*) AS ${this.tempField('count')}
					FROM ${this.model(this)}
				`,
				undefined,
				'count',
				new CommonsDatabaseTypeInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
		);
		
		if (count === undefined) throw new Error('Unable to count model entries');
		
		return count;
	}

	public async insert(values: TPropertyObject): Promise<void> {
		const params: TCommonsDatabaseParams = commonsObjectMapObject(
				values,
				(value: any, key: string): CommonsDatabaseParam => {
					if (!commonsTypeHasProperty(this.structure, key)) throw new Error(`Parameter variable is missing in the model structure: ${key}`);
					return new CommonsDatabaseParam(value, this.structure[key]);
				}
		);

		await this.database.insertRow(this.tableName, params);
	}
}
