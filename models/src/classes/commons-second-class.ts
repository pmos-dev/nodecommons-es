import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyString,
		commonsTypeAssertObject,
		commonsArrayChunk,
		commonsArrayUnique
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import {
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeText,
		CommonsDatabaseTypeEncrypted
} from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseOrderBy, fromECommonsDatabaseOrderBy } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';
import { CommonsFirstClass } from './commons-first-class';
import { CommonsModelForeignKey } from './commons-model-foreign-key';

function buildSecondClassStructure(
		structure: TCommonsModelFields,
		firstClassField: string
): TCommonsModelFields {
	if (structure[firstClassField] !== undefined) throw new Error('The first class field is implicit for SecondClass models and should not be explicitly stated in the structure');

	structure[firstClassField] = new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL);
	
	return structure;
}

function buildSecondClassForeignKeys<P extends ICommonsFirstClass>(
		foreignKeys: TCommonsModelForeignKeys,
		firstClass: CommonsFirstClass<P>,	// can't use P because it is static
		firstClassField: string,
		tableName: string
): TCommonsModelForeignKeys {
	if (foreignKeys[firstClassField] !== undefined) throw new Error('Do not specify the foreign key relationship directly for second-class models');
	foreignKeys[firstClassField] = new CommonsModelForeignKey(
			firstClass,
			ECommonsDatabaseReferentialAction.CASCADE,
			ECommonsDatabaseReferentialAction.CASCADE,
			`${tableName}__fk_2nd`
	);
	
	return foreignKeys;
}

export class CommonsSecondClass<
		ModelI extends ICommonsSecondClass<ParentI>,
		ParentI extends ICommonsFirstClass
> extends CommonsFirstClass<ModelI> {
	protected firstClass: CommonsFirstClass<ParentI>;
	protected firstClassField: string;
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			_firstClass: CommonsFirstClass<ParentI>,
			_firstClassField: string,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildSecondClassStructure(structure, _firstClassField),
				buildSecondClassForeignKeys<ParentI>(
						foreignKeys,
						_firstClass,
						_firstClassField,
						tableName
				),
				uniqueKeys,
				indexKeys
		);
		
		this.firstClass = _firstClass;
		this.firstClassField = _firstClassField;
	}
	
	public getFirstClass(): CommonsFirstClass<ParentI> {
		return this.firstClass;
	}
	
	public getFirstClassField(): string {
		return this.firstClassField;
	}

	//-------------------------------------------------------------------------

	protected override defineViews(): void {
		super.defineViews();

		this.defineView(
				'SECONDCLASS_JOIN',
				`
					SELECT ${this.fields},
						${this.modelField(this.firstClass, this.firstClass.getIdField())} AS ${this.tempField('firstClassId')}
					FROM ${this.model(this)}
					INNER JOIN ${this.model(this.firstClass)}
						ON ${this.modelField(this, this.firstClassField)} = ${this.modelField(this.firstClass, this.firstClass.getIdField())}
				`
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();

		const fields: string = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'SECONDCLASS_JOIN', field))
				.join(',');

		this.database.preprepare(
				`MODELS_SECONDCLASS__${this.tableName}__GET_BY_FIRSTCLASS_AND_ID`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())} = :id
					AND ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
				`,
				{
						id: this.structure[this.getIdField()],
						firstClass: this.structure[this.firstClassField]
				},
				this.structure
		);

		this.database.preprepare(
				`MODELS_SECONDCLASS__${this.tableName}__LIST_BY_FIRSTCLASS`,
				`
					SELECT ${fields}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
				`,
				{
						firstClass: this.structure[this.firstClassField]
				},
				this.structure
		);

		this.database.preprepare(
				`MODELS_SECONDCLASS__${this.tableName}__LIST_ALL_IDS_BY_FIRSTCLASS`,
				`
					SELECT ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())} AS ${this.tempField('idField')}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass
				`,
				{
						firstClass: this.structure[this.firstClassField]
				},
				{
						idField: this.structure[this.getIdField()]
				}
		);
	}

	//-------------------------------------------------------------------------

	public async listByFirstClass(firstClassObject: ParentI): Promise<ModelI[]> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
				
		return await this.database.executeParams(
				`MODELS_SECONDCLASS__${this.tableName}__LIST_BY_FIRSTCLASS`,
				{
						firstClass: firstClassObject[this.firstClass.getIdField()] as number
				}
		);
	}

	public override async getById(_id: number): Promise<ModelI|undefined> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call get_by_id directly, use get_by_firstClass_and_id instead. Or if unavoidable, use getByIdOnly');
	}
	
	// Occasionally it is unavoidable to need to get an ID independently of knowing its firstclass parent.
	// e.g. when using a REST API and an M2MLinkTable, where we only have A and B, rather than their parents.
	public async getByIdOnly(id: number): Promise<ModelI|undefined> {
		return super.getById(id);
	}

	protected async internalSecondClassGetByFirstClassAndId(
			firstClassObject: ParentI,
			id: number
	): Promise<ModelI|undefined> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		return await this.database.executeParamsSingle(
				`MODELS_SECONDCLASS__${this.tableName}__GET_BY_FIRSTCLASS_AND_ID`,
				{
						id: id,
						firstClass: firstClassObject[this.firstClass.getIdField()] as number
				}
		);
	}

	public async getByFirstClassAndId(
			firstClassObject: ParentI,
			id: number
	): Promise<ModelI|undefined> {
		CommonsModel.assertId(id);
		
		return await this.internalSecondClassGetByFirstClassAndId(firstClassObject, id);
	}

	// eslint-disable-next-line @typescript-eslint/require-await
	public override async bulkGetByIds(
			_ids: number[],
			_orderBy?: { [field: string]: ECommonsDatabaseOrderBy }
	): Promise<ModelI[]> {
		throw new Error('Do not call bulkGetByIds directly, use bulkGetByFirstClassAndIds instead');
	}

	public async bulkGetByFirstClassAndIds(
			firstClassObject: ParentI,
			ids: number[],
			orderBy?: { [field: string]: ECommonsDatabaseOrderBy }
	): Promise<ModelI[]> {
		if (ids.length === 0) return [];
		if (ids.length > 255) {
			const slice: number[] = ids.slice(0, 255);
			const remainder: number[] = ids.slice(255);
			
			return [
					...(await this.bulkGetByFirstClassAndIds(firstClassObject, slice, orderBy)),
					...(await this.bulkGetByFirstClassAndIds(firstClassObject, remainder, orderBy))
			];
		}

		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		for (const id of ids) CommonsModel.assertId(id);

		const idsString: string = ids
				.map((id: number): string => id.toString())
				.join(',');

		const fields: string[] = Object.keys(this.structure)
				.map((field: string): string => this.modelViewField(this, 'SECONDCLASS_JOIN', field));
		
		const query: string[] = [
				`SELECT ${fields.join(',')}`,
				`FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}`,
				`WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())} IN (${idsString})`,
				`AND ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')} = :firstClass`
		];
		
		if (orderBy) {
			const orderByClauses: string[] = [];
			for (const field of Object.keys(orderBy)) {
				const direction: ECommonsDatabaseOrderBy = orderBy[field];
				orderByClauses.push(`${this.modelField(this, field)} ${fromECommonsDatabaseOrderBy(direction)}`);
			}
			
			if (orderByClauses.length > 0) query.push(`ORDER BY ${orderByClauses.join(',')}`);
		}

		return await this.database.queryParams<ModelI>(
				query.join(' '),
				{
						firstClass: new CommonsDatabaseParam(firstClassObject[this.firstClass.getIdField()], this.structure[this.firstClassField])
				},
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------

	public override async searchIdsForNonencryptedFields(_value: string, _fields: string[]): Promise<number[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call searchIdsForEncryptedFields directly, use searchIdsForEncryptedFieldsByFirstclass instead');
	}

	public override async searchIdsForEncryptedFields(_value: string, _fields: string[]): Promise<number[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call searchIdsForEncryptedFields directly, use searchIdsForEncryptedFieldsByFirstclass instead');
	}

	public override async searchFields(_value: string, _fields: string[], _orderBy?: { [field: string]: ECommonsDatabaseOrderBy }): Promise<ModelI[]> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call search directly, use searchByFirstclass instead');
	}

	public async searchIdsForNonencryptedFieldsByFirstClass(firstClass: ParentI, value: string, fields: string[]): Promise<number[]> {
		if (!commonsTypeHasPropertyNumber(firstClass, this.firstClass.getIdField())) throw new Error('Invalid ID object for firstClass');
		if (fields.length === 0) return [];

		const conditions: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) throw new Error(`Attempting to search on an encrypted field: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeString || this.structure[field] instanceof CommonsDatabaseTypeText)) throw new Error(`Attempting to search on a non-string/text field: ${field}`);

			conditions.push(`${this.modelViewField(this, 'SECONDCLASS_JOIN', field)} LIKE :valuea`);
			conditions.push(`${this.modelViewField(this, 'SECONDCLASS_JOIN', field)} LIKE :valueb`);
			conditions.push(`${this.modelViewField(this, 'SECONDCLASS_JOIN', field)} LIKE :valuec`);
			conditions.push(`${this.modelViewField(this, 'SECONDCLASS_JOIN', field)} LIKE :valued`);
		}

		return await this.database.queryParamsList<number>(
				`
					SELECT ${this.getIdField()}
					FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
					WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstClassId')}=:firstClass
					AND (${conditions.join(' OR ')})
				`,
				{
						valuea: new CommonsDatabaseParam(value, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valueb: new CommonsDatabaseParam(`${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valuec: new CommonsDatabaseParam(`%${value}`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valued: new CommonsDatabaseParam(`%${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						firstClass: new CommonsDatabaseParam(firstClass[this.firstClass.getIdField()], this.structure[this.firstClassField])
				},
				this.getIdField(),
				this.structure[this.getIdField()]
		);
	}

	public async searchIdsForEncryptedFieldsByFirstClass(firstClass: ParentI, value: string, fields: string[]): Promise<number[]> {
		if (!commonsTypeHasPropertyNumber(firstClass, this.firstClass.getIdField())) throw new Error('Invalid ID object for firstClass');
		if (fields.length === 0) return [];

		const results: TCommonsDatabaseTypes = {};
		const fieldnames: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeEncrypted)) throw new Error(`Attempting to search on a non-encrypted field: ${field}`);

			results[field] = this.structure[field];
			fieldnames.push(this.modelViewField(this, 'SECONDCLASS_JOIN', field));
		}

		results[this.getIdField()] = this.structure[this.getIdField()];

		const allIds: number[] = await this.database.executeParamsList<number>(
				`MODELS_SECONDCLASS__${this.tableName}__LIST_ALL_IDS_BY_FIRSTCLASS`,
				{
						firstClass: firstClass[this.firstClass.getIdField()] as number
				}
		);
		if (allIds.length === 0) return [];

		const chunks: number[][] = commonsArrayChunk<number>(allIds, 500);
		const matches: TPropertyObject = {};
		for (const ids of chunks) {
			if (ids.length === 0) continue;

			// don't need to include the firstclass in the where clause, as it was already done in the lookup for the id numbers
			const result: TPropertyObject[] = await this.database.queryParams<TPropertyObject>(
					`
						SELECT ${fieldnames.join(',')},
							${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())}
						FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
						WHERE ${this.modelViewField(this, 'SECONDCLASS_JOIN', this.getIdField())} IN (${ids.join(',')})
					`,
					{
							firstClass: new CommonsDatabaseParam(
									firstClass[this.firstClass.getIdField()] as number,
									this.structure[this.firstClassField]
							)
					}
			);

			for (const row of result) {
				for (const field of fields) {
					if (!commonsTypeHasPropertyString(row, field)) continue;
					const s: string = row[field] as string;
					
					if (!s.toLowerCase().includes(value.toLowerCase())) continue;
					
					matches[row[this.getIdField()]] = row[this.getIdField()];	// eslint-disable-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
				}
			}
		}

		return Object.values(matches);	// eslint-disable-line @typescript-eslint/no-unsafe-return
	}

	public async searchFieldsByFirstClass(firstClass: ParentI, value: string, fields: string[], orderBy?: { [field: string]: ECommonsDatabaseOrderBy }): Promise<ModelI[]> {
		const encrypted: string[] = [];
		const regular: string[] = [];

		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform search on: ${field}`);

			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) {
				encrypted.push(field);
			} else {
				regular.push(field);
			}
		}

		const ids: number[] = [
				...(await this.searchIdsForEncryptedFieldsByFirstClass(firstClass, value, encrypted)),
				...(await this.searchIdsForNonencryptedFieldsByFirstClass(firstClass, value, regular))
		];

		return await this.bulkGetByFirstClassAndIds(firstClass, commonsArrayUnique<number>(ids), orderBy);
	}

	//-------------------------------------------------------------------------

	public override async insertFirstClass(_values: TPropertyObject): Promise<ModelI> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call insertFirstClass directly, use insertForFirstClass instead');
	}

	public async insertForFirstClass(
			firstClassObject: ParentI,
			values: TPropertyObject
	): Promise<ModelI> {
		commonsTypeAssertObject(values);
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		if (commonsTypeHasProperty(values, this.firstClassField)) throw new Error('Firstclass fields should not be specified explicitly for second-class models');

		// oddly, the above typecasts values as 'never', so we have to fix this manually
		
		const typecast: TPropertyObject = values;
		typecast[this.firstClassField] = firstClassObject[this.firstClass.getIdField()] as number;
		
		return super.insertFirstClass(typecast);
	}

	public override async delete(_object: ModelI): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call delete directly, use deleteForFirstClass instead');
	}
	
	public async deleteForFirstClass(
			firstClassObject: ParentI,
			object: ModelI
	): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
		
		if (firstClassObject[this.firstClass.getIdField()] !== object[this.firstClassField]) throw new Error('Firstclass orientator mismatch in the SecondClass model data');

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], this.structure[this.getIdField()]);
		conditions[this.firstClassField] = new CommonsDatabaseParam(firstClassObject[this.firstClass.getIdField()], this.structure[this.firstClassField]);
		
		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions,
				true
		);
	}

	public async deleteAllForFirstClass(firstClassObject: ParentI): Promise<void> {
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());
	
		const conditions: TCommonsDatabaseParams = {};
		conditions[this.firstClassField] = new CommonsDatabaseParam(firstClassObject[this.firstClass.getIdField()], this.structure[this.firstClassField]);
		
		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions,
				false
		);
	}

	public async updateForFirstClass(
			firstClassObject: ParentI,
			object: ModelI
	): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());
		CommonsModel.assertIdObject(firstClassObject, this.firstClass.getIdField());

		if (firstClassObject[this.firstClass.getIdField()] !== object[this.firstClassField]) throw new Error('Firstclass orientator mismatch in the SecondClass model data');
		
		await this.update(object);
	}
}
