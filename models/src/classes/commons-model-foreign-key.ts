import { CommonsDatabaseForeignKey } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';

import { CommonsFirstClass } from './commons-first-class';

export class CommonsModelForeignKey extends CommonsDatabaseForeignKey {
	constructor(
			destModel: CommonsFirstClass<any>,
			onDelete = ECommonsDatabaseReferentialAction.CASCADE,
			onUpdate = ECommonsDatabaseReferentialAction.CASCADE,
			constraintName?: string
	) {
		super(
				destModel.getTable(),
				destModel.getIdField(),
				onDelete,
				onUpdate,
				constraintName
		);
	}
}
