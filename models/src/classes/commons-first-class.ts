import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyString,
		commonsObjectMapObject,
		commonsArrayChunk,
		commonsArrayUnique
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeSerialId } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import {
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeText,
		CommonsDatabaseTypeEncrypted
} from 'nodecommons-es-database';
import { ECommonsDatabaseEngine } from 'nodecommons-es-database';
import { ECommonsDatabaseOrderBy, fromECommonsDatabaseOrderBy } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { TCommonsDatabaseParams } from 'nodecommons-es-database';

import { TCommonsModelFields } from '../types/tcommons-model-fields';
import { TCommonsModelForeignKeys } from '../types/tcommons-model-foreign-keys';

import { CommonsModel } from './commons-model';

export const COMMONS_FIRSTCLASS_DEFAULT_ID_FIELD: string = 'id';

function buildFirstClassStructure(
		structure: TCommonsModelFields,
		idField: string
): TCommonsModelFields {
	if (structure[idField] !== undefined) throw new Error('The id field is implicit for FirstClass models and should not be explicitly stated in the structure');

	structure[idField] = new CommonsDatabaseTypeSerialId();
	
	return structure;
}

export class CommonsFirstClass<ModelI extends ICommonsFirstClass> extends CommonsModel<ModelI> {
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				buildFirstClassStructure(
						structure,
						COMMONS_FIRSTCLASS_DEFAULT_ID_FIELD
				),
				foreignKeys,
				uniqueKeys,
				indexKeys
		);
	}
	
	public getIdField(): string {
		return COMMONS_FIRSTCLASS_DEFAULT_ID_FIELD;
	}

	//-------------------------------------------------------------------------
	
	protected override preprepare(): void {
		super.preprepare();

		this.database.preprepare(
				`MODELS_FIRSTCLASS__${this.tableName}__GET_BY_ID`,
				`
					SELECT ${this.fields}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, this.getIdField())} = :id
				`,
				{
						id: this.structure[this.getIdField()]
				},
				this.structure
		);
	
		const results: TCommonsDatabaseTypes = {};
		results[this.getIdField()] = this.structure[this.getIdField()];
		this.database.preprepare(
				`MODELS_FIRSTCLASS__${this.tableName}__LIST_ALL_IDS`,
				`
					SELECT ${this.modelField(this, this.getIdField())}
					FROM ${this.model(this)}
				`,
				undefined,
				results
		);

		switch (this.database.getEngine()) {
			case ECommonsDatabaseEngine.MYSQL:
				this.database.preprepare(
						`MODELS_FIRSTCLASS__${this.tableName}__DEFRAGMENT`,
						`
							UPDATE ${this.model(this)}
							SET ${this.modelField(this, this.getIdField())} = :newid
							WHERE ${this.modelField(this, this.getIdField())} = :oldid
							LIMIT 1
						`,
						{
								newid: this.structure[this.getIdField()],
								oldid: this.structure[this.getIdField()]
						}
				);
				break;
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				this.database.preprepare(
						`MODELS_FIRSTCLASS__${this.tableName}__DEFRAGMENT`,
						`
							UPDATE ${this.model(this)}
							SET ${this.database.delimit(this.getIdField())} = :newid
							WHERE ${this.modelField(this, this.getIdField())} = :oldid
						`,
						{
								newid: this.structure[this.getIdField()],
								oldid: this.structure[this.getIdField()]
						}
				);
				break;
			default:
				throw new Error('Database type is not supported');
		}
	}

	//-------------------------------------------------------------------------

	protected async internalFirstClassGetById(id: number): Promise<ModelI|undefined> {
		return await this.database.executeParamsSingle<ModelI>(
				`MODELS_FIRSTCLASS__${this.tableName}__GET_BY_ID`,
				{
						id: id
				}
		);
	}

	public async getById(id: number): Promise<ModelI|undefined> {
		CommonsModel.assertId(id);
		
		return await this.internalFirstClassGetById(id);
	}

	public async refresh(object: ModelI): Promise<ModelI> {
		CommonsModel.assertIdObject(object, this.getIdField());
		
		const refreshed: ModelI|undefined = await this.internalFirstClassGetById(object[this.getIdField()] as number);
		if (refreshed === undefined) throw new Error('Refreshed object no longer exists');
		
		return refreshed;
	}

	public async bulkGetByIds(ids: number[], orderBy?: { [field: string]: ECommonsDatabaseOrderBy }): Promise<ModelI[]> {
		if (ids.length === 0) return [];
		if (ids.length > 255) {
			const slice: number[] = ids.slice(0, 255);
			const remainder: number[] = ids.slice(255);
			
			return [
					...(await this.bulkGetByIds(slice, orderBy)),
					...(await this.bulkGetByIds(remainder, orderBy))
			];
		}

		for (const id of ids) CommonsModel.assertId(id);
		
		const idsString = ids
				.map((id: number): string => id.toString())
				.join(',');

		const query: string[] = [
				`SELECT ${this.fields}`,
				`FROM ${this.model(this)}`,
				`WHERE ${this.modelField(this, this.getIdField())} IN (${idsString})`
		];
		
		if (orderBy) {
			const orderByClauses: string[] = [];
			for (const field of Object.keys(orderBy)) {
				const direction: ECommonsDatabaseOrderBy = orderBy[field];
				orderByClauses.push(`${this.modelField(this, field)} ${fromECommonsDatabaseOrderBy(direction)}`);
			}
			
			if (orderByClauses.length > 0) query.push(`ORDER BY ${orderByClauses.join(',')}`);
		}

		return await this.database.queryParams<ModelI>(
				query.join(' '),
				undefined,
				this.structure
		);
	}

	//-------------------------------------------------------------------------

	public async searchIdsForNonencryptedFields(value: string, fields: string[]): Promise<number[]> {
		if (fields.length === 0) return [];

		const conditions: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) throw new Error(`Attempting to search on an encrypted field: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeString || this.structure[field] instanceof CommonsDatabaseTypeText)) throw new Error(`Attempting to search on a non-string/text field: ${field}`);

			conditions.push(`${this.modelField(this, field)} LIKE :valuea`);
			conditions.push(`${this.modelField(this, field)} LIKE :valueb`);
			conditions.push(`${this.modelField(this, field)} LIKE :valuec`);
			conditions.push(`${this.modelField(this, field)} LIKE :valued`);
		}

		return await this.database.queryParamsList<number>(
				`
					SELECT ${this.getIdField()}
					FROM ${this.model(this)}
					WHERE ${conditions.join(' OR ')}
				`,
				{
						valuea: new CommonsDatabaseParam(value, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valueb: new CommonsDatabaseParam(`${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valuec: new CommonsDatabaseParam(`%${value}`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valued: new CommonsDatabaseParam(`%${value}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL))
				},
				this.getIdField(),
				this.structure[this.getIdField()]
		);
	}

	public async searchIdsForEncryptedFields(value: string, fields: string[]): Promise<number[]> {
		if (fields.length === 0) return [];

		const results: TCommonsDatabaseTypes = {};
		const fieldnames: string[] = [];
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform non encrypted search on: ${field}`);
			if (!(this.structure[field] instanceof CommonsDatabaseTypeEncrypted)) throw new Error(`Attempting to search on a non-encrypted field: ${field}`);

			results[field] = this.structure[field];
			fieldnames.push(this.modelField(this, field));
		}
		
		results[this.getIdField()] = this.structure[this.getIdField()];

		const allIds: number[] = await this.database.executeParamsList<number>(`MODELS_FIRSTCLASS__${this.tableName}__LIST_ALL_IDS`);
		if (allIds.length === 0) return [];

		const chunks: number[][] = commonsArrayChunk<number>(allIds, 500);
		const matches: TPropertyObject = {};
		for (const ids of chunks) {
			if (ids.length === 0) continue;

			const result: TPropertyObject[] = await this.database.queryParams<TPropertyObject>(
					`
						SELECT ${fieldnames.join(',')},
							${this.modelField(this, this.getIdField())}
						FROM ${this.model(this)}
						WHERE ${this.modelField(this, this.getIdField())} IN (${ids.join(',')})
					`,
					undefined,
					results
			);

			for (const row of result) {
				for (const field of fields) {
					if (!commonsTypeHasPropertyString(row, field)) continue;
					const s: string = row[field] as string;
					
					if (!s.toLowerCase().includes(value.toLowerCase())) continue;
					
					matches[row[this.getIdField()]] = row[this.getIdField()];	// eslint-disable-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
				}
			}
		}

		return Object.values(matches);	// eslint-disable-line @typescript-eslint/no-unsafe-return
	}

	public async searchFields(value: string, fields: string[], orderBy?: { [field: string]: ECommonsDatabaseOrderBy }): Promise<ModelI[]> {
		const encrypted: string[] = [];
		const regular: string[] = [];
		
		for (const field of fields) {
			if (!commonsTypeHasPropertyObject(this.structure, field)) throw new Error(`Unknown field to perform search on: ${field}`);

			if (this.structure[field] instanceof CommonsDatabaseTypeEncrypted) {
				encrypted.push(field);
			} else {
				regular.push(field);
			}
		}

		const ids: number[] = [
				...(await this.searchIdsForEncryptedFields(value, encrypted)),
				...(await this.searchIdsForNonencryptedFields(value, regular))
		];

		return await this.bulkGetByIds(commonsArrayUnique<number>(ids), orderBy);
	}

	//-------------------------------------------------------------------------

	public override async insert(_values: TPropertyObject): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		throw new Error('Do not call insert directly, use insertFirstClass instead');
	}

	public async insertFirstClass(values: TPropertyObject): Promise<ModelI> {
		if (commonsTypeHasProperty(values, this.getIdField())) throw new Error('ID fields should not be specified explicitly for FirstClass models');

		const params: TCommonsDatabaseParams = commonsObjectMapObject(
				values,
				(value: any, field: string): CommonsDatabaseParam => {
					if (!commonsTypeHasProperty(this.structure, field)) throw new Error(`Parameter variable is missing in the model structure: ${field}`);
					return new CommonsDatabaseParam(value, this.structure[field]);
				}
		);

		const isTransactionOwner: boolean = await this.database.transactionClaim();

		try {
			const id: number|void = await this.database.insertRow(
					this.tableName,
					params,
					this.getIdField()
			);

			if (id === undefined) throw new Error(`There was an error inserting the data into table: ${this.tableName}`);

			CommonsModel.assertId(id);

			const object: ModelI|undefined = await this.internalFirstClassGetById(id);
			if (object === undefined) throw new Error('Data was not actually inserted into the table');

			CommonsModel.assertIdObject(object, this.getIdField());
			
			if (isTransactionOwner) await this.database.transactionCommit();
			
			return object;
		} catch (e) {
			if (isTransactionOwner) await this.database.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public async delete(object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());

		const conditions: TCommonsDatabaseParams = {};
		conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], this.structure[this.getIdField()]);
		
		await this.database.deleteRowsByConditions(
				this.tableName,
				conditions,
				true
		);
	}

	public async update(object: ModelI): Promise<void> {
		CommonsModel.assertIdObject(object, this.getIdField());

		const values: TCommonsDatabaseParams = {};
		
		for (const field of Object.keys(object)) {
			if (field === this.getIdField()) continue;

			if (!Object.keys(this.structure).includes(field)) throw new Error(`Parameter variable is missing in the model structure: ${field}`);
			values[field] = new CommonsDatabaseParam(object[field], this.structure[field]);
		}
		
		const conditions: TCommonsDatabaseParams = {};
		conditions[this.getIdField()] = new CommonsDatabaseParam(object[this.getIdField()], this.structure[this.getIdField()]);
		
		await this.database.updateRowsByConditions(
				this.tableName,
				values,
				conditions,
				true
		);
	}

	//-------------------------------------------------------------------------

	public async defragment(): Promise<void> {
		// not implemented yet
	}
}
