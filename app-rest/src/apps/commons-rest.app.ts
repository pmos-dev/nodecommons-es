import { TPropertyObject } from 'tscommons-es-core';
import { commonsBase62GenerateRandomId } from 'tscommons-es-core';
import { commonsAsyncTimeout, commonsAsyncAbortTimeout } from 'tscommons-es-async';

import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsApp } from 'nodecommons-es-app';
import { CommonsStrictHttpServer, ICommonsHttpConfig, ICommonsStrictParamsRequest, TCommonsHttpResponse, commonsNginxProxyPath, isICommonsHttpConfig } from 'nodecommons-es-http';

import { TCommonsApiCallback } from '../types/tcommons-api-callback';

export abstract class CommonsRestApp<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ServerT extends CommonsStrictHttpServer<RequestT, ResponseT>
> extends CommonsApp {
	protected httpConfig: ICommonsHttpConfig;
	private httpServer: ServerT;

	private restServer: CommonsRestServer<RequestT, ResponseT>;
	private apis: TCommonsApiCallback<RequestT, ResponseT>[] = [];
	
	private closed: boolean = false;
	private spinningId: string;
	
	constructor(
			name: string,
			httpConfigArea: string,
			configFile?: string,
			configPath?: string,	// NB, a direct path, not the name of the args parameter
			envVar?: string,
			argsKey: string = 'config-path'
	) {
		super(
				name,
				configFile,
				configPath,
				envVar,
				argsKey,
				false
		);
	
		const httpConfig: TPropertyObject = this.getConfigArea(httpConfigArea);
		if (!isICommonsHttpConfig(httpConfig)) throw new Error('Http config is not valid');
		this.httpConfig = httpConfig;
		
		this.httpServer = this.buildHttpServer();
		
		this.restServer = new CommonsRestServer<RequestT, ResponseT>(this.httpServer);
		
		this.spinningId = commonsBase62GenerateRandomId();
	}

	protected abstract buildHttpServer(): ServerT;

	protected getHttpServer(): ServerT {
		return this.httpServer;
	}

	protected getNginxProxyPath(): string {
		return commonsNginxProxyPath(this.httpConfig.path);
	}

	protected async init(): Promise<void> {
		await super.init();
		
		this.httpServer.init();
		
		for (const api of this.apis) api(this.restServer, this.getNginxProxyPath());
	}
	
	public close(): void {
		this.closed = true;
		commonsAsyncAbortTimeout(this.spinningId);
	}
	
	public abort(): void {
		super.abort();
		commonsAsyncAbortTimeout(this.spinningId);
	}
	
	protected listening(): void {
		// can be overridden by subclasses if desired.
		// called when the server is up and ready (e.g. to run Chrome etc.)
	}
	
	protected async run(): Promise<void> {
		await super.run();
		
		await this.httpServer.listen();
		
		this.listening();
		while (!this.closed && !this.isAborted()) {
			
			try {
				await commonsAsyncTimeout(1000, this.spinningId);
			} catch (e) {
				// ignore
			}
		}
	}
	
	protected async shutdown(): Promise<void> {
		this.httpServer.close();

		await super.shutdown();
	}
	
	public install(
			callback: TCommonsApiCallback<RequestT, ResponseT>
	): void {
		this.apis.push(callback);
	}
}
