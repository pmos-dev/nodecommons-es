import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';
import { CommonsRestServer } from 'nodecommons-es-rest';

export type TCommonsApiCallback<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> = (restServer: CommonsRestServer<RequestT, ResponseT>, path: string) => void;
