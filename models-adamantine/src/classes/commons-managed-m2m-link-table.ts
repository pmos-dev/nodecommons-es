import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsM2MLinkTable } from 'nodecommons-es-models';

import { CommonsManageable } from '../interfaces/commons-manageable';

export abstract class CommonsManagedM2MLinkTable<
		M extends ICommonsM2MLink<A, B>,
		A extends ICommonsFirstClass,
		B extends ICommonsFirstClass
> extends CommonsM2MLinkTable<M, A, B> implements CommonsManageable<M> {
	//-------------------------------------------------------------------------

	public isUidSupported(): boolean {
		return false;
	}

	//-------------------------------------------------------------------------

	public isModelManageable(): boolean {
		return true;
	}

	public isAliasingSupported(): boolean {
		return false;
	}

	public getManageableFields(): string[] {
		const internalFields: string[] = [ this.aField, this.bField ];
		
		return [
				...Object.keys(this.structure)
						.filter((field: string): boolean => !internalFields.includes(field))
		];
	}

	public getAccessRequiredToManage(): ECommonsAdamantineAccess {
		return ECommonsAdamantineAccess.FULL;
	}
	
	public getImportExportableFields(): string[] {
		const fields: string[] = this.getManageableFields();
		
		if (this.isUidSupported()) fields.push('uid');
		
		return fields;
	}

	public isObjectViewable(_: M): boolean {
		return true;
	}

	public isObjectEditable(_: M): boolean {
		return true;
	}

	public getFieldDescription(field: string): string|undefined {
		return field;
	}

	public getFieldSuffix(_: string): string|undefined {
		return undefined;
	}

	public getFieldHelper(_: string): string|undefined {
		return undefined;
	}
	
	//-------------------------------------------------------------------------
}
