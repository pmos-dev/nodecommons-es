import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { TCommonsModelFields } from 'nodecommons-es-models';
import { TCommonsModelForeignKeys } from 'nodecommons-es-models';

import { CommonsManageable } from '../interfaces/commons-manageable';

import { CommonsManaged } from './commons-managed';

export abstract class CommonsManagedFirstClass<
		M extends ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
> extends CommonsFirstClass<M> implements CommonsManageable<M> {
	private hasUidSupport: boolean;

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = [],
			addAliasSupport: boolean = false,
			_addUidSupport: boolean = false
	) {
		super(
				database,
				tableName,
				CommonsManaged.extendStructure(
						structure,
						_addUidSupport
				),
				foreignKeys,
				CommonsManaged.extendUniqueKeysForFirstClass(
						tableName,
						uniqueKeys,
						_addUidSupport
				),
				indexKeys
		);
		
		if (addAliasSupport) {
			throw new Error('Alias support is not implemented for managed first class models yet');
		}
		this.hasUidSupport = _addUidSupport;
	}

	//-------------------------------------------------------------------------

	public isUidSupported(): boolean {
		return this.hasUidSupport;
	}

	//-------------------------------------------------------------------------

	public override async createTable(): Promise<void> {
		await super.createTable();
		
		// if (this.aliasModel) await this.aliasModel.createTable();
	}
	
	public override async dropTable(): Promise<void> {
		// if (this.aliasModel) await this.aliasModel.dropTable();

		await super.dropTable();
	}

	//-------------------------------------------------------------------------

	public isModelManageable(): boolean {
		return true;
	}

	public isAliasingSupported(): boolean {
		// return this.aliasModel !== undefined;
		return false;
	}

	public getManageableFields(): string[] {
		const internalFields: string[] = [ 'name', 'id' ];
		if (this.hasUidSupport) internalFields.push('uid');
		
		return [
				'name',
				...Object.keys(this.structure)
						.filter((field: string): boolean => !internalFields.includes(field))
		];
	}

	public getAccessRequiredToManage(): ECommonsAdamantineAccess {
		return ECommonsAdamantineAccess.FULL;
	}
	
	public getImportExportableFields(): string[] {
		const fields: string[] = this.getManageableFields();
		
		if (this.isUidSupported()) fields.push('uid');
		
		return fields;
	}

	public isObjectViewable(_: M): boolean {
		return true;
	}

	public isObjectEditable(_: M): boolean {
		return true;
	}

	public getFieldDescription(field: string): string|undefined {
		return field;
	}

	public getFieldSuffix(_: string): string|undefined {
		return undefined;
	}

	public getFieldHelper(_: string): string|undefined {
		return undefined;
	}
	
	//-------------------------------------------------------------------------

	protected override preprepare(): void {
		super.preprepare();

		CommonsManaged.preprepareForFirstClass<M>(
				this.database,
				this,
				this.hasUidSupport
		);
	}

	//-------------------------------------------------------------------------

	public async getByName(name: string): Promise<M|undefined> {
		return await CommonsManaged.getByName<M>(
				this.database,
				this,
				name
		);
	}

	public async getByUid(uid: string): Promise<M|undefined> {
		if (!this.hasUidSupport) throw new Error('UID support is not available for this model');
		
		return await CommonsManaged.getByUid<M>(
				this.database,
				this,
				uid
		);
	}
}
