import {
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyNumber
} from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_BASE62_ID } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsOrientatedOrdered } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { TCommonsModelFields } from 'nodecommons-es-models';
import { TCommonsModelForeignKeys } from 'nodecommons-es-models';

import { CommonsUniqueNamedHelper } from '../helpers/commons-unique-named';

import { CommonsManagedOrientatedOrdered } from './commons-managed-orientated-ordered';

export interface ICommonsStandardKey<
		P extends ICommonsFirstClass
> extends ICommonsManaged, ICommonsOrientatedOrdered<P> {
		key: string|undefined;
}

export abstract class CommonsStandardKey<
		M extends ICommonsStandardKey<P>,
		P extends ICommonsFirstClass
> extends CommonsManagedOrientatedOrdered<M, P> {
	public static extendStructureStandardKey(structure: TCommonsModelFields): TCommonsModelFields {
		const clone: TCommonsModelFields = Object.assign({}, structure);
		
		if (commonsTypeHasPropertyObject(structure, 'key')) throw new Error('The name field is implicit for managed models and should not be explicitly stated in the structure');
		clone['key'] = new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL);
		
		return clone;
	}

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			firstClass: CommonsFirstClass<P>,
			firstClassField: string,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = []
	) {
		super(
				database,
				tableName,
				CommonsStandardKey.extendStructureStandardKey(structure),
				firstClass,
				firstClassField,
				foreignKeys,
				CommonsUniqueNamedHelper.extendUniqueKeysForFirstClass(	// NB, first not second, as we wan't the key truly unique
						tableName,
						'key',
						uniqueKeys
				),
				indexKeys
		);
	}

	//-------------------------------------------------------------------------

	protected override preprepare(): void {
		super.preprepare();

		CommonsUniqueNamedHelper
				.build<ICommonsStandardKey<P>>(this.database)
				.buildPreprepareForFirstClass(this, 'key', true);
		CommonsUniqueNamedHelper
				.build<ICommonsStandardKey<P>>(this.database)
				.buildPreprepareForSecondClass(this, 'key', true);
	}

	//-------------------------------------------------------------------------

	public override getManageableFields(): string[] {
		return super.getManageableFields()
				.filter((field: string): boolean => field !== 'key');
	}

	//-------------------------------------------------------------------------

	public async getByFirstClassAndKey(firstClass: P, key: string): Promise<M|undefined> {
		if (!commonsTypeHasPropertyNumber(firstClass, this.getFirstClass().getIdField())) throw new Error('Invalid ID object for standard key firstClass');

		if (!COMMONS_REGEX_PATTERN_BASE62_ID.test(key)) throw new Error('Invalid key supplied');

		return await CommonsUniqueNamedHelper
				.build<M>(this.database)
				.getSecondClassByFirstClassAndName(this, firstClass, key, 'key');
	}

	public async getByKey(key: string): Promise<M|undefined> {
		if (!COMMONS_REGEX_PATTERN_BASE62_ID.test(key)) throw new Error('Invalid key supplied');

		return await CommonsUniqueNamedHelper
				.build<M>(this.database)
				.getFirstClassByName(this, key, 'key');
	}

	//-------------------------------------------------------------------------

	public async regenerateKey(object: M): Promise<boolean> {
		if (!commonsTypeHasPropertyNumber(object, this.getIdField())) throw new Error('Invalid ID object for object');

		const TRANSACTION_OWNER: boolean = await this.database.transactionClaim();
		try {
			const key: string = await CommonsUniqueNamedHelper
					.build<M>(this.database)
					.generateUid(this, 'key');

			object.key = key;

			await this.update(object);

			if (TRANSACTION_OWNER) await this.database.transactionCommit();
			return true;
		} catch (e) {
			if (TRANSACTION_OWNER) await this.database.transactionRollback();
			return false;
		}
	}

	public async clearKey(object: M): Promise<void> {
		if (!commonsTypeHasPropertyNumber(object, this.getIdField())) throw new Error('Invalid ID object for object');

		object.key = undefined;

		return await this.update(object);
	}
}
