import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { TCommonsModelFields } from 'nodecommons-es-models';
import { TCommonsModelForeignKeys } from 'nodecommons-es-models';

import { CommonsManageable } from '../interfaces/commons-manageable';

import { CommonsAlias, ICommonsAlias } from './commons-alias';
import { CommonsManaged } from './commons-managed';

export abstract class CommonsManagedSecondClass<
		M extends (ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)) & ICommonsSecondClass<P>,
		P extends ICommonsFirstClass
> extends CommonsSecondClass<M, P> implements CommonsManageable<M> {
	private aliasModel?: CommonsAlias<M>;
	private hasUidSupport: boolean;

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsModelFields,
			firstClass: CommonsFirstClass<P>,
			firstClassField: string,
			foreignKeys: TCommonsModelForeignKeys = {},
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			indexKeys: CommonsDatabaseIndexKey[] = [],
			addAliasSupport: boolean = false,
			_addUidSupport: boolean = false
	) {
		super(
				database,
				tableName,
				CommonsManaged.extendStructure(
						structure,
						_addUidSupport
				),
				firstClass,
				firstClassField,
				foreignKeys,
				CommonsManaged.extendUniqueKeysForSecondClass(
						tableName,
						firstClassField,
						uniqueKeys,
						_addUidSupport
				),
				indexKeys
		);
		
		if (addAliasSupport) this.aliasModel = new CommonsAlias(database, this);
		this.hasUidSupport = _addUidSupport;
	}

	//-------------------------------------------------------------------------

	public isUidSupported(): boolean {
		return this.hasUidSupport;
	}

	//-------------------------------------------------------------------------

	public override async createTable(): Promise<void> {
		await super.createTable();
		
		if (this.aliasModel) await this.aliasModel.createTable();
	}
	
	public override async dropTable(): Promise<void> {
		if (this.aliasModel) await this.aliasModel.dropTable();

		await super.dropTable();
	}

	//-------------------------------------------------------------------------

	public isModelManageable(): boolean {
		return true;
	}

	public isAliasingSupported(): boolean {
		return this.aliasModel !== undefined;
	}

	public getManageableFields(): string[] {
		const internalFields: string[] = [ 'name', 'id', this.firstClassField ];
		if (this.hasUidSupport) internalFields.push('uid');

		return [
				'name',
				...Object.keys(this.structure)
						.filter((field: string): boolean => !internalFields.includes(field))
		];
	}

	public getAccessRequiredToManage(): ECommonsAdamantineAccess {
		return ECommonsAdamantineAccess.FULL;
	}
	
	public getImportExportableFields(): string[] {
		const fields: string[] = this.getManageableFields();
		
		if (this.isUidSupported()) fields.push('uid');
		
		return fields;
	}

	public isObjectViewable(_: M): boolean {
		return true;
	}

	public isObjectEditable(_: M): boolean {
		return true;
	}

	public getFieldDescription(field: string): string|undefined {
		return field;
	}

	public getFieldSuffix(_: string): string|undefined {
		return undefined;
	}

	public getFieldHelper(_: string): string|undefined {
		return undefined;
	}
	
	//-------------------------------------------------------------------------

	protected override preprepare(): void {
		super.preprepare();

		CommonsManaged.preprepareForSecondClass<M, P>(
				this.database,
				this,
				this.hasUidSupport
		);
	}

	//-------------------------------------------------------------------------

	public async getByName(firstClass: P, name: string): Promise<M|undefined> {
		return await CommonsManaged.getByFirstClassAndName<M, P>(
				this.database,
				this,
				firstClass,
				name
		);
	}

	public async getByUid(uid: string): Promise<M|undefined> {
		if (!this.hasUidSupport) throw new Error('UID support is not available for this model');
		
		return await CommonsManaged.getByUid<M>(
				this.database,
				this,
				uid
		);
	}
	
	public async listByAlias(firstClass: P, name: string): Promise<M[]> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.listByFirstClassAndAlias<M, P>(
				this.aliasModel,
				firstClass,
				name
		);
	}

	public async searchByNameOrAlias(firstClass: P, name: string): Promise<M[]> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.searchByNameOrAlias<M, P>(
				this.database,
				this,
				this.aliasModel,
				firstClass,
				name
		);
	}

	//-------------------------------------------------------------------------

	public async listAliasesByRow(row: M): Promise<string[]> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.listAliasesByRow<M, P>(
				this,
				this.aliasModel,
				row
		);
	}

	//-------------------------------------------------------------------------

	public async addAlias(row: M, name: string): Promise<ICommonsAlias<M>> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.addAlias<M, P>(
				this,
				this.aliasModel,
				row,
				name
		);
	}

	public async removeAlias(row: M, name: string): Promise<boolean> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.removeAlias<M, P>(
				this.database,
				this,
				this.aliasModel,
				row,
				name
		);
	}

	public async replaceAliases(row: M, names: string[]): Promise<boolean> {
		if (!this.aliasModel) throw new Error('Aliasing is not supported for this managed model');

		return await CommonsManaged.replaceAliases<M, P>(
				this.database,
				this,
				this.aliasModel,
				row,
				names
		);
	}
}
