import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniqueNamed } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseTypeId } from 'nodecommons-es-database';
import { CommonsDatabaseTypeIdName } from 'nodecommons-es-database';
import { CommonsDatabaseTypeString } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';

import { CommonsUniqueNamedHelper } from '../helpers/commons-unique-named';

export interface ICommonsAlias<
		P extends ICommonsFirstClass
> extends ICommonsSecondClass<P>, ICommonsUniqueNamed {
		name: string;
}

export class CommonsAlias<
		P extends ICommonsFirstClass & ICommonsUniqueNamed
> extends CommonsSecondClass<ICommonsAlias<P>, P> {
	protected uniquenamed: CommonsFirstClass<P>;

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			uniquenamed: CommonsFirstClass<P>
	) {
		super(
				database,
				`${uniquenamed.getTable()}__alias`,
				{
						name: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				uniquenamed,
				'uniquenamed',
				{},
				[
						new CommonsDatabaseUniqueKey([ 'uniquenamed', 'name' ], `${uniquenamed.getTable()}__alias_ukname`)
				]
		);
		
		this.uniquenamed = uniquenamed;
	}

	//-------------------------------------------------------------------------

	protected override defineViews(): void {
		super.defineViews();

		if (this.uniquenamed instanceof CommonsSecondClass) {
			const fields: string = Object.keys(this.uniquenamed.getStructure())
					.map((field: string): string => this.modelViewField(this.uniquenamed, 'SECONDCLASS_JOIN', field))
					.join(',');

			this.defineView(
					'ALIAS_JOIN',
					`
						SELECT ${fields},
							${this.modelViewField(this, 'SECONDCLASS_JOIN', 'name')} AS ${this.tempField('alias')},
							${this.modelViewField(this.uniquenamed, 'SECONDCLASS_JOIN', 'firstclassId')} AS ${this.tempField('grandparentId')}
						FROM ${this.modelView(this, 'SECONDCLASS_JOIN')}
						INNER JOIN${this.modelView(this.uniquenamed, 'SECONDCLASS_JOIN')}
							ON ${this.modelViewField(this, 'SECONDCLASS_JOIN', 'firstclassId')}=${this.modelViewField(this.uniquenamed, 'SECONDCLASS_JOIN', this.uniquenamed.getIdField())}
					`
			);
		}
	}

	protected override preprepare(): void {
		super.preprepare();

		if (this.uniquenamed instanceof CommonsSecondClass) {
			const fields: string = Object.keys(this.uniquenamed.getStructure())
					.map((field: string): string => this.modelViewField(this, 'ALIAS_JOIN', field))
					.join(',');

			const results: TCommonsDatabaseTypes = this.uniquenamed.getStructure();
			results.alias = new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL);

			this.database.preprepare(
					`MODELS_ALIAS__${this.tableName}__LIST_BY_GRANDPARENT_AND_NAME`,
					`
							SELECT ${fields}, ${this.modelViewField(this, 'ALIAS_JOIN', 'alias')}
							FROM ${this.modelView(this, 'ALIAS_JOIN')}
							WHERE ${this.modelViewField(this, 'ALIAS_JOIN', 'grandparentId')}=:grandparent
								AND ${this.modelViewField(this, 'ALIAS_JOIN', 'alias')}=:name
					`,
					{
							grandparent: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
							name: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL)
					},
					results
			);
		} else {
			const fields: string = Object.keys(this.uniquenamed.getStructure())
					.map((field: string): string => this.modelField(this.uniquenamed, field))
					.join(',');

			const results: TCommonsDatabaseTypes = this.uniquenamed.getStructure();
			results.alias = new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL);

			this.database.preprepare(
					`MODELS_ALIAS__${this.tableName}__LIST_BY_NAME`,
					`
							SELECT ${fields}, ${this.modelField(this, 'name')} AS ${this.tempField('alias')}
							FROM ${this.model(this)}
							INNER JOIN ${this.model(this.uniquenamed)}
								ON ${this.modelField(this, 'uniquenamed')}=${this.modelField(this.uniquenamed, this.uniquenamed.getIdField())}
							WHERE ${this.modelField(this, 'name')}=:name
					`,
					{
							name: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL)
					},
					results
			);
		}

		CommonsUniqueNamedHelper
				.build<ICommonsAlias<P>>(this.database)
				.buildPreprepareForSecondClass(this);
	}

	//-------------------------------------------------------------------------

	public async getByName(uniquenamed: P, name: string): Promise<ICommonsAlias<P>|undefined> {
		return await CommonsUniqueNamedHelper
				.build<ICommonsAlias<P>>(this.database)
				.getSecondClassByFirstClassAndName(this, uniquenamed, name);
	}

	public async listByName(name: string): Promise<P[]> {
		if (!(this.uniquenamed instanceof CommonsFirstClass)) throw new Error('Call to listByName on a SecondClass model. This cannot be done. Use listByGrandparentAndName instead.');

		return await this.database.executeParams<P>(
				`MODELS_ALIAS__${this.tableName}__LIST_BY_NAME`,
				{
						name: name
				}
		);
	}

	public async listByGrandparentAndName(grandparent: ICommonsFirstClass, name: string): Promise<P[]> {
		if (!(this.uniquenamed instanceof CommonsSecondClass)) throw new Error('Call to listByGrandparentAndName on a FirstClass model. This cannot be done. Use listByName instead.');

		if (!commonsTypeHasPropertyNumber(grandparent, this.uniquenamed.getFirstClass().getIdField())) throw new Error('Invalid ID object for grandparent');

		return await this.database.executeParams<P>(
				`MODELS_ALIAS__${this.tableName}__LIST_BY_GRANDPARENT_AND_NAME`,
				{
						grandparent: grandparent[this.uniquenamed.getFirstClass().getIdField()] as number,
						name: name
				}
		);
	}

	public async searchByName(name: string): Promise<P[]> {
		if (!(this.uniquenamed instanceof CommonsFirstClass)) throw new Error('Call to searchByName on a SecondClass model. This cannot be done. Use searchByGrandparentAndName instead.');

		const fields: string = Object.keys(this.uniquenamed.getStructure())
				.map((field: string): string => this.modelField(this.uniquenamed, field))
				.join(',');

		const results: TCommonsDatabaseTypes = this.uniquenamed.getStructure();
		results.alias = new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL);

		return await this.database.queryParams<P>(
				`
					SELECT ${fields}, ${this.modelField(this, 'name')} AS ${this.tempField('alias')}
					FROM ${this.model(this)}
					INNER JOIN ${this.model(this.uniquenamed)}
						ON ${this.modelField(this, 'uniquenamed')}=${this.modelField(this.uniquenamed, this.uniquenamed.getIdField())}
					WHERE (
						${this.modelField(this, 'name')} LIKE :valuea
						OR
						${this.modelField(this, 'name')} LIKE :valueb
						OR
						${this.modelField(this, 'name')} LIKE :valuec
						OR
						${this.modelField(this, 'name')} LIKE :valued
					)
				`,
				{
						valuea: new CommonsDatabaseParam(name, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valueb: new CommonsDatabaseParam(`${name}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valuec: new CommonsDatabaseParam(`%${name}`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valued: new CommonsDatabaseParam(`%${name}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL))
				},
				results
		);
	}

	public async searchByGrandparentAndName(
			grandparent: ICommonsFirstClass,
			name: string
	): Promise<P[]> {
		if (!(this.uniquenamed instanceof CommonsSecondClass)) throw new Error('Call to searchByGrandparentAndName on a FirstClass model. This cannot be done. Use searchByName instead.');

		if (!commonsTypeHasPropertyNumber(grandparent, this.uniquenamed.getFirstClass().getIdField())) throw new Error('Invalid ID object for grandparent');

		const fields: string = Object.keys(this.uniquenamed.getStructure())
				.map((field: string): string => this.modelViewField(this, 'ALIAS_JOIN', field))
				.join(',');

		const results: TCommonsDatabaseTypes = this.uniquenamed.getStructure();
		results.alias = new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL);

		return await this.database.queryParams<P>(
				`
					SELECT ${fields}, ${this.modelViewField(this, 'ALIAS_JOIN', 'alias')}
					FROM ${this.modelView(this, 'ALIAS_JOIN')}
					WHERE ${this.modelViewField(this, 'ALIAS_JOIN', 'grandparentId')}=:grandparent
						AND (
							${this.modelViewField(this, 'ALIAS_JOIN', 'alias')} LIKE :valuea
							OR
							${this.modelViewField(this, 'ALIAS_JOIN', 'alias')} LIKE :valueb
							OR
							${this.modelViewField(this, 'ALIAS_JOIN', 'alias')} LIKE :valuec
							OR
							${this.modelViewField(this, 'ALIAS_JOIN', 'alias')} LIKE :valued
						)
				`,
				{
						grandparent: new CommonsDatabaseParam(grandparent[this.uniquenamed.getFirstClass().getIdField()], new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)),
						valuea: new CommonsDatabaseParam(name, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valueb: new CommonsDatabaseParam(`${name}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valuec: new CommonsDatabaseParam(`%${name}`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)),
						valued: new CommonsDatabaseParam(`%${name}%`, new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL))
				},
				results
		);
	}
}
