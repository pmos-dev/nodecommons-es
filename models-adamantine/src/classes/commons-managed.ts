import {
		commonsTypeHasPropertyObject,
		commonsTypeHasPropertyNumber,
		commonsArrayUnique,
		commonsArrayRemoveUndefineds
} from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseTypeIdName } from 'nodecommons-es-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { TCommonsModelFields } from 'nodecommons-es-models';

import { CommonsUniqueNamedHelper } from '../helpers/commons-unique-named';

import { CommonsAlias, ICommonsAlias } from './commons-alias';

// This is a helper method class

export abstract class CommonsManaged {
	public static extendStructure(
			structure: TCommonsModelFields,
			addUidSupport: boolean
	): TCommonsModelFields {
		const clone: TCommonsModelFields = Object.assign({}, structure);
		
		if (commonsTypeHasPropertyObject(structure, 'name')) throw new Error('The name field is implicit for managed models and should not be explicitly stated in the structure');
		clone['name'] = new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL);
		
		if (addUidSupport) {
			if (commonsTypeHasPropertyObject(structure, 'uid')) throw new Error('The uid field is being added automatically for this managed model and should not be explicitly stated in the structure');
			clone['uid'] = new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL);
		}
		
		return clone;
	}

	public static extendUniqueKeysForFirstClass(
			tableName: string,
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			addUidSupport: boolean
	): CommonsDatabaseUniqueKey[] {
		let extended: CommonsDatabaseUniqueKey[] = CommonsUniqueNamedHelper.extendUniqueKeysForFirstClass(
				tableName,
				'name',
				uniqueKeys
		);
		
		if (addUidSupport) {
			// uids are supposed to be unique across their first-class context too, not just second class
			extended = CommonsUniqueNamedHelper.extendUniqueKeysForFirstClass(
					tableName,
					'uid',
					uniqueKeys
			);
		}
		
		return extended;
	}

	public static extendUniqueKeysForSecondClass(
			tableName: string,
			firstClassField: string,
			uniqueKeys: CommonsDatabaseUniqueKey[] = [],
			addUidSupport: boolean
	): CommonsDatabaseUniqueKey[] {
		let extended: CommonsDatabaseUniqueKey[] = CommonsUniqueNamedHelper.extendUniqueKeysForSecondClass(
				tableName,
				'name',
				firstClassField,
				uniqueKeys
		);
		
		if (addUidSupport) {
			// uids are supposed to be unique across their first-class context too, not just second class
			extended = CommonsUniqueNamedHelper.extendUniqueKeysForFirstClass(
					tableName,
					'uid',
					uniqueKeys
			);
		}
		
		return extended;
	}

	//-------------------------------------------------------------------------

	public static preprepareForFirstClass<M extends ICommonsManaged>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsFirstClass<M>,
			hasUidSupport: boolean
	): void {
		CommonsUniqueNamedHelper
				.build<M>(database)
				.buildPreprepareForFirstClass(model);
		
		if (hasUidSupport) {
			CommonsUniqueNamedHelper
					.build<M>(database)
					.buildPreprepareForFirstClass(
							model,
							'uid',
							true
					);
			
		}
	}

	public static preprepareForSecondClass<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsSecondClass<M, P>,
			hasUidSupport: boolean
	): void {
		CommonsUniqueNamedHelper
				.build<M>(database)
				.buildPreprepareForSecondClass(model);
		
		if (hasUidSupport) {
			CommonsUniqueNamedHelper
					.build<M>(database)
					.buildPreprepareForFirstClass(	// uids are unique, so ForFirstClass is ok
							model,
							'uid',
							true
					);
			
		}
	}
	
	//-------------------------------------------------------------------------

	public static async getByName<M extends ICommonsManaged>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsFirstClass<M>,
			name: string
	): Promise<M|undefined> {
		return await CommonsUniqueNamedHelper
				.build<M>(database)
				.getFirstClassByName(model, name);
	}

	public static async getByFirstClassAndName<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsSecondClass<M, P>,
			firstClass: P,
			name: string
	): Promise<M|undefined> {
		return await CommonsUniqueNamedHelper
				.build<M>(database)
				.getSecondClassByFirstClassAndName(model, firstClass, name);
	}

	public static async getByUid<M extends ICommonsManaged>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsFirstClass<M>,
			uid: string
	): Promise<M|undefined> {
		return await CommonsUniqueNamedHelper
				.build<M>(database)
				.getFirstClassByName(model, uid, 'uid');
	}
	
	public static async listByFirstClassAndAlias<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			aliasModel: CommonsAlias<M>,
			firstClass: P,
			name: string
	): Promise<M[]> {
		return await aliasModel.listByGrandparentAndName(firstClass, name);
	}

	public static async searchByNameOrAlias<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsSecondClass<M, P>,
			aliasModel: CommonsAlias<M>,
			firstClass: P,
			name: string
	): Promise<M[]> {
		const results: (M|undefined)[] = [
				await CommonsManaged.getByFirstClassAndName<M, P>(database, model, firstClass, name),
				...(await aliasModel.listByGrandparentAndName(firstClass, name)),
				...(await model.searchFieldsByFirstClass(firstClass, name, [ 'name' ])),
				...(await aliasModel.searchByGrandparentAndName(firstClass, name))
		];

		return commonsArrayUnique<M>(
				commonsArrayRemoveUndefineds<M>(results),
				(_m: unknown): _m is M => true,
				(
						a: M,
						b: M
				): boolean => a.id === b.id
		);
	}

	//-------------------------------------------------------------------------

	 public static async listAliasesByRow<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			model: CommonsSecondClass<M, P>,
			aliasModel: CommonsAlias<M>,
			row: M
	): Promise<string[]> {
		if (!commonsTypeHasPropertyNumber(row, model.getIdField())) throw new Error('Invalid ID object for row');

		return (await aliasModel.listByFirstClass(row))
				.map((alias: ICommonsAlias<M>): string => alias.name);
	}

	//-------------------------------------------------------------------------

	public static async addAlias<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			model: CommonsSecondClass<M, P>,
			aliasModel: CommonsAlias<M>,
			row: M,
			name: string
	): Promise<ICommonsAlias<M>> {
		if (!commonsTypeHasPropertyNumber(row, model.getIdField())) throw new Error('Invalid ID object for row');

		return await aliasModel.insertForFirstClass(
				row,
				{
						name: name
				}
		);
	}

	public static async removeAlias<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsSecondClass<M, P>,
			aliasModel: CommonsAlias<M>,
			row: M,
			name: string
	): Promise<boolean> {
		if (!commonsTypeHasPropertyNumber(row, model.getIdField())) throw new Error('Invalid ID object for row');

		const TRANSACTION_OWNER: boolean = await database.transactionClaim();
		try {
			const existing: ICommonsAlias<M>|undefined = await aliasModel.getByName(row, name);
			if (!existing) throw new Error('No such alias by that name exists for this row');
	
			await aliasModel.deleteForFirstClass(row, existing);
	
			if (TRANSACTION_OWNER) await database.transactionCommit();
			return true;
		} catch (e) {
			if (TRANSACTION_OWNER) await database.transactionRollback();
			return false;
		}
	}

	public static async replaceAliases<
			M extends ICommonsManaged,
			P extends ICommonsFirstClass
	>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			model: CommonsSecondClass<M, P>,
			aliasModel: CommonsAlias<M>,
			row: M,
			names: string[]
	): Promise<boolean> {
		if (!commonsTypeHasPropertyNumber(row, model.getIdField())) throw new Error('Invalid ID object for row');

		const TRANSACTION_OWNER: boolean = await database.transactionClaim();
		try {
			await aliasModel.deleteAllForFirstClass(row);
	
			for (const name of names) {
				await CommonsManaged.addAlias(
						model,
						aliasModel,
						row,
						name
				);
			}
	
			if (TRANSACTION_OWNER) await database.transactionCommit();
			return true;
		} catch (e) {
			if (TRANSACTION_OWNER) await database.transactionRollback();
			return false;
		}
	}
}
