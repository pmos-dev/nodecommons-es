import { ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsModelable } from 'nodecommons-es-models';

// this is a true class interface, not a data structure

// tslint:disable-next-line:interface-name
export interface CommonsManageable<
		M extends ICommonsManaged|ICommonsM2MLink<A, B>,
		
		// these have to have generic defaults as they only are used and apply to the M2MLink, not Managed
		A extends ICommonsFirstClass = ICommonsFirstClass,
		B extends ICommonsFirstClass = ICommonsFirstClass
> extends CommonsModelable<M> {
	isModelManageable(): boolean;
	isAliasingSupported(): boolean;
	isUidSupported(): boolean;
	getManageableFields(): string[];
	getAccessRequiredToManage(): ECommonsAdamantineAccess;
	getImportExportableFields(): string[];
	getFieldDescription(field: string): string|undefined;
	getFieldSuffix(field: string): string|undefined;
	getFieldHelper(field: string): string|undefined;
}
