import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';
import { ICommonsAdamantineManagedSecondClassUser } from 'tscommons-es-models-adamantine';

import { ICommonsUserPwHash } from 'nodecommons-es-security';

// this is a true class interface, not a data structure

// tslint:disable-next-line:interface-name
export interface CommonsManagedFirstClassUserModel<
		M extends ICommonsAdamantineManagedFirstClassUser & ICommonsUserPwHash
> {
	getUserByUsername(
			username: string
	): Promise<M|undefined>;
}

// this is a true class interface, not a data structure

// tslint:disable-next-line:interface-name
export interface CommonsManagedSecondClassUserModel<
		M extends ICommonsAdamantineManagedSecondClassUser<P> & ICommonsUserPwHash,
		P extends ICommonsFirstClass
> {
	getUserByUsername(
			firstClass: P,
			username: string
	): Promise<M|undefined>;
}
