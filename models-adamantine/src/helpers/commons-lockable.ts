import {
		commonsTypeHasPropertyNumber,
		commonsBase62GenerateRandomId
} from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';
import { ICommonsFirstClass } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseForeignKey } from 'nodecommons-es-database';
import {
		CommonsDatabaseTypeMd5,
		CommonsDatabaseTypeId,
		CommonsDatabaseTypeDateTime
} from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { TCommonsDatabaseForeignKeys } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull, ECommonsDatabaseReferentialAction, ECommonsDatabaseEngine } from 'nodecommons-es-database';
import { CommonsModel } from 'nodecommons-es-models';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { commonsHashMd5 } from 'nodecommons-es-security';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICommonsLockable extends ICommonsModel {}

type TLocked = {
		lockHash: string|undefined;
		lockUser: number|undefined;
		lockTimestamp: Date|undefined;
		lastModified: Date|undefined;
};

export class CommonsLockableHelper<M extends ICommonsLockable, U extends CommonsFirstClass<ICommonsFirstClass>> extends CommonsModel<M> {
	
	public static build<M extends ICommonsLockable, U extends CommonsFirstClass<ICommonsFirstClass>>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>
	): CommonsLockableHelper<M, U> {
		return new CommonsLockableHelper<M, U>(database);
	}
	
	public static extendStructure(
			structure: TCommonsDatabaseTypes,
			hashField: string = 'lockHash',
			userField: string = 'lockUser',
			timestampField: string = 'lockTimestamp',
			modifiedField: string = 'lastModified'
	): TCommonsDatabaseTypes {
		const clone: TCommonsDatabaseTypes = Object.assign({}, structure);
		
		clone[hashField] = new CommonsDatabaseTypeMd5(ECommonsDatabaseTypeNull.ALLOW_NULL);
		clone[userField] = new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL);
		clone[timestampField] = new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.ALLOW_NULL);
		clone[modifiedField] = new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL);
		
		return clone;
	}
	
	public static extendForeignKeys<U extends CommonsFirstClass<ICommonsFirstClass>>(
			foreignKeys: TCommonsDatabaseForeignKeys,
			user: U,
			foreignKeyName: string,
			userField: string = 'lockUser'
	): TCommonsDatabaseForeignKeys {
		const clone: TCommonsDatabaseForeignKeys = Object.assign({}, foreignKeys);
		
		clone[userField] = new CommonsDatabaseForeignKey(
				user.getTable(),
				user.getIdField(),
				ECommonsDatabaseReferentialAction.SET_NULL,
				ECommonsDatabaseReferentialAction.CASCADE,
				foreignKeyName
		);
		
		return clone;
	}
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>
	) {
		super(database, '', {});	// only to get access to the modelField methods etc.
	}

	public buildPreprepareForFirstClass(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			hashField: string = 'lockHash',
			userField: string = 'lockUser',
			timestampField: string = 'lockTimestamp',
			modifiedField: string = 'lastModified'
	): void {
		this.database.preprepare(
				`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__GET_LOCK`,
				`
					SELECT ${this.modelField(firstClass, hashField)} AS ${this.tempField('lockHash')},
						${this.modelField(firstClass, userField)} AS ${this.tempField('lockUser')},
						${this.modelField(firstClass, timestampField)} AS ${this.tempField('lockTimestamp')},
						${this.modelField(firstClass, modifiedField)} AS ${this.tempField('lastModified')}
					FROM ${this.model(firstClass)}
					WHERE ${this.modelField(firstClass, firstClass.getIdField())}=:id
					LIMIT 1
					FOR UPDATE
				`,
				{
						id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				{
						lockHash: new CommonsDatabaseTypeMd5(ECommonsDatabaseTypeNull.ALLOW_NULL),
						lockUser: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
						lockTimestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.ALLOW_NULL),
						lastModified: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		switch (this.database.getEngine()) {
			case ECommonsDatabaseEngine.MYSQL:
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_LOCK`,
						`
							UPDATE ${this.model(firstClass)}
							SET ${this.modelField(firstClass, hashField)} = :hash,
								${this.modelField(firstClass, timestampField)} = :timestamp,
								${this.modelField(firstClass, userField)} = :user
							WHERE ${this.modelField(firstClass, firstClass.getIdField())} = :id
						`,
						{
								hash: new CommonsDatabaseTypeMd5(ECommonsDatabaseTypeNull.ALLOW_NULL),
								timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.ALLOW_NULL),
								user: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
								id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_MODIFIED`,
						`
							UPDATE ${this.model(firstClass)}
							SET ${this.modelField(firstClass, modifiedField)} = :modified
							WHERE ${this.modelField(firstClass, firstClass.getIdField())} = :id
						`,
						{
								modified: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
								id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__CLEAN_EXPIRED_LOCKS`,
						`
							UPDATE ${this.model(firstClass)}
							SET ${this.modelField(firstClass, hashField)} = undefined,
								${this.modelField(firstClass, timestampField)} = undefined,
								${this.modelField(firstClass, userField)} = undefined
							WHERE ${this.modelField(firstClass, timestampField)} < :timestamp
						`,
						{
								timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				break;
			case ECommonsDatabaseEngine.POSTGRES:
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_LOCK`,
						`
							UPDATE ${this.model(firstClass)}
							SET ' . this.database.delimit(hashField) . ' = :hash,
								' . this.database.delimit(timestampField) . ' = :timestamp,
								' . this.database.delimit(userField) . ' = :user
							WHERE ${this.modelField(firstClass, firstClass.getIdField())} = :id
						`,
						{
								hash: new CommonsDatabaseTypeMd5(ECommonsDatabaseTypeNull.ALLOW_NULL),
								timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.ALLOW_NULL),
								user: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
								id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_MODIFIED`,
						`
							UPDATE ${this.model(firstClass)}
							SET ' . this.database.delimit(modifiedField) . ' = :modified
							WHERE ${this.modelField(firstClass, firstClass.getIdField())} = :id
						`,
						{
								modified: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
								id: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				this.database.preprepare(
						`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__CLEAN_EXPIRED_LOCKS`,
						`
							UPDATE ${this.model(firstClass)}
							SET ' . this.database.delimit(hashField) . ' = undefined,
								' . this.database.delimit(timestampField) . ' = undefined,
								' . this.database.delimit(userField) . ' = undefined
							WHERE ${this.modelField(firstClass, timestampField)} < :timestamp
						`,
						{
								timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL)
						}
				);
				break;
			default:
				throw new Error('Database type is not supported');
		}
	}

	//-------------------------------------------------------------------------
	
	public async cleanExpiredLocks(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			seconds: number
	): Promise<void> {
		await this.database.executeParams(
				`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__CLEAN_EXPIRED_LOCKS`,
				{
						timestamp: new Date(new Date().getTime() - (seconds * 1000))
				}
		);
	}
	
	public async claimLock(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			lockable: M & ICommonsFirstClass & ICommonsLockable,
			user: U,
			locker: ICommonsFirstClass,
			seconds: number
	): Promise<boolean|string> {
		if (!commonsTypeHasPropertyNumber(lockable, firstClass.getIdField())) throw new Error('Invalid lockable object');
		if (!commonsTypeHasPropertyNumber(locker, user.getIdField())) throw new Error('Invalid locker object');
	
		const TRANSACTION_OWNER: boolean = await this.database.transactionClaim();
		try {
			await this.cleanExpiredLocks(firstClass, seconds);
			
			const lock: TLocked|undefined = await this.database.executeParamsSingle<TLocked>(
					`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__GET_LOCK`,
					{
							id: lockable[firstClass.getIdField()] as number
					}
			);
			
			if (!lock) throw new Error('Attempt to claim event row lock returned undefined -- this should not occur');
		
			if (lock.lockUser !== undefined) {
				if (!lock.lockTimestamp) throw new Error('Hashed lock timestamp is undefined -- this should not be possible');
				
				const delta: number = new Date().getTime() - lock.lockTimestamp.getTime();
					
				if (delta < (seconds * 1000)) {
					if (TRANSACTION_OWNER) await this.database.transactionCommit();
					return lock.lockUser === locker[user.getIdField()];	// true for this user, false for another
				}
			}
	
			const hash: string = commonsHashMd5(commonsBase62GenerateRandomId());
			
			await this.database.executeParams(
					`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_LOCK`,
					{
							id: lockable[firstClass.getIdField()] as number,
							hash: hash,
							timestamp: new Date(),
							user: locker[user.getIdField()] as number
					}
			);
			
			if (TRANSACTION_OWNER) await this.database.transactionCommit();
		
			return hash;
		} catch (e) {
			if (TRANSACTION_OWNER) await this.database.transactionRollback();
			throw e;
		}
	}
	
	public validateLock(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			lockable: M & ICommonsFirstClass & ICommonsLockable,
			hash: string,
			hashField: string = 'lockHash'
	): boolean {
		if (!commonsTypeHasPropertyNumber(lockable, firstClass.getIdField())) throw new Error('Invalid lockable object');
	
		return lockable[hashField] === hash;
	}

	public async releaseLock(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			lockable: M & ICommonsFirstClass & ICommonsLockable,
			hash: string,
			seconds: number
	): Promise<void> {
		if (!commonsTypeHasPropertyNumber(lockable, firstClass.getIdField())) throw new Error('Invalid lockable object');
	
		const TRANSACTION_OWNER: boolean = await this.database.transactionClaim();
		try {
			await this.cleanExpiredLocks(firstClass, seconds);
			
			const lock: TLocked|undefined = await this.database.executeParamsSingle<TLocked>(
					`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__GET_LOCK`,
					{
							id: lockable[firstClass.getIdField()] as number
					}
			);
		
			if (!lock) throw new Error('Attempt to claim event row lock returned undefined -- this should not occur');
		
			if (lock.lockUser === undefined) {
				// lock has probably been purged
				if (TRANSACTION_OWNER) await this.database.transactionCommit();
				return;
			}
		
			if (lock.lockHash !== hash) throw new Error('Hashed event lock does not match expected value -- this should not occur');

			await this.database.executeParams(
					`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_LOCK`,
					{
							id: lockable[firstClass.getIdField()] as number,
							hash: undefined,
							timestamp: undefined,
							user: undefined
					}
			);
				
			if (TRANSACTION_OWNER) await this.database.transactionCommit();
		} catch (e) {
			if (TRANSACTION_OWNER) await this.database.transactionRollback();
			throw e;
		}
	}
	
	public async touchModified(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsLockable>,
			lockable: M & ICommonsFirstClass & ICommonsLockable
	): Promise<void> {
		if (!commonsTypeHasPropertyNumber(lockable, firstClass.getIdField())) throw new Error('Invalid lockable object');
	
		await this.database.executeParams(
				`COMMONS_LOCKABLE_FIRSTCLASS__${firstClass.getTable()}__SET_MODIFIED`,
				{
						id: lockable[firstClass.getIdField()] as number,
						modified: new Date()
				}
		);
	}
	
	public isModifiedSince(
			lockable: M & ICommonsFirstClass & ICommonsLockable,
			timestamp: Date,
			modifiedField: string = 'lastModified'
	): boolean {
		return (lockable[modifiedField] as Date).getTime() > timestamp.getTime();
	}
}
