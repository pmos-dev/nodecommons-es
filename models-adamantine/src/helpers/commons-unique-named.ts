import {
		commonsTypeHasPropertyNumber,
		commonsBase62GenerateRandomId
} from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_ID_NAME } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsUniqueNamed } from 'tscommons-es-models-adamantine';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import {
		CommonsDatabaseTypeId,
		CommonsDatabaseTypeIdName
} from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseEngine } from 'nodecommons-es-database';
import { CommonsModel } from 'nodecommons-es-models';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';

export class CommonsUniqueNamedHelper<
		M extends ICommonsUniqueNamed,
		
		// this is optional as it is only used by secondclass
		P extends ICommonsFirstClass = ICommonsFirstClass
> extends CommonsModel<M> {
	
	public static build<M extends ICommonsUniqueNamed>(
			database: CommonsSqlDatabaseService<ICommonsCredentials>
	): CommonsUniqueNamedHelper<M> {
		return new CommonsUniqueNamedHelper<M>(database);
	}
	
	public static extendUniqueKeysForFirstClass(
			firstClassTable: string,
			uniqueField: string,
			uniqueKeys: CommonsDatabaseUniqueKey[] = []
	): CommonsDatabaseUniqueKey[] {
		return [
				...uniqueKeys,
				new CommonsDatabaseUniqueKey([ uniqueField ], `${firstClassTable}__unique__${uniqueField}`)
		];
	}

	public static extendUniqueKeysForSecondClass(
			secondClassTable: string,
			uniqueField: string,
			firstClassField: string,
			uniqueKeys: CommonsDatabaseUniqueKey[] = []
	): CommonsDatabaseUniqueKey[] {
		return [
				...uniqueKeys,
				new CommonsDatabaseUniqueKey(
						[
								firstClassField,
								uniqueField
						],
						`${secondClassTable}__unique__${firstClassField}_${uniqueField}`
				)
		];
	}

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>
	) {
		super(database, '', {});	// only to get access to the modelField methods etc.
	}
	
	public buildPreprepareForFirstClass(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsUniqueNamed>,
			namefield: string = 'name',
			caseSensitive: boolean = false
	): void {
		// MySQL does case insensitive varchar string comparisons.
		// Postgres does case sensitive
		// 2do: check for SQLite.
		const cs: string = (caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.MYSQL) ? 'BINARY ' : '';
		const lowerOpen: string = (!caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.POSTGRES) ? 'LOWER(' : '';
		const lowerClose: string = (!caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.POSTGRES) ? ')' : '';
		
		this.database.preprepare(
				`COMMONS_UNIQUENAMED_FIRSTCLASS__${firstClass.getTable()}__GET_BY_NAME__${namefield}`,
				`
					SELECT ${this.modelFieldCsv(firstClass)}
					FROM ${this.model(firstClass)}
					WHERE ${lowerOpen}${this.modelField(firstClass, namefield)}${lowerClose}=${lowerOpen}${cs}:name${lowerClose}
				`,
				{
						name: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				firstClass.getStructure()
		);

		this.database.preprepare(
				`COMMONS_UNIQUENAMED_FIRSTCLASS__${firstClass.getTable()}__LIST_ORDER_BY_NAME__${namefield}`,
				`
					SELECT ${this.modelFieldCsv(firstClass)}
					FROM ${this.model(firstClass)}
					ORDER BY ${this.modelField(firstClass, namefield)} ASC
				`,
				undefined,
				firstClass.getStructure()
		);
	}
	
	public async getFirstClassByName(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsUniqueNamed>,
			name: string,
			namefield: string = 'name'
	): Promise<M|undefined> {
		if (!name.match(COMMONS_REGEX_PATTERN_ID_NAME)) throw new Error(`Invalid value for name ${name}`);
		
		return await this.database.executeParamsSingle<M>(
				`COMMONS_UNIQUENAMED_FIRSTCLASS__${firstClass.getTable()}__GET_BY_NAME__${namefield}`,
				{
						name: name
				}
		 );
	}

	public async listFirstClassOrderByName(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsUniqueNamed>,
			namefield: string = 'name'
	): Promise<M[]> {
		return await this.database.executeParams<M>(
				`COMMONS_UNIQUENAMED_FIRSTCLASS__${firstClass.getTable()}__LIST_ORDER_BY_NAME__${namefield}`
		);
	}
	
	public buildPreprepareForSecondClass(
			secondClass: CommonsSecondClass<
					M & ICommonsSecondClass<P> & ICommonsUniqueNamed,
					P
			>,
			namefield: string = 'name',
			caseSensitive: boolean = false
	): void {
		// MySQL does case insensitive varchar string comparisons.
		// Postgres does case sensitive
		// 2do: check for SQLite.
		const cs: string = (caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.MYSQL) ? 'BINARY ' : '';
		const lowerOpen: string = (!caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.POSTGRES) ? 'LOWER(' : '';
		const lowerClose: string = (!caseSensitive && this.database.getEngine() === ECommonsDatabaseEngine.POSTGRES) ? ')' : '';
		
		this.database.preprepare(
				`COMMONS_UNIQUENAMED_SECONDCLASS__${secondClass.getTable()}__GET_BY_NAME__${namefield}`,
				`
					SELECT ${this.modelFieldCsv(secondClass)}
					FROM ${this.model(secondClass)}
					INNER JOIN ${this.model(secondClass.getFirstClass())}
						ON ${this.modelField(secondClass, secondClass.getFirstClassField())}=${this.modelField(secondClass.getFirstClass(), secondClass.getFirstClass().getIdField())}
					WHERE ${this.modelField(secondClass.getFirstClass(), secondClass.getFirstClass().getIdField())}=:firstClass
					AND ${lowerOpen}${this.modelField(secondClass, namefield)}${lowerClose}=${lowerOpen}${cs}:name${lowerClose}
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						name: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				secondClass.getStructure()
		);

		this.database.preprepare(
				`COMMONS_UNIQUENAMED_SECONDCLASS__${secondClass.getTable()}__LIST_ORDER_BY_NAME__${namefield}`,
				`
					SELECT ${this.modelFieldCsv(secondClass)}
					FROM ${this.model(secondClass)}
					INNER JOIN ${this.model(secondClass.getFirstClass())}
						ON ${this.modelField(secondClass, secondClass.getFirstClassField())}=${this.modelField(secondClass.getFirstClass(), secondClass.getFirstClass().getIdField())}
					WHERE ${this.modelField(secondClass.getFirstClass(), secondClass.getFirstClass().getIdField())}=:firstClass
					ORDER BY ${this.modelField(secondClass, namefield)} ASC
				`,
				{
						firstClass: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				secondClass.getStructure()
		);
	}
	
	public async getSecondClassByFirstClassAndName(
			secondClass: CommonsSecondClass<
					M & ICommonsFirstClass & ICommonsUniqueNamed,
					P
			>,
			firstClass: ICommonsFirstClass,
			name: string,
			namefield: string = 'name'
	): Promise<M|undefined> {
		if (!commonsTypeHasPropertyNumber(firstClass, secondClass.getFirstClass().getIdField())) throw new Error('Invalid firstClass object');
		if (!name.match(COMMONS_REGEX_PATTERN_ID_NAME)) throw new Error(`Invalid value for name ${name}`);
	
		return await this.database.executeParamsSingle<M>(
				`COMMONS_UNIQUENAMED_SECONDCLASS__${secondClass.getTable()}__GET_BY_NAME__${namefield}`,
				{
						firstClass: firstClass[secondClass.getFirstClass().getIdField()] as number,
						name: name
				}
		);
	}

	public async listSecondClassByFirstClassOrderByName(
			secondClass: CommonsSecondClass<
					M & ICommonsFirstClass & ICommonsUniqueNamed,
					P
			>,
			firstClass: P,
			namefield: string = 'name'
	): Promise<M[]> {
		if (!commonsTypeHasPropertyNumber(firstClass, secondClass.getFirstClass().getIdField())) throw new Error('Invalid firstClass object');
		
		return await this.database.executeParams<M>(
				`COMMONS_UNIQUENAMED_SECONDCLASS__${secondClass.getTable()}__LIST_ORDER_BY_NAME__${namefield}`,
				{
						firstClass: firstClass[secondClass.getFirstClass().getIdField()] as number
				}
		);
	}
	
	public async generateUid(
			firstClass: CommonsFirstClass<M & ICommonsFirstClass & ICommonsUniqueNamed>,
			uidField: string = 'uid'
	): Promise<string> {
		let key: string|undefined;
		for (let i = 1000; i-- > 0;) {
			const attempt: string = commonsBase62GenerateRandomId();
		
			if (!(await this.getFirstClassByName(firstClass, attempt, uidField))) {
				key = attempt;
				break;
			}
		}
		
		if (!key) throw new Error('Unable to generate a unique key in over 1000 attempts. This is highly unlikely.');
		
		return key;
	}
}
