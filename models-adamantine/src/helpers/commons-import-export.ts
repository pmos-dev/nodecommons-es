import {
		commonsTypeIsString,
		commonsTypeHasProperty
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass, isICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsOrientatedOrdered } from 'tscommons-es-models';
import { ICommonsHierarchy } from 'tscommons-es-models';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { CommonsDatabaseTypeBoolean } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsOrientatedOrdered } from 'nodecommons-es-models';
import { CommonsHierarchy } from 'nodecommons-es-models';
import { CommonsImportExport as CommonsImportExportModels } from 'nodecommons-es-models';
import { TCommonsModelFields } from 'nodecommons-es-models';
import { TCommonsImportExportEncodings } from 'nodecommons-es-models';
import { TCommonsImportExportValues } from 'nodecommons-es-models';
import { ECommonsImportExportEncoding } from 'nodecommons-es-models';

import { CommonsManagedSecondClass } from '../classes/commons-managed-second-class';
import { CommonsManagedOrientatedOrdered } from '../classes/commons-managed-orientated-ordered';
import { CommonsManagedHierarchy } from '../classes/commons-managed-hierarchy';
import { CommonsManagedM2MLinkTable } from '../classes/commons-managed-m2m-link-table';

import { CommonsManageable } from '../interfaces/commons-manageable';

import { ECommonsImportExportBranch } from '../enums/ecommons-import-export-branch';

function instanceOfCommonsManageable(test: unknown): test is CommonsManageable<any, any, any> {
	if (!test) return false;
	if ('object' !== typeof test) return false;
	
	if (test === null) return false;
	return 'isModelManageable' in test;
}

/**
 * Calculates the chain steps needed for an automatic export of a managed model
 * 
 * @param \Abstraction\Models\FirstClass $model the FirstClass (or subclass, e.g. SecondClass) model being exported
 * @param mixed[] $object the current object being exported
 * @param array $chain an array of objects back to either the Installed or Instance object
 * @throws Adamantine\Exception
 * @return array an associated array of chain 'steps' as fields
 */
async function getExportChainForModel<
		M extends ICommonsSecondClass<any> & ICommonsManaged,
		R extends ICommonsFirstClass & ICommonsManaged
>(
		model: CommonsSecondClass<M, any>,
		chain: (ICommonsFirstClass|ICommonsSecondClass<any>)[] = [],
		rootModel: CommonsFirstClass<R>
): Promise<TCommonsImportExportValues> {
	const chainClone: (ICommonsFirstClass|ICommonsSecondClass<any>)[] = chain.slice();
	
	// step downwards through the model chain back to Installed or Instance
	const modelChain: (CommonsFirstClass<any>|CommonsSecondClass<any, any>)[] = [];
	const stepNames: string[] = [];
	let untypecast: CommonsSecondClass<any, any> = model;
	while (true) {
		stepNames.push(untypecast.getFirstClassField());
		
		const next: CommonsFirstClass<any>|CommonsSecondClass<any, any> = untypecast.getFirstClass();
		modelChain.push(next);
		
		if (next.constructor.name === rootModel.constructor.name) break;
		if (!(next instanceof CommonsSecondClass)) throw new Error('Exported chain went back to a firstclass rather than second before encountering the rootModel');
		
		untypecast = next;
	}

	if (modelChain.length !== chainClone.length) {
		throw new Error(`Model chain and firstclass chain lengths are not equal: ${modelChain.length} ${chainClone.length}`);
	}
	
	let chainModelItem: CommonsFirstClass<any>|CommonsSecondClass<any, any> = modelChain.pop()!;
	//if (!(chainModelItem instanceof CommonsSecondClass)) throw new Error('Managed exporting of firstclass-rooted models is not supported yet!');

	let last: ICommonsFirstClass|ICommonsSecondClass<any>|undefined = chainClone.pop();
	stepNames.pop();

	const exportChain: TCommonsImportExportValues = {};
	
	// step upwards through the model chain back back to the top
	while (modelChain.length > 0) {
		chainModelItem = modelChain.pop()!;
		
		const step: ICommonsFirstClass|ICommonsSecondClass<any> = chainClone.pop()!;
		const stepName: string = stepNames.pop()!;
		
		if (last === undefined) throw new Error('Last is undefined. Cannot proceed.');
		const check: ICommonsSecondClass<any> = (await (chainModelItem as CommonsSecondClass<any, any>).getByFirstClassAndId(
				last,
				step[chainModelItem.getIdField()] as number
		)) as ICommonsSecondClass<any>;
		if (!check) throw new Error('No such elements exists within the chain');
		
		exportChain[stepName.toUpperCase()] = {
				encoding: ECommonsImportExportEncoding.SIMPLE,
				value: check['name'] as string
		};
		
		last = step;
	}
	
	return exportChain;
}

/**
 * Recursive method to export the tree
 * 
 * @internal
 */
async function exportRecursiveHierarchy<
		M extends ICommonsHierarchy<any> & ICommonsManaged
>(
		areaName: string,
		model: CommonsManagedHierarchy<M, any>,
		firstClass: ICommonsFirstClass|ICommonsSecondClass<any>,
		node: M,
		encodings: TCommonsImportExportEncodings,
		name: string,
		prefields: TCommonsImportExportValues = {},
		out: string[]
): Promise<void> {
	let first: boolean = true;
	let anyChildren: unknown = false;
	for (const childNode of await model.listChildrenOrderedByFirstClass(firstClass, node)) {
		if (first) {
			first = false;
			anyChildren = true;
			out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} BRANCH ${name.toUpperCase()} IN`, prefields));
		}
		
		const fields: TCommonsImportExportValues = { ...prefields };
		const attributes: string[] = [];
		for (const [ field, encoding ] of Object.entries(encodings)) {
			if (encoding === ECommonsImportExportEncoding.BOOL) {
				if (childNode[field]) attributes.push(field.toUpperCase());
			} else {
				fields[field.toUpperCase()] = {
						encoding: encoding,
						value: childNode[field] as (string | number | boolean | Date)
				};
			}
		}
			
		out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} CREATE ${name.toUpperCase()}`, fields, attributes));
		
		if (model.isAliasingSupported()) {
			const fields2: TCommonsImportExportValues = { ...prefields };
			fields2['ALIASES'] = {
					encoding: ECommonsImportExportEncoding.SIMPLE,
					value: (await model.listAliasesByRow(childNode)).join(',')
			};
			
			out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} SET ALIASES FOR LAST ${name.toUpperCase()}`, fields2));
		}
		
		await exportRecursiveHierarchy(areaName, model, firstClass, childNode, encodings, name, prefields, out);
	}
	
	if (anyChildren) out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} BRANCH ${name.toUpperCase()} OUT`, prefields));
}

/**
 * Extracts the chain of firstclass ancestor objects by interrogating the chain back to Installed or Instance.
 * 
 * @param Models\SecondClass $model the Managed... model being imported
 * @param mixed[] $installed the root Installed context
 * @param string $line the import source string
 * @throws Adamantine\Exception
 * @return array|boolean the ancestor chain, or FALSE if an error occurs.
 */
async function getParentFirstClassChainBySteps<
		_M extends (ICommonsSecondClass<any>|ICommonsOrientatedOrdered<any>|ICommonsHierarchy<any>) & ICommonsManaged,	// eslint-disable-line @typescript-eslint/no-unused-vars
		R extends ICommonsFirstClass & ICommonsManaged
>(
		model: CommonsManagedSecondClass<any, any>|CommonsManagedOrientatedOrdered<any, any>|CommonsManagedHierarchy<any, any>,
		rootModel: CommonsFirstClass<R>,
		root: R,
		line: string
): Promise<((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[]> {
	// step downwards through the model chain back to Installed or Instance
	const modelChain: CommonsManagedSecondClass<any, any>[] = [];
	const stepNames: string[] = [];
	
	let model2: CommonsManagedSecondClass<any, any>|CommonsManagedOrientatedOrdered<any, any>|CommonsManagedHierarchy<any, any> = model;

	while (true) {
		stepNames.push(model2.getFirstClassField());
		
		const prev: CommonsFirstClass<any> = model2.getFirstClass();

		// this typing generics hack is ok because we pop the root anyway.
		modelChain.push(prev as CommonsManagedSecondClass<any, any>);
	
		if (prev.constructor.name === rootModel.constructor.name) break;
		if (!(prev instanceof CommonsManagedSecondClass)) throw new Error('Encountered a first class rather than a second class field before starting the root');
		model2 = prev;
	}
	
	modelChain.pop();	// no need to store

	// generics hack
	let last: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)|undefined = root as unknown as ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged);
	stepNames.pop();

	const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = [ root ];
	
	// step upwards through the model chain back back to the top
	while (modelChain.length > 0) {
		model2 = modelChain.pop()!;
		const stepName: string = stepNames.pop()!.toUpperCase();
	
		const step: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(stepName, line);
		if (step === undefined) throw new Error('Missing step value during import');
		if (!commonsTypeIsString(step)) throw new Error('Step value is not string');
		
		last = await model2.getByName(last, step);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
		if (last === undefined) throw new Error('No such step value exists');
		
		chain.push(last);
	}
	
	return chain;
}

export abstract class CommonsImportExport extends CommonsImportExportModels {
	//-------------------------------------------------------------------------
	
	/**
	 * Attempts to perform an automatic export of a managed model.
	 *
	 * @see Models\ManagedSecondClass::get_importexportable_fields()
	 * @param Models\ManagedSecondClass $model the ManagedSecondClass model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	public static async exportManagedSecondClass<
			M extends ICommonsSecondClass<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			areaName: string,
			model: CommonsManagedSecondClass<M, any>,
			name: string,
			chain: (ICommonsFirstClass|ICommonsSecondClass<any>)[] = [],
			rootModel: CommonsFirstClass<R>,
			prefields: TCommonsImportExportValues = {},
			out: string[]
	): Promise<void> {
		if (!(model instanceof CommonsSecondClass)) throw new Error('Trying to use exportManagedSecondClass to export a non-secondclass');
		
		if (chain.length === 0) throw new Error('Chain must contain at least an Installed context to export on');
		for (const c of chain) if (!isICommonsFirstClass(c)) throw new Error('Supplied firstclass in chain is not valid');
		
		const encodings: TCommonsImportExportEncodings = CommonsImportExportModels.buildExportFieldEncodings<M>(model, model.getImportExportableFields());
		
		const firstClass: ICommonsFirstClass|ICommonsSecondClass<any> = chain[0];
		for (const data of (await model.listByFirstClass(firstClass))) {
			const fields: TCommonsImportExportValues = { ...prefields };
			const attributes: string[] = [];
			
			for (const [ stepName, step ] of Object.entries(
					await getExportChainForModel(
							model,
							chain,
							rootModel
					))
			) fields[stepName] = step;
		
			for (const [ field, encoding ] of Object.entries(encodings)) {
				if (encoding === ECommonsImportExportEncoding.BOOL) {
					if (data[field]) attributes.push(field.toUpperCase());
				} else {
					fields[field.toUpperCase()] = {
							encoding: encoding,
							value: data[field] as string | number | boolean | Date
					};
				}
			}
			
			out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} CREATE ${name.toUpperCase()}`, fields, attributes));
		}
	}

	/**
	 * Attempts to perform an automatic export of a managed model.
	 *
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\ManagedOrientatedOrdered $model the ManagedOrientatedOrdered model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	public static async exportManagedOrientatedOrdered<
			M extends ICommonsOrientatedOrdered<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			areaName: string,
			model: CommonsManagedOrientatedOrdered<M, any>,
			name: string,
			chain: (ICommonsFirstClass|ICommonsSecondClass<any>)[] = [],
			rootModel: CommonsFirstClass<R>,
			prefields: TCommonsImportExportValues = {},
			out: string[]
	): Promise<void> {
		if (!(model instanceof CommonsOrientatedOrdered)) throw new Error('Trying to use exportManagedOrientatedOrdered to export a non-orientatedoredered');
		
		if (chain.length === 0) throw new Error('Chain must contain at least an Installed context to export on');
		for (const c of chain) if (!isICommonsFirstClass(c)) throw new Error('Supplied firstclass in chain is not valid');
		
		const encodings: TCommonsImportExportEncodings = CommonsImportExportModels.buildExportFieldEncodings<M>(model, model.getImportExportableFields());

		const firstClass: ICommonsFirstClass|ICommonsSecondClass<any> = chain[0];
		for (const data of await model.listOrderedByFirstClass(firstClass)) {
			const fields: TCommonsImportExportValues = { ...prefields };
			const attributes: string[] = [];
			
			for (const [ stepName, step ] of Object.entries(
					await getExportChainForModel(
							model,
							chain,
							rootModel
					))
			) fields[stepName] = step;
		
			for (const [ field, encoding ] of Object.entries(encodings)) {
				if (encoding === ECommonsImportExportEncoding.BOOL) {
					if (data[field]) attributes.push(field.toUpperCase());
				} else {
					fields[field.toUpperCase()] = {
							encoding: encoding,
							value: data[field] as (string | number | boolean | Date)
					};
				}
			}
			
			out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} CREATE ${name.toUpperCase()}`, fields, attributes));
		}
	}

	/**
	 * Attempts to perform an automatic export of a managed hierarchy.
	 *
	 * @see Models\ManagedHierarchy::get_importexportable_fields()
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	public static async exportManagedHierarchy<
			M extends ICommonsHierarchy<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			areaName: string,
			model: CommonsManagedHierarchy<M, any>,
			name: string,
			chain: (ICommonsFirstClass|ICommonsSecondClass<any>)[] = [],
			rootModel: CommonsFirstClass<R>,
			prefields: TCommonsImportExportValues = {},
			out: string[]
	): Promise<void> {
		if (!(model instanceof CommonsHierarchy)) throw new Error('Trying to use exportManagedHierarchy to export a non-hierarchy');

		if (chain.length === 0) throw new Error('Chain must contain at least an Installed context to export on');
		for (const c of chain) if (!isICommonsFirstClass(c)) throw new Error('Supplied firstclass in chain is not valid');
		
		const encodings: TCommonsImportExportEncodings = CommonsImportExportModels.buildExportFieldEncodings<M>(model, model.getImportExportableFields());
		
		const firstClass: ICommonsFirstClass|ICommonsSecondClass<any> = chain[0];
		const rootNode: M|undefined = await model.getRootByFirstClass(firstClass);
		if (!rootNode) throw new Error('No root for this firstclass');
		
		for (const [ stepName, step ] of Object.entries(
				await getExportChainForModel(
						model,
						chain,
						rootModel
				))
		) {
			prefields[stepName] = step;
		}

		out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} BRANCH ${name.toUpperCase()} ROOT`, prefields));
		
		await exportRecursiveHierarchy(areaName, model, firstClass, rootNode, encodings, name, prefields, out);
	}
	
	public static async exportM2MLinkTable<
			M extends ICommonsM2MLink<A, B>,
			A extends ICommonsFirstClass & ICommonsManaged,
			B extends ICommonsFirstClass & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			areaName: string,
			model: CommonsManagedM2MLinkTable<M, A, B>,
			name: string,
			chainA: (ICommonsFirstClass|ICommonsSecondClass<any>)[],
			chainB: (ICommonsFirstClass|ICommonsSecondClass<any>)[],
			rootModel: CommonsFirstClass<R>,
			out: string[]
	): Promise<void> {
		const tableA: CommonsFirstClass<A> = model.getAFirstClass();
		const tableB: CommonsFirstClass<B> = model.getBFirstClass();
		if (!(tableA instanceof CommonsSecondClass)) throw new Error('TableA must be an instance of second class');
		if (!(tableB instanceof CommonsSecondClass)) throw new Error('TableB must be an instance of second class');
		if (!instanceOfCommonsManageable(tableA)) throw new Error('M2MLinkTable can only be automatically exported if both the associated models are Manageable (A table failure)');
		if (!instanceOfCommonsManageable(tableB)) throw new Error('M2MLinkTable can only be automatically exported if both the associated models are Manageable (B table failure)');
		
		const nameA: string = model.getAField().toUpperCase();
		const nameB: string = model.getBField().toUpperCase();

		if (chainA.length === 0) throw new Error('Chain A must contain at least an Installed context to export on');
		for (const c of chainA) if (!isICommonsFirstClass(c)) throw new Error('Supplied firstclass in chain A is not valid');

		if (chainB.length === 0) throw new Error('Chain B must contain at least an Installed context to export on');
		for (const c of chainB) if (!isICommonsFirstClass(c)) throw new Error('Supplied firstclass in chain B is not valid');
		
		// need to clone this so that the deletes don't get applied to the model structure
		const structure: TCommonsModelFields = { ...model.getStructure() };
		delete structure[model.getAField()];
		delete structure[model.getBField()];
		
		const firstclassA: ICommonsFirstClass|ICommonsSecondClass<any> = chainA[0];
		for (const a of await (tableA as CommonsSecondClass<A, any>).listByFirstClass(firstclassA)) {
			const fieldsA: TCommonsImportExportValues = await getExportChainForModel(
					tableA,
					chainA,
					rootModel
			);
			
			for (const b of await model.listBsByA(a)) {
				const fields: TCommonsImportExportValues = { ...fieldsA };
				const attributes: string[] = [];
				
				for (const [ stepName, step ] of Object.entries(
						await getExportChainForModel(
								tableB,
								chainB,
								rootModel
						))
				) {
					if (commonsTypeHasProperty(fieldsA, stepName)) {
						const compare1: string|number|boolean|Date = fieldsA[stepName].value;
						const compare2: string|number|boolean|Date = step.value;
							
						if (compare1 === compare2) continue;
						throw new Error('The two M2MLinkTable model chains have a common ancestor model but not a common ancestor object within that model!');
					}
				
					fields[stepName] = step;
				}
				
				const encodings: TCommonsImportExportEncodings = CommonsImportExportModels.buildExportFieldEncodings(model, Object.keys(structure));

				fields[nameA] = {
						encoding: ECommonsImportExportEncoding.SIMPLE,
						value: a.name
				};
				fields[nameB] = {
						encoding: ECommonsImportExportEncoding.SIMPLE,
						value: b.name
				};
				
				if (Object.keys(encodings).length > 0) {
					const data: M = await model.getLinkRow(a, b);
					for (const [ field, encoding ] of Object.entries(encodings)) {
						if (encoding === ECommonsImportExportEncoding.BOOL) {
							if (data[field]) attributes.push(field.toUpperCase());
						} else {
							fields[field.toUpperCase()] = {
									encoding: encoding,
									value: data[field] as (string | number | boolean | Date)
							};
						}
					}
				}
				
				out.push(CommonsImportExportModels.writeLine(`${areaName.toUpperCase()} LINK ${name}`, fields, attributes));
			}
		}
	}
	
	//-------------------------------------------------------------------------
	
	// tslint:disable:member-ordering
	protected lastSecondClassImport: ICommonsFirstClass|undefined;
	private parentNode: { [ index: string ]: ICommonsHierarchy<any>|undefined } = {};
	private parentStack: { [ index: string ]: ICommonsHierarchy<any>[] } = {};
	private lastNode: { [ index: string ]: ICommonsHierarchy<any> } = {};
	// tslint:enable:member-ordering
	
	/**
	 * Attempts to perform an automatic import of a managed model from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedSecondClass::get_importexportable_fields()
	 * @param Models\ManagedSecondClass $model the ManagedSecondClass model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	public async importManagedSecondClass<
			M extends ICommonsSecondClass<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedSecondClass<M, any>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			line: string
	): Promise<boolean> {
		if (!(model instanceof CommonsSecondClass)) throw new Error('Trying to use importManagedSecondClass to import a non-secondclass');
		const structure: TCommonsModelFields = model.getStructure();
		
		const data: TPropertyObject = {};
		
		for (const field of model.getImportExportableFields()) {
			if (structure[field] instanceof CommonsDatabaseTypeBoolean) {
				data[field] = CommonsImportExportModels.hasAttribute(field.toUpperCase(), line);
			} else {
				const value: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(field.toUpperCase(), line);
				if (value === undefined && structure[field].getNotNull() === ECommonsDatabaseTypeNull.NOT_NULL) return false;
			
				try {
					structure[field].assert(value);
				} catch (e) {
					return false;
				}
				
				data[field] = value;
			}
		}
		
		try {
			const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = await getParentFirstClassChainBySteps(model, rootModel, root, line);
			const last: (ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged = chain.pop()!;
			
			this.lastSecondClassImport = await model.insertForFirstClass(last, data);
			if (!this.lastSecondClassImport) return false;
			// if (model.postCreateProcessing)) model.postCreateProcessing(this.repository, this.lastSecondClassImport);
			
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}

	/**
	 * Attempts to perform an automatic import of a managed model from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\ManagedOrientatedOrdered $model the ManagedOrientatedOrdered model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	public async importManagedOrientatedOrdered<
			M extends ICommonsOrientatedOrdered<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedOrientatedOrdered<M, any>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			line: string
	): Promise<boolean> {
		if (!(model instanceof CommonsOrientatedOrdered)) throw new Error('Trying to use importManagedOrientatedOrdered to import a non-orientatedordered');
		const structure: TCommonsModelFields = model.getStructure();

		const data: TPropertyObject = {};
		for (const field of model.getImportExportableFields()) {
			if (structure[field] instanceof CommonsDatabaseTypeBoolean) {
				data[field] = CommonsImportExportModels.hasAttribute(field.toUpperCase(), line);
			} else {
				const value: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(field.toUpperCase(), line);
				if (value === undefined && structure[field].getNotNull() === ECommonsDatabaseTypeNull.NOT_NULL) return false;
			
				try {
					structure[field].assert(value);
				} catch (e) {
					return false;
				}
				
				data[field] = value;
			}
		}
		
		try {
			const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = await getParentFirstClassChainBySteps(model, rootModel, root, line);
			const last: (ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged = chain.pop()!;
			
			this.lastSecondClassImport = await model.insertForFirstClass(last, data);
			if (!this.lastSecondClassImport) return false;
			// if (model.postCreateProcessing)) model.postCreateProcessing(this.repository, this.lastSecondClassImport);
			
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
	
	/**
	 * Attempts to perform an automatic import of a managed hierarchy from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedHierarchy::get_importexportable_fields()
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	public async importManagedHierarchy<
			M extends ICommonsHierarchy<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedHierarchy<M, any>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			line: string
	): Promise<boolean> {
		if (!(model instanceof CommonsHierarchy)) throw new Error('Trying to use importManagedHierarchy to import a non-hierarchy');
		const structure: TCommonsModelFields = model.getStructure();
		
		const data: TPropertyObject = {};
		for (const field of model.getImportExportableFields()) {
			if (structure[field] instanceof CommonsDatabaseTypeBoolean) {
				data[field] = CommonsImportExportModels.hasAttribute(field.toUpperCase(), line);
			} else {
				const value: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(field.toUpperCase(), line);
				if (value === undefined && structure[field].getNotNull() === ECommonsDatabaseTypeNull.NOT_NULL) return false;
			
				try {
					structure[field].assert(value);
				} catch (e) {
					return false;
				}
				
				data[field] = value;
			}
		}
		
		try {
			const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = await getParentFirstClassChainBySteps(model, rootModel, root, line);
			
			const ids: string[] = chain
					.map((c: ICommonsFirstClass): string => c.id.toString(10));
			const index: string = `${model.getTable()}__${ids.join(',')}`;
			const last: ICommonsFirstClass = chain.pop()!;
			
			if (!commonsTypeHasProperty(this.parentNode, index)) return false;
			
			this.lastSecondClassImport = await model.insertForFirstClassAndParent(last, this.parentNode[index] as M, data);
			if (!this.lastSecondClassImport) return false;
			// if (model.postCreateProcessing) model.postCreateProcessing(this.repository, this.lastSecondClassImport);
		
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}

	/**
	 * Attempts to perform an automatic import of a branch from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model context
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $direction either BRANCH_IN or BRANCH_OUT
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	public async importManagedHierarchyBranch<
			M extends ICommonsHierarchy<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedHierarchy<M, any>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			direction: ECommonsImportExportBranch,
			line: string
	): Promise<boolean> {
		if (!(model instanceof CommonsHierarchy)) throw new Error('Trying to use importManagedHierarchyBranch to import a non-hierarchy');

		try {
			const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = await getParentFirstClassChainBySteps(model, rootModel, root, line);

			const ids: string[] = chain
					.map((c: ICommonsFirstClass): string => c.id.toString(10));
			const index: string = `${model.getTable()}__${ids.join(',')}`;
			const last: ICommonsFirstClass = chain.pop()!;
		
			switch (direction) {
				case ECommonsImportExportBranch.ROOT:
					if (!commonsTypeHasProperty(this.lastNode, index)) return false;
					
					const rootNode: M|undefined = await model.getRootByFirstClass(last);
					if (!rootNode) throw new Error('No root exists for this hierarchy');
					this.lastNode[index] = rootNode;
					this.parentNode[index] = undefined;
					this.parentStack[index] = [];
					
					return true;
				case ECommonsImportExportBranch.IN:
					if (!commonsTypeHasProperty(this.lastNode, index)) return false;
					if (this.lastNode[index] === undefined) return false;
					
					this.parentStack[index].push(this.parentNode[index] = this.lastNode[index]);
					
					return true;
				case ECommonsImportExportBranch.OUT:
					if (!commonsTypeHasProperty(this.lastNode, index)) return false;
					if (this.parentStack[index].length === 0) return false;
					
					// this.currentNode[index] = null; ??
					this.parentStack[index].pop();
					
					if (this.parentStack[index].length === 0) {
						this.parentNode[index] = undefined;
					} else {
						this.parentStack[index].push(this.parentNode[index] = this.parentStack[index].pop()!);
					}
					
					return true;
				default:
					return false;
			}
		} catch (e) {
			console.log(e);
			return false;
		}
	}

	/**
	 * Attempts to perform an automatic import of a managed model row alias set from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @param Models\Manageable $model the model context, which must have alias support enabled
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	public async importManagedAlias<
			M extends ICommonsSecondClass<any> & ICommonsManaged,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedSecondClass<M, any>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			line: string
	): Promise<boolean> {
		if (model.isAliasingSupported()) return false;

		try {
			const chain: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = await getParentFirstClassChainBySteps(model, rootModel, root, line);

			const ids: string[] = chain
					.map((c: ICommonsFirstClass): string => c.id.toString(10));
			const index: string = `${model.getTable()}__${ids.join(',')}`;
		
			if (this.lastNode[index] === undefined) return false;
	
			const a: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField('ALIASES', line);

			const aliases: string[] = [];
			if (commonsTypeIsString(a) && a.trim() !== '') aliases.push(...a.trim().split(','));
			
			if (!await model.replaceAliases(this.lastSecondClassImport as M, aliases)) return false;
		
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}

	// tslint:disable-next-line:member-ordering
	public static async importM2MLinkTable<
			M extends ICommonsM2MLink<A, B>,
			A extends ICommonsFirstClass,
			B extends ICommonsFirstClass,
			R extends ICommonsFirstClass & ICommonsManaged
	>(
			model: CommonsManagedM2MLinkTable<M, A, B>,
			rootModel: CommonsFirstClass<R>,
			root: R,
			line: string
	): Promise<boolean> {
		const tableA: CommonsFirstClass<A> = model.getAFirstClass();
		const tableB: CommonsFirstClass<B> = model.getBFirstClass();
		if (!(tableA instanceof CommonsSecondClass)) throw new Error('TableA must be an instance of second class');
		if (!(tableB instanceof CommonsSecondClass)) throw new Error('TableB must be an instance of second class');
		if (!instanceOfCommonsManageable(tableA)) throw new Error('M2MLinkTable can only be automatically imported if both the associated models are Manageable (A table failure)');
		if (!instanceOfCommonsManageable(tableB)) throw new Error('M2MLinkTable can only be automatically imported if both the associated models are Manageable (B table failure)');
		
		try {
			const chainA: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = [];
			
			if (tableA instanceof CommonsManagedSecondClass) {
				chainA.push(...await getParentFirstClassChainBySteps(tableA, rootModel, root, line));
			} else if (tableA instanceof CommonsManagedOrientatedOrdered) {
				chainA.push(...await getParentFirstClassChainBySteps(tableA, rootModel, root, line));
			} else if (tableA instanceof CommonsManagedHierarchy) {
				chainA.push(...await getParentFirstClassChainBySteps(tableA, rootModel, root, line));
			} else {
				throw new Error('Automatic import from TableA must be an instance of ManagedSecondClass/OrientatedOrdered/Hierarchy');
			}
			
			const chainB: ((ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged)[] = [];
			
			if (tableB instanceof CommonsManagedSecondClass) {
				chainB.push(...await getParentFirstClassChainBySteps(tableB, rootModel, root, line));
			} else if (tableB instanceof CommonsManagedOrientatedOrdered) {
				chainB.push(...await getParentFirstClassChainBySteps(tableB, rootModel, root, line));
			} else if (tableB instanceof CommonsManagedHierarchy) {
				chainB.push(...await getParentFirstClassChainBySteps(tableB, rootModel, root, line));
			} else {
				throw new Error('Automatic import from TableB must be an instance of ManagedSecondClass/OrientatedOrdered/Hierarchy');
			}
			
			const lastA: (ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged = chainA.pop()!;
			const lastB: (ICommonsFirstClass|ICommonsSecondClass<any>) & ICommonsManaged = chainB.pop()!;
		
			const nameA: string = model.getAField().toUpperCase();
			const nameB: string = model.getBField().toUpperCase();
		
			const idA: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(nameA, line);
			if (!commonsTypeIsString(idA)) throw new Error('Invalid value for idA during import');
			const objectA: A = await tableA.getByName(lastA, idA) as A;
		
			const idB: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(nameB, line);
			if (!commonsTypeIsString(idB)) throw new Error('Invalid value for idB during import');
			const objectB: B = await tableB.getByName(lastB, idB) as B;
		
			// need to clone this so that the deletes don't get applied to the model structure
			const structure: TCommonsModelFields = { ...model.getStructure() };
			delete structure[model.getAField()];
			delete structure[model.getBField()];
		
			const data: TPropertyObject = {};
			for (const [ field, encoding ] of Object.entries(structure)) {
				if (encoding instanceof CommonsDatabaseTypeBoolean) {
					data[field] = CommonsImportExportModels.hasAttribute(field.toUpperCase(), line);
				} else {
					const value: string|number|boolean|Date|string[]|undefined = CommonsImportExportModels.extractField(field.toUpperCase(), line);
					if (value === undefined && structure[field].getNotNull() === ECommonsDatabaseTypeNull.NOT_NULL) return false;
						
					try {
						encoding.assert(value);
					} catch (e) {
						return false;
					}
			
					data[field] = value;
				}
			}
		
			await model.link(objectA, objectB, data);
		
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
}
