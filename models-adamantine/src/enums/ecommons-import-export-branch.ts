import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsImportExportBranch {
		IN = 'in',
		OUT = 'out',
		ROOT = 'root'
}

export function toECommonsImportExportBranch(type: string): ECommonsImportExportBranch|undefined {
	switch (type) {
		case ECommonsImportExportBranch.IN.toString():
			return ECommonsImportExportBranch.IN;
		case ECommonsImportExportBranch.OUT.toString():
			return ECommonsImportExportBranch.OUT;
		case ECommonsImportExportBranch.ROOT.toString():
			return ECommonsImportExportBranch.ROOT;
	}
	return undefined;
}

export function fromECommonsImportExportBranch(type: ECommonsImportExportBranch): string {
	switch (type) {
		case ECommonsImportExportBranch.IN:
			return ECommonsImportExportBranch.IN.toString();
		case ECommonsImportExportBranch.OUT:
			return ECommonsImportExportBranch.OUT.toString();
		case ECommonsImportExportBranch.ROOT:
			return ECommonsImportExportBranch.ROOT.toString();
	}
	
	throw new Error('Unknown ECommonsImportExportBranch');
}

export function isECommonsImportExportBranch(test: unknown): test is ECommonsImportExportBranch {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsImportExportBranch(test) !== undefined;
}

export function keyToECommonsImportExportBranch(key: string): ECommonsImportExportBranch {
	switch (key) {
		case 'IN':
			return ECommonsImportExportBranch.IN;
		case 'OUT':
			return ECommonsImportExportBranch.OUT;
		case 'ROOT':
			return ECommonsImportExportBranch.ROOT;
	}
	
	throw new Error(`Unable to obtain ECommonsImportExportBranch for key: ${key}`);
}

export const ECOMMONS_IMPORT_EXPORT_BRANCHS: ECommonsImportExportBranch[] = Object.keys(ECommonsImportExportBranch)
		.map((e: string): ECommonsImportExportBranch => keyToECommonsImportExportBranch(e));
