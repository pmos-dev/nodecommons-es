import { TCommonsStrictParamResult } from './tcommons-strict-param-result';

export type TCommonsStrictParamPropertyObject = {
		[ name: string ]: TCommonsStrictParamResult|TCommonsStrictParamResult[];
};
