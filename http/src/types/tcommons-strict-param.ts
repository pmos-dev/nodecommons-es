import { ECommonsStrictParamPlurality } from '../enums/ecommons-strict-param-plurality';
import { ECommonsStrictParamType } from '../enums/ecommons-strict-param-type';

export type TCommonsHttpStrictParam = {
		variable: string;
		type?: ECommonsStrictParamType;
		plurality: ECommonsStrictParamPlurality;
		options?: string[];
};
