// This is used as a pseudo for things like express.Response

export type TCommonsHttpResponse = {
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		setHeader(key: string, value: string);

		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		status(statusCode: number);

		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		send(body?: string|number|boolean|object|Buffer);
}
