import { ICommonsStrictParamsRequest } from '../interfaces/icommons-strict-params-request';

import { TCommonsHttpResponse } from './tcommons-http-response';

export type TCommonsHttpHandler<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> = (
		request: RequestT,
		response: ResponseT
) => Promise<void>;
