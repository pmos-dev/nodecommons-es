import { ClientRequest, IncomingMessage } from 'http';
import * as https from 'https';

import { commonsPromiseTimeout } from 'tscommons-es-async';
import { TPropertyObject } from 'tscommons-es-core/dist';

export function commonsSslGetCertificate(
		domain: string,
		timeout: number,
		port: number = 443
): Promise<TPropertyObject|null> {
	return commonsPromiseTimeout<TPropertyObject|null>(
			async (): Promise<TPropertyObject|null> => new Promise((resolve: (certificate: TPropertyObject|null) => void, reject: (e: Error) => void): void => {
				const request: ClientRequest = https.request(
						{
								hostname: domain,
								port: port,
								method: 'GET',
								timeout: timeout,
								rejectUnauthorized: true,
								agent: false
								
						},
						(result: IncomingMessage): void => {
							// eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
							resolve((result.socket as any).getPeerCertificate());
						}
				);
				request.on('error', (e: Error): void => {
					reject(e);
				});
				
				request.end();
			}),
			timeout
	);
}
