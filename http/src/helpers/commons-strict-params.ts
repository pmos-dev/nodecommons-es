import {
		COMMONS_REGEX_PATTERN_BASE36_ID,
		COMMONS_REGEX_PATTERN_BASE62_ID,
		COMMONS_REGEX_PATTERN_BASE62_LONG_ID,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_ID,
		COMMONS_REGEX_PATTERN_ID_NAME,
		TKeyObject,
		commonsDateYmdHisToDate,
		commonsDateYmdToDate,
		commonsTypeAttemptNumber,
		commonsTypeIsArray
} from 'tscommons-es-core';

import { ECommonsStrictParamPlurality } from '../enums/ecommons-strict-param-plurality';
import { ECommonsStrictParamType, toECommonsStrictParamType } from '../enums/ecommons-strict-param-type';
import { TCommonsStrictParamResult } from '../types/tcommons-strict-param-result';
import { TCommonsHttpStrictParam } from '../types/tcommons-strict-param';
import { TCommonsStrictParamPropertyObject } from '../types/tcommons-strict-param-property-object';

export const COMMONS_HTTP_STRICT_PARAM_TYPE_REGEX: RegExp = /^:([-_a-z0-9]+)(?:(?:\[(id|idname|int|base36|base62|base62long|ymd|ymdhis)([*+])?\])|(?:\(([-a-z0-9]{1,32}(?:\|[-a-z0-9]{1,32})+)\)))?([^-a-z0-9].*)?$/i;

export function commonsRemoveStrictsFromPathDefinition(p: string): string {
	return p
			.split('/')
			.map((part) => {
				const result = COMMONS_HTTP_STRICT_PARAM_TYPE_REGEX.exec(part);
				if (result === null) return part;

				return `:${result[1]}${result[5] || ''}`;
			})
			.join('/');
}

export function commonsBuildStrictsFromPathDefinition(definedPath: string): TCommonsHttpStrictParam[] {
	return definedPath
			.split('/')
			.filter((param: string): boolean => COMMONS_HTTP_STRICT_PARAM_TYPE_REGEX.test(param))
			.map((param: string): TCommonsHttpStrictParam => {
				const match: RegExpExecArray|null = COMMONS_HTTP_STRICT_PARAM_TYPE_REGEX.exec(param);
				if (match === null) throw new Error('Should not be possible');
				
				const strict: TCommonsHttpStrictParam = {
						variable: match[1],
						plurality: ECommonsStrictParamPlurality.SINGLE
				};
	
				if (match[4] !== undefined) {
					strict.type = ECommonsStrictParamType.IDNAME;
					strict.options = match[4].split('|');
				} else {
					if (match[2] !== undefined) strict.type = toECommonsStrictParamType(match[2]);
					
					if (match[3] !== undefined) {
						switch (match[3]) {
							case '+':
								strict.plurality = ECommonsStrictParamPlurality.ARRAY;
								break;
						}
						switch (match[3]) {
							case '*':
								strict.plurality = ECommonsStrictParamPlurality.ANY;
								break;
						}
					}
				}
				
				return strict;
			});
}

export function commonsValueToStrictParamResult(
		value: string,
		type: ECommonsStrictParamType,
		plurality: ECommonsStrictParamPlurality,
		options?: string[]
): TCommonsStrictParamResult|TCommonsStrictParamResult[]|undefined {
	if (
		plurality === ECommonsStrictParamPlurality.ARRAY
		|| (plurality === ECommonsStrictParamPlurality.ANY && value.indexOf(',') > -1)
	) {
		const items: (TCommonsStrictParamResult|undefined)[] = value.split(',')
				// allow trailing comma to differentiate single item arrays from singular types
				.map((item: string): string => item.trim())
				.filter((item: string): boolean => item !== '')
				.map((item: string): TCommonsStrictParamResult|undefined => commonsValueToStrictParamResult(
						item,
						type,
						ECommonsStrictParamPlurality.SINGLE,
						options
				) as TCommonsStrictParamResult|undefined);	// can't be recursively array, as the plurality is fixed at single
		
		if (items
				.filter((item: any): boolean => item === undefined)
				.length > 0
		) return undefined;
		
		return items
				.map((item: TCommonsStrictParamResult|undefined): TCommonsStrictParamResult => item!);
	}
	
	switch (type) {
		case ECommonsStrictParamType.ID: {
			if (!COMMONS_REGEX_PATTERN_ID.test(value)) return undefined;
			
			const id: number|undefined = commonsTypeAttemptNumber(value);
			if (id === undefined || id < 1) return undefined;
			return id;
		}
		case ECommonsStrictParamType.IDNAME: {
			if (!COMMONS_REGEX_PATTERN_ID_NAME.test(value)) return undefined;
			if (options && !options.includes(value)) return undefined;
			return value;
		}
		case ECommonsStrictParamType.INT: {
			const int: number|undefined = commonsTypeAttemptNumber(value);
			if (int === undefined) return undefined;
			return int;
		}
		case ECommonsStrictParamType.BASE36: {
			if (!COMMONS_REGEX_PATTERN_BASE36_ID.test(value)) return undefined;
			return value;
		}
		case ECommonsStrictParamType.BASE62: {
			if (!COMMONS_REGEX_PATTERN_BASE62_ID.test(value)) return undefined;
			return value;
		}
		case ECommonsStrictParamType.BASE62LONG: {
			if (!COMMONS_REGEX_PATTERN_BASE62_LONG_ID.test(value)) return undefined;
			return value;
		}
		case ECommonsStrictParamType.YMD: {
			if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(value)) return undefined;
			return commonsDateYmdToDate(value);
		}
		case ECommonsStrictParamType.YMDHIS: {
			if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(value)) return undefined;
			return commonsDateYmdHisToDate(value);
		}
		default:
			throw new Error('Unknown parameter type');
	}
}

// NB this isn't actually used by the express server. It is used by other pseudo ones like the socket io one.
export function commonsExtractStrictParamsFromUrlPath(definedPath: string, suppliedUrlPath: string): [ TKeyObject<string>, TCommonsStrictParamPropertyObject ]|false {
	const definedParts: string[] = commonsRemoveStrictsFromPathDefinition(definedPath).split('/');
	const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(definedPath);
	const suppliedParts: string[] = suppliedUrlPath.split('/');

	if (definedParts.length !== suppliedParts.length) return false;

	const params: TKeyObject<string> = {};
	const strictParams: TCommonsStrictParamPropertyObject = {};

	for (let i = 0; i < definedParts.length; i++) {
		const d: string = definedParts[i];
		const s: string = suppliedParts[i];

		if (!d.startsWith(':')) {
			if (d !== s) return false;
			continue;
		}

		const match: TCommonsHttpStrictParam|undefined = stricts
				.find((strict: TCommonsHttpStrictParam): boolean => strict.variable === d.substring(1));
		if (!match) return false;

		params[match.variable] = s;

		if (!match.type) {
			// no type defined, so always assume matches
		} else {
			const strictResult: TCommonsStrictParamResult|TCommonsStrictParamResult[]|undefined = commonsValueToStrictParamResult(
					s,
					match.type,
					match.plurality,
					match.options
			);
			if (!strictResult) return false;

			if (match.plurality === ECommonsStrictParamPlurality.ARRAY && !commonsTypeIsArray(strictResult)) return false;
			if (match.plurality === ECommonsStrictParamPlurality.SINGLE && commonsTypeIsArray(strictResult)) return false;

			strictParams[match.variable] = strictResult;
		}
	}

	return [ params, strictParams ];
}

// const test1: string = '/tests/:test1/values/like/:test2[id]/:test3[base62]/test4/:test5/:test6(a|b|c)/so-as/:test7[ymdhis*]';
// console.log(removeStrictsFromPathDefinition(test1));

// const testYes1: string = '/tests/t1/values/like/123/abcd1234/test4/99-z/c/so-as/2023-08-01 10:00:00,2023-08-01 11:00:00,2023-08-01 12:00:00';
// console.log('testYes1', matchesPath(test1, testYes1));

// const testNo1: string = '/tests/t1/value/like/123/abcd1234/test4/99-z/c/so-as/2023-08-01 10:00:00,2023-08-01 11:00:00,2023-08-01 12:00:00';
// console.log('testNo1', matchesPath(test1, testNo1));

// const testNo2: string = '/tests/t1/values/like/123/bcd1234/test4/99-z/c/so-as/2023-08-01 10:00:00,2023-08-01 11:00:00,2023-08-01 12:00:00';
// console.log('testNo2', matchesPath(test1, testNo2));

// const testYes2: string = '/tests/t1/values/like/123/abcd1234/test4/99-z/a/so-as/2023-08-01 10:00:00';
// console.log('testYes2', matchesPath(test1, testYes2));
