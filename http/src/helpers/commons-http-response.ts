import { TCommonsHttpResponse } from '../types/tcommons-http-response';

export function commonsHttpNoCache(res: TCommonsHttpResponse): void {
	res.setHeader('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.setHeader('Expires', '-1');
	res.setHeader('Pragma', 'no-cache');
}

export function commonsHttpCache(res: TCommonsHttpResponse, seconds: number): void {
	res.setHeader('Cache-Control', `public, max-age=${seconds}`);
}

export function commonsHttpUseCacheIfUnchanged(res: TCommonsHttpResponse): void {
	// see "Requiring revalidation": https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
	// Makes browsers check for a new version, but not download if it isn't changed
	res.setHeader('Cache-Control', 'no-cache, max-age=0');
}

export function commonsHttpSetLastModified(res: TCommonsHttpResponse, timestamp: Date): void {
	res.setHeader('Last-Modified', timestamp.toUTCString());
}
