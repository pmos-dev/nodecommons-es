import * as querystring from 'querystring';

import {
		commonsTypeIsString,
		commonsTypeIsNumber,
		commonsTypeIsDate,
		commonsTypeIsBoolean,
		commonsTypeIsStringArray,
		commonsTypeIsNumberArray,
		commonsTypeIsBooleanArray,
		commonsDateDateToYmdHis
} from 'tscommons-es-core';
import { TEncoded, TEncodedObject } from 'tscommons-es-core';

export function commonsHttpEncodedObjectToQueryInput(encoded: TEncodedObject): querystring.ParsedUrlQueryInput {
	const converted: querystring.ParsedUrlQueryInput = {};
	
	for (const key of Object.keys(encoded)) {
		if (encoded[key] === undefined || encoded[key] === null) {
			converted[key] = '';
			continue;
		}
		
		const value: TEncoded = encoded[key];
		
		if (
			commonsTypeIsString(value)
			|| commonsTypeIsNumber(value)
			|| commonsTypeIsBoolean(value)
		) {
			converted[key] = value;
			continue;
		}
		
		if (commonsTypeIsDate(value)) {
			converted[key] = commonsDateDateToYmdHis(value);
			continue;
		}
		
		if (
			commonsTypeIsStringArray(value)
			|| commonsTypeIsNumberArray(value)
			|| commonsTypeIsBooleanArray(value)
		) {
			converted[key] = value;
			continue;
		}
		
		const type: string = typeof value;
		throw new Error(`Unable to convert value of type ${type} in CommonsUrl.encodedObjectToQueryString`);
	}
	
	return converted;
}

export function commonsHttpAppendEncodedObjectParams(url: string, params: TEncodedObject): string {
	if (params.length === 0) return url;
	
	const separator: string = url.indexOf('?') === -1 ? '?' : '&';
	
	const converted: querystring.ParsedUrlQueryInput = commonsHttpEncodedObjectToQueryInput(params);
	return `${url}${separator}${querystring.stringify(converted)}`;
}
