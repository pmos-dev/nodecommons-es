export function commonsNginxProxyPath(p?: string): string {
	if (p === undefined) return '/';
	return `${p}${p.endsWith('/') ? '' : '/'}`;
}
