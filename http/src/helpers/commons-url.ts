// this an simple replacement implementation of the deprecated nodecommons-url

import { commonsObjectStripNulls, TEncodedObject } from 'tscommons-es-core';
import { CommonsHttpClientService } from 'tscommons-es-http';
import { TCommonsHttpInternalRequestOptions } from 'tscommons-es-http';
import { ECommonsHttpResponseDataType } from 'tscommons-es-http';

import { CommonsInternalHttpClientImplementation } from '../classes/commons-internal-http-client-implementation';

export async function commonsHttpReadUrlAsBuffer(
		url: string,
		options?: TCommonsHttpInternalRequestOptions,
		headers?: TEncodedObject
): Promise<Buffer> {
	const implementation: CommonsInternalHttpClientImplementation = new CommonsInternalHttpClientImplementation();
	const service: CommonsHttpClientService = new CommonsHttpClientService(implementation);

	const uint8Array: Uint8Array = await (service.get(
			url,
			undefined,
			headers,
			{
					...options,
					responseDataType: ECommonsHttpResponseDataType.UINT8ARRAY
			}
	) as Promise<Uint8Array>);
	
	return Buffer.from(uint8Array);
}

export async function commonsHttpReadUrlAsString(
		url: string,
		options?: TCommonsHttpInternalRequestOptions,
		headers?: TEncodedObject
): Promise<string> {
	const implementation: CommonsInternalHttpClientImplementation = new CommonsInternalHttpClientImplementation();
	const service: CommonsHttpClientService = new CommonsHttpClientService(implementation);

	return service.get(
			url,
			undefined,
			headers,
			{
					...options,
					responseDataType: ECommonsHttpResponseDataType.STRING
			}
	) as Promise<string>;
}

export async function commonsHttpReadUrlAsJsonWithoutNulls(
		url: string,
		options?: TCommonsHttpInternalRequestOptions,
		headers?: TEncodedObject
): Promise<any> {
	const raw: string = await commonsHttpReadUrlAsString(
			url,
			options,
			headers
	);
	const json: any = JSON.parse(raw);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
	
	return commonsObjectStripNulls(json);	// eslint-disable-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-argument
}
