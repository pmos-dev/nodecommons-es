import * as dns from 'dns';

import { commonsPromiseTimeout } from 'tscommons-es-async';

export function commonsDnsResolveDomain4(
		domain: string,
		timeout: number
): Promise<string[]> {
	return commonsPromiseTimeout<string[]>(
			async (): Promise<string[]> => new Promise((resolve: (address: string[]) => void, reject: (e: Error) => void): void => {
				dns.resolve4(
						domain,
						(err: NodeJS.ErrnoException|null, address: string[]): void => {
							if (err) {
								reject(err);
								return;
							}
		
							resolve(address);
						}
				);
			}),
			timeout
	);
}

export function commonsDnsResolveDomain6(
		domain: string,
		timeout: number
): Promise<string[]> {
	return commonsPromiseTimeout<string[]>(
			async (): Promise<string[]> => new Promise((resolve: (address: string[]) => void, reject: (e: Error) => void): void => {
				dns.resolve6(
						domain,
						(err: NodeJS.ErrnoException|null, address: string[]): void => {
							if (err) {
								reject(err);
								return;
							}
		
							resolve(address);
						}
				);
			}),
			timeout
	);
}

export function commonsDnsResolveReverse(
		ip: string,
		timeout: number
): Promise<string[]> {
	return commonsPromiseTimeout<string[]>(
			async (): Promise<string[]> => new Promise((resolve: (address: string[]) => void, reject: (e: Error) => void): void => {
				dns.reverse(
						ip,
						(err: NodeJS.ErrnoException|null, hostnames: string[]): void => {
							if (err) {
								reject(err);
								return;
							}
		
							resolve(hostnames);
						}
				);
			}),
			timeout
	);
}
