import { URL } from 'url';
import * as https from 'https';
import * as http from 'http';

import { default as formurlencoded } from 'form-urlencoded';

import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { CommonsHttpError } from 'tscommons-es-http';
import { CommonsHttpTimeoutError } from 'tscommons-es-http';
import { commonsHttpBuildErrorFromResponseCode } from 'tscommons-es-http';
import { ICommonsHttpClientImplementation } from 'tscommons-es-http';
import { THttpHeaderOrParamObject } from 'tscommons-es-http';
import { TCommonsHttpInternalRequestOptions } from 'tscommons-es-http';
import { ECommonsHttpContentType } from 'tscommons-es-http';
import { ECommonsHttpResponseDataType } from 'tscommons-es-http';
import { ECommonsHttpMethod } from 'tscommons-es-http';

import { commonsHttpAppendEncodedObjectParams } from '../helpers/commons-http-query-string';

import { TCommonsHttpRequest } from '../types/tcommons-http-request';

type TProtocol = typeof http | typeof https;

export class CommonsInternalHttpClientImplementation implements ICommonsHttpClientImplementation {
	private static request(
			protocol: TProtocol,
			options: TCommonsHttpRequest,
			bodyString: string,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType
	): Promise<string|Uint8Array|void> {
		return new Promise<string|Uint8Array|void>((resolve: (result: string|Uint8Array|void) => void, reject: (e: CommonsHttpError|Error) => void): void => {
			let timedOut: boolean = false;

			const request: http.ClientRequest = protocol.request(options, (res: http.IncomingMessage): void => {
				if (responseDataType === ECommonsHttpResponseDataType.STRING) res.setEncoding('utf8');
				if (options.timeout) res.setTimeout(options.timeout);
				
				const rawData: Buffer[] = [];
				let stringData: string = '';
				res.on('data', (chunk: any): void => {	// the any type has is actually the Node type's official type
					if (responseDataType === ECommonsHttpResponseDataType.UINT8ARRAY) {
						rawData.push(Buffer.from(chunk));	// eslint-disable-line @typescript-eslint/no-unsafe-argument
					} else {
						stringData += chunk;
					}
				});
				
				res.on('end', (): void => {
					if (res.statusCode && res.statusCode >= 400) {
						// we only consider stringData for errors, even if we are actually in a raw request. It gets too complex trying to manage potential buffers for error messages.
						try {
							const json: any = JSON.parse(stringData);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
							if (commonsTypeHasPropertyString(json, 'message')) {
								reject(commonsHttpBuildErrorFromResponseCode(res.statusCode, json.message as string));
							} else {
								reject(commonsHttpBuildErrorFromResponseCode(res.statusCode, stringData));
							}
						} catch (e) {
							reject(commonsHttpBuildErrorFromResponseCode(res.statusCode, stringData));
						}
						return;
					}

					if (!returnResult) {
						resolve();
						return;
					}
					
					if (responseDataType === ECommonsHttpResponseDataType.UINT8ARRAY) {
						const concat: Buffer = Buffer.concat(rawData);
						
						// the resulting concat.buffer may not be purely the data, and there can be memory reuse, so we need to offset and length to avoid dirty data being returned
						const uint8Array: Uint8Array = new Uint8Array(concat.buffer, concat.byteOffset, concat.byteLength);
						
						resolve(uint8Array);
						return;
					}

					resolve(stringData);
				});
			});
			
			request.on('error', (e: Error): void => {
				// these are underpinning errors like socket errors rather than HTTP response codes (404, 500 etc.)
				
				if (timedOut) return;	// ignore duplicate timeout error throws
				
				reject(e);
			});
			
			if (options.timeout) {
				request.on('timeout', (): void => {
					timedOut = true;	// prevent duplicate timeout error throws
					
					reject(new CommonsHttpTimeoutError());
					
					// do this after, as it might cause an error to be thrown itself
					// it's a clean-up more than anything else
					request.destroy();
				});
				
				request.setTimeout(options.timeout);
			}
	
			request.write(bodyString);
			request.end();
		});
	}
	
	private static requestHeadGetDelete(
			method: ECommonsHttpMethod.HEAD | ECommonsHttpMethod.GET | ECommonsHttpMethod.DELETE,
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): Promise<string|Uint8Array|void> {
		const protocol: TProtocol = url.match(/^https:/) ? https : http;

		if (params && Object.keys(params).length > 0) {
			url = commonsHttpAppendEncodedObjectParams(url, params);
		}
		
		const urlObject: URL = new URL(url);
		const options: TCommonsHttpRequest = {
				protocol: urlObject.protocol,
				hostname: urlObject.hostname,
				port: parseInt(urlObject.port, 10),
				path: urlObject.search ? `${urlObject.pathname}${urlObject.search}` : urlObject.pathname,
				method: method,
				headers: headers,
				timeout: timeout
		};

		return CommonsInternalHttpClientImplementation.request(
				protocol,
				options,
				'',
				returnResult,
				responseDataType
		);
	}

	private static requestPostPutPatch(
			method: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
			url: string,
			body: TEncodedObject,
			contentType: ECommonsHttpContentType,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): Promise<string|Uint8Array|void> {
		const protocol: TProtocol = url.match(/^https:/) ? https : http;

		if (params && Object.keys(params).length > 0) {
			url = commonsHttpAppendEncodedObjectParams(url, params);
		}
		
		const urlObject: URL = new URL(url);
		const options: TCommonsHttpRequest = {
				protocol: urlObject.protocol,
				hostname: urlObject.hostname,
				port: parseInt(urlObject.port, 10),
				path: urlObject.search ? `${urlObject.pathname}${urlObject.search}` : urlObject.pathname,
				method: method,
				headers: headers,
				timeout: timeout
		};

		let bodyData: string = '';
		switch (contentType) {
			case ECommonsHttpContentType.JSON:
				bodyData = JSON.stringify(body);
				break;
			case ECommonsHttpContentType.FORM_URL:
				bodyData = formurlencoded(body);
				break;
			default:
				throw new Error('Unknown encoding type');
		}
		
		// we can't do this in the tscommons-es-http, as formurlencoded isn't used in Angular
		options.headers['Content-Length'] = Buffer.byteLength(bodyData, 'utf-8').toString(10);

		return this.request(
				protocol,
				options,
				bodyData,
				returnResult,
				responseDataType
		);
	}

	public async internalHead(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			options: TCommonsHttpInternalRequestOptions
	): Promise<void> {
		await CommonsInternalHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.HEAD,
				url,
				params,
				headers,
				false,
				ECommonsHttpResponseDataType.UINT8ARRAY,	// na
				options.timeout
		);
	}

	public internalGet(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.GET,
				url,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		) as Promise<string|Uint8Array>;
	}

	public internalDelete(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.DELETE,
				url,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		) as Promise<string|Uint8Array>;
	}
	
	public internalPost<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.POST,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		) as Promise<string|Uint8Array>;
	}
	
	public internalPut<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.PUT,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		) as Promise<string|Uint8Array>;
	}
	
	public internalPatch<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.PATCH,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		) as Promise<string|Uint8Array>;
	}
}
