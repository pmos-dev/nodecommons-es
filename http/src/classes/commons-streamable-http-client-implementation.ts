import { URL } from 'url';
import * as https from 'https';
import * as http from 'http';

import { Subject } from 'rxjs';

import { default as formurlencoded } from 'form-urlencoded';

import { TEncodedObject } from 'tscommons-es-core';
import { CommonsHttpError, ICommonsStreamableHttpClientImplementation, TCommonsStreamableHttpObservable } from 'tscommons-es-http';
import { CommonsHttpTimeoutError } from 'tscommons-es-http';
import { commonsHttpBuildErrorFromResponseCode } from 'tscommons-es-http';
import { THttpHeaderOrParamObject } from 'tscommons-es-http';
import { TCommonsHttpInternalRequestOptions } from 'tscommons-es-http';
import { ECommonsHttpContentType } from 'tscommons-es-http';
import { ECommonsHttpResponseDataType } from 'tscommons-es-http';
import { ECommonsHttpMethod } from 'tscommons-es-http';

import { commonsHttpAppendEncodedObjectParams } from '../helpers/commons-http-query-string';

import { TCommonsHttpRequest } from '../types/tcommons-http-request';

type TProtocol = typeof http | typeof https;

export class CommonsStreamableHttpClientImplementation implements ICommonsStreamableHttpClientImplementation {
	private static request(
			protocol: TProtocol,
			options: TCommonsHttpRequest,
			bodyString: string,
			responseDataType: ECommonsHttpResponseDataType
	): TCommonsStreamableHttpObservable {
		const dataSubject: Subject<string|Uint8Array> = new Subject<string|Uint8Array>();
		const outcomeSubject: Subject<true|CommonsHttpError> = new Subject<true|CommonsHttpError>();

		const promise: Promise<true|CommonsHttpError> = new Promise<true|CommonsHttpError>((resolve: (outcome: true|CommonsHttpError) => void, reject: (e: Error) => void): void => {
			let hasCompleted: boolean = false;
			const complete: () => void = (): void => {
				if (hasCompleted) return;

				try {
					dataSubject.complete();
					outcomeSubject.complete();
				} finally {
					hasCompleted = true;
				}
			};

			const completeOutcome: (outcome: true|CommonsHttpError) => void = (outcome: true|CommonsHttpError): void => {
				resolve(outcome);
				complete();
			};

			const completeError: (error: Error) => void = (error: Error): void => {
				reject(error);
				complete();
			};

			let timedOut: boolean = false;

			const request: http.ClientRequest = protocol.request(options, (res: http.IncomingMessage): void => {
				if (responseDataType === ECommonsHttpResponseDataType.STRING) res.setEncoding('utf8');
				if (options.timeout) res.setTimeout(options.timeout);
				
				res.on('data', (chunk: any): void => {	// the any type has is actually the Node type's official type
					if (responseDataType === ECommonsHttpResponseDataType.UINT8ARRAY) {
						dataSubject.next(Buffer.from(chunk));	// eslint-disable-line @typescript-eslint/no-unsafe-argument
					} else {
						dataSubject.next(chunk as string);
					}
				});
				
				res.on('end', (): void => {
					if (res.statusCode && res.statusCode >= 400) {
						completeOutcome(commonsHttpBuildErrorFromResponseCode(res.statusCode, ''));
					} else {
						completeOutcome(true);
					}
				});
			});
			
			request.on('error', (e: Error): void => {
				// these are underpinning errors like socket errors rather than HTTP response codes (404, 500 etc.)
				
				if (timedOut) return;	// ignore duplicate timeout error throws

				completeError(e);
			});
			
			if (options.timeout) {
				request.on('timeout', (): void => {
					timedOut = true;	// prevent duplicate timeout error throws
					
					completeError(new CommonsHttpTimeoutError());
					
					// do this after, as it might cause an error to be thrown itself
					// it's a clean-up more than anything else
					request.destroy();
				});
				
				request.setTimeout(options.timeout);
			}

			request.write(bodyString);
			request.end();
		});

		return {
				dataStream: dataSubject,
				outcome: promise
		};
	}
	
	private static requestHeadGetDelete(
			method: ECommonsHttpMethod.HEAD | ECommonsHttpMethod.GET | ECommonsHttpMethod.DELETE,
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): TCommonsStreamableHttpObservable {
		const protocol: TProtocol = url.match(/^https:/) ? https : http;

		if (params && Object.keys(params).length > 0) {
			url = commonsHttpAppendEncodedObjectParams(url, params);
		}
		
		const urlObject: URL = new URL(url);
		const options: TCommonsHttpRequest = {
				protocol: urlObject.protocol,
				hostname: urlObject.hostname,
				port: parseInt(urlObject.port, 10),
				path: urlObject.search ? `${urlObject.pathname}${urlObject.search}` : urlObject.pathname,
				method: method,
				headers: headers,
				timeout: timeout
		};

		return CommonsStreamableHttpClientImplementation.request(
				protocol,
				options,
				'',
				responseDataType
		);
	}

	private static requestPostPutPatch(
			method: ECommonsHttpMethod.POST | ECommonsHttpMethod.PUT | ECommonsHttpMethod.PATCH,
			url: string,
			body: TEncodedObject,
			contentType: ECommonsHttpContentType,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): TCommonsStreamableHttpObservable {
		const protocol: TProtocol = url.match(/^https:/) ? https : http;

		if (params && Object.keys(params).length > 0) {
			url = commonsHttpAppendEncodedObjectParams(url, params);
		}
		
		const urlObject: URL = new URL(url);
		const options: TCommonsHttpRequest = {
				protocol: urlObject.protocol,
				hostname: urlObject.hostname,
				port: parseInt(urlObject.port, 10),
				path: urlObject.search ? `${urlObject.pathname}${urlObject.search}` : urlObject.pathname,
				method: method,
				headers: headers,
				timeout: timeout
		};

		let bodyData: string = '';
		switch (contentType) {
			case ECommonsHttpContentType.JSON:
				bodyData = JSON.stringify(body);
				break;
			case ECommonsHttpContentType.FORM_URL:
				bodyData = formurlencoded(body);
				break;
			default:
				throw new Error('Unknown encoding type');
		}
		
		// we can't do this in the tscommons-es-http, as formurlencoded isn't used in Angular
		options.headers['Content-Length'] = Buffer.byteLength(bodyData, 'utf-8').toString(10);

		return this.request(
				protocol,
				options,
				bodyData,
				responseDataType
		);
	}

	public internalHead(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.HEAD,
				url,
				params,
				headers,
				ECommonsHttpResponseDataType.UINT8ARRAY,	// na
				options.timeout
		);
	}

	public internalGet(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.GET,
				url,
				params,
				headers,
				responseDataType,
				options.timeout
		);
	}

	public internalDelete(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestHeadGetDelete(
				ECommonsHttpMethod.DELETE,
				url,
				params,
				headers,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPost<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.POST,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPut<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.PUT,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPatch<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): TCommonsStreamableHttpObservable {
		return CommonsStreamableHttpClientImplementation.requestPostPutPatch(
				ECommonsHttpMethod.PATCH,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				responseDataType,
				options.timeout
		);
	}
}
