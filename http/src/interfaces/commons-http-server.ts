// This is a true interface

import { TCommonsHttpHandler } from '../types/tcommons-http-handler';
import { TCommonsHttpResponse } from '../types/tcommons-http-response';

import { ICommonsStrictParamsRequest } from './icommons-strict-params-request';

export interface CommonsStrictHttpServer<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> {
	head(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	get(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	post(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	put(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	patch(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	delete(
			query: string,
			handler: TCommonsHttpHandler<RequestT, ResponseT>
	): void;

	init(): void;
	listen(): Promise<void>;
	close(): void;
}
