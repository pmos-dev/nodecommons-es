import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyStringOrUndefined
} from 'tscommons-es-core';

export interface ICommonsHttpConfig {
		port: number;
		path?: string;
}

export function isICommonsHttpConfig(test: any): test is ICommonsHttpConfig {
	if (!commonsTypeHasPropertyNumber(test, 'port')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'path')) return false;

	return true;
}
