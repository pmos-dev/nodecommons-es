// fudge hacks for the fact tha express.Request doesn't seem to have well-defined definitions for these, and so is hard to integrate into ICommonsStrictParamsRequest at a generic level
// Some of this is because express.Request relies on things like bodyparser and the like
export interface ICommonsRequestWithGetMethodTypecast {
	get(field: string): string|undefined;
}

export interface ICommonsRequestWithBodyTypecast {
	body: { [ field: string ]: string|number|boolean|null };
}

export interface ICommonsRequestWithQueryTypecast {
	query: { [ field: string ]: string|number|boolean };
}
