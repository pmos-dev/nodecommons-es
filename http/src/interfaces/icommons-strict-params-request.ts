import { TCommonsStrictParamPropertyObject } from '../types/tcommons-strict-param-property-object';

export interface ICommonsStrictParamsRequest {
		strictParams: TCommonsStrictParamPropertyObject;
}
