export enum ECommonsStrictParamPlurality {
		SINGLE = 'single',
		ARRAY = 'array',
		ANY = 'any'
}
