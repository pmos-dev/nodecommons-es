export enum ECommonsStrictParamType {
		ID = 'id',
		IDNAME = 'idname',
		INT = 'int',
		BASE36 = 'base36',
		BASE62 = 'base62',
		BASE62LONG = 'base62long',
		YMD = 'ymd',
		YMDHIS = 'ymdhis'
}

export function toECommonsStrictParamType(type: string): ECommonsStrictParamType|undefined {
	switch (type) {
		case ECommonsStrictParamType.ID.toString():
			return ECommonsStrictParamType.ID;
		case ECommonsStrictParamType.IDNAME.toString():
			return ECommonsStrictParamType.IDNAME;
		case ECommonsStrictParamType.INT.toString():
			return ECommonsStrictParamType.INT;
		case ECommonsStrictParamType.BASE36.toString():
			return ECommonsStrictParamType.BASE36;
		case ECommonsStrictParamType.BASE62.toString():
			return ECommonsStrictParamType.BASE62;
		case ECommonsStrictParamType.BASE62LONG.toString():
			return ECommonsStrictParamType.BASE62LONG;
		case ECommonsStrictParamType.YMD.toString():
			return ECommonsStrictParamType.YMD;
		case ECommonsStrictParamType.YMDHIS.toString():
			return ECommonsStrictParamType.YMDHIS;
		default:
			return undefined;
	}
}

export function fromECommonsStrictParamType(type: ECommonsStrictParamType): string {
	switch (type) {
		case ECommonsStrictParamType.ID:
			return ECommonsStrictParamType.ID.toString();
		case ECommonsStrictParamType.IDNAME:
			return ECommonsStrictParamType.IDNAME.toString();
		case ECommonsStrictParamType.INT:
			return ECommonsStrictParamType.INT.toString();
		case ECommonsStrictParamType.BASE36:
			return ECommonsStrictParamType.BASE36.toString();
		case ECommonsStrictParamType.BASE62:
			return ECommonsStrictParamType.BASE62.toString();
		case ECommonsStrictParamType.BASE62LONG:
			return ECommonsStrictParamType.BASE62LONG.toString();
		case ECommonsStrictParamType.YMD:
			return ECommonsStrictParamType.YMD.toString();
		case ECommonsStrictParamType.YMDHIS:
			return ECommonsStrictParamType.YMDHIS.toString();
	}
	
	throw new Error('Unknown ECommonsStrictParamType');
}
