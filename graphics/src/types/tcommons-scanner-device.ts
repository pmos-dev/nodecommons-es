export type TCommonsScannerDevice = {
		name: string;
		vendor: string;
		model: string;
		type: string;
		index: string;
};
