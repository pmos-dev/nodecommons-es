import { CommonsByteWriter } from 'nodecommons-es-data';

enum EIfdEntryType {
	BYTE = 1,
	ASCII = 2,
	SHORT = 3,
	LONG = 4,
	RATIONAL = 5
}

type TIfdEntry = {
	tag: number;
	type: EIfdEntryType;
	count: number;
	valueOffset: number;
};

type TStripOffsetAndCount = {
	offset: number;
	count: number;
};

export class CommonsTiffWriter extends CommonsByteWriter {
	private dataOffset: number = 256;
	private stripOffset: number = 512;
	private imageOffset: number = 1024;

	constructor(
			private samples: number[],
			private width: number,
			private height: number,
			private bitsPerSample: number = 8,
			private samplesPerPixel: number = 3,
			private rowsPerStrip: number = 128,
			private xResolution: number = 300,
			private yResolution: number = 300
	) {
		super(1024 * 100);

		this.remember();

		this.jump(this.dataOffset);
		this.writeUInt32LE(0);	// just to make a visual break, not really needed
		this.dataOffset = this.getOffset();

		this.jump(this.stripOffset);
		this.writeUInt32LE(0);	// just to make a visual break, not really needed
		this.stripOffset = this.getOffset();

		this.jump(this.imageOffset);
		this.writeUInt32LE(0);	// just to make a visual break, not really needed
		this.imageOffset = this.getOffset();

		this.recall();
	}

	private writeURational(value: number): void {
		if (value < 0) throw new Error('Signed rationals not supported');

		if (Math.floor(value) !== value) throw new Error('Actual rationals are not supported yet, only fake integers');

		this.writeUInt32LE(value);
		this.writeUInt32LE(1);
	}

	private tiffIfdEntryValue(
			tag: number,
			value: number
	): TIfdEntry {
		if (value < 65536) {
			return {
					tag: tag,
					type: EIfdEntryType.SHORT,
					count: 1,
					valueOffset: value
			};
		}
		if (value < Math.pow(256, 4)) {
			return {
					tag: tag,
					type: EIfdEntryType.LONG,
					count: 1,
					valueOffset: value
			};
		}
		throw new Error('Value overflow');
	}

	private tiffIfdEntryArray(
			tag: number,
			values: number[]
	): TIfdEntry {
		if (values.length === 0) throw new Error('Array is empty');

		let max: number = 0;
		for (const v of values) max = Math.max(max, v);

		if (max < 65536) {
			if (values.length <= 2) {
				let value: number = values[0];
				if (values.length === 2) {
					value = (values[1] * 65536) + values[0];
				}
				return {
						tag: tag,
						type: EIfdEntryType.SHORT,
						count: values.length,
						valueOffset: value
				};
			}

			const dataStart: number = this.dataOffset;

			this.remember();
			this.jump(this.dataOffset);

			for (const v of values) this.writeUInt16LE(v);

			this.dataOffset = this.getOffset();
			this.recall();

			return {
					tag: tag,
					type: EIfdEntryType.SHORT,
					count: values.length,
					valueOffset: dataStart
			};
		}
		if (max < Math.pow(256, 4)) {
			const dataStart: number = this.dataOffset;

			this.remember();
			this.jump(this.dataOffset);

			for (const v of values) this.writeUInt32LE(v);

			this.dataOffset = this.getOffset();
			this.recall();

			return {
					tag: tag,
					type: EIfdEntryType.LONG,
					count: values.length,
					valueOffset: dataStart
			};
		}
		throw new Error('Value overflow');
	}

	private tiffIfdEntryRational(
			tag: number,
			value: number
	): TIfdEntry {
		const dataStart: number = this.dataOffset;

		this.remember();
		this.jump(this.dataOffset);

		this.writeURational(value);

		this.dataOffset = this.getOffset();
		this.recall();

		return {
				tag: tag,
				type: EIfdEntryType.RATIONAL,
				count: 1,
				valueOffset: dataStart
		};
	}

	private imageWidth(width: number): TIfdEntry {
		return this.tiffIfdEntryValue(256, width);
	}

	private imageLength(length: number): TIfdEntry {
		return this.tiffIfdEntryValue(257, length);
	}
	
	private imageBitsPerSample(bits: number): TIfdEntry {
		return this.tiffIfdEntryArray(258, [ bits, bits, bits ]);
	}

	private imageStripOffsets(stripOffsetAndCounts: TStripOffsetAndCount[]): TIfdEntry {
		const offsets: number[] = stripOffsetAndCounts
				.map((stripOffsetAndCount: TStripOffsetAndCount): number => stripOffsetAndCount.offset);

		return this.tiffIfdEntryArray(273, offsets);
	}

	private imageStripCounts(stripOffsetAndCounts: TStripOffsetAndCount[]): TIfdEntry {
		const counts: number[] = stripOffsetAndCounts
				.map((stripOffsetAndCount: TStripOffsetAndCount): number => stripOffsetAndCount.count);
				
		return this.tiffIfdEntryArray(279, counts);
	}

	private writeEntry(entry: TIfdEntry): void {
		this.writeUInt16LE(entry.tag);
		this.writeUInt16LE(entry.type);
		this.writeUInt32LE(entry.count);
		this.writeUInt32LE(entry.valueOffset);
	}
	
	private writeIfd(stripOffsetAndCounts: TStripOffsetAndCount[]): void {
		const entries: TIfdEntry[] = [
				this.imageWidth(this.width),
				this.imageLength(this.height),
				this.imageBitsPerSample(this.bitsPerSample),
				this.tiffIfdEntryValue(259, 1),	// no compression
				this.tiffIfdEntryValue(262, 2),	// photometric interpretation
				this.imageStripOffsets(stripOffsetAndCounts),
				this.tiffIfdEntryValue(277, this.samplesPerPixel),
				this.tiffIfdEntryValue(278, this.rowsPerStrip),
				this.imageStripCounts(stripOffsetAndCounts),
				this.tiffIfdEntryRational(282, this.xResolution),	// x resolution
				this.tiffIfdEntryRational(283, this.yResolution),	// y resolution
				this.tiffIfdEntryValue(284, 1),	// planarconfiguration?
				this.tiffIfdEntryValue(296, 2)	// inch, i.e. dpi
		];

		this.writeUInt16LE(entries.length);

		for (const entry of entries) {
			this.writeEntry(entry);
		}
	}

	private writeStrips(strips: number[][]): TStripOffsetAndCount[] {
		this.remember();
		this.jump(this.imageOffset);

		const bytesPerSample: number = this.bitsPerSample / 8;

		const offsetAndCounts: TStripOffsetAndCount[] = [];
		for (const strip of strips) {
			offsetAndCounts.push({
					offset: this.getOffset(),
					count: strip.length * bytesPerSample
			});

			for (const b of strip) {
				switch (this.bitsPerSample) {
					case 8:
						this.writeUInt8(b);
						break;
					case 16:
						this.writeUInt16LE(b);
						break;
					case 32:
						this.writeUInt32LE(b);
						break;
					default:
						throw new Error('Unsupported bit depth');
				}
			}
		}

		this.recall();

		return offsetAndCounts;
	}

	public generate(): void {
		// little endian
		this.writeUInt8(0x49);
		this.writeUInt8(0x49);

		// magic number for TIFF
		this.writeUInt16LE(42);

		// IFD offset (fixed)
		this.writeUInt32LE(8);

		const samplesCopy: number[] = this.samples.slice();

		const strips: number[][] = [];
		for (let s = Math.ceil(this.height / this.rowsPerStrip); s-- > 0;) {
			strips.push(samplesCopy.splice(0, Math.min(samplesCopy.length, this.width * this.samplesPerPixel * this.rowsPerStrip)));
		}

		const stripOffsetAndCounts: TStripOffsetAndCount[] = this.writeStrips(strips);

		this.writeIfd(stripOffsetAndCounts);
		this.writeUInt32LE(0);	// no next ifd
	}
}
