import * as Jimp from 'jimp';

import {
		commonsMatrixBuild,
		commonsMatrixMirror
} from 'tscommons-es-core';
import { IRgba } from 'tscommons-es-graphics';
import { ERgba } from 'tscommons-es-graphics';

export class CommonsJimp {
	protected image: Jimp|undefined;
	
	public getRawJimpImage(): Jimp {
		if (!this.image) throw new Error('Image not loaded');
		
		return this.image;
	}
	
	public setRawJimpImage(image: Jimp): void {
		this.image = image;
	}
	
	public async loadFromFile(path: string): Promise<void> {
		this.image = await Jimp.default.read(path);
	}
	
	public async loadFromBuffer(buffer: Buffer): Promise<void> {
		this.image = await Jimp.default.read(buffer);
	}
	
	public createNew(
			width: number,
			height: number,
			background?: number
	): Promise<void> {
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			// it may be possible to supply undefined to the constructor, not sure. Use an explicit different overload instead for now.
			 
			if (background !== undefined) {
				this.image = new Jimp.default(
						width,
						height,
						background,
						(err: Error, _jimp: Jimp): void => {
							if (err) return reject(err);

							_jimp.background(
									background,
									(_err2: Error|null, _jimp2: Jimp): void => {
										resolve();
									});
						}
				);
				this.image.background(background);
			} else {
				this.image = new Jimp.default(
						width,
						height,
						(err: Error, _jimp: Jimp): void => {
							if (err) return reject(err);
							resolve();
						}
				);
			}
		});
	}
	
	public clone(): CommonsJimp {
		if (!this.image) throw new Error('Image not loaded');

		const clone: CommonsJimp = new CommonsJimp();
		clone.image = this.image.clone();
		
		return clone;
	}
	
	public getWidth(): number {
		if (!this.image) throw new Error('Image not loaded');
		
		return this.image.getWidth();
	}
	
	public getHeight(): number {
		if (!this.image) throw new Error('Image not loaded');
		
		return this.image.getHeight();
	}
	
	public background(
			color: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.background(
							color,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public invert(): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.invert(
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public crop(
			x: number,
			y: number,
			width: number,
			height: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.crop(
							x,
							y,
							width,
							height,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public resize(
			width: number,
			height: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.resize(
							width,
							height,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public rotate(
			angle: number,
			autoResize: boolean = true
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.rotate(
							angle,
							autoResize,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public flip(
			horizontal: boolean,
			vertical: boolean
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.flip(
							horizontal,
							vertical,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public contrast(factor: number): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.contrast(
							factor,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public brightness(adjustment: number): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.brightness(
							adjustment,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public blit(
			src: CommonsJimp,
			x: number,
			y: number,
			srcx?: number,
			srcy?: number,
			width?: number,
			height?: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			// annoyingly it doesn't accept optional for all srcx/srcy/width/height, just all or none, so have to use a check here
			if (srcx !== undefined && srcy !== undefined && width !== undefined && height !== undefined) {
				this.image!
						.blit(
								src.image!,
								x,
								y,
								srcx,
								srcy,
								width,
								height,
								(err: Error|null, _jimp: Jimp): void => {
									if (err) return reject(err);
									resolve();
								}
						);
			} else {
				this.image!
						.blit(
								src.image!,
								x,
								y,
								(err: Error|null, _jimp: Jimp): void => {
									if (err) return reject(err);
									resolve();
								}
						);
			}
		});
	}
	
	public async grow(
			distance: number,
			background?: number
	): Promise<void> {
		const replacement: CommonsJimp = new CommonsJimp();
		await replacement.createNew(
				this.getWidth() + (distance * 2),
				this.getHeight() + (distance * 2),
				background
		);
		
		await replacement.blit(
				this,
				distance,
				distance
		);
		
		this.image = replacement.image;
	}

	public async sharpen(amount: number = 0.125): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.convolute(
							[ [ 0, -amount, 0 ], [ -amount, 1 + (amount * 4), -amount ], [ 0, -amount, 0 ] ],
							(err: Error|null, _jimp: unknown): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}

	public absDiff(comparator: CommonsJimp, fixAlpha: number = 255): Promise<void> {
		// there is a built-in diff function, but it isn't very efficient

		if (!this.image) throw new Error('Image not loaded');
		if (!comparator.image) throw new Error('Comparator image not loaded');

		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			if (
				this.image!.getWidth() !== comparator.getWidth()
				|| this.image!.getHeight() !== comparator.getHeight()
			) return reject(new Error('Original and comparator image are not the same size'));

			this.image!
					.scan(
							0,
							0,
							this.image!.bitmap.width,
							this.image!.bitmap.height,
							(
									_x: number,
									_y: number,
									i: number
							): void => {
								this.image!.bitmap.data[i] = Math.abs(this.image!.bitmap.data[i] - comparator.image!.bitmap.data[i]);
								this.image!.bitmap.data[++i] = Math.abs(this.image!.bitmap.data[i] - comparator.image!.bitmap.data[i]);
								this.image!.bitmap.data[++i] = Math.abs(this.image!.bitmap.data[i] - comparator.image!.bitmap.data[i]);
								this.image!.bitmap.data[++i] = fixAlpha === undefined ? Math.abs(this.image!.bitmap.data[i] - comparator.image!.bitmap.data[i]) : fixAlpha;
							},
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public absDiffAverage(comparator: CommonsJimp, ignoreAlpha: boolean = true): Promise<number> {
		// there is a built-in diff function, but it isn't very efficient

		if (!this.image) throw new Error('Image not loaded');
		if (!comparator.image) throw new Error('Comparator image not loaded');

		return new Promise((resolve: (average: number) => void, reject: (e: Error) => void): void => {
			if (
				this.image!.getWidth() !== comparator.getWidth()
				|| this.image!.getHeight() !== comparator.getHeight()
			) return reject(new Error('Original and comparator image are not the same size'));

			let diff: number = 0;
			const size: number = this.image!.getWidth() * this.image!.getHeight();
			this.image!
					.scan(
							0,
							0,
							this.image!.bitmap.width,
							this.image!.bitmap.height,
							(
									_x: number,
									_y: number,
									i: number
							): void => {
								let pixel: number = (
									Math.abs(this.image!.bitmap.data[i] - comparator.image!.bitmap.data[i])
									+ Math.abs(this.image!.bitmap.data[++i] - comparator.image!.bitmap.data[i])
									+ Math.abs(this.image!.bitmap.data[++i] - comparator.image!.bitmap.data[i])
								);

								if (!ignoreAlpha) pixel += Math.abs(this.image!.bitmap.data[++i] - comparator.image!.bitmap.data[i]);

								pixel /= ignoreAlpha ? 3 : 4;
								diff += pixel / size;
							},
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve(diff);
							}
					);
		});
	}
	
	public toRgba(): Promise<IRgba[]> {
		if (!this.image) throw new Error('Image not loaded');

		return new Promise((resolve: (result: IRgba[]) => void, reject: (e: Error) => void): void => {
			const data: IRgba[] = [];
			this.image!
					.scan(
							0,
							0,
							this.image!.bitmap.width,
							this.image!.bitmap.height,
							(
									_x: number,
									_y: number,
									i: number
							): void => {
								data.push({
										[ ERgba.RED ]: this.image!.bitmap.data[i + 0],
										[ ERgba.GREEN ]: this.image!.bitmap.data[i + 1],
										[ ERgba.BLUE ]: this.image!.bitmap.data[i + 2],
										[ ERgba.ALPHA ]: this.image!.bitmap.data[i + 3]
								});
							},
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve(data);
							}
					);
		});
	}
	
	public fromRgba(rgba: IRgba[]): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			if ((this.image!.bitmap.width * this.image!.bitmap.height) !== rgba.length) return reject(new Error('Dimension/array length mismatch'));
			
			let index: number = 0;
			this.image!
					.scan(
							0,
							0,
							this.image!.bitmap.width,
							this.image!.bitmap.height,
							(
									_x: number,
									_y: number,
									i: number
							): void => {
								this.image!.bitmap.data[i + 0] = rgba[index][ERgba.RED];
								this.image!.bitmap.data[i + 1] = rgba[index][ERgba.GREEN];
								this.image!.bitmap.data[i + 2] = rgba[index][ERgba.BLUE];
								this.image!.bitmap.data[i + 3] = rgba[index][ERgba.ALPHA];
								index++;
							},
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public toRgbaXyMatrix(): Promise<IRgba[][]> {
		if (!this.image) throw new Error('Image not loaded');

		return new Promise((resolve: (result: IRgba[][]) => void, reject: (e: Error) => void): void => {
			const matrix: IRgba[][] = commonsMatrixBuild<IRgba>(
					this.getWidth(),
					this.getHeight()
			);
			
			this.image!
					.scan(
							0,
							0,
							this.image!.bitmap.width,
							this.image!.bitmap.height,
							(
									x: number,
									y: number,
									i: number
							): void => {
								matrix[x][y] = {
										[ ERgba.RED ]: this.image!.bitmap.data[i + 0],
										[ ERgba.GREEN ]: this.image!.bitmap.data[i + 1],
										[ ERgba.BLUE ]: this.image!.bitmap.data[i + 2],
										[ ERgba.ALPHA ]: this.image!.bitmap.data[i + 3]
								};
							},
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve(matrix);
							}
					);
		});
	}
	
	public async toRgbaYxMatrix(): Promise<IRgba[][]> {
		const xyMatrix: IRgba[][] = await this.toRgbaXyMatrix();

		return commonsMatrixMirror<IRgba>(xyMatrix);
	}
	
	public async toLinearRgba(): Promise<number[]> {
		const rgbas: IRgba[] = await this.toRgba();
		
		const linear: number[] = [];
		for (const rgba of rgbas) {
			linear.push(...[
					rgba[ERgba.RED],
					rgba[ERgba.GREEN],
					rgba[ERgba.BLUE],
					rgba[ERgba.ALPHA]
			]);
		}
		
		return linear;
	}
	
	public async toLinearRgb(): Promise<number[]> {
		const rgbas: IRgba[] = await this.toRgba();
		
		const linear: number[] = [];
		for (const rgba of rgbas) {
			linear.push(...[
					rgba[ERgba.RED],
					rgba[ERgba.GREEN],
					rgba[ERgba.BLUE]
			]);
		}
		
		return linear;
	}

	// NB, these appear to actually be slower when performance measured?
	public toLinearRgbaViaOptimisedDirectRawBufferAccess(): number[] {
		const imageBuffer: Buffer = this.getRawJimpImage().bitmap.data;

		const originalLength: number = imageBuffer.length;
		if ((originalLength % 4) !== 0) throw new Error('Buffer length is not divisible by 4');

		const newBuffer: Buffer = Buffer.allocUnsafe(originalLength);
		for (let j = originalLength, k = originalLength - 4; k >= 0; j -= 4, k -= 4) {
			imageBuffer.copy(newBuffer, k, j - 4, j);
		}

		return Array.from<number>(newBuffer);
	}

	// NB, these appear to actually be slower when performance measured?
	public toLinearRgbViaOptimisedDirectRawBufferAccess(): number[] {
		const imageBuffer: Buffer = this.getRawJimpImage().bitmap.data;

		const originalLength: number = imageBuffer.length;
		if ((originalLength % 4) !== 0) throw new Error('Buffer length is not divisible by 4');
		const newSize: number = imageBuffer.length * (3 / 4);

		const newBuffer: Buffer = Buffer.allocUnsafe(newSize);
		for (let j = originalLength, k = newSize - 3; k >= 0; j -= 4, k -= 3) {
			imageBuffer.copy(newBuffer, k, j - 4, j - 1);
		}

		return Array.from<number>(newBuffer);
	}
	
	public quality(quality: number): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.quality(
							quality,
							(err: Error|null, _jimp: Jimp): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}
	
	public async save(
			path: string,
			jpegQuality?: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');
		
		if (/\.(jpg|jpeg)$/.test(path) && jpegQuality) await this.quality(jpegQuality);
		
		await this.image.writeAsync(path);
	}
}
