import * as child_process from 'child_process';

import { TCommonsScannerDevice } from '../types/tcommons-scanner-device';

import { ECommonsScanMode, fromECommonsScanMode } from '../enums/ecommons-scan-mode';

export class CommonsScanner {
	public static listDevices(): Promise<TCommonsScannerDevice[]> {
		return new Promise((resolve: (devices: TCommonsScannerDevice[]) => void, reject: (reason: Error) => void) => {
			const scanimage: child_process.ChildProcessWithoutNullStreams = child_process.spawn(
					'scanimage',
					[ '--formatted-device-list={"name":"%d","vendor":"%v","model":"%m","type":"%t","index":"%i"}%n' ]
			);
			
			scanimage.stdout.once('error', reject);

			let result: string = '';
			scanimage.stdout.on(
					'data',
					(chunk: string|Buffer): void => {
						result += chunk.toString();
					}
			);
			scanimage.stdout.once(
					'end',
					(): void => {
						if (!result) return resolve([]);
						
						const devices: TCommonsScannerDevice[] = result
								.split('\n')
								.filter(Boolean)
								.map((line: string): TCommonsScannerDevice => JSON.parse(line) as TCommonsScannerDevice);
						
						return resolve(devices);
					}
			);
		});
	}
	
	private progressCallback: ((progress: number) => void)|undefined;
	
	constructor(
			private device: TCommonsScannerDevice
	) {}
	
	public setProgressCallback(callback: (progress: number) => void): void {
		this.progressCallback = callback;
	}
	
	public scan(
			mode: ECommonsScanMode = ECommonsScanMode.GRAY,
			resolution: number = 150,
			brightness: number = 0,
			format: string = 'png',
			height?: number,
			autoFeed?: boolean
	): Promise<Buffer> {
		const args: string[] = [
				`--device=${this.device.name}`,
				`--mode=${fromECommonsScanMode(mode)}`,
				`--resolution=${resolution.toString()}`,
				`--brightness=${brightness.toString()}`
		];
		if (height) args.push(`-y ${Math.floor(height).toString(10)}mm`);
		if (autoFeed !== undefined) args.push(`--autofeed=${autoFeed ? 'yes' : 'no'}`);

		args.push(...[
				`--format=${format}`,
				'--progress'
		]);

		return new Promise((resolve: (image: Buffer) => void, reject: (reason: Error) => void) => {
			const scanimage: child_process.ChildProcessWithoutNullStreams = child_process.spawn(
					'scanimage',
					args
			);
			
			scanimage.stdout.once('error', reject);

			const data: Buffer[] = [];
	
			scanimage.stdout.on(
					'data',
					(chunk: Buffer): void => {
						data.push(chunk);
					}
			);
			scanimage.stdout.once(
					'end',
					(): void => {
						const buffer: Buffer = Buffer.concat(data);
						return resolve(buffer);
					}
			);

			scanimage.stderr.on(
					'data',
					(chunk: Buffer): void => {
						const s: string = chunk.toString();
						const result: RegExpExecArray|null = /^Progress: ([0-9.]+)%$/.exec(s.trim());
						
						if (result && this.progressCallback) this.progressCallback(parseFloat(result[1]) / 100);
					}
			);
		});
	}
}
