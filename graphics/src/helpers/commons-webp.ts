import * as child_process from 'child_process';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { commonsFileExists, commonsFileReadBinaryFileAsync, commonsFileWriteBinaryFileAsync } from 'nodecommons-es-file';

import { ECommonsWebpPreset, fromECommonsWebpPreset } from '../enums/ecommons-webp-preset';

export function commonsWebpEncodeFile(
		srcFile: string,
		destFile: string,
		quality: number = 75,
		preset: ECommonsWebpPreset = ECommonsWebpPreset.DEFAULT,
		compressionMethod: number = 4,
		multiThreaded: boolean = false,
		autoFilter: boolean = false
): Promise<void> {
	if (/[^-./a-z0-9_]/i.test(srcFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the src filename');
	if (/[^-./a-z0-9_]/i.test(destFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the dest filename');

	if (!commonsFileExists(srcFile)) throw new Error(`${srcFile} does not exist`);

	const args: string[] = [
			`-preset ${fromECommonsWebpPreset(preset)}`,
			`-q ${quality.toString(10)}`,
			`-m ${compressionMethod.toString(10)}`
	];
	if (multiThreaded) args.push('-mt');
	if (autoFilter) args.push('-af');

	args.push(srcFile);
	args.push(`-o ${destFile}`);

	return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
		child_process.exec(
				`cwebp ${args.join(' ')}`,
				{},
				(error: child_process.ExecException|null, _stdout: string, _stderr: string): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export async function commonsWebpEncodeFileToBuffer(
		srcFile: string,
		quality: number = 75,
		preset: ECommonsWebpPreset = ECommonsWebpPreset.DEFAULT,
		compressionMethod: number = 4,
		multiThreaded: boolean = false,
		autoFilter: boolean = false
): Promise<Buffer> {
	if (/[^-./a-z0-9_]/i.test(srcFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the src filename');

	const tempFile: string = `/tmp/${commonsBase62GenerateRandomId()}.webp`;

	await commonsWebpEncodeFile(
			srcFile,
			tempFile,
			quality,
			preset,
			compressionMethod,
			multiThreaded,
			autoFilter
	);

	const output: Buffer|undefined = await commonsFileReadBinaryFileAsync(tempFile);
	if (!output) throw new Error('Error whilst converting the file');

	return output;
}

export async function commonsWebpEncodeBuffer(
		src: Buffer,
		quality: number = 75,
		preset: ECommonsWebpPreset = ECommonsWebpPreset.DEFAULT,
		compressionMethod: number = 4,
		multiThreaded: boolean = false,
		autoFilter: boolean = false,
		srcFormat: string = 'jpeg'
): Promise<Buffer> {
	const tempFile: string = `/tmp/${commonsBase62GenerateRandomId()}.${srcFormat}`;

	await commonsFileWriteBinaryFileAsync(tempFile, src);

	return await commonsWebpEncodeFileToBuffer(
			tempFile,
			quality,
			preset,
			compressionMethod,
			multiThreaded,
			autoFilter
	);
}
