import * as child_process from 'child_process';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { commonsFileExists, commonsFileReadBinaryFileAsync, commonsFileWriteBinaryFileAsync } from 'nodecommons-es-file';

export function commonsAvifEncodeFile(
		srcFile: string,
		destFile: string,
		quality: number = 60,
		speed: number = 4
): Promise<void> {
	if (/[^-./a-z0-9_]/i.test(srcFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the src filename');
	if (/[^-./a-z0-9_]/i.test(destFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the dest filename');

	if (!commonsFileExists(srcFile)) throw new Error(`${srcFile} does not exist`);

	const args: string[] = [
			`--quality ${quality.toString(10)}`,
			`--speed ${speed.toString(10)}`,
			'--overwrite',
			`--output ${destFile}`,
			srcFile
	];

	return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
		child_process.exec(
				`cavif ${args.join(' ')}`,
				{},
				(error: child_process.ExecException|null, _stdout: string, _stderr: string): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export async function commonsAvifEncodeFileToBuffer(
		srcFile: string,
		quality: number = 60,
		speed: number = 4
): Promise<Buffer> {
	if (/[^-./a-z0-9_]/i.test(srcFile)) throw new Error('Only alphanumeric characers and dot/hyphen/slash/underscore are allowed for the src filename');

	const tempFile: string = `/tmp/${commonsBase62GenerateRandomId()}.avif`;

	await commonsAvifEncodeFile(
			srcFile,
			tempFile,
			quality,
			speed
	);

	const output: Buffer|undefined = await commonsFileReadBinaryFileAsync(tempFile);
	if (!output) throw new Error('Error whilst converting the file');

	return output;
}

export async function commonsAvifEncodeBuffer(
		src: Buffer,
		quality: number = 60,
		speed: number = 4,
		srcFormat: string = 'jpeg'
): Promise<Buffer> {
	const tempFile: string = `/tmp/${commonsBase62GenerateRandomId()}.${srcFormat}`;

	await commonsFileWriteBinaryFileAsync(tempFile, src);

	return await commonsAvifEncodeFileToBuffer(
			tempFile,
			quality,
			speed
	);
}
