import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsWebpPreset {
		DEFAULT = 'default',
		PHOTO = 'photo',
		PICTURE = 'picture',
		DRAWING = 'drawing',
		ICON = 'icon',
		TEXT = 'text'
}

export function toECommonsWebpPreset(type: string): ECommonsWebpPreset|undefined {
	switch (type) {
		case ECommonsWebpPreset.DEFAULT.toString():
			return ECommonsWebpPreset.DEFAULT;
		case ECommonsWebpPreset.PHOTO.toString():
			return ECommonsWebpPreset.PHOTO;
		case ECommonsWebpPreset.PICTURE.toString():
			return ECommonsWebpPreset.PICTURE;
		case ECommonsWebpPreset.DRAWING.toString():
			return ECommonsWebpPreset.DRAWING;
		case ECommonsWebpPreset.ICON.toString():
			return ECommonsWebpPreset.ICON;
		case ECommonsWebpPreset.TEXT.toString():
			return ECommonsWebpPreset.TEXT;
	}
	return undefined;
}

export function fromECommonsWebpPreset(type: ECommonsWebpPreset): string {
	switch (type) {
		case ECommonsWebpPreset.DEFAULT:
			return ECommonsWebpPreset.DEFAULT.toString();
		case ECommonsWebpPreset.PHOTO:
			return ECommonsWebpPreset.PHOTO.toString();
		case ECommonsWebpPreset.PICTURE:
			return ECommonsWebpPreset.PICTURE.toString();
		case ECommonsWebpPreset.DRAWING:
			return ECommonsWebpPreset.DRAWING.toString();
		case ECommonsWebpPreset.ICON:
			return ECommonsWebpPreset.ICON.toString();
		case ECommonsWebpPreset.TEXT:
			return ECommonsWebpPreset.TEXT.toString();
	}
	
	throw new Error('Unknown ECommonsWebpPreset');
}

export function isECommonsWebpPreset(test: unknown): test is ECommonsWebpPreset {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsWebpPreset(test) !== undefined;
}

export function keyToECommonsWebpPreset(key: string): ECommonsWebpPreset {
	switch (key) {
		case 'DEFAULT':
			return ECommonsWebpPreset.DEFAULT;
		case 'PHOTO':
			return ECommonsWebpPreset.PHOTO;
		case 'PICTURE':
			return ECommonsWebpPreset.PICTURE;
		case 'DRAWING':
			return ECommonsWebpPreset.DRAWING;
		case 'ICON':
			return ECommonsWebpPreset.ICON;
		case 'TEXT':
			return ECommonsWebpPreset.TEXT;
	}
	
	throw new Error(`Unable to obtain ECommonsWebpPreset for key: ${key}`);
}

export const ECOMMONS_WEBP_PRESETS: ECommonsWebpPreset[] = Object.keys(ECommonsWebpPreset)
		.map((e: string): ECommonsWebpPreset => keyToECommonsWebpPreset(e));
