import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsScanMode {
		LINEART = 'lineart',
		GRAY = 'gray',
		COLOR = 'color'
}

export function toECommonsScanMode(type: string): ECommonsScanMode|undefined {
	switch (type) {
		case ECommonsScanMode.LINEART.toString():
			return ECommonsScanMode.LINEART;
		case ECommonsScanMode.GRAY.toString():
			return ECommonsScanMode.GRAY;
		case ECommonsScanMode.COLOR.toString():
			return ECommonsScanMode.COLOR;
	}
	return undefined;
}

export function fromECommonsScanMode(type: ECommonsScanMode): string {
	switch (type) {
		case ECommonsScanMode.LINEART:
			return ECommonsScanMode.LINEART.toString();
		case ECommonsScanMode.GRAY:
			return ECommonsScanMode.GRAY.toString();
		case ECommonsScanMode.COLOR:
			return ECommonsScanMode.COLOR.toString();
	}
	
	throw new Error('Unknown ECommonsScanMode');
}

export function isECommonsScanMode(test: unknown): test is ECommonsScanMode {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsScanMode(test) !== undefined;
}

export function keyToECommonsScanMode(key: string): ECommonsScanMode {
	switch (key) {
		case 'LINEART':
			return ECommonsScanMode.LINEART;
		case 'GRAY':
			return ECommonsScanMode.GRAY;
		case 'COLOR':
			return ECommonsScanMode.COLOR;
	}
	
	throw new Error(`Unable to obtain ECommonsScanMode for key: ${key}`);
}

export const ECOMMONS_SCAN_MODES: ECommonsScanMode[] = Object.keys(ECommonsScanMode)
		.map((e: string): ECommonsScanMode => keyToECommonsScanMode(e));
