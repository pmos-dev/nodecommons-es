import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';
import {
		commonsColorRgbToHsl,
		commonsColorRgbToHsv,
		commonsColorRgbToYiq,
		commonsColorRgbToYuv,
		commonsColorRgbToYCbCr,
		commonsColorRgbToLab,
		commonsColorColorspaceToRatios
} from 'tscommons-es-graphics';
import {
		IRgb, IRgba, ERgba,
		IHsl, EHsla,
		IHsv, EHsva,
		IYiq, EYiqa,
		IYuv, EYuva,
		IYCbCr, EYCbCra,
		ILab, ELaba
} from 'tscommons-es-graphics';
import { TOmniColorspace } from 'tscommons-es-graphics';

import { CommonsJimp } from 'nodecommons-es-graphics';

function colorspaceToNumbers<T extends TOmniColorspace>(
		values: T[],
		includeAlpha: boolean = false
): number[] {
	const converted: number[][] = values
			.map((value: T): number[] => {
				const ratios: T = commonsColorColorspaceToRatios<T>(value);
				
				const numbers: number[] = [];

				if (
					commonsTypeHasPropertyNumber(ratios, ERgba.RED)
					&& commonsTypeHasPropertyNumber(ratios, ERgba.GREEN)
					&& commonsTypeHasPropertyNumber(ratios, ERgba.BLUE)
				) {
					numbers.push(ratios[ERgba.RED]);
					numbers.push(ratios[ERgba.GREEN]);
					numbers.push(ratios[ERgba.BLUE]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, EHsla.HUE)
					&& commonsTypeHasPropertyNumber(ratios, EHsla.SATURATION)
					&& commonsTypeHasPropertyNumber(ratios, EHsla.LIGHTNESS)
				) {
					numbers.push(ratios[EHsla.HUE]);
					numbers.push(ratios[EHsla.SATURATION]);
					numbers.push(ratios[EHsla.LIGHTNESS]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, EHsva.HUE)
					&& commonsTypeHasPropertyNumber(ratios, EHsva.SATURATION)
					&& commonsTypeHasPropertyNumber(ratios, EHsva.VALUE)
				) {
					numbers.push(ratios[EHsva.HUE]);
					numbers.push(ratios[EHsva.SATURATION]);
					numbers.push(ratios[EHsva.VALUE]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, EYiqa.Y)
					&& commonsTypeHasPropertyNumber(ratios, EYiqa.I)
					&& commonsTypeHasPropertyNumber(ratios, EYiqa.Q)
				) {
					numbers.push(ratios[EYiqa.Y]);
					numbers.push(ratios[EYiqa.I]);
					numbers.push(ratios[EYiqa.Q]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, EYuva.Y)
					&& commonsTypeHasPropertyNumber(ratios, EYuva.U)
					&& commonsTypeHasPropertyNumber(ratios, EYuva.V)
				) {
					numbers.push(ratios[EYuva.Y]);
					numbers.push(ratios[EYuva.U]);
					numbers.push(ratios[EYuva.V]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, EYCbCra.Y)
					&& commonsTypeHasPropertyNumber(ratios, EYCbCra.CB)
					&& commonsTypeHasPropertyNumber(ratios, EYCbCra.CR)
				) {
					numbers.push(ratios[EYCbCra.Y]);
					numbers.push(ratios[EYCbCra.CB]);
					numbers.push(ratios[EYCbCra.CR]);
				}
				
				if (
					commonsTypeHasPropertyNumber(ratios, ELaba.L)
					&& commonsTypeHasPropertyNumber(ratios, ELaba.A)
					&& commonsTypeHasPropertyNumber(ratios, ELaba.B)
				) {
					numbers.push(ratios[ELaba.L]);
					numbers.push(ratios[ELaba.A]);
					numbers.push(ratios[ELaba.B]);
				}
				
				if (includeAlpha && commonsTypeHasPropertyNumber(value, ERgba.ALPHA)) {
					numbers.push(ratios[ERgba.ALPHA]);
				}
				
				return numbers;
			});
			
	const combined: number[] = [];
	
	for (const c of converted) {
		combined.push(...c);
	}
	
	return combined;
}

export async function commonsAiJimpToGreyscaleNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();

	return rgbas
			.map((rgb: IRgb): number => (rgb[ERgba.RED] + rgb[ERgba.GREEN] + rgb[ERgba.BLUE]) / (255 * 3));
}

export function commonsAiJimpToGreyscaleNumbersOptimised(image: CommonsJimp): number[] {
	const imageBuffer: Buffer = image.getRawJimpImage().bitmap.data;

	const originalLength: number = imageBuffer.length;
	if ((originalLength % 4) !== 0) throw new Error('Buffer length is not divisible by 4');
	const newSize: number = imageBuffer.length / 4;

	const newBuffer: number[] = new Array<number>(newSize).fill(0);
	for (let j = 0, i = 0; j < originalLength; i++, j += 4) {
		newBuffer[i] = (imageBuffer[j + 0] + imageBuffer[j + 1] + imageBuffer[j + 2]) / (255 * 3);
	}

	return newBuffer;
}

export async function commonsAiJimpToRgbNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	return colorspaceToNumbers(rgbas, false);
}

export function commonsAiJimpToRgbNumbersOptimised(image: CommonsJimp): number[] {
	const imageBuffer: Buffer = image.getRawJimpImage().bitmap.data;

	const originalLength: number = imageBuffer.length;
	if ((originalLength % 4) !== 0) throw new Error('Buffer length is not divisible by 4');
	const newSize: number = (imageBuffer.length / 4) * 3;

	const newBuffer: number[] = new Array<number>(newSize).fill(0);
	for (let j = 0, i = 0; j < originalLength; i += 3, j += 4) {
		newBuffer[i + 0] = imageBuffer[j + 0] / 255;
		newBuffer[i + 1] = imageBuffer[j + 1] / 255;
		newBuffer[i + 2] = imageBuffer[j + 2] / 255;
	}

	return newBuffer;
}

export async function commonsAiJimpToHslNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const hsls: IHsl[] = rgbas
			.map((rgba: IRgb): IHsl => commonsColorRgbToHsl(rgba));
	
	return colorspaceToNumbers(hsls, false);
}

export async function commonsAiJimpToHueNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const hsls: IHsl[] = rgbas
			.map((rgba: IRgb): IHsl => commonsColorRgbToHsl(rgba));

	return hsls
			.map((hsl: IHsl): number => hsl[EHsla.HUE] / 360);
}

export async function commonsAiJimpToHsvNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const hsvs: IHsv[] = rgbas
			.map((rgba: IRgb): IHsv => commonsColorRgbToHsv(rgba));
	
	return colorspaceToNumbers(hsvs, false);
}

export async function commonsAiJimpToYiqNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const yiqs: IYiq[] = rgbas
			.map((rgba: IRgb): IYiq => commonsColorRgbToYiq(rgba));
	
	return colorspaceToNumbers(yiqs, false);
}

export async function commonsAiJimpToYuvNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const yuvs: IYuv[] = rgbas
			.map((rgba: IRgb): IYuv => commonsColorRgbToYuv(rgba));
	
	return colorspaceToNumbers(yuvs, false);
}

export async function commonsAiJimpToYcbcrNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const ycbcrs: IYCbCr[] = rgbas
			.map((rgba: IRgb): IYCbCr => commonsColorRgbToYCbCr(rgba));
	
	return colorspaceToNumbers(ycbcrs, false);
}

export async function commonsAiJimpToLabNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const labs: ILab[] = rgbas
			.map((rgba: IRgb): ILab => commonsColorRgbToLab(rgba));
	
	return colorspaceToNumbers(labs, false);
}

export async function commonsAiJimpToMultipleColorSpaceNumbers(image: CommonsJimp): Promise<number[]> {
	const rgbas: IRgba[] = await image.toRgba();
	
	const rgbs: IRgb[] = rgbas
			.map((rgba: IRgba): IRgb => ({
					[ERgba.RED]: rgba.red,
					[ERgba.GREEN]: rgba.green,
					[ERgba.BLUE]: rgba.blue
					
			}));

	const hsls: IHsl[] = rgbas
			.map((rgba: IRgb): IHsl => commonsColorRgbToHsl(rgba));
	
	const hsvs: IHsv[] = rgbas
			.map((rgba: IRgb): IHsv => commonsColorRgbToHsv(rgba));

	const yiqs: IYiq[] = rgbas
			.map((rgba: IRgb): IYiq => commonsColorRgbToYiq(rgba));

	const yuvs: IYuv[] = rgbas
			.map((rgba: IRgb): IYuv => commonsColorRgbToYuv(rgba));

	const ycbcrs: IYCbCr[] = rgbas
			.map((rgba: IRgb): IYCbCr => commonsColorRgbToYCbCr(rgba));
	
	const labs: ILab[] = rgbas
			.map((rgba: IRgb): ILab => commonsColorRgbToLab(rgba));
	
	if (
		hsls.length !== rgbs.length
		|| hsvs.length !== rgbs.length
		|| yiqs.length !== rgbs.length
		|| yuvs.length !== rgbs.length
		|| ycbcrs.length !== rgbs.length
		|| labs.length !== rgbs.length
	) throw new Error('Colorspace length mismatch. Bug in library');
	
	const rgbns: number[] = colorspaceToNumbers(rgbs, false);
	const hslns: number[] = colorspaceToNumbers(hsls, false);
	const hsvns: number[] = colorspaceToNumbers(hsvs, false);
	const yiqns: number[] = colorspaceToNumbers(yiqs, false);
	const yuvns: number[] = colorspaceToNumbers(yuvs, false);
	const ycbcrns: number[] = colorspaceToNumbers(ycbcrs, false);
	const labns: number[] = colorspaceToNumbers(labs, false);
	
	const combined: number[] = [];
	for (let i = 0; i < rgbs.length; i++) {
		combined.push(
				rgbns[i],
				hslns[i],
				hsvns[i],
				yiqns[i],
				yuvns[i],
				ycbcrns[i],
				labns[i]
		);
	}
	
	return combined;
}
