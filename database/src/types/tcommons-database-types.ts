import { CommonsDatabaseType } from '../classes/commons-database-type';

export type TCommonsDatabaseTypes = {
		[field: string]: CommonsDatabaseType;
};
