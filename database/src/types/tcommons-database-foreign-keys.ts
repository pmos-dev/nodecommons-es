import { CommonsDatabaseForeignKey } from '../classes/commons-database-key';

export type TCommonsDatabaseForeignKeys = {
		[field: string]: CommonsDatabaseForeignKey;
};
