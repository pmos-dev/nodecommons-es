import { CommonsDatabaseUniqueKey } from '../classes/commons-database-key';

export type TCommonsDatabaseUniqueKeys = {
		[field: string]: CommonsDatabaseUniqueKey;
};
