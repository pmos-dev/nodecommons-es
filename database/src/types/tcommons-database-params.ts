import { CommonsDatabaseParam } from '../classes/commons-database-param';

export type TCommonsDatabaseParams = {
		[field: string]: CommonsDatabaseParam;
};
