import { ICommonsCredentials } from '../interfaces/icommons-credentials';

export type TQueryParams = {
		[name: string]: any;
};

export abstract class CommonsNosqlDatabaseService<CredentialsI extends ICommonsCredentials> {
	constructor(
			protected credentials: CredentialsI
	) {}
}
