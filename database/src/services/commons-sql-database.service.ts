import {
		commonsTypeIsNumber,
		commonsObjectIsEmpty,
		commonsObjectMapObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';

import { CommonsDatabaseType } from '../classes/commons-database-type';
import { CommonsDatabaseParam } from '../classes/commons-database-param';
import { CommonsDatabaseTypeEnum } from '../classes/commons-database-type-enum';
import { CommonsDatabaseTypeSerialId } from '../classes/commons-database-type-serial-id';
import { CommonsDatabaseTypeInt } from '../classes/commons-database-type-int';
import { CommonsDatabaseTypeNumber } from '../classes/commons-database-type-number';

import { CommonsDatabase } from '../helpers/commons-database';

import { ICommonsCredentials } from '../interfaces/icommons-credentials';

import { TCommonsDatabaseParams } from '../types/tcommons-database-params';
import { TCommonsDatabaseTypes } from '../types/tcommons-database-types';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';
import { ECommonsDatabaseReferentialAction } from '../enums/ecommons-database-referential-action';
import { ECommonsDatabaseInsertMethod } from '../enums/ecommons-database-insert-method';
import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseTypeSigned } from '../enums/ecommons-database-type-signed';

type TPreprepared = {
		sql: string;
		isSelect: boolean;
		params?: TCommonsDatabaseTypes;
		results?: TCommonsDatabaseTypes;
};

enum ETransactionState {
		FREE,
		PENDING,
		RUNNING
}

function strongTypeResult(
		value: any,
		type: CommonsDatabaseType,
		engine: ECommonsDatabaseEngine
): any {
	if (value !== undefined) value = type.processOut(value, engine);
	 
	type.assert(value);

	return value;
}

function singleRow<T extends TPropertyObject>(rows: T[]): T|undefined {
	if (rows.length === 0) return undefined;
	if (rows.length > 1) throw new Error('execute/queryParamsSingle didn\'t return one exact row');
	
	return rows[0];
}

function singleList<U>(rows: TPropertyObject[]): U[] {
	return rows
			.map((row: TPropertyObject): U => {
				const keys: string[] = Object.keys(row);
				if (keys.length !== 1) throw new Error('execute/queryParamsList returned more than one column');
				
				return row[keys[0]] as U;	// assume correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
			});
}

export function commonsDatabaseAssertField(field: string): string {
	if (/[\[\];\/\\?'\"\r\n\t\\0`]/.test(field)) throw new Error(`Condition field contains an invalid character: ${field}`);
	
	return field;
}

export abstract class CommonsSqlDatabaseService<CredentialsI extends ICommonsCredentials> extends CommonsDatabase {
	private preprepared: Map<string, TPreprepared> = new Map<string, TPreprepared>();
	
	private transactionState: ETransactionState = ETransactionState.FREE;
	
	constructor(
			engine: ECommonsDatabaseEngine,
			protected credentials: CredentialsI
	) {
		super(engine);
	}

	public abstract connect(): Promise<void>;
	
	public preprepare(
			name: string,
			sql: string,
			params?: TCommonsDatabaseTypes,	// this is deliberate, we don't have the param values yet
			results?: TCommonsDatabaseTypes
	) {
		if (this.preprepared.has(name)) throw new Error('Duplicate name for preprepared statement');
		
		const prep: TPreprepared = {
				sql: sql,
				isSelect: /^SELECT /i.test(sql),
				params: params,
				results: results
		};
		this.preprepared.set(name, prep);
	}
	
	public abstract doesTableExist(name: string): Promise<boolean>;
	
	protected abstract internalSelect(
			sql: string,
			params?: TPropertyObject
	): Promise<TPropertyObject[]>;

	protected abstract internalNone(
			sql: string,
			params?: TPropertyObject
	): Promise<void>;
	
	protected abstract internalTransactionBegin(): Promise<void>;
	protected abstract internalTransactionCommit(): Promise<void>;
	protected abstract internalTransactionRollback(): Promise<void>;

	public async transactionClaim(): Promise<boolean> {
		if (this.transactionState !== ETransactionState.FREE) return false;
		this.transactionState = ETransactionState.PENDING;

		try {
			await this.internalTransactionBegin();
			if (this.transactionState !== ETransactionState.PENDING) throw new Error('Transaction clash');
			
			this.transactionState = ETransactionState.RUNNING;

			return true;
		} catch (e) {
			console.log(e);
			this.transactionState = ETransactionState.FREE;
			
			return false;
		}
	}

	public async transactionCommit(): Promise<void> {
		if (this.transactionState !== ETransactionState.RUNNING) throw new Error('No transaction is running');

		try {
			await this.internalTransactionCommit();
		} catch (e) {
			console.log(e);
		} finally {
			this.transactionState = ETransactionState.FREE;
		}
	}

	public async transactionRollback(): Promise<void> {
		if (this.transactionState !== ETransactionState.RUNNING) throw new Error('No transaction is running');

		try {
			await this.internalTransactionRollback();
		} catch (e) {
			console.log(e);
		} finally {
			this.transactionState = ETransactionState.FREE;
		}
	}

	private buildParams(params: TCommonsDatabaseParams): TPropertyObject {
		return commonsObjectMapObject(
				params,
				(object: CommonsDatabaseParam): any => {
					object.process(this.getEngine());
					return object.getValue();
				}
		);
	}
	
	public async queryParams<T extends TPropertyObject>(
			sql: string,
			params?: TCommonsDatabaseParams,
			results?: TCommonsDatabaseTypes
	): Promise<T[]> {
		let internalData: TPropertyObject|undefined;
		if (params) internalData = this.buildParams(params);

		const data: TPropertyObject[] = await this.internalSelect(sql.trim(), internalData);
		if (data.length === 0) return [];

		if (!results) return data as T[];	// can't check if not result definition to check against
		
		const typecasted: TPropertyObject[] = [];
		for (const row of data) {
			const resultsKeys: string[] = Object.keys(results);
			const rowKeys: string[] = Object.keys(row);
			
			if (resultsKeys.length !== rowKeys.length) throw new Error('Supplied result types is not the same length as actual results');
			
			const extra: string[] = rowKeys
					.filter((field: string): boolean => !resultsKeys.includes(field));
			if (extra.length > 0) throw new Error('Returned row result field does not exist in the defined result structure');
			
			const missing: string[] = resultsKeys
					.filter((field: string): boolean => !rowKeys.includes(field));
			if (missing.length > 0) throw new Error('Defined result structure field is missing from the returned row results');

			const typecast: TPropertyObject = commonsObjectMapObject(
					row,
					(value: any, field: string): any => strongTypeResult(
							value,
							results[field],
							this.getEngine()
					)
			);
			typecasted.push(typecast);
		}

		return typecasted as T[];	// assume correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
	}

	public async noneParams(
			sql: string,
			params?: TCommonsDatabaseParams
	): Promise<void> {
		let internalData: TPropertyObject|undefined;
		if (params) internalData = this.buildParams(params);
		
		await this.internalNone(sql.trim(), internalData);
	}

	public async queryParamsSingle<T extends TPropertyObject>(
			sql: string,
			params?: TCommonsDatabaseParams,
			results?: TCommonsDatabaseTypes
	): Promise<T|undefined> {
		const rows: T[] = await this.queryParams<T>(sql, params, results);

		return singleRow<T>(rows);
	}

	public async queryParamsList<U>(
			sql: string,
			params?: TCommonsDatabaseParams,
			field?: string,
			type?: CommonsDatabaseType
	): Promise<U[]> {
		// can use TPropertyObject as we don't care about the mid-way structure types, only the end generic result.
		// can assume the mid-way is correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
			
		let results: TCommonsDatabaseTypes|undefined;
		
		if (field && type) {
			results = {};
			results[field] = type;
		}
		
		const rows: TPropertyObject[] = await this.queryParams<TPropertyObject>(
				sql,
				params,
				results
		);
		
		return singleList<U>(rows);
	}

	public async queryParamsValue<U>(
			sql: string,
			params?: TCommonsDatabaseParams,
			field?: string,
			type?: CommonsDatabaseType,
			allowNone: boolean = false
	): Promise<U|undefined> {
		// can use TPropertyObject as we don't care about the mid-way structure types, only the end generic result.
		// can assume the mid-way is correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
			
		const rows: U[] = await this.queryParamsList<U>(sql, params, field, type);
		
		if (rows.length === 0) {
			if (allowNone) return undefined;
			throw new Error('queryParamsValue did not return a result and allowNone is false');
		}
		
		if (rows.length !== 1) throw new Error('queryParamsValue returned more than one row');
		
		return rows[0];
	}

	// helper method to avoid |undefined
	public async queryParamsValueNoNone<U>(
			sql: string,
			params?: TCommonsDatabaseParams,
			field?: string,
			type?: CommonsDatabaseType
	): Promise<U> {
		return (await this.queryParamsValue<U>(sql, params, field, type))!;
	}
	
	private buildPrepreparedParams(
			values: TPropertyObject,
			types: TCommonsDatabaseTypes	// intentional
	): TCommonsDatabaseParams {
		const params: TCommonsDatabaseParams = {};
		
		for (const field of Object.keys(values)) {
			params[field] = new CommonsDatabaseParam(
					values[field],
					types[field]
			);
			params[field].process(this.getEngine());
		}
		
		return params;
	}
	
	public async executeParams<T extends TPropertyObject>(
			name: string,
			values?: TPropertyObject
	): Promise<T[]> {
		if (!this.preprepared.has(name)) throw new Error('Preprepared statement does not exist');
		const preprepared: TPreprepared = this.preprepared.get(name)!;

		let params: TCommonsDatabaseParams|undefined;
		if (values && preprepared.params) {
			params = this.buildPrepreparedParams(values, preprepared.params);
		}

		return await this.queryParams<T>(
				preprepared.sql,
				params,
				preprepared.results
		);
	}
	
	public async executeParamsSingle<T extends TPropertyObject>(
			name: string,
			values?: TPropertyObject
	): Promise<T|undefined> {
		const rows: T[] = await this.executeParams<T>(name, values);
		
		return singleRow<T>(rows);
	}

	public async executeParamsList<U>(
			name: string,
			values?: TPropertyObject
	): Promise<U[]> {
		// can use TPropertyObject as we don't care about the mid-way structure types, only the end generic result.
		// can assume the mid-way is correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
			
		const rows: TPropertyObject[] = await this.executeParams<TPropertyObject>(
				name,
				values
		);
		
		return singleList<U>(rows);
	}

	public async executeParamsValue<U>(
			name: string,
			values?: TPropertyObject,
			allowNone: boolean = false
	): Promise<U|undefined> {
		// can use TPropertyObject as we don't care about the mid-way structure types, only the end generic result.
		// can assume the mid-way is correct. if not, the results vs generic isn't correct, which is a programmer error and can't be detected at runtime.
			
		const row: TPropertyObject|undefined = await this.executeParamsSingle<TPropertyObject>(name, values);
		if (allowNone && row === undefined) return undefined;
		if (row === undefined) {
			if (allowNone) return undefined;
			throw new Error('executeParamsValue did not return a result and allowNone is false');
		}

		return singleList<U>([ row ])[0];
	}

	// helper method to avoid |undefined
	public async executeParamsValueNoNone<U>(
			name: string,
			values?: TPropertyObject
	): Promise<U> {
		return (await this.executeParamsValue<U>(name, values))!;
	}
	
	// this has to be async as it might need to create enums for Postgres etc.
	// can't use commonsObjectMapObject for the same reason
	private async generateCreateTableSql(
			table: string,
			structure: TCommonsDatabaseTypes,
			ifNotExists: boolean
	): Promise<string> {
		const fields: string[] = [];
		for (const field of Object.keys(structure)) {
			commonsDatabaseAssertField(field);
			
			const type: CommonsDatabaseType = structure[field];

			const build: string[] = [
					this.delimit(field),
					type.render(this.getEngine())
			];
			
			if (type instanceof CommonsDatabaseTypeEnum) {
				const createEnum: string|undefined = type.getCreateEnum(this.getEngine());
				if (createEnum) await this.noneParams(createEnum);
			}
			
			if (type instanceof CommonsDatabaseTypeNumber) {
				type.renderAppendCheck(build, field, this.getEngine());
			}

			fields.push(build.join(' '));
		}
		
		const query: string[] = [
				`CREATE TABLE ${ifNotExists ? 'IF NOT EXISTS' : ''}`,
				this.table(table),
				`(${fields.join(',')})`
		];

		if (this.getEngine() === ECommonsDatabaseEngine.MYSQL) query.push('ENGINE=InnoDB');

		return query.join(' ');
	}

	public async createTable(
			table: string,
			structure: TCommonsDatabaseTypes,
			ifNotExists: boolean = false
	): Promise<void> {
		const sql: string = await this.generateCreateTableSql(table, structure, ifNotExists);
		await this.noneParams(sql);
	}

	public async dropTable(
			table: string,
			structure: TCommonsDatabaseTypes,
			ifExists: boolean = false
	): Promise<void> {
		await this.noneParams(`DROP TABLE ${ifExists ? 'IF EXISTS' : ''} ${this.table(table)}`);
		
		for (const field of Object.keys(structure)) {
			commonsDatabaseAssertField(field);
			
			const type: CommonsDatabaseType = structure[field];

			if (type instanceof CommonsDatabaseTypeEnum) {
				const dropEnum: string|undefined = (type as CommonsDatabaseTypeEnum<unknown>).getDropEnum(this.getEngine());
				if (dropEnum) await this.noneParams(dropEnum);
			}
		}
	}
	
	public async createView(
			table: string,
			name: string,
			sql: string,
			ifNotExists: boolean = false
	): Promise<void> {
		await this.noneParams(`CREATE VIEW ${ifNotExists ? 'IF NOT EXISTS' : ''} ${this.view(table, name)} AS ${sql}`);
	}
	
	public async dropView(
			table: string,
			name: string,
			ifExists: boolean = true	// non-existant views aren't really much of an issue
	): Promise<void> {
		await this.noneParams(`DROP VIEW ${ifExists ? 'IF EXISTS' : ''} ${this.view(table, name)} CASCADE`);
	}

	public async createForeignKey(
			table: string,
			destTable: string,
			field: string,
			destId: string,
			onDelete: ECommonsDatabaseReferentialAction = ECommonsDatabaseReferentialAction.CASCADE,
			onUpdate: ECommonsDatabaseReferentialAction = ECommonsDatabaseReferentialAction.CASCADE,
			constraintName?: string
	): Promise<void> {
		const sql: string = this.generateCreateForeignKeySql(
				table,
				destTable,
				field,
				destId,
				onDelete,
				onUpdate,
				constraintName
		);
		
		await this.noneParams(sql);
	}

	public generateCreateForeignKeySql(
			table: string,
			destTable: string,
			field: string,
			destId: string,
			onDelete: ECommonsDatabaseReferentialAction = ECommonsDatabaseReferentialAction.CASCADE,
			onUpdate: ECommonsDatabaseReferentialAction = ECommonsDatabaseReferentialAction.CASCADE,
			constraintName?: string
	): string {
		if (!constraintName) constraintName = `f_${table}_${destTable}_${field}_${destId}`;
		
		return `
			ALTER TABLE ${this.table(table)}
			ADD CONSTRAINT ${this.delimit(constraintName)}
			FOREIGN KEY (${this.delimit(field)})
				REFERENCES ${this.delimit(destTable)} (${this.delimit(destId)})
				ON DELETE ${onDelete}
				ON UPDATE ${onUpdate}
		`;
	}

	public async createUniqueKey(
			table: string,
			fields: string[],
			constraintName?: string
	): Promise<void> {
		const sql: string = this.generateCreateUniqueKeySql(
				table,
				fields,
				constraintName
		);
		await this.noneParams(sql);
	}

	public generateCreateUniqueKeySql(
			table: string,
			fields: string[],
			constraintName?: string
	): string {
		const build: string[] = [];
		const names: string[] = [];
		for (const field of fields) {
			commonsDatabaseAssertField(field);
			
			build.push(this.delimit(field));
			names.push(field);
		}
		
		if (!constraintName) constraintName = `constraint_unique_${table}_${names.join('_')}`;
		
		const query: string[] = [
				'ALTER TABLE',
				this.table(table),
				'ADD CONSTRAINT',
				this.delimit(constraintName),
				'UNIQUE'
		];
		if (this.getEngine() !== ECommonsDatabaseEngine.POSTGRES) query.push('KEY');

		query.push(`(${build.join(',')})`);
		
		return query.join(' ');
	}

	public async createIndexKey(
			table: string,
			fields: string[],
			constraintName?: string
	): Promise<void> {
		const sql: string = this.generateCreateIndexKeySql(
				table,
				fields,
				constraintName
		);
		await this.noneParams(sql);
	}

	public generateCreateIndexKeySql(
			table: string,
			fields: string[],
			indexName?: string
	): string {
		const build: string[] = [];
		const names: string[] = [];
		for (const field of fields) {
			commonsDatabaseAssertField(field);
			
			build.push(this.delimit(field));
			names.push(field);
		}
		
		if (!indexName) indexName = `index_${table}_${names.join('_')}`;
		
		return `
			CREATE INDEX ${this.delimit(indexName)}
			ON ${this.delimit(table)} (${build.join(',')})
		`;
	}

	private async insertOrReplaceRow(
			method: ECommonsDatabaseInsertMethod,
			table: string,
			values: { [key: string]: CommonsDatabaseParam },
			conflictFields?: string[],
			autoinc?: string
	): Promise<number|void> {
		const params: { [key: string]: CommonsDatabaseParam } = {};
		const fields: string[] = [];
		const qs: string[] = [];
		
		let i: number = 1;

		for (const field of Object.keys(values)) {
			commonsDatabaseAssertField(field);
			
			if (
				this.getEngine() === ECommonsDatabaseEngine.POSTGRES
				|| this.getEngine() === ECommonsDatabaseEngine.SQLITE
			) {
				fields.push(this.delimit(field));	// some databases don't like Table.Field syntax for some reason?
			} else {
				fields.push(this.tableField(table, field));
			}
			
			qs.push(`:value${i}`);
			params[`value${i}`] = values[field];
			i++;
		}

		const query: string[] = [];

		switch (this.getEngine()) {
			case ECommonsDatabaseEngine.POSTGRES:
				query.push('INSERT');
				break;
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				switch (method) {
					case ECommonsDatabaseInsertMethod.INSERT:
						query.push('INSERT');
						break;
					case ECommonsDatabaseInsertMethod.UPSERT:
						query.push('REPLACE');
						break;
				}
				break;
		}
		
		query.push(`INTO ${this.table(table)}`);
		query.push(`(${fields.join(',')})`);
		query.push(`VALUES (${qs.join(',')})`);
		
		if (method === ECommonsDatabaseInsertMethod.UPSERT && this.getEngine() === ECommonsDatabaseEngine.POSTGRES) {
			if (!conflictFields || conflictFields.length === 0) throw new Error('No conflict fields supplied for a Postgres upsert');

			const excludeds: string[] = Object.keys(values)
					.filter((field: string): boolean => !conflictFields.includes(field))
					.map((field: string): string => `${this.delimit(field)} = EXCLUDED.${this.delimit(field)}`);

			query.push(`ON CONFLICT (${conflictFields.map((field: string): string => this.delimit(field)).join(',')})`);
			if (excludeds.length === 0) {
				query.push('DO NOTHING');
			} else {
				query.push(`DO UPDATE SET ${excludeds.join(',')}`);
			}
		}
		
		if (autoinc !== undefined && this.getEngine() === ECommonsDatabaseEngine.POSTGRES) {
			query.push(`RETURNING "${autoinc}"`);
		}

		if (autoinc !== undefined) {
			switch (this.getEngine()) {
				case ECommonsDatabaseEngine.POSTGRES:
				case ECommonsDatabaseEngine.MYSQL: {
					const id: number|undefined = await this.queryParamsValue<number>(
							query.join(' '),
							params,
							'id',
							new CommonsDatabaseTypeSerialId()
					);
					if (!commonsTypeIsNumber(id) || id < 1) throw new Error('Autoinc query failed');
					
					return id;
				}
				case ECommonsDatabaseEngine.SQLITE: {
					await this.noneParams(
							query.join(' '),
							params
					);
					return (await this.queryParamsValue<number>(
							'SELECT last_insert_rowid() AS id',
							undefined,
							'id',
							new CommonsDatabaseTypeInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
					))!;
				}
				default:
					throw new Error('Unknown database type for autoincrement value return');
			}
		} else {
			await this.noneParams(
					query.join(' '),
					params
			);
		}
	}

	public async insertRow(
			table: string,
			values: { [key: string]: CommonsDatabaseParam },
			autoinc?: string
	): Promise<number|void> {
		return await this.insertOrReplaceRow(
				ECommonsDatabaseInsertMethod.INSERT,
				table,
				values,
				undefined,
				autoinc
		);
	}

	public async upsertRow(
			table: string,
			values: { [key: string]: CommonsDatabaseParam },
			conflictFields?: string[],
			autoinc?: string
	): Promise<number|void> {
		return await this.insertOrReplaceRow(
				ECommonsDatabaseInsertMethod.UPSERT,
				table,
				values,
				conflictFields,
				autoinc
		);
	}
	
	private buildClause(
			table: string,
			inParams: TCommonsDatabaseParams,
			outParams: TCommonsDatabaseParams,
			prefix: string,
			delimiter: string,
			write: boolean
	): string {
		if (commonsObjectIsEmpty(inParams)) throw new Error('No inParams');

		const build: string[] = [];
		
		let i: number = 1;
		
		for (const field of Object.keys(inParams)) {
			commonsDatabaseAssertField(field);
		
			const param: CommonsDatabaseParam = inParams[field];
			
			if (
				write
				&& (
					this.getEngine() === ECommonsDatabaseEngine.POSTGRES
					|| this.getEngine() === ECommonsDatabaseEngine.SQLITE
				)
			) {
				// postgres and sqlite don't like Table.Field syntax for writing data for some reason?
				build.push(`${this.delimit(field)} = :${prefix}${i}`);
			} else {
				build.push(`${this.tableField(table, field)} = :${prefix}${i}`);
			}
			outParams[`${prefix}${i}`] = param;
			i++;
		}
		
		return build.join(delimiter);
	}
	
	public async updateRowsByConditions(
			table: string,
			values: TCommonsDatabaseParams,
			conditions: TCommonsDatabaseParams,
			singular: boolean = false
	): Promise<void> {
		if (commonsObjectIsEmpty(values)) throw new Error('No update values');
		if (commonsObjectIsEmpty(conditions)) throw new Error('No conditions supplied for update');

		const isTransactionOwner: boolean = await this.transactionClaim();

		try {
			if (singular) {
				const count: number = await this.countRowsByConditions(table, conditions);
				if (1 !== count) throw new Error(`Singular check failure: ${count} rows match`);
			}
	
			const params: TCommonsDatabaseParams = {};
			
			const setClauses: string = this.buildClause(
					table,
					values,
					params,
					'value',
					',',
					true
			);

			const whereClauses: string = this.buildClause(
					table,
					conditions,
					params,
					'condition',
					' AND ',
					false
			);
	
			await this.noneParams(
					`
						UPDATE ${this.table(table)}
						SET ${setClauses}
						WHERE ${whereClauses}
					`,
					params
			);

			if (isTransactionOwner) await this.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public async deleteRowsByConditions(
			table: string,
			conditions: TCommonsDatabaseParams,
			singular: boolean = false
	): Promise<void> {
		if (commonsObjectIsEmpty(conditions)) throw new Error('No conditions supplied for delete');
		
		const isTransactionOwner: boolean = await this.transactionClaim();

		try {
			if (singular) {
				const count: number = await this.countRowsByConditions(table, conditions);
				if (1 !== count) throw new Error(`Singular check failure: ${count} rows match`);
			}
	
			const params: TCommonsDatabaseParams = {};
			
			const whereClauses: string = this.buildClause(
					table,
					conditions,
					params,
					'condition',
					' AND ',
					false
			);
	
			await this.noneParams(
					`
						DELETE FROM ${this.table(table)}
						WHERE ${whereClauses}
					`,
					params
			);
			
			if (isTransactionOwner) await this.transactionCommit();
		} catch (e) {
			if (isTransactionOwner) await this.transactionRollback();
			console.error(e);
			
			throw e;
		}
	}

	public async selectRowsByConditions<T extends TPropertyObject>(
			table: string,
			fields: TCommonsDatabaseTypes,
			conditions: TCommonsDatabaseParams
	): Promise<T[]> {
		if (commonsObjectIsEmpty(fields)) throw new Error('Select rows by conditions supplied zero-length field array');
		if (commonsObjectIsEmpty(conditions)) throw new Error('No conditions supplied for select rows by conditions');
		
		const queryFields: string[] = [];
		for (const field of Object.keys(fields)) {
			commonsDatabaseAssertField(field);
			
			if (!(fields[field] instanceof CommonsDatabaseType)) throw new Error('Field type is not an instance of CommonsDatabaseType');
			
			queryFields.push(this.tableField(table, field));
		}
		
		const params: TCommonsDatabaseParams = {};
		
		const whereClauses: string = this.buildClause(
				table,
				conditions,
				params,
				'condition',
				' AND ',
				false
		);
		
		return await this.queryParams<T>(
				`
					SELECT ${queryFields.join(',')}
					FROM ${this.table(table)}
					WHERE ${whereClauses}
				`,
				params,
				fields
		);
	}

	public async countRowsByConditions(
			table: string,
			conditions: TCommonsDatabaseParams
	): Promise<number> {
		if (commonsObjectIsEmpty(conditions)) throw new Error('No conditions supplied for count rows by conditions');
		
		const params: TCommonsDatabaseParams = {};
		
		const whereClauses: string = this.buildClause(
				table,
				conditions,
				params,
				'condition',
				' AND ',
				false
		);
		
		return (await this.queryParamsValue<number>(
				`
					SELECT COUNT(*) AS ${this.delimit('count')}
					FROM ${this.table(table)}
					WHERE ${whereClauses}
				`,
				params,
				'count',
				new CommonsDatabaseTypeInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
		))!;
	}

	public async doesRowExistByConditions(
			table: string,
			conditions: TCommonsDatabaseParams
	) {
		return (await this.countRowsByConditions(table, conditions)) > 0;
	}
}
