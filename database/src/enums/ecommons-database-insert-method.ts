export enum ECommonsDatabaseInsertMethod {
		INSERT = 'insert',
		UPSERT = 'upsert'
}
