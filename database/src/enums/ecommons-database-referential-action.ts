export enum ECommonsDatabaseReferentialAction {
		CASCADE = 'CASCADE',
		RESTRICT = 'RESTRICT',
		SET_NULL = 'SET NULL'
}
