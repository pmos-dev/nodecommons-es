export enum ECommonsDatabaseOrderBy {
		ASC = 'asc',
		DESC = 'desc'
}
export function fromECommonsDatabaseOrderBy(direction: ECommonsDatabaseOrderBy): string {
	switch (direction) {
		case ECommonsDatabaseOrderBy.ASC:
			return 'ASC';
		case ECommonsDatabaseOrderBy.DESC:
			return 'DESC';
	}
	
	throw new Error('Unknown ECommonsDatabaseOrderBy direction');
}
