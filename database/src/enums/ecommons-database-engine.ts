export enum ECommonsDatabaseEngine {
		MYSQL = 'mysql',
		POSTGRES = 'postgres',
		MONGODB = 'mongodb',
		SQLITE = 'sqlite'
}
