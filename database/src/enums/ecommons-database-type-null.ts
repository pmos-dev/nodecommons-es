export enum ECommonsDatabaseTypeNull {
		ALLOW_NULL = 'allowNull',
		NOT_NULL = 'notNull'
}
