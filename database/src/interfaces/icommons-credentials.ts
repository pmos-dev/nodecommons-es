import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

export interface ICommonsCredentials {
		name: string;
		user?: string;
		password?: string;
		host?: string;
		port?: number;
		authSource?: string;
}

export function isICommonsCredentials(test: any): test is ICommonsCredentials {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'user')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'password')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'host')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'port')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'authSource')) return false;

	return true;
}
