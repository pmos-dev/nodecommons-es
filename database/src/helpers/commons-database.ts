import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

export function commonsDatabaseMysqlAddSlashes(value: string): string {
	return value
			.replace(/\\/g, '\\\\')
			.replace(/\u0008/g, '\\b')
			.replace(/\t/g, '\\t')
			.replace(/\n/g, '\\n')
			.replace(/\f/g, '\\f')
			.replace(/\r/g, '\\r')
			.replace(/'/g, '\\\'')
			.replace(/"/g, '\\"');
}

export function commonsDatabaseQuote(value: string, engine: ECommonsDatabaseEngine): string {
	switch (engine) {
		case ECommonsDatabaseEngine.POSTGRES:
		case ECommonsDatabaseEngine.SQLITE:
			return `'${value.replace('\'', '\'\'')}'`;
		case ECommonsDatabaseEngine.MYSQL:
			return `"${commonsDatabaseMysqlAddSlashes(value)}"`;
		default:
			throw new Error('Quote support has not been defined for this database engine');
	}
}

export function commonsDatabaseDelimit(param: string, engine: ECommonsDatabaseEngine): string {
	switch (engine) {
		case ECommonsDatabaseEngine.POSTGRES:
		case ECommonsDatabaseEngine.SQLITE:
			return `"${param}"`;
		case ECommonsDatabaseEngine.MYSQL:
			return '`' + param + '`';
		default:
			throw new Error('Delimitor support has not been defined for this database engine');
	}
}

export function commonsDatabaseTable(table: string, engine: ECommonsDatabaseEngine): string {
	return commonsDatabaseDelimit(table, engine);
}

export function commonsDatabaseTableField(table: string, field: string, engine: ECommonsDatabaseEngine): string {
	return `${commonsDatabaseTable(table, engine)}.${commonsDatabaseDelimit(field, engine)}`;
}

export function commonsDatabaseView(table: string, name: string, engine: ECommonsDatabaseEngine): string {
	return commonsDatabaseDelimit(`${table}__${name}`, engine);
}

export function commonsDatabaseViewField(table: string, name: string, field: string, engine: ECommonsDatabaseEngine): string {
	return `${commonsDatabaseView(table, name, engine)}.${commonsDatabaseDelimit(field, engine)}`;
}

export function commonsDatabaseSetField(table: string, field: string, engine: ECommonsDatabaseEngine): string {
	switch (engine) {
		case ECommonsDatabaseEngine.POSTGRES:
		case ECommonsDatabaseEngine.SQLITE:
			return commonsDatabaseDelimit(field, engine);
		case ECommonsDatabaseEngine.MYSQL:
			return commonsDatabaseTableField(table, field, engine);
		default:
			throw new Error('SetField support has not been defined for this database engine');
	}
}

export function commonsDatabaseTempField(field: string, engine: ECommonsDatabaseEngine): string {
	return commonsDatabaseDelimit(field, engine);
}

export abstract class CommonsDatabase {
	constructor(
			private engine: ECommonsDatabaseEngine
	) {}
	
	public getEngine(): ECommonsDatabaseEngine {
		return this.engine;
	}
	
	public quote(value: string): string {
		return commonsDatabaseQuote(value, this.engine);
	}

	public delimit(param: string): string {
		return commonsDatabaseDelimit(param, this.engine);
	}

	public table(table: string): string {
		return commonsDatabaseTable(table, this.engine);
	}

	public tableField(table: string, field: string): string {
		return commonsDatabaseTableField(table, field, this.engine);
	}

	public view(table: string, name: string): string {
		return commonsDatabaseView(table, name, this.engine);
	}

	public viewField(table: string, name: string, field: string): string {
		return commonsDatabaseViewField(table, name, field, this.engine);
	}

	public tempField(field: string): string {
		return commonsDatabaseTempField(field, this.engine);
	}

	public setField(table: string, field: string): string {
		return commonsDatabaseSetField(table, field, this.engine);
	}
}
