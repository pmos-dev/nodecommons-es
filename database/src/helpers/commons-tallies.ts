import { TPropertyObject } from 'tscommons-es-core';
import { TInterquartileHoursModel } from 'tscommons-es-core';
import { TInterquartileDowsModel } from 'tscommons-es-core';

import { CommonsDatabaseTypeSmallInt } from '../classes/commons-database-type-small-int';
import { CommonsDatabaseTypeFloat } from '../classes/commons-database-type-float';

import { CommonsSqlDatabaseService } from '../services/commons-sql-database.service';

import { ICommonsCredentials } from '../interfaces/icommons-credentials';

import { TCommonsDatabaseTypes } from '../types/tcommons-database-types';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseTypeSigned } from '../enums/ecommons-database-type-signed';

export function commonsTalliesInterquartileHoursModel(source: string): string {
	// Be aware, this isn't SQL injection protected.
	
	return `
		WITH "source" AS (
			${source}
		),
		"datehours" AS (
			SELECT
				"source"."timestamp",
				DATE("source"."timestamp") AS "date",
				EXTRACT(HOUR FROM "source"."timestamp") AS "hour",
				"source"."tally"
			FROM "source"
		),
		"hours" AS (
			SELECT "datehours"."date"
			FROM "datehours"
			GROUP BY "datehours"."date", "datehours"."hour"
		),
		"complete_dates" AS (
			SELECT "hours"."date"
			FROM "hours"
			GROUP BY "hours"."date"
			HAVING COUNT("hours"."date") = 24
		),
		"window" AS (
			SELECT
				"datehours"."date",
				"datehours"."hour",
				"datehours"."tally",
				LAG("datehours"."tally", 1) OVER (
					ORDER BY "datehours"."timestamp" ASC
					ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
				) AS "prev"
			FROM "datehours"
		),
		"group_per_date_and_hour" AS (
			SELECT
				"window"."date",
				"window"."hour",
				SUM(GREATEST(0::smallint, "window"."tally" - "window"."prev")) AS "tally"
			FROM "window"
			WHERE "window"."prev" IS NOT NULL
			GROUP BY "window"."date", "window"."hour"
		),
		"complete_groups_per_date_and_hour" AS (
			SELECT
				"group_per_date_and_hour"."date",
				"group_per_date_and_hour"."hour",
				"group_per_date_and_hour"."tally"
			FROM "group_per_date_and_hour", "complete_dates"
			WHERE "group_per_date_and_hour"."date" IN ("complete_dates"."date")
		),
		"max_per_date" AS (
			SELECT
				"complete_groups_per_date_and_hour"."date",
				MAX("complete_groups_per_date_and_hour"."tally") AS "max"
			FROM "complete_groups_per_date_and_hour"
			GROUP BY "complete_groups_per_date_and_hour"."date"
		),
		"relative_model_per_date" AS (
			SELECT
				"complete_groups_per_date_and_hour"."date",
				"complete_groups_per_date_and_hour"."hour",
				"complete_groups_per_date_and_hour"."tally",
				"max_per_date"."max",
				"complete_groups_per_date_and_hour"."tally"::float / GREATEST(1, "max_per_date"."max")::float AS "relative"
			FROM "complete_groups_per_date_and_hour"
			LEFT JOIN "max_per_date"
				ON "complete_groups_per_date_and_hour"."date" = "max_per_date"."date"
		),
		"relative_overall_model" AS (
			SELECT
				"relative_model_per_date"."hour",
				percentile_disc(array[ 0, 0.25, 0.5, 0.75, 1 ]) WITHIN GROUP (ORDER BY "relative") AS "percentiles"
			FROM "relative_model_per_date"
			GROUP BY "relative_model_per_date"."hour"
		)
		SELECT
			"relative_overall_model"."hour",
			"relative_overall_model"."percentiles"[1] AS "min",
			"relative_overall_model"."percentiles"[2] AS "q1",
			"relative_overall_model"."percentiles"[3] AS "median",
			"relative_overall_model"."percentiles"[4] AS "q3",
			"relative_overall_model"."percentiles"[5] AS "max"
		FROM "relative_overall_model"
		ORDER BY "relative_overall_model"."hour" ASC
	`
			.replace(/^\t\t\t/gm, '')
			.trim();
}

export function commonsTalliesInterquartileDowsModel(
		source: string,
		dowStart: number = 0
): string {
	const dowOffset: number = (7 - dowStart) % 7;
	const weekOffset: number = (7 + (1 - dowStart)) % 7;
	
	// Be aware, this isn't SQL injection protected.
	
	return `
		WITH "source" AS (
			${source}
		),
		"datehours" AS (
			SELECT
				"source"."timestamp",
				DATE("source"."timestamp") AS "date",
				EXTRACT(HOUR FROM "source"."timestamp") AS "hour",
				"source"."tally"
			FROM "source"
		),
		"hours" AS (
			SELECT "datehours"."date"
			FROM "datehours"
			GROUP BY "datehours"."date", "datehours"."hour"
		),
		"complete_dates" AS (
			SELECT
				"hours"."date",
				EXTRACT(WEEK FROM ("hours"."date" + '${weekOffset} day'::interval)) AS "week"
			FROM "hours"
			GROUP BY "hours"."date"
			HAVING COUNT("hours"."date") = 24
		),
		"complete_weeks" AS (
			SELECT "complete_dates"."week"
			FROM "complete_dates"
			GROUP BY "complete_dates"."week"
			HAVING COUNT("complete_dates"."week") = 7
		),
		"available_dates" AS (
			SELECT "complete_dates"."date"
			FROM "complete_dates", "complete_weeks"
			WHERE "complete_dates"."week" IN ("complete_weeks"."week")
		),
		"window" AS (
			SELECT
				"datehours"."date",
				"datehours"."tally",
				LAG("datehours"."tally", 1) OVER (
					ORDER BY "datehours"."timestamp" ASC
					ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
				) AS "prev"
			FROM "datehours"
		),
		"group_per_date" AS (
			SELECT
				"window"."date",
				SUM(GREATEST(0::smallint, "window"."tally" - "window"."prev")) AS "tally"
			FROM "window"
			WHERE "window"."prev" IS NOT NULL
			GROUP BY "window"."date"
		),
		"complete_groups_per_date" AS (
			SELECT
				"group_per_date"."date",
				EXTRACT(WEEK FROM ("group_per_date"."date" + '${weekOffset} day'::interval)) AS "week",
				(EXTRACT(DOW FROM "group_per_date"."date") + ${dowOffset})::int % 7 AS "dow",
				"group_per_date"."tally"
			FROM "group_per_date", "available_dates"
			WHERE "group_per_date"."date" IN ("available_dates"."date")
		),
		"max_per_week" AS (
			SELECT
				"complete_groups_per_date"."week",
				MAX("complete_groups_per_date"."tally") AS "max"
			FROM "complete_groups_per_date"
			GROUP BY "complete_groups_per_date"."week"
		),
		"relative_model_per_week" AS (
			SELECT
				"complete_groups_per_date"."week",
				"complete_groups_per_date"."dow",
				"complete_groups_per_date"."tally",
				"max_per_week"."max",
				"complete_groups_per_date"."tally"::float / GREATEST(1, "max_per_week"."max")::float AS "relative"
			FROM "complete_groups_per_date"
			LEFT JOIN "max_per_week"
				ON "complete_groups_per_date"."week" = "max_per_week"."week"
		),
		"relative_overall_model" AS (
			SELECT
				"relative_model_per_week"."dow",
				percentile_disc(array[ 0, 0.25, 0.5, 0.75, 1 ]) WITHIN GROUP (ORDER BY "relative") AS "percentiles"
			FROM "relative_model_per_week"
			GROUP BY "relative_model_per_week"."dow"
		)
		SELECT
			"relative_overall_model"."dow",
			"relative_overall_model"."percentiles"[1] AS "min",
			"relative_overall_model"."percentiles"[2] AS "q1",
			"relative_overall_model"."percentiles"[3] AS "median",
			"relative_overall_model"."percentiles"[4] AS "q3",
			"relative_overall_model"."percentiles"[5] AS "max"
		FROM "relative_overall_model"
		ORDER BY "relative_overall_model"."dow" ASC
	`
			.replace(/^\t\t\t/gm, '')
			.trim();
}

export function commonsTalliesPreprepare<CredentialsI extends ICommonsCredentials>(
		database: CommonsSqlDatabaseService<CredentialsI>,
		prefix: string,
		selectSql: string,
		selectParamTypes?: TCommonsDatabaseTypes,
		dowStart: number = 0
): void {
	database.preprepare(
			`${prefix}__LIST_INTERQUARTILE_HOURS_MODEL`,
			commonsTalliesInterquartileHoursModel(selectSql),
			selectParamTypes,
			{
					hour: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					min: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					q1: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					median: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					q3: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					max: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
			}
	);

	database.preprepare(
			`${prefix}__LIST_INTERQUARTILE_DOWS_MODEL`,
			commonsTalliesInterquartileDowsModel(selectSql, dowStart),
			selectParamTypes,
			{
					dow: new CommonsDatabaseTypeSmallInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					min: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					q1: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					median: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					q3: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
					max: new CommonsDatabaseTypeFloat(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
			}
	);
}

export async function commonsTalliesListInterquartileHoursModel<CredentialsI extends ICommonsCredentials>(
		database: CommonsSqlDatabaseService<CredentialsI>,
		prefix: string,
		params?: TPropertyObject
): Promise<TInterquartileHoursModel[]> {
	return await database.executeParams<TInterquartileHoursModel>(
			`${prefix}__LIST_INTERQUARTILE_HOURS_MODEL`,
			params
	);
}

export async function commonsTalliesListInterquartileDowsModel<CredentialsI extends ICommonsCredentials>(
		database: CommonsSqlDatabaseService<CredentialsI>,
		prefix: string,
		params?: TPropertyObject
): Promise<TInterquartileDowsModel[]> {
	return await database.executeParams<TInterquartileDowsModel>(
			`${prefix}__LIST_INTERQUARTILE_DOWS_MODEL`,
			params
	);
}
