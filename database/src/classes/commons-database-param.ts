import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseParam {
	constructor(
			private value: any,
			private type: CommonsDatabaseType
	) {}
	
	public process(engine: ECommonsDatabaseEngine) {
		if (this.value === null) this.value = undefined;
		
		this.type.assert(this.value);
		this.type.processIn(this, engine);
	}
	
	public getValue(): any {
		return this.value;
	}
	
	public setValue(value: any): void {
		this.value = value;	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
	}
}
