import { CommonsFixedDate } from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseParam } from './commons-database-param';
import { InternalCommonsDatabaseTypeFixedDate } from './fixed-date';

export class CommonsDatabaseTypeFixedDate extends InternalCommonsDatabaseTypeFixedDate {
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				return 'DATE';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override processIn(param: CommonsDatabaseParam, engine: ECommonsDatabaseEngine): void {
		const value: unknown = param.getValue();
		if (value !== undefined) {
			if (!CommonsFixedDate.is(value)) throw new Error('Value to be processed in is not a CommonsFixedDate object');

			const clone: CommonsFixedDate = value.clone;
			clone.HisMs = '00:00:00.0';

			param.setValue(clone);
		}
		
		super.processIn(param, engine);
	}
	
	public override processOut(value: any, engine: ECommonsDatabaseEngine): unknown {
		value = super.processOut(value, engine);
		if (!value) return undefined;

		this.assert(value);

		const clone: CommonsFixedDate = (value as CommonsFixedDate).clone;
		clone.HisMs = '00:00:00.0';

		return clone;
	}
}
