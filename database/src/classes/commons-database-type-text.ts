import { commonsTypeAssertString } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseTypeText extends CommonsDatabaseType {
	constructor(
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string
	) {
		super(
				notNull,
				defaultValue,
				false
		);
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				return 'TEXT';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		commonsTypeAssertString(value);
	}
}
