import {
		commonsTypeIsNumber,
		commonsTypeIsBoolean,
		commonsTypeIsString
} from 'tscommons-es-core';

import { commonsDatabaseQuote } from '../helpers/commons-database';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseParam } from './commons-database-param';

export abstract class CommonsDatabaseType {
	constructor(
			private notNull: ECommonsDatabaseTypeNull,
			private defaultValue?: number|string|boolean,
			private primaryKey: boolean = false
	) {}
	
	public getNotNull(): ECommonsDatabaseTypeNull {
		return this.notNull;
	}
	
	public getDefaultValue(): number|string|boolean|undefined {
		return this.defaultValue;
	}

	public render(engine: ECommonsDatabaseEngine): string {
		const modifiers: string[] = [];
		if (this.notNull === ECommonsDatabaseTypeNull.NOT_NULL) modifiers.push(this.renderNotNull(engine));

		if (this.defaultValue !== undefined) {
			if (commonsTypeIsNumber(this.defaultValue) && !isNaN(this.defaultValue)) {
				modifiers.push(`DEFAULT ${this.defaultValue.toString(10)}`);
			} else if (commonsTypeIsBoolean(this.defaultValue)) {
				modifiers.push(`DEFAULT ${this.defaultValue ? 'true' : 'false'}`);
			} else if (commonsTypeIsString(this.defaultValue)) {
				modifiers.push(`DEFAULT ${commonsDatabaseQuote(this.defaultValue, engine)}`);
			} else {
				throw new Error('Unknown CommonsDatabaseType default value type');
			}
		} else {
			if (!this.notNull) modifiers.push('DEFAULT NULL');
		}
	
		if (this.primaryKey) modifiers.push('PRIMARY KEY');

		modifiers.unshift(this.renderEngineType(engine));
		
		return modifiers.join(' ');
	}
	
	protected abstract renderEngineType(engine: ECommonsDatabaseEngine): string;
	
	// almost all databases and types use this default, but some like SQLite's ROWID don't
	protected renderNotNull(_engine: ECommonsDatabaseEngine): string {
		return 'NOT NULL';
	}

	public assert(value: any): void|never {
		if (value === undefined && this.notNull === ECommonsDatabaseTypeNull.NOT_NULL) throw new Error('NOTNULL modifier is set');
	}

	public processIn(_param: CommonsDatabaseParam, _engine: ECommonsDatabaseEngine): void {
		// to be overridden if desired
	}
	
	public processOut(value: any, _engine: ECommonsDatabaseEngine): unknown {
		if (value === null) return undefined;
		return value;
	}
}
