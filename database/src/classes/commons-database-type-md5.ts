import { commonsTypeIsString, COMMONS_REGEX_PATTERN_MD5 } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';

import { CommonsDatabaseTypeString } from './commons-database-type-string';

export class CommonsDatabaseTypeMd5 extends CommonsDatabaseTypeString {
	constructor(
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string,
			primaryKey: boolean = false
	) {
		super(
				32,
				notNull,
				defaultValue,
				primaryKey
		);
	}
	
	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;

		if (!commonsTypeIsString(value)) throw new Error('MD5 value is not a string');
		if (!COMMONS_REGEX_PATTERN_MD5.test(value)) throw new Error(`Invalid database type value for md5: ${value}`);
	}
}
