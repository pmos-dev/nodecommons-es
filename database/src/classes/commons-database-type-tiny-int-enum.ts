import { commonsTypeIsNumber } from 'tscommons-es-core';

import { commonsDatabaseDelimit } from '../helpers/commons-database';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';
import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseTypeSigned } from '../enums/ecommons-database-type-signed';

import { CommonsDatabaseTypeTinyInt } from './commons-database-type-tiny-int';

export class CommonsDatabaseTypeTinyIntEnum extends CommonsDatabaseTypeTinyInt {
	constructor(
			private options: number[],
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: number,
			primaryKey: boolean = false
	) {
		super(
				ECommonsDatabaseTypeSigned.UNSIGNED,
				notNull,
				defaultValue,
				primaryKey
		);
		
		if (options.length === 0) throw new Error('TinyIntEnum cannot have a zero length array of options');
	}

	public override renderAppendCheck(modifiers: string[], fieldname: string, engine: ECommonsDatabaseEngine): string|undefined {
		switch (engine) {
			case ECommonsDatabaseEngine.POSTGRES:
				modifiers.push(`CHECK (${commonsDatabaseDelimit(fieldname, engine)} IN (${this.options.join(',')}))`);
				return;
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				return;
			default:
				throw new Error('Signing support has not been defined for this database engine');
		}
	}
	
	public override assert(value: any): void|never {
		super.assert(value);

		if (value === undefined) return;

		if (!commonsTypeIsNumber(value)) throw new Error('TINYINTENUM is not a number');
		if (!this.options.includes(value)) throw new Error('TINYINTENUM is not a value in the ENUM array');
	}
}
