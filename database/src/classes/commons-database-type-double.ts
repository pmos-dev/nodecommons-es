import {
		commonsTypeAssertNumber,
		commonsTypeAttemptNumber
} from 'tscommons-es-core';
import { MAX_SIGNED_53BIT_INTEGER } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseTypeSigned } from '../enums/ecommons-database-type-signed';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseTypeNumber } from './commons-database-type-number';

export class CommonsDatabaseTypeDouble extends CommonsDatabaseTypeNumber {
	constructor(
			unsigned: ECommonsDatabaseTypeSigned = ECommonsDatabaseTypeSigned.SIGNED,
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: number,
			primaryKey: boolean = false
	) {
		super(
				unsigned === ECommonsDatabaseTypeSigned.UNSIGNED ? 0 : -MAX_SIGNED_53BIT_INTEGER,
				MAX_SIGNED_53BIT_INTEGER,
				notNull,
				defaultValue,
				primaryKey
		);
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.POSTGRES: {
				const render: string[] = [ 'DOUBLE', 'PRECISION' ];
				this.renderAppendSigning(render, engine);
				
				return render.join(' ');
			}
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE: {
				const render: string[] = [ 'DOUBLE' ];
				this.renderAppendSigning(render, engine);
				
				return render.join(' ');
			}
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}
	
	public override assert(value: any): void|never {
		super.assert(value);
		
		if (value === undefined) return;
		
		const typecast: number = commonsTypeAssertNumber(value);
		
		this.assertRange(typecast);
	}
	
	public override processOut(value: any, _engine: ECommonsDatabaseEngine): number|undefined {
		if (value === undefined || value === null) return undefined;
		
		const typecast: number|undefined = commonsTypeAttemptNumber(value);
		if (typecast === undefined) throw new Error('Invalid DOUBLE value for processOut');
		
		return typecast;
	}
}
