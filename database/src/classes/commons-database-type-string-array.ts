import { commonsTypeAssertStringArray, commonsTypeIsString } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';
import { CommonsDatabaseParam } from './commons-database-param';

export class CommonsDatabaseTypeStringArray extends CommonsDatabaseType {
	constructor(notNull: ECommonsDatabaseTypeNull) {
		super(
				notNull,
				undefined,
				false
		);
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				return 'TEXT';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		commonsTypeAssertStringArray(value);
	}

	public override processIn(param: CommonsDatabaseParam, engine: ECommonsDatabaseEngine): void {
		const value: unknown = param.getValue();
		if (value !== undefined) {
			if (!commonsTypeAssertStringArray(value)) throw new Error('Value to be processed in is not a string array');
			param.setValue(JSON.stringify(value));
		}
		
		super.processIn(param, engine);
	}
	
	public override processOut(value: any, engine: ECommonsDatabaseEngine): unknown {
		value = super.processOut(value, engine);
		if (!value) return undefined;
		
		try {
			if (!commonsTypeIsString(value)) throw new Error('String array JSON encoded value is not a string');
			return JSON.parse(value);
		} catch (e) {
			return undefined;
		}
	}
}
