import {
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeAssertDate
} from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

// don't import from the pg library, as we might not be using postgres
type TPostgresInterval = {
		years?: number;
		months?: number;
		days?: number;
		hours?: number;
		minutes?: number;
		seconds?: number;
		milliseconds?: number;
};
function isTPostgresInterval(test: unknown): test is TPostgresInterval {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'years')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'months')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'days')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'hours')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'minutes')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'seconds')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'milliseconds')) return false;
	
	return true;
}

export class CommonsDatabaseTypeTime extends CommonsDatabaseType {
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				return 'TIME';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		commonsTypeAssertDate(value);
	}
	
	public override processOut(value: any, engine: ECommonsDatabaseEngine): Date|undefined {
		if (value === undefined || value === null) return undefined;

		if (engine === ECommonsDatabaseEngine.POSTGRES && isTPostgresInterval(value)) {
			const time: Date = new Date(0);

			// ignore the date components
			if (commonsTypeHasPropertyNumber(value, 'hours')) time.setHours(value.hours!);
			if (commonsTypeHasPropertyNumber(value, 'minutes')) time.setMinutes(value.minutes!);
			if (commonsTypeHasPropertyNumber(value, 'seconds')) time.setSeconds(value.seconds!);
			if (commonsTypeHasPropertyNumber(value, 'milliseconds')) time.setMilliseconds(value.milliseconds!);
			
			return time;
		}
		
		return value;	// eslint-disable-line @typescript-eslint/no-unsafe-return
	}
}
