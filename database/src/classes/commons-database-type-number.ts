import { commonsTypeAssertNumber } from 'tscommons-es-core';
import { MAX_SIGNED_32BIT_INTEGER } from 'tscommons-es-core';
import { MAX_SIGNED_53BIT_INTEGER } from 'tscommons-es-core';

import { commonsDatabaseDelimit } from '../helpers/commons-database';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseTypeSigned } from '../enums/ecommons-database-type-signed';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

// NB, JavaScript only supports numbers up to (2^53)-1, so this is not 64 bit. See tscommons-core.consts

export abstract class CommonsDatabaseTypeNumber extends CommonsDatabaseType {
	protected unsigned: ECommonsDatabaseTypeSigned;
	
	constructor(
			protected lower: number,
			protected upper: number,
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: number,
			primaryKey: boolean = false,
			use53Bit: boolean = true
	) {
		super(
				notNull,
				defaultValue,
				primaryKey
		);
		
		commonsTypeAssertNumber(this.lower);
		commonsTypeAssertNumber(this.upper);
		
		const absoluteMax: number = use53Bit ? MAX_SIGNED_53BIT_INTEGER : MAX_SIGNED_32BIT_INTEGER;
		const bitSize: number = use53Bit ? 53 : 32;
		
		if (this.upper > absoluteMax) throw new Error(`Type upper range is greater than signed ${bitSize.toString(10)} bit integer support`);
		if (this.lower < -absoluteMax) throw new Error(`Type lower range is less than signed ${bitSize.toString(10)} bit integer support`);
		
		this.unsigned = this.lower < 0 ? ECommonsDatabaseTypeSigned.SIGNED : ECommonsDatabaseTypeSigned.UNSIGNED;
	}
	
	protected renderAppendSigning(modifiers: string[], engine: ECommonsDatabaseEngine): void {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				if (this.lower >= 0) modifiers.push('UNSIGNED');
				return;
			case ECommonsDatabaseEngine.POSTGRES:
				return;
			default:
				throw new Error('Signing support has not been defined for this database engine');
		}
	}

	public renderAppendCheck(modifiers: string[], fieldname: string, engine: ECommonsDatabaseEngine): string|undefined {
		switch (engine) {
			case ECommonsDatabaseEngine.POSTGRES:
				if (this.unsigned === ECommonsDatabaseTypeSigned.UNSIGNED && (this.upper < 0 || this.lower < 0)) throw new Error('Unsigned numerics cannot be assigned a negative range');
				modifiers.push(`CHECK (${commonsDatabaseDelimit(fieldname, engine)} BETWEEN ${this.lower} AND ${this.upper})`);
				return;
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				return;
			default:
				throw new Error('Signing support has not been defined for this database engine');
		}
	}
	
	protected assertRange(value: number): void|never {
		commonsTypeAssertNumber(value);
		
		if (value < this.lower) throw new Error(`Underflow from ${this.lower}`);
		if (value > this.upper) throw new Error(`Overflow from ${this.upper}`);
	}
}
