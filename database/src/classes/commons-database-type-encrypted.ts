import * as crypto from 'crypto';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';

import { CommonsDatabaseTypeBinary } from './commons-database-type-binary';

export class CommonsDatabaseTypeEncrypted extends CommonsDatabaseTypeBinary {
	private key: Buffer;
	
	constructor(
			passphrase: string,
			notNull: ECommonsDatabaseTypeNull,
			private algorithm: string = 'aes-256-cbc'
	) {
		super(notNull);
		
		this.key = Buffer.alloc(32);
		this.key.fill(0);
		
		const converted: Buffer = Buffer.from(passphrase, 'utf-8');
		converted.copy(this.key, 0, 0, converted.byteLength);
	}

	public encrypt(value: Buffer|undefined): Buffer|undefined {
		if (!value) return value;
		
		const iv: Buffer = crypto.randomBytes(16);
		const cipher: crypto.Cipher = crypto.createCipheriv(this.algorithm, this.key, iv);
		
		const encrypted: Buffer = cipher.update(value);
		const result: Buffer = Buffer.concat([ encrypted, cipher.final() ]);

		const header: Buffer = Buffer.from('CRYPT', 'utf-8');

		return Buffer.concat([ header, iv, result ]);
	}

	public decrypt(value: Buffer|undefined): Buffer|undefined {
		if (!value) return value;

		const header: Buffer = Buffer.from('CRYPT', 'utf-8');

		if (value.byteLength < header.byteLength || value.slice(0, header.byteLength).toString('utf-8') !== 'CRYPT') throw new Error('Encrypted \'CRYPT\' preface is not present; this value does not appear to be encrypted!');

		const iv: Buffer = value.slice(header.byteLength, 16);
		const decipher: crypto.Decipher = crypto.createDecipheriv(this.algorithm, this.key, iv);
		
		const encrypted: Buffer = value.slice(header.byteLength + 16);
		const decrypted: Buffer = decipher.update(encrypted);
		
		const result: Buffer = Buffer.concat([ decrypted, decipher.final() ]);

		return result;
	}
}
