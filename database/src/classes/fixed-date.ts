import { CommonsFixedDate, commonsTypeAssertDate } from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';
import { CommonsDatabaseParam } from './commons-database-param';

export abstract class InternalCommonsDatabaseTypeFixedDate extends CommonsDatabaseType {
	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		if (!CommonsFixedDate.is(value)) throw new Error('Assertion fail: variable is not a CommonsFixedDate instance');
	}

	public override processIn(param: CommonsDatabaseParam, engine: ECommonsDatabaseEngine): void {
		const value: unknown = param.getValue();
		if (value !== undefined) {
			if (!CommonsFixedDate.is(value)) throw new Error('processIn value is not a CommonsFixedDate instance');
			param.setValue(value.UTCDate);
		}
		
		super.processIn(param, engine);
	}
	
	public override processOut(value: any, engine: ECommonsDatabaseEngine): unknown {
		value = super.processOut(value, engine);
		if (!value) return undefined;
		
		try {
			commonsTypeAssertDate(value);

			return CommonsFixedDate.fromUTCDate(value as Date);
		} catch (e) {
			return undefined;
		}
	}
}
