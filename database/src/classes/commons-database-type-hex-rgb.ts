import { COMMONS_REGEX_PATTERN_RGB_COLOR, COMMONS_REGEX_PATTERN_RGBA_COLOR, commonsTypeIsString } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';

import { CommonsDatabaseTypeString } from './commons-database-type-string';

export class CommonsDatabaseTypeHexRgb extends CommonsDatabaseTypeString {
	constructor(
			private alpha: boolean,
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string,
			primaryKey: boolean = false
	) {
		super(
				alpha ? 9 : 7,
				notNull,
				defaultValue,
				primaryKey
		);
	}
	
	public hasAlpha(): boolean {
		return this.alpha;
	}
	
	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;

		if (!commonsTypeIsString(value)) throw new Error('HexRgb value is not a string');
		if (this.alpha) {
			if (!COMMONS_REGEX_PATTERN_RGBA_COLOR.test(value)) throw new Error(`Invalid database type value for RGBA: ${value}`);
		} else {
			if (!COMMONS_REGEX_PATTERN_RGB_COLOR.test(value)) throw new Error(`Invalid database type value for RGB: ${value}`);
		}
	}
}
