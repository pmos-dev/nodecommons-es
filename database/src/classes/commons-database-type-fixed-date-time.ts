import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { InternalCommonsDatabaseTypeFixedDate } from './fixed-date';

export class CommonsDatabaseTypeFixedDateTime extends InternalCommonsDatabaseTypeFixedDate {
	protected override renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
				return 'DATETIME';
			case ECommonsDatabaseEngine.POSTGRES:
				return 'TIMESTAMP WITHOUT TIME ZONE';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}
}
