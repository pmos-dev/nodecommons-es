import { commonsTypeAssertString, commonsTypeIsString } from 'tscommons-es-core';

import { commonsDatabaseQuote, commonsDatabaseDelimit } from '../helpers/commons-database';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseTypeEnum<E> extends CommonsDatabaseType {
	constructor(
			private options: E[],
			private fromE: (e: E) => string,
			private toE: (e: string) => E|undefined,
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: E,
			primaryKey: boolean = false,
			private enumId?: string	// for postgres,
	) {
		super(
				notNull,
				defaultValue ? fromE(defaultValue) : undefined,
				primaryKey
		);

		if (this.options.length === 0) throw new Error('No options supplied for ENUM type');
		
		if (defaultValue !== undefined) {
			if (!this.options.includes(defaultValue)) throw new Error('Default value is not in the listed options');
		}
	}
	
	public getOptions(): E[] {
		return this.options.slice();
	}
	
	public getCreateEnum(engine: ECommonsDatabaseEngine): string|undefined {
		switch (engine) {
			case ECommonsDatabaseEngine.POSTGRES:
				if (!this.enumId) throw new Error('Postgres ENUM types need to have enum_id names defined');

				const os: string[] = this.options
						.map((option: E): string => commonsDatabaseQuote(this.fromE(option), engine));

				return `CREATE TYPE ${commonsDatabaseDelimit(this.enumId, engine)} AS ENUM(${os.join(',')})`;
			default:
				return undefined;
		}
	}
	
	public getDropEnum(engine: ECommonsDatabaseEngine): string|undefined {
		switch (engine) {
			case ECommonsDatabaseEngine.POSTGRES:
				if (!this.enumId) throw new Error('Postgres ENUM types need to have enum_id names defined');

				return `DROP TYPE ${commonsDatabaseDelimit(this.enumId, engine)}`;
			default:
				return undefined;
		}
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				const os: string[] = this.options
						.map((option: E): string => commonsDatabaseQuote(this.fromE(option), engine));
				
				return `ENUM(${os.join(',')})`;
			case ECommonsDatabaseEngine.POSTGRES:
				if (!this.enumId) throw new Error('EnumId has not been set for Postgres Enum type');
				return commonsDatabaseDelimit(this.enumId, engine);
			default:
				throw new Error('This type has not been implemented for this database yet');
		}
	}

	public override assert(value: any): void|never {
		super.assert(value);
		
		if (value === undefined) return undefined;
		
		const typecast: string = commonsTypeAssertString(value);
		if (typecast.length > 255) throw new Error('Enum greater than 255 characters. Not supported');
		if (undefined === this.toE(typecast)) throw new Error('No such option in the ENUM array');
	}
	
	public override processOut(value: any, _engine: ECommonsDatabaseEngine): E|undefined {
		if (value === undefined || value === null) return undefined;
		
		if (!commonsTypeIsString(value)) throw new Error('Enum value is not a string');
		const typecast: E|undefined = this.toE(value);
		if (typecast === undefined) throw new Error('Invalid enum value for processOut');
		
		return typecast;
	}
}
