import {
		commonsTypeAssertNumber,
		commonsTypeAttemptNumber
} from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseTypeId } from './commons-database-type-id';

export class CommonsDatabaseTypeSerialId extends CommonsDatabaseTypeId {
	constructor() {
		super(
				ECommonsDatabaseTypeNull.NOT_NULL,
				undefined,
				false	// do the primary key setting in the renderEngineType rather than here
		);
	}
	
	protected override renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
				return 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY';
			case ECommonsDatabaseEngine.POSTGRES:
				return 'SERIAL UNIQUE PRIMARY KEY';
			case ECommonsDatabaseEngine.SQLITE:
				return 'INTEGER PRIMARY KEY';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}
	
	protected override renderNotNull(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.SQLITE:
				return '';
			default:
				return super.renderNotNull(engine);
		}
	}
	
	public override assert(value: any): void|never {
		super.assert(value);
		
		if (value === undefined) throw new Error('SerialId types cannot be undefined');
		
		const typecast: number = commonsTypeAssertNumber(value);
		if (typecast !== Math.floor(typecast)) throw new Error('SerialId has a float value');
		
		this.assertRange(typecast);
	}
	
	public override processOut(value: any, _engine: ECommonsDatabaseEngine): number|undefined {
		if (value === undefined || value === null) return undefined;
		
		const typecast: number|undefined = commonsTypeAttemptNumber(value);
		if (typecast === undefined) throw new Error('Invalid SerialId value for processOut');
		
		return typecast;
	}
}
