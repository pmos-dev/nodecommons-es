import { commonsTypeIsString, COMMONS_REGEX_PATTERN_ID_NAME } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';

import { CommonsDatabaseTypeString } from './commons-database-type-string';

export class CommonsDatabaseTypeIdName extends CommonsDatabaseTypeString {
	constructor(
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string,
			primaryKey: boolean = false
	) {
		super(
				255,
				notNull,
				defaultValue,
				primaryKey
		);
	}
	
	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;

		if (!commonsTypeIsString(value)) throw new Error('IdName value is not a string');
		if (!COMMONS_REGEX_PATTERN_ID_NAME.test(value)) throw new Error(`Invalid database type value for IdName: ${value}`);
	}
}
