import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseTypeBinary extends CommonsDatabaseType {
	constructor(
			notNull: ECommonsDatabaseTypeNull
			// decision: binary isn't allowed to have a default value
	) {
		super(
				notNull,
				undefined,
				false
		);
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.SQLITE:
			case ECommonsDatabaseEngine.MYSQL:
				return 'BLOB';
			case ECommonsDatabaseEngine.POSTGRES:
				return 'BYTEA';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}
}
