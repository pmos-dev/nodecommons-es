import {
		commonsTypeAssertNumber,
		commonsTypeAssertString
} from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';
import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseTypeString extends CommonsDatabaseType {
	constructor(
			private maxLength: number,
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string,
			primaryKey: boolean = false
	) {
		super(
				notNull,
				defaultValue,
				primaryKey
		);
		
		commonsTypeAssertNumber(this.maxLength);
		if (this.maxLength < 1) throw new Error(`Bad max length: ${this.maxLength}`);
		if (this.maxLength > 255) throw new Error('Max length for STRING cannot be more than 255; consider using TEXT instead');
	}
	
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.POSTGRES:
			case ECommonsDatabaseEngine.SQLITE:
				return `VARCHAR(${this.maxLength})`;
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		const typecast: string = commonsTypeAssertString(value);
		if (typecast.length > this.maxLength) throw new Error(`Greater than ${this.maxLength} characters.`);
	}
}
