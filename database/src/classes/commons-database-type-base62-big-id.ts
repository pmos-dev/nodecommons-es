import { commonsTypeIsString, COMMONS_REGEX_PATTERN_BASE62_ID } from 'tscommons-es-core';

import { ECommonsDatabaseTypeNull } from '../enums/ecommons-database-type-null';

import { CommonsDatabaseTypeString } from './commons-database-type-string';

export class CommonsDatabaseTypeBase62BigId extends CommonsDatabaseTypeString {
	constructor(
			notNull: ECommonsDatabaseTypeNull,
			defaultValue?: string,
			primaryKey: boolean = false
	) {
		super(
				8,
				notNull,
				defaultValue,
				primaryKey
		);
	}
	
	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;

		if (!commonsTypeIsString(value)) throw new Error('Base62 value is not a string');
		if (!COMMONS_REGEX_PATTERN_BASE62_ID.test(value)) throw new Error(`Invalid database type value for base62bigid: ${value}`);
	}
}
