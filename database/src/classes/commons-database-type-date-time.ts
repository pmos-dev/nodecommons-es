import { commonsTypeAssertDate } from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from '../enums/ecommons-database-engine';

import { CommonsDatabaseType } from './commons-database-type';

export class CommonsDatabaseTypeDateTime extends CommonsDatabaseType {
	protected renderEngineType(engine: ECommonsDatabaseEngine): string {
		switch (engine) {
			case ECommonsDatabaseEngine.MYSQL:
			case ECommonsDatabaseEngine.SQLITE:
				return 'DATETIME';
			case ECommonsDatabaseEngine.POSTGRES:
				return 'TIMESTAMP WITHOUT TIME ZONE';
			default:
				throw new Error('This type has not been defined for this database engine');
		}
	}

	public override assert(value: unknown): void|never {
		super.assert(value);

		if (value === undefined) return;
		
		commonsTypeAssertDate(value);
	}
	
	public override processOut(value: any, engine: ECommonsDatabaseEngine): unknown {
		if (value === null) return undefined;
		
		if (engine === ECommonsDatabaseEngine.SQLITE) {
			// SQLite stores its timestamps as longs
			return new Date(value as number);
		}
		
		return value;
	}
}
