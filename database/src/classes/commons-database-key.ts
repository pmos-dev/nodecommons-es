import { ECommonsDatabaseReferentialAction } from '../enums/ecommons-database-referential-action';

export abstract class CommonsDatabaseKey {
	constructor(
			private constraintName?: string
	) {}

	public getConstraintName(): string|undefined {
		return this.constraintName;
	}
}

export class CommonsDatabaseForeignKey extends CommonsDatabaseKey {
	constructor(
			private table: string,
			private field: string,
			private onDelete = ECommonsDatabaseReferentialAction.CASCADE,
			private onUpdate = ECommonsDatabaseReferentialAction.CASCADE,
			constraintName?: string
	) {
		super(constraintName);
	}

	public getTable(): string {
		return this.table;
	}

	public getField(): string {
		return this.field;
	}

	public getOnDelete(): ECommonsDatabaseReferentialAction {
		return this.onDelete;
	}

	public getOnUpdate(): ECommonsDatabaseReferentialAction {
		return this.onUpdate;
	}
}

export class CommonsDatabaseIndexKey extends CommonsDatabaseKey {
	constructor(
			private fields: string[],
			constraintName?: string
	) {
		super(constraintName);
	}

	public getFields(): string[] {
		return this.fields;
	}
}

export class CommonsDatabaseUniqueKey extends CommonsDatabaseIndexKey {}
