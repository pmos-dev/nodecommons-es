import { Worker } from 'worker_threads';

import { TEncoded } from 'tscommons-es-core';

export async function commonsWorkerPromise<
		I extends TEncoded|void,
		O extends TEncoded|void
>(
		filepath: string,
		data?: I,
		outputValidator: (test: unknown) => test is O = (_: unknown): _ is O => true
): Promise<O> {
	return new Promise<O>((resolve: (_: O) => void, reject: (e: Error) => void): void => {
		const worker: Worker = new Worker(
				filepath,
				{
						workerData: data
				}
		);
		worker.on(
				'message',
				(output: unknown): void => {
					if (!outputValidator(output)) {
						reject(new Error('Invalid output returned'));
						return;
					}
					
					resolve(output);
				}
		);
		worker.on(
				'error',
				(error: Error): void => {
					reject(error);
				}
		);
		worker.on(
				'exit',
				(code: number): void => {
					if (code !== 0) reject(new Error(`Worker stopped with exit code ${code}`));
				}
		);
	});
}
