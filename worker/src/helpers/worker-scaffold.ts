import { parentPort, workerData } from 'worker_threads';

import { TEncoded } from 'tscommons-es-core';

export function commonsWorkerScaffold<
		I extends TEncoded|void,
		O extends TEncoded|void
>(
		callback: (data: I) => O,
		inputValidator: (test: unknown) => test is I = (_: unknown): _ is I => true
): void {
	if (!parentPort) throw new Error('No parentPort available');

	const data: unknown = workerData;
	if (!inputValidator(data)) throw new Error('Invalid data passed to worker');

	const result: O = callback(data);

	parentPort.postMessage(result);
}

export async function commonsWorkerScaffoldPromise<
		I extends TEncoded|void,
		O extends TEncoded|void
>(
		callback: (data: I) => Promise<O>,
		inputValidator: (test: unknown) => test is I = (_: unknown): _ is I => true
): Promise<void> {
	if (!parentPort) throw new Error('No parentPort available');

	const data: unknown = workerData;
	if (!inputValidator(data)) throw new Error('Invalid data passed to worker');

	const result: O = await callback(data);

	parentPort.postMessage(result);
}
