import * as fs from 'fs';
import * as child_process from 'child_process';
import * as path from 'path';
import * as os from 'os';
import * as process from 'process';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

import {
		commonsBase62GenerateRandomId,
		commonsNumberSampleIndices
} from 'tscommons-es-core';

// we're doing this in here rather than in a nodecommons-process or similar, as it's very bespoke to this code
enum ESimplifiedPlatform {
		POSIX = 'posix',
		WIN32 = 'win32'
}

// eslint-disable-next-line no-underscore-dangle
const __dirname: string = dirname(fileURLToPath(import.meta.url));

// we're doing this in here rather than in a nodecommons-process or similar, as it's very bespoke to this code
function simplifiedPlatform(): ESimplifiedPlatform {
	switch (os.platform()) {
		case 'aix':
		case 'android':
		case 'darwin':
		case 'freebsd':
		case 'linux':
		case 'netbsd':
		case 'openbsd':
		case 'sunos':
			return ESimplifiedPlatform.POSIX;
		case 'win32':
		case 'cygwin':
			return ESimplifiedPlatform.WIN32;
		default:
			throw new Error('OS platform not supported for CommonsFile');
	}
}

export function commonsFileRootRelativePath(relative: string): string {
	return path.join(__dirname, ...[ '..', '..', '..', '..' ], relative );
}

export function commonsFileDistRelativePath(relative: string): string {
	return path.join(commonsFileRootRelativePath('.'), 'dist', relative);
}

export function commonsFileCwdRelativeOrAbsolutePath(relativeOrAbsolute: string): string {
	switch (simplifiedPlatform()) {
		case ESimplifiedPlatform.WIN32:
			// as per https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file#fully-qualified-vs-relative-paths
			if (/^\\\\/.test(relativeOrAbsolute)) return relativeOrAbsolute;
			if (/^[A-Z]:\\/.test(relativeOrAbsolute)) return relativeOrAbsolute;
			if (/^\\/.test(relativeOrAbsolute)) return relativeOrAbsolute;
			break;
		case ESimplifiedPlatform.POSIX:
			if (/^\//.test(relativeOrAbsolute)) return relativeOrAbsolute;
			break;
	}
	
	return path.join(process.cwd(), relativeOrAbsolute);
}

export function commonsFileExists(p: string): boolean {
	return fs.existsSync(p);
}

export function commonsFileIsDirectory(p: string): boolean {
	if (!commonsFileExists(p)) return false;
	if (!fs.lstatSync(p).isDirectory()) return false;
	return true;
}

export function commonsFileIsFile(f: string): boolean {
	if (!commonsFileExists(f)) return false;
	if (!fs.lstatSync(f).isFile()) return false;
	return true;
}

export function commonsFileList(p: string): string[] {
	return fs.readdirSync(p)
			.filter((file: string): boolean => !(/^[.]{1,2}$/.test(file)));
}

export function commonsFileListFiles(p: string): string[] {
	return commonsFileList(p)
			.filter((file: string): boolean => !fs.lstatSync(path.join(p, file)).isDirectory());
}

export function commonsFileListDirectories(p: string): string[] {
	return commonsFileList(p)
			.filter((file: string): boolean => fs.lstatSync(path.join(p, file)).isDirectory());
}

export function commonsFileListFilesWithExtensions(p: string, exts: string|string[]): string[] {
	if ('string' === typeof exts) exts = [ exts ];
	for (const test of exts) if (!/^[A-Za-z0-9]{1,16}$/.test(test)) throw new Error('Invalid extension supplied');
	
	const regex: RegExp = new RegExp(`[.](${exts.join('|')})$`, 'i');
	
	return commonsFileListFiles(p)
			.filter((file: string): boolean => regex.test(file));
}

export function commonsFileSync(silentIgnoreUnsupported: boolean = true): void {
	switch (os.platform()) {
		case 'freebsd':
		case 'linux':
		case 'netbsd':
		case 'openbsd':
			child_process.execSync('sync;sync');
			return;
		default:
			if (!silentIgnoreUnsupported) throw new Error('OS platform not supported for commonsFileSync');
	}
}

export function commonsFileUmount(p: string, silentIgnoreUnsupported: boolean = false): void {
	switch (os.platform()) {
		case 'linux':
			child_process.execSync(`sudo umount "${p}"`);
			return;
		default:
			if (!silentIgnoreUnsupported) throw new Error('OS platform not supported for commonsFileUmount');
	}
}

export function commonsFileMkdir(p: string): void {
	if (!fs.existsSync(p)) fs.mkdirSync(p);
}

// As of NodeJs 10: superceded by native fs.mkdirSync(..., { recursive: true })
// maintained for backwards compatibility only
export function commonsFileMkdirRecursive(p: string): void {
	if (parseInt(process.versions.node.split('.')[0], 10) >= 10) {
		if (!fs.existsSync(p)) fs.mkdirSync(p, { recursive: true });
		return;
	}
	
	switch (simplifiedPlatform()) {
		case ESimplifiedPlatform.POSIX:
			const paths: string[] = p.split('/');
			const done: string[] = [];
			for (const pstep of paths) {
				done.push(pstep);
				const current: string = done.join('/');
				
				if (current === '/' || current === '') continue;
				
				if (!commonsFileExists(current)) commonsFileMkdir(current);
				if (!commonsFileIsDirectory(current)) throw new Error(`Unable to recursively create directory for: ${current}`);
			}
			break;
		case ESimplifiedPlatform.WIN32:
			throw new Error('OS platform not supported for commonsFileMkdirRecursive on win32 for NodeJS <10');
	}
}

export function commonsFileLastModified(file: string): Date {
	return new Date(fs.lstatSync(file).mtimeMs);
}

export function commonsFileGenerateTmpFilePath(ext?: string, file?: string): string {
	if (!file) file = `tmp_${commonsBase62GenerateRandomId()}${commonsBase62GenerateRandomId()}`;

	if (ext) {
		if (!ext.startsWith('.')) ext = `.${ext}`;
		file = `${file}${ext}`;
	}

	switch (simplifiedPlatform()) {
		case ESimplifiedPlatform.POSIX:
			return `/tmp/${file}`;
		case ESimplifiedPlatform.WIN32:
			throw new Error('OS platform not supported for commonsFileGenerateTmpFilePath on win32');
	}
}

export function commonsFileReadBinaryFile(file: string): Buffer|undefined {
	return fs.readFileSync(file, { encoding: null });
}

export function commonsFileReadBinaryFileAsync(file: string): Promise<Buffer|undefined> {
	return new Promise((resolve: (_: Buffer|undefined) => void, reject: (reason: Error) => void): void => {
		if (!fs.existsSync(file)) {
			resolve(undefined);
			return;
		}

		fs.readFile(
				file,
				{ encoding: null },
				(error: Error|null|undefined, data: Buffer): void => {
					if (error) reject(error);
					resolve(data);
				}
		);
	});
}

export function commonsFileReadTextFile(file: string): string|undefined {
	if (!fs.existsSync(file)) return undefined;
	return fs.readFileSync(file, { encoding: 'utf8' });
}

export function commonsFileReadTextFileAsync(file: string): Promise<string|undefined> {
	return new Promise((resolve: (_: string|undefined) => void, reject: (reason: Error) => void): void => {
		if (!fs.existsSync(file)) {
			resolve(undefined);
			return;
		}

		fs.readFile(
				file,
				{ encoding: 'utf8' },
				(error: Error|null|undefined, data: string): void => {
					if (error) reject(error);
					resolve(data);
				}
		);
	});
}

export function commonsFileReadJsonFile<T>(file: string): T|undefined {
	const data: any = commonsFileReadTextFile(file);
	if (data === undefined) return undefined;
	
	try {
		return JSON.parse(data) as T;	// eslint-disable-line @typescript-eslint/no-unsafe-argument
	} catch (ex) {
		return undefined;
	}
}

export async function commonsFileReadJsonFileAsync<T>(file: string): Promise<T|undefined> {
	const data: string|undefined = await commonsFileReadTextFileAsync(file);
	if (data === undefined) return undefined;
	
	try {
		return JSON.parse(data) as T;
	} catch (ex) {
		return undefined;
	}
}

export function commonsFileWriteBinaryFile(file: string, data: Buffer): void {
	fs.writeFileSync(file, data, { encoding: null });
}

export function commonsFileWriteBinaryFileAsync(file: string, data: Buffer): Promise<void> {
	return new Promise((resolve: () => void, reject: (reason: Error) => void): void => {
		fs.writeFile(
				file,
				data,
				{ encoding: null },
				(error?: Error|null): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileWriteTextFile(file: string, data: string): void {
	fs.writeFileSync(file, data, { encoding: 'utf8' });
}

export function commonsFileWriteTextFileAsync(file: string, data: string): Promise<void> {
	return new Promise((resolve: () => void, reject: (reason: Error) => void): void => {
		fs.writeFile(
				file,
				data,
				{ encoding: 'utf8' },
				(error?: Error|null): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileWriteJsonFile<T>(file: string, data: T, space?: string|number): void {
	return commonsFileWriteTextFile(file, JSON.stringify(data, undefined, space));
}

export function commonsFileWriteJsonFileAsync<T>(file: string, data: T, space?: string|number): Promise<void> {
	return commonsFileWriteTextFileAsync(file, JSON.stringify(data, undefined, space));
}

export function commonsFileAppendBinaryFile(file: string, data: Buffer): void {
	fs.appendFileSync(file, data, { encoding: null });
}

export function commonsFileAppendBinaryFileAsync(file: string, data: Buffer): Promise<void> {
	return new Promise((resolve: () => void, reject: (reason: Error) => void): void => {
		fs.appendFile(
				file,
				data,
				{ encoding: null },
				(error?: Error|null): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileAppendTextFile(file: string, data: string): void {
	fs.appendFileSync(file, data, { encoding: 'utf8' });
}

export function commonsFileAppendTextFileAsync(file: string, data: string): Promise<void> {
	return new Promise((resolve: () => void, reject: (reason: Error) => void): void => {
		fs.appendFile(
				file,
				data,
				{ encoding: 'utf8' },
				(error?: Error|null): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileAppendJsonFile<T>(file: string, data: T, space?: string|number): void {
	return commonsFileAppendTextFile(file, JSON.stringify(data, undefined, space));
}

export function commonsFileAppendJsonFileAsync<T>(file: string, data: T, space?: string|number): Promise<void> {
	return commonsFileAppendTextFileAsync(file, JSON.stringify(data, undefined, space));
}

export function commonsFileRm(file: string): void {
	fs.unlinkSync(file);
}

export function commonsFileCopy(src: string, dest: string): void {
	if (!fs.existsSync(src)) throw new Error('File does not exists');

	fs.copyFileSync(
			src,
			dest
	);
}

export function commonsFileCopyAsync(src: string, dest: string): Promise<void> {
	return new Promise((resolve: (_: void) => void, reject: (reason: Error) => void): void => {
		if (!fs.existsSync(src)) {
			reject(new Error('File does not exists'));
			return;
		}

		fs.copyFile(
				src,
				dest,
				(error: Error|null|undefined): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileRenameAsync(src: string, dest: string): Promise<void> {
	return new Promise((resolve: (_: void) => void, reject: (reason: Error) => void): void => {
		if (!fs.existsSync(src)) {
			reject(new Error('File does not exists'));
			return;
		}

		fs.rename(
				src,
				dest,
				(error: Error|null|undefined): void => {
					if (error) reject(error);
					resolve();
				}
		);
	});
}

export function commonsFileRename(src: string, dest: string): void {
	if (!fs.existsSync(src)) throw new Error('File does not exists');

	fs.renameSync(
			src,
			dest
	);
}

export function commonsFileMoveAsync(src: string, dest: string): Promise<void> {
	return commonsFileRenameAsync(src, dest);
}

export function commonsFileMove(src: string, dest: string): void {
	return commonsFileRename(src, dest);
}

export function commonsFileSize(file: string): number {
	return fs.statSync(file).size;
}

export function commonsFileModified(file: string): Date {
	return fs.statSync(file).mtime;
}

export function commonsFileChmod(file: string, mode: number): void {
	fs.chmodSync(file, mode);
}

export function commonsFileSample(file: string, samples: number, blockSize: number): Buffer {
	const length: number = commonsFileSize(file);
	
	const indices: number[] = commonsNumberSampleIndices(length, samples, blockSize);
	
	const fd: number = fs.openSync(file, 'r');
	const buffers: Buffer[] = indices
			.map((index: number): Buffer => {
				const buffer: Buffer = Buffer.alloc(blockSize);
				fs.readSync(fd, buffer, 0, blockSize, index);
				return buffer;
			});
	fs.closeSync(fd);
	
	return Buffer.concat(buffers);
}
