import { commonsArrayDifference, commonsTypeIsString, TPropertyObject } from 'tscommons-es-core';
import { CommonsCsv as CommonsCsvTypescript, ECommonsCsvBooleanFormat } from 'tscommons-es-format';
import { TCommonsCsvColumn } from 'tscommons-es-format';
import { ECommonsCsvDateFormat } from 'tscommons-es-format';

import {
		commonsFileReadTextFile,
		commonsFileWriteTextFile
} from './commons-file';

export class CommonsCsv extends CommonsCsvTypescript {
	public static loadGenericStringColumns(path: string): TPropertyObject[] {
		const source: string|undefined = commonsFileReadTextFile(path);
		if (source === undefined) throw new Error('Unable to read CSV file');
		
		return CommonsCsv.buildGenericStringColumns(source);
	}
	
	constructor(
			private _headers: TCommonsCsvColumn[],
			separator: string = ',',
			textDelimiter: string = '"',
			dateFormat: ECommonsCsvDateFormat = ECommonsCsvDateFormat.YMDHIS,
			booleanFormat: ECommonsCsvBooleanFormat = ECommonsCsvBooleanFormat.NORMAL,
			iso: boolean = false,
			fixed: boolean = false
	) {
		super(
				_headers,
				separator,
				textDelimiter,
				dateFormat,
				booleanFormat,
				(test: unknown): test is string => {
					if (!commonsTypeIsString(test)) return false;

					const names: string[] = _headers
							.map((header: TCommonsCsvColumn): string => header.name);

					return names.includes(test);
				},
				iso,
				fixed
		);
	}

	public load(
			path: string,
			hasHeaderRow: boolean = false
	): TPropertyObject[] {
		let source: string|undefined = commonsFileReadTextFile(path);
		if (source === undefined) throw new Error('Unable to read CSV file');
		
		if (hasHeaderRow) {
			source = source.replace('\r\n', '\n').replace('\r', '\n').trim();
			const columns: TCommonsCsvColumn[] = this.readHeader(source);

			const missing: TCommonsCsvColumn[] = commonsArrayDifference(
					columns,
					this._headers,	// eslint-disable-line no-underscore-dangle
					undefined,
					(a: TCommonsCsvColumn, b: TCommonsCsvColumn): boolean => a.name === b.name
			);
			if (missing.length > 0) throw new Error(`CSV parse header mismatch. Difference columns were ${missing.map((c: TCommonsCsvColumn): string => c.name).join(', ')}`);

			const rows: string[] = source.split('\n');
			rows.shift();
			source = rows.join('\n');
		}

		return this.parse(source);
	}

	public save(values: TPropertyObject[], path: string, withHeaderRow: boolean = false): void {
		const csv: string = this.encode(values, withHeaderRow);
		commonsFileWriteTextFile(path, csv);
	}
}
