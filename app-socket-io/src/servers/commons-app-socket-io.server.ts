import { ServerOptions } from 'socket.io';

import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { ICommonsExpressConfig } from 'nodecommons-es-express';
import {
		CommonsSocketIoServer,
		commonsSocketIoBuildDefaultOptions
} from 'nodecommons-es-socket-io';

export class CommonsAppSocketIoServer extends CommonsSocketIoServer {
	constructor(
			expressServer: CommonsStrictExpressServer,
			expressConfig: ICommonsExpressConfig,
			defaultEnabled: boolean = true,
			options?: Partial<ServerOptions>
	) {
		super(
				expressServer.getServer(),
				defaultEnabled,
				{
						...commonsSocketIoBuildDefaultOptions(expressConfig),
						...(options || {})
				}
		);
	}
}
