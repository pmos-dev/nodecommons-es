import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export interface ICommonsSocketIoPseudoConfig {
		pseudoDomain: string;
}

export function isICommonsSocketIoPseudoConfig(test: any): test is ICommonsSocketIoPseudoConfig {
	if (!commonsTypeHasPropertyString(test, 'pseudoDomain')) return false;

	return true;
}
