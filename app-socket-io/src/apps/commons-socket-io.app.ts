import * as express from 'express';

import { TPropertyObject } from 'tscommons-es-core';

import { CommonsStrictExpressServer, ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { ICommonsExpressConfig, isICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsRestApp } from 'nodecommons-es-app-rest';

import { CommonsAppSocketIoServer } from '../servers/commons-app-socket-io.server';

export abstract class CommonsSocketIoApp<S extends CommonsAppSocketIoServer> extends CommonsRestApp<
		ICommonsRequestWithStrictParams,
		express.Response,
		CommonsStrictExpressServer
> {
	private socketIoServer: S|undefined;
	
	constructor(
			name: string,
			configFile?: string,
			configPath?: string,	// NB, a direct path, not the name of the args parameter
			envVar?: string,
			argsKey: string = 'config-path'
	) {
		super(
				name,
				'express',
				configFile,
				configPath,
				envVar,
				argsKey
		);
	}
	
	// this should be overridden by subclasses to provide the socket.io reverse communications back to the UI
	// we do this as an overideable method rather than provided in the constructor, as often it may be require config data that has to be fetched after the app has been constructed so "this" is available
	protected abstract buildSocketIoServer(
			_expressServer: CommonsStrictExpressServer,
			_expressConfig: ICommonsExpressConfig
	): S;
	
	protected getSocketIoServer(): S|undefined {
		return this.socketIoServer;
	}

	protected async init(): Promise<void> {
		await super.init();

		const expressConfig: TPropertyObject = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');

		this.socketIoServer = this.buildSocketIoServer(
				this.getHttpServer(),
				expressConfig
		);
	}
}
