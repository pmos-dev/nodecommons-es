import { commonsTypeHasPropertyString } from 'tscommons-es-core';

import { ICommonsCredentials, isICommonsCredentials } from 'nodecommons-es-database';

export interface ICommonsPostgresCredentials extends ICommonsCredentials {
		user: string;
		password: string;
}

export function isICommonsPostgresCredentials(test: any): test is ICommonsPostgresCredentials {
	if (!isICommonsCredentials(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'user')) return false;
	if (!commonsTypeHasPropertyString(test, 'password')) return false;

	return true;
}
