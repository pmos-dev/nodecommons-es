import pgPromise from 'pg-promise';
import { IMain, IDatabase } from 'pg-promise';

import { TPropertyObject } from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';

import { ICommonsPostgresCredentials } from '../interfaces/icommons-postgres-credentials';

export function commonsPostgresServiceToUrl(credentials: ICommonsPostgresCredentials): string {
	return `postgres://${credentials.user}:${credentials.password}@${credentials.host || 'localhost'}:${credentials.port || 5432}/${credentials.name}`;
}

function rewriteSql(sql: string, params: TPropertyObject): string {
	for (const field of Object.keys(params)) {
		const regex: RegExp = new RegExp(`:${field}(\\W|$)`, 'g');
		sql = sql.replace(regex, '${' + field + '}$1');
	}
	
	return sql;
}

export class CommonsPostgresService extends CommonsSqlDatabaseService<ICommonsPostgresCredentials> {
	private database?: IDatabase<any>;
	
	constructor(credentials: ICommonsPostgresCredentials) {
		super(
				ECommonsDatabaseEngine.POSTGRES,
				credentials
		);
	}

	public async connect(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		const pgp: IMain = pgPromise({
				// Initialization Options
		});
		
		const cn: string = commonsPostgresServiceToUrl(this.credentials);
		this.database = pgp(cn);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
	}
	
	public async doesTableExist(name: string): Promise<boolean> {
		return (await this.internalSelect(`
				SELECT 1
				FROM information_schema.tables
				WHERE table_schema = 'public'
				AND table_name = ${this.quote(name)}
		`)).length > 0;
	}
	
	public async internalSelect(
			sql: string,
			params?: TPropertyObject
	): Promise<TPropertyObject[]> {
		if (!this.database) throw new Error('No connection to database');

		try {
			let rows: { [key: string]: unknown }[] = [];
			
			if (params) {
				sql = rewriteSql(sql, params);
				
				rows = await this.database.manyOrNone(sql, params);
			} else {
				rows = await this.database.manyOrNone(sql);
			}

			return rows as TPropertyObject[];
		} catch (e) {
			throw e;
		}
	}

	public async internalNone(
			sql: string,
			params?: TPropertyObject
	): Promise<void> {
		if (!this.database) throw new Error('No connection to database');

		try {
			if (params) {
				sql = rewriteSql(sql, params);
				
				await this.database.none(sql, params);
			} else {
				await this.database.none(sql);
			}
		} catch (e) {
			console.log(e);
			throw e;
		}
	}

	protected async internalTransactionBegin(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
	
	protected async internalTransactionCommit(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
	
	protected async internalTransactionRollback(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
}
