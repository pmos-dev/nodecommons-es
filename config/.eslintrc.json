{
	"env": {
		"es6": true
	},
	"extends": [
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"prettier"
	],
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"project": "tsconfig.json",
		"sourceType": "module"
	},
	"plugins": [
		"eslint-plugin-import",
		"eslint-plugin-jsdoc",
		"eslint-plugin-prefer-arrow",
		"@typescript-eslint"
	],
	"rules": {
		"@typescript-eslint/adjacent-overload-signatures": "error",
		"@typescript-eslint/array-type": [
			"error",
			{
				"default": "array"
			}
		],
		"@typescript-eslint/ban-types": [
			"error",
			{
				"types": {
					"Object": {
						"message": "Avoid using the `Object` type. Did you mean `object`?"
					},
					"Function": {
						"message": "Avoid using the `Function` type. Prefer a specific function type, like `() => void`."
					},
					"Boolean": {
						"message": "Avoid using the `Boolean` type. Did you mean `boolean`?"
					},
					"Number": {
						"message": "Avoid using the `Number` type. Did you mean `number`?"
					},
					"String": {
						"message": "Avoid using the `String` type. Did you mean `string`?"
					},
					"Symbol": {
						"message": "Avoid using the `Symbol` type. Did you mean `symbol`?"
					}
				}
			}
		],
		"@typescript-eslint/consistent-type-assertions": [
			"error",
			{
				"assertionStyle": "as",
				"objectLiteralTypeAssertions": "allow-as-parameter"
			}
		],
		"@typescript-eslint/consistent-type-definitions": "off",
		"@typescript-eslint/explicit-member-accessibility": [
			"error",
			{ "overrides":
				{
					"constructors": "no-public"
				}
			}
		],
		"@typescript-eslint/member-delimiter-style": "error",
		"@typescript-eslint/member-ordering": "off",
		"@typescript-eslint/naming-convention": "off",
		"no-empty-function": "off",
		"@typescript-eslint/no-empty-function": "error",
		"@typescript-eslint/no-empty-interface": "error",
		"@typescript-eslint/no-explicit-any": "off",
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/no-misused-new": "error",
		"@typescript-eslint/no-namespace": "off",
		"@typescript-eslint/no-non-null-assertion": "off",
		"@typescript-eslint/no-parameter-properties": [
			"error",
			{
				"allows": [ "private", "protected" ]
			}
		],
		"@typescript-eslint/no-shadow": [
			"error",
			{
				"hoist": "all"
			}
		],
		"@typescript-eslint/no-unused-expressions": "error",
		"no-unused-vars": "off",
		"@typescript-eslint/no-unused-vars": [
			"error",
			{ "argsIgnorePattern": "^_" }
		],
		"no-use-before-define": "off",
		"@typescript-eslint/no-use-before-define": "error",
		"no-useless-constructor": "off",
		"@typescript-eslint/no-useless-constructor": "error",
		"@typescript-eslint/no-var-requires": "error",
		"@typescript-eslint/prefer-for-of": "error",
		"@typescript-eslint/prefer-function-type": "error",
		"@typescript-eslint/prefer-namespace-keyword": "off",
		"quotes": "off",
		"@typescript-eslint/quotes": [
			"error",
			"single"
		],
		"@typescript-eslint/triple-slash-reference": [
			"error",
			{
				"path": "always",
				"types": "prefer-import",
				"lib": "always"
			}
		],
		"@typescript-eslint/unified-signatures": "error",
		"arrow-body-style": [ "error", "as-needed" ],
		"arrow-parens": [
			"error",
			"always"
		],
		"brace-style": [
			"error",
			"1tbs"
		],
		"comma-dangle": "error",
		"complexity": "off",
		"constructor-super": "error",
		"curly": [
			"error",
			"multi-line"
		],
		"dot-notation": "off",
		"eqeqeq": [
			"error",
			"always"
		],
		"guard-for-in": "error",
		"id-denylist": [
			"error",
			"any",
			"Number",
			"number",
			"String",
			"string",
			"Boolean",
			"boolean",
			"Undefined",
			"undefined"
		],
		"id-match": "error",
		"import/no-deprecated": "warn",
		"import/order": "error",
		"indent": [
			"error",
			"tab",
			{
				"ArrayExpression": 2,
				"ObjectExpression": 2,
				"ImportDeclaration": 2,
				"FunctionDeclaration": {
					"parameters": 2,
					"body": 1
				},
				"FunctionExpression": {
					"parameters": 2,
					"body": 1
				},
				"CallExpression": {
					"arguments": 2
				},
				"SwitchCase": 1,
				"MemberExpression": 2
			}
		],
		"max-classes-per-file": "off",
		"max-len": "off",
		"new-parens": "error",
		"no-bitwise": "error",
		"no-caller": "error",
		"no-cond-assign": [ "error", "except-parens" ],
		"no-empty": "error",
		"no-eval": "error",
		"no-fallthrough": "error",
		"no-invalid-this": "error",
		"no-multiple-empty-lines": [
			"error",
			{
				"max": 1
			}
		],
		"no-new-wrappers": "error",
		"no-shadow": "off",
		"no-sparse-arrays": "error",
		"no-template-curly-in-string": "error",
		"no-throw-literal": "error",
		"no-trailing-spaces": [
			"error",
			{
				"ignoreComments": true,
				"skipBlankLines": true
			}
		],
		"no-undef-init": "error",
		"no-underscore-dangle": "error",
		"no-unsafe-finally": "error",
		"no-unused-expressions": "error",
		"no-unused-labels": "error",
		"no-var": "error",
		"object-shorthand": [
			"error",
			"never"
		],
		"one-var": [
			"error",
			"never"
		],
		"prefer-arrow/prefer-arrow-functions": [
			"error",
			{
				"allowStandaloneDeclarations": true
			}
		],
		"prefer-const": "error",
		"quote-props": [
			"error",
			"as-needed"
		],
		"radix": "error",
		"semi": "error",
		"use-isnan": "error",
		"valid-typeof": "error"
	}
}
