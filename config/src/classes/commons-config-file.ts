import {
		commonsTypeIsEncodedObject,
		commonsTypeDecodePropertyObject,
		commonsTypeEncodePropertyObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { CommonsConfig } from 'tscommons-es-config';

import {
		commonsFileReadTextFile,
		commonsFileWriteTextFile,
		commonsFileIsFile
} from 'nodecommons-es-file';

export function readFileAsPropertyObject(filepath: string): TPropertyObject {
	const data: string|undefined = commonsFileReadTextFile(filepath);
	if (data === undefined) throw new Error(`Unable to read config file from ${filepath}`);
	
	const parsed: unknown = JSON.parse(data);
	if (!commonsTypeIsEncodedObject(parsed)) throw new Error('Config file is not JSON object');

	return commonsTypeDecodePropertyObject(parsed);
}

export class CommonsConfigFile extends CommonsConfig {
	private filepath: string|undefined;
	
	constructor(
			filepath?: string
	) {
		super(
				(filepath && commonsFileIsFile(filepath))
					? readFileAsPropertyObject(filepath)
					: {}
		);
		
		this.filepath = filepath;
	}
	
	public getFilepath(): string|undefined {
		return this.filepath;
	}
	
	public saveAs(filepath: string): void {
		const encoded: TEncodedObject = commonsTypeEncodePropertyObject(super.getRawObject());
		const stringify: string = JSON.stringify(encoded, null, '\t');
		
		commonsFileWriteTextFile(filepath, stringify);
	}
	
	public save(): void {
		if (!this.filepath) throw new Error('No filepath was supplied at the time of CommonsConfigFile creation. Cannot save.');
		this.saveAs(this.filepath);
	}
}
