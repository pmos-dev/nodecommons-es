import { commonsEnvGetStringOrUndefined } from 'nodecommons-es-cli';
import { commonsFileCwdRelativeOrAbsolutePath } from 'nodecommons-es-file';

import { CommonsConfigFile } from './commons-config-file';

function assertEnvironmentVariable(variable: string): string {
	const file: string|undefined = commonsEnvGetStringOrUndefined(variable);
	if (!file) throw new Error(`No such environment variable exists for ${variable}`);

	return commonsFileCwdRelativeOrAbsolutePath(file);
}

export class CommonsConfigEnvironmentVariable extends CommonsConfigFile {
	
	constructor(
			variable: string
	) {
		super(assertEnvironmentVariable(variable));
	}
}
