export class CommonsByteWriter {
	private data: Buffer = Buffer.from([]);
	private offset: number = 0;
	private length: number = 0;
	private memory: number[] = [];
	private points: Map<string, number> = new Map<string, number>();

	constructor(
			private expansionSize: number = 1024
	) {}

	public getOffset(): number {
		return this.offset;
	}

	public getLength(): number {
		return this.length;
	}

	public jump(offset: number): void {
		this.offset = offset;
	}

	public skip(offset: number): void {
		this.offset += offset;
	}

	public remember(): void {
		this.memory.push(this.offset);
	}

	public recall(): void {
		if (this.memory.length === 0) throw new Error('Remember underflow');

		this.offset = this.memory.pop()!;
	}

	public forget(all: boolean): void {
		if (all) this.memory = [];
		else this.memory.pop();
	}

	public store(name: string): void {
		this.points.set(name, this.offset);
	}

	public restore(name: string): void {
		if (!this.points.has(name)) throw new Error('No such named store to restore');

		this.offset = this.points.get(name)!;
	}

	public reset(): void {
		this.data = Buffer.from([]);
	}

	private prep(size: number): void {
		while ((this.offset + size) >= this.data.length) {
			const needed: number = ((this.offset + size) - this.data.length) + 1;

			// the extra expansion size makes it a bit more efficient than expanding each time a new byte is written etc.
			const expanded: Buffer = Buffer.alloc(this.offset + needed + this.expansionSize, 0);

			this.data.copy(expanded, 0, 0, this.length);

			this.data = expanded;
		}
	}

	public writeStringUtf8(value: string): void {
		this.prep(value.length);

		this.data.write(value, this.offset, 'utf8');
		this.offset += value.length;
		this.length = Math.max(this.length, this.offset);
	}

	public writeUInt8(value: number): void {
		this.prep(1);

		this.data.writeUInt8(value, this.offset);
		this.offset++;
		this.length = Math.max(this.length, this.offset);
	}

	public writeUInt16LE(value: number): void {
		this.prep(2);

		this.data.writeUInt16LE(value, this.offset);
		this.offset += 2;
		this.length = Math.max(this.length, this.offset);
	}

	public writeUInt32LE(value: number): void {
		this.prep(4);

		this.data.writeUInt32LE(value, this.offset);
		this.offset += 4;
		this.length = Math.max(this.length, this.offset);
	}

	public writeUInt16BE(value: number): void {
		this.prep(2);

		this.data.writeUInt16BE(value, this.offset);
		this.offset += 2;
		this.length = Math.max(this.length, this.offset);
	}

	public writeUInt32BE(value: number): void {
		this.prep(4);

		this.data.writeUInt32BE(value, this.offset);
		this.offset += 4;
		this.length = Math.max(this.length, this.offset);
	}

	public toBuffer(): Buffer {
		const clone: Buffer = Buffer.allocUnsafe(this.length);
		this.data.copy(clone, 0, 0, this.length);

		return clone;
	}
}
