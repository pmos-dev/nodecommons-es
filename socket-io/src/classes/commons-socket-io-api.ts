import { Socket } from 'socket.io';

import { CommonsSocketIoServer } from '../servers/commons-socket-io.server';

type TCallback = (_: any) => any;

export abstract class CommonsSocketIoApi {
	constructor(
			private socketIoServer: CommonsSocketIoServer
	) {}

	protected on(
			command: string,
			callback: TCallback
	): void {
		this.socketIoServer.on(command, callback);
	}

	public onAsync(
			command: string,
			callback: TCallback
	): void {
		this.socketIoServer.onAsync(command, callback);
	}
	
	public async broadcast(
			command: string,
			data?: any
	): Promise<void> {
		await this.socketIoServer.broadcast(command, data);
	}

	public async direct(
			socket: Socket,
			command: string,
			data?: any
	): Promise<void> {
		await this.socketIoServer.direct(socket, command, data);
	}
}
