import * as http from 'http';
import * as https from 'https';

import { Socket, Server, ServerOptions } from 'socket.io';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { ICommonsHttpConfig, commonsNginxProxyPath } from 'nodecommons-es-http';

type TResponse = (_?: any) => void;
type TCallback = (_: any) => any;
type TInternalOn = (_: any, _2: TResponse) => void;
type TInternalOnAsync = (_: any, _2: TResponse) => Promise<void>;

export function commonsSocketIoBuildDefaultOptions(httpConfig: ICommonsHttpConfig): Partial<ServerOptions> {
	const path: string = commonsNginxProxyPath(httpConfig.path);

	const options: Partial<ServerOptions> = {
			transports: [ 'websocket', 'polling' ]
	};
	
	if (path !== undefined) options.path = `${path}${path.endsWith('/') ? '' : '/'}socket.io`;
	
	return options;
}

export abstract class CommonsSocketIoServer {
	private socketIo: Server;
	private enabled: boolean|undefined;
	private sockets: Map<string, Socket> = new Map<string, Socket>();
	private ons: Map<string, TInternalOn> = new Map<string, TInternalOn>();
	private onAsyncs: Map<string, TInternalOnAsync> = new Map<string, TInternalOnAsync>();
	private ids: string[] = [];

	constructor(
			httpServer: http.Server|https.Server,
			private defaultEnabled?: boolean|undefined,
			options?: Partial<ServerOptions>
	) {
		this.socketIo = new Server(httpServer, options);
		
		this.init();
		
		this.enabled = this.defaultEnabled === false ? false : true;
	}

	protected onConnect(id: string, socket: Socket): boolean {
		this.sockets.set(id, socket);
		return true;
	}
	protected onDisconnect(id: string): void {
		this.sockets.delete(id);
	}
	
	private init(): void {
		this.socketIo.on('connection', (socket: Socket): void => {
			let id: string = '';
			for (let i = 100; i-- > 0;) {
				id = commonsBase62GenerateRandomId();
				if (!this.ids.includes(id)) break;
			}
			if (id === '') throw new Error('Unable to generate ID. This is extremely unlikely?');
			
			if (!this.onConnect(id, socket)) {
				socket.disconnect(true);
				return;
			}
			
			for (const command of this.ons.keys()) {
				const callback: TInternalOn = this.ons.get(command)!;
				socket.on(command, callback);
			}
			for (const command of this.onAsyncs.keys()) {
				const callback: TInternalOnAsync = this.onAsyncs.get(command)!;
				socket.on(
						command,
						(a: any, response: TResponse): void => {
							void callback(a, response);
						}
				);
			}
			
			socket.on('disconnect', (): void => {
				this.onDisconnect(id);
			});

			// This isn't part of the socket.io library, so is implemented manually
			socket.on(
					'commons-socket.io-identify-self-uid',
					(_: unknown, response: TResponse): void => {
						response(id);
					}
			);
		});
	}

	protected getSocketById(id: string): Socket|undefined {
		return this.sockets.get(id);
	}
	
	public kickAll(): void {
		for (const socket of this.sockets.values()) {
			socket.disconnect(true);
		}
	}

	public setEnabled(state: boolean): void {
		this.enabled = state;
	}
	
	public getEnabled(): boolean {
		return this.enabled ? true : false;
	}

	public on(
			command: string,
			callback: TCallback
	): void {
		this.ons.set(command, (data: any, response: TResponse) => {
			const outcome: any = callback(data);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
			if (outcome !== undefined) response(outcome);
			else response();
		});
	}

	public onAsync(
			command: string,
			callback: TCallback
	): void {
		this.onAsyncs.set(command, async (data: any, response: TResponse) => {
			try {
				const outcome: any = await callback(data);	// eslint-disable-line @typescript-eslint/no-unsafe-assignment
				if (outcome !== undefined) response(outcome);
				else response();
			} catch (e) {
				response(e);
			}
		});
	}

	public async broadcast(
			command: string,
			data?: any
	): Promise<void> {
		return new Promise<void>((resolve, reject): void => {
			if (!this.getEnabled()) {
				reject();
				return;
			}
		
			if (data === undefined) {
				this.socketIo.emit(command);
				resolve();
			} else {
				this.socketIo.emit(command, data);
				resolve();
			}
		});
	}

	public async direct(
			socket: Socket,
			command: string,
			data?: any
	): Promise<void> {
		return new Promise<void>((resolve, reject): void => {
			if (!this.getEnabled()) {
				reject();
				return;
			}
		
			if (data === undefined) {
				socket.emit(command);
				resolve();
			} else {
				socket.emit(command, data);
				resolve();
			}
		});
	}
}
