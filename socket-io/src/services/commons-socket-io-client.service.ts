import { io, Socket, ManagerOptions } from 'socket.io-client';

import { commonsBase62IsId, commonsTypeIsString } from 'tscommons-es-core';

type TConnectionCallback = () => void;
type TErrorCallback = (error: Error) => void;

export function commonsSocketIoBuildOptions(url: string): Partial<ManagerOptions> {
	const options: Partial<ManagerOptions> = {
			transports: [ 'websocket', 'polling' ]
	};

	if (url.startsWith('/')) {
		options.path = `${url}${url.endsWith('/') ? '' : '/'}socket.io`;
	} else {
		const regexp: RegExp = /^http(?:s?):\/\/[^\/]+\/(.+)$/i;
		const match: RegExpExecArray|null = regexp.exec(url);
		
		if (match !== null) {
			options.path = `/${match[1]}${match[1].endsWith('/') ? '' : '/'}socket.io`;
		}
	}
	
	return options;
}

export function commonsSocketIoBuildUrl(url: string): string {
	if (url.startsWith('/')) return '/';
	
	const regexp: RegExp = /^(http(?:s?):\/\/[^\/]+\/).+$/i;
	const match: RegExpExecArray|null = regexp.exec(url);
	
	if (match !== null) return match[1];
	
	return url;
}

export abstract class CommonsSocketIoClientService {
	private connectCallbacks: TConnectionCallback[] = [];
	private disconnectCallbacks: TConnectionCallback[] = [];
	private errorCallbacks: TErrorCallback[] = [];

	private socket?: Socket;
	private connected: boolean = false;
	private enabled: boolean|undefined;

	constructor(
		private url: string,
		private defaultEnabled?: boolean|undefined,
		private options?: Partial<ManagerOptions>
	) {}

	public addConnectCallback(callback: TConnectionCallback): void {
		this.connectCallbacks.push(callback);
	}

	public addDisconnectCallback(callback: TConnectionCallback): void {
		this.disconnectCallbacks.push(callback);
	}

	public addErrorCallback(callback: TErrorCallback): void {
		this.errorCallbacks.push(callback);
	}

	// This isn't part of the socket.io library, so requests any server to implement it manually
	public async getId(): Promise<string|undefined> {
		const result: unknown = await this.emit('commons-socket.io-identify-self-uid', {});
		if (!commonsTypeIsString(result)) return undefined;
		if (!commonsBase62IsId(result)) return undefined;

		return result;
	}

	public connect(params?: { [ key: string ]: string|number|boolean }|undefined): boolean {
		if (this.socket) return true;	// already connected

		const options: Partial<ManagerOptions> = this.options || {};
		
		if (params) {
			const asStrings: { [ key: string ]: string } = {};
			
			for (const key of Object.keys(params)) {
				asStrings[key] = params[key].toString();
			}
			options.query = asStrings;
		}

		this.socket = io(this.url, options);
		if (!this.socket) return false;
		
		this.socket.on('connect', (): void => {
			this.connected = true;
			if (this.enabled === undefined) this.enabled = this.defaultEnabled === false ? false : true;
			for (const callback of this.connectCallbacks) callback();
		});
		this.socket.on('disconnect', (): void => {
			this.connected = false;
			for (const callback of this.disconnectCallbacks) callback();
		});
		this.socket.on('error', (e: Error): void => {
			for (const callback of this.errorCallbacks) callback(e);
		});
		this.socket.on('connect_error', (e: Error): void => {
			for (const callback of this.errorCallbacks) callback(e);
		});

		this.setupOns();
		
		return true;
	}
	
	public disconnect(): void {
		if (this.socket === undefined) return;
		this.socket.disconnect();
	}

	protected abstract setupOns(): void;

	public setEnabled(state: boolean): void {
		this.enabled = state;
	}
	
	public getEnabled(): boolean {
		return this.enabled ? true : false;
	}

	public isConnected(): boolean {
		return this.socket !== undefined && this.connected;
	}

	protected on(
			command: string,
			callback: (data: any) => void
	): void {
		if (!this.socket) return;	// shouldn't really be possible if the class is being used properly with setupOns
		
		this.socket.on(
				command,
				(data: any): void => {
					if (!this.isConnected()) return;	// is this possible?
					if (!this.getEnabled()) return;
					
					callback(data);
				}
		);
	}

	protected async emit(
			command: string,
			data: any
	): Promise<any> {
		return new Promise((resolve, reject): void => {
			if (this.socket === undefined || !this.isConnected() || !this.getEnabled()) {
				reject();
				return;
			}
		
			this.socket.emit(command, data, (response: any): void => {
				resolve(response);
			});
		});
	}
}
