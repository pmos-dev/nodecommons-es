import {
		commonsTypeHasProperty,
		commonsTypeHasPropertyObject,
		commonsBase62GenerateRandomId
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsStepFlowStep } from 'tscommons-es-models-step-flow';
import { ECommonsStepFlowStepType, fromECommonsStepFlowStepType, toECommonsStepFlowStepType, ECOMMONS_STEP_FLOW_STEP_TYPES } from 'tscommons-es-models-step-flow';

import { CommonsDatabaseTypeId, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeEnum } from 'nodecommons-es-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-es-database';
import { CommonsDatabaseParam } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { CommonsDatabaseUniqueKey } from 'nodecommons-es-database';
import { CommonsDatabaseIndexKey } from 'nodecommons-es-database';
import { TCommonsDatabaseTypes } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { TCommonsModelForeignKeys } from 'nodecommons-es-models';
import { CommonsUniqueNamedHelper } from 'nodecommons-es-models-adamantine';

export abstract class CommonsStepFlowStepModel<
		T extends ICommonsSecondClass<P> & ICommonsStepFlowStep,
		P extends ICommonsFirstClass
> extends CommonsSecondClass<T, P> {
	public static extendStructure(
			structure: TCommonsDatabaseTypes,
			tableName: string
	): TCommonsDatabaseTypes {
		if (commonsTypeHasProperty(structure, 'type')) throw new Error('The type field is being added automatically for this StepFlow model and should not be explicitly stated in the structure');
		if (commonsTypeHasPropertyObject(structure, 'uid')) throw new Error('The uid field is being added automatically for this StepFlow model and should not be explicitly stated in the structure');
		
		return {
				...(structure as TCommonsDatabaseTypes),	// TS fudge
				type: new CommonsDatabaseTypeEnum<ECommonsStepFlowStepType>(
						ECOMMONS_STEP_FLOW_STEP_TYPES,
						fromECommonsStepFlowStepType,
						toECommonsStepFlowStepType,
						ECommonsDatabaseTypeNull.NOT_NULL,
						ECommonsStepFlowStepType.DATA,
						undefined,
						`${tableName}__ECommonsStepFlowStepType`
				),
				uid: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL)
		};
	}
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			structure: TCommonsDatabaseTypes,
			parentModel: CommonsFirstClass<P>,
			private parentField: string,
			foreignKeys?: TCommonsModelForeignKeys,
			uniqueKeys?: CommonsDatabaseUniqueKey[],
			indexKeys?: CommonsDatabaseIndexKey[]
	) {
		super(
				database,
				tableName,
				CommonsStepFlowStepModel.extendStructure(structure, tableName),
				parentModel,
				parentField,
				foreignKeys,
				CommonsUniqueNamedHelper.extendUniqueKeysForFirstClass(
						tableName,
						'uid',
						uniqueKeys
				),
				indexKeys
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();
		
		CommonsUniqueNamedHelper.build<T>(this.database)
				.buildPreprepareForFirstClass(
						this,
						'uid',
						true
				);
	}

	public async getByUid(uid: string): Promise<T|undefined> {
		return CommonsUniqueNamedHelper.build<T>(this.database)
				.getFirstClassByName(
						this,
						uid,
						'uid'
				);
	}

	public async getRootByParent(parent: P): Promise<T|undefined> {
		const params: {
				[key: string]: CommonsDatabaseParam;
		} = {
				type: new CommonsDatabaseParam(ECommonsStepFlowStepType.ROOT, this.structure['type'])
		};
		params[this.parentField] = new CommonsDatabaseParam(parent.id, new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL));
		
		const rows: unknown[] = await this.database
				.selectRowsByConditions(
						this.tableName,
						this.structure,
						params
				);
		
		if (rows.length === 0) return undefined;
		
		return rows[0] as T;
	}
	
	public async create(
			parent: P,
			type: ECommonsStepFlowStepType,
			data: TPropertyObject
	): Promise<T> {
		data['type'] = type;
		data['uid'] = commonsBase62GenerateRandomId();
		
		return await this.insertForFirstClass(
				parent,
				data
		);
	}
}
