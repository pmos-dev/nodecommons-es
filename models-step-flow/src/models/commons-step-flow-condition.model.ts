import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsOrientatedOrdered } from 'tscommons-es-models';
import { ICommonsStepFlowStep } from 'tscommons-es-models-step-flow';
import { ICommonsStepFlowFlow } from 'tscommons-es-models-step-flow';
import { ICommonsStepFlowCondition } from 'tscommons-es-models-step-flow';

import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsOrientatedOrdered } from 'nodecommons-es-models';

export abstract class CommonsStepFlowConditionModel<
		C extends ICommonsSecondClass<F> & ICommonsStepFlowCondition<F, S>,
		F extends ICommonsOrientatedOrdered<S> & ICommonsStepFlowFlow<S>,
		S extends ICommonsSecondClass<P> & ICommonsStepFlowStep,
		P extends ICommonsFirstClass
> extends CommonsSecondClass<C, F> {}

export abstract class CommonsStepFlowConditionOrderedModel<
		C extends ICommonsOrientatedOrdered<F> & ICommonsStepFlowCondition<F, S>,
		F extends ICommonsOrientatedOrdered<S> & ICommonsStepFlowFlow<S>,
		S extends ICommonsSecondClass<P> & ICommonsStepFlowStep,
		P extends ICommonsFirstClass
> extends CommonsOrientatedOrdered<C, F> {}
