import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsSecondClass } from 'tscommons-es-models';
import { ICommonsStepFlowStep } from 'tscommons-es-models-step-flow';
import { ICommonsStepFlowFlow } from 'tscommons-es-models-step-flow';

import { CommonsDatabaseTypeId, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseForeignKey } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull, ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { CommonsOrientatedOrdered } from 'nodecommons-es-models';

import { CommonsStepFlowStepModel } from './commons-step-flow-step.model';

export abstract class CommonsStepFlowFlowModel<
		F extends ICommonsSecondClass<S> & ICommonsStepFlowFlow<S>,
		S extends ICommonsSecondClass<P> & ICommonsStepFlowStep,
		P extends ICommonsFirstClass
> extends CommonsOrientatedOrdered<
		F,
		S
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			tableName: string,
			stepModel: CommonsStepFlowStepModel<S, P>,
			stepField: string
	) {
		super(
				database,
				tableName,
				{
						outstep: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				stepModel,
				stepField,
				{
						outstep: new CommonsDatabaseForeignKey(
								stepModel.getTable(),
								stepModel.getIdField(),
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						)
				}
		);
	}
}
