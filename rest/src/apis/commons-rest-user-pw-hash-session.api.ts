import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsSession } from 'tscommons-es-session';

import { CommonsUserPwHashSessionService } from 'nodecommons-es-security';
import { ICommonsUserPwHash } from 'nodecommons-es-security';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestUserSessionApi } from './commons-rest-user-session.api';

export abstract class CommonsRestUserPwHashSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		UserT extends ICommonsUserPwHash,
		SessionServiceT extends CommonsUserPwHashSessionService<UserT>
> extends CommonsRestUserSessionApi<
		RequestT,
		ResponseT,
		UserT,
		SessionServiceT
> {
	protected async authenticate(	// eslint-disable-line @typescript-eslint/require-await
			user: UserT,
			sessionService: SessionServiceT,
			req: RequestT
	): Promise<ICommonsSession<UserT>|undefined> {
		const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

		if (!commonsTypeHasPropertyString(bodyRequestTypecast.body, 'pw')) return undefined;
		
		return sessionService.authenticateWithPwHash(
				user,
				bodyRequestTypecast.body.pw as string
		);
	}
}
