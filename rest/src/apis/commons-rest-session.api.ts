import { TEncoded } from 'tscommons-es-core';
import { ICommonsSession } from 'tscommons-es-session';

import { CommonsSessionService } from 'nodecommons-es-security';
import { ICommonsRequestWithGetMethodTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestServer } from '../servers/commons-rest.server';

import { ICommonsRequestWithSession } from '../interfaces/icommons-request-with-session';

import { TCommonsContentHandler } from '../types/tcommons-content-handler';
import { TCommonsNoContentHandler } from '../types/tcommons-no-content-handler';
import { TCommonsSessionContentHandler } from '../types/tcommons-session-content-handler';
import { TCommonsSessionNoContentHandler } from '../types/tcommons-session-no-content-handler';
import { TCommonsContent } from '../types/tcommons-content';

import {
		CommonsRestApi,
		commonsRestApiUnauthorized,
		commonsRestApiForbidden
} from './commons-rest.api';

export abstract class CommonsRestSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		SessionT,
		SessionServiceT extends CommonsSessionService<SessionT>
> extends CommonsRestApi<RequestT, ResponseT> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			protected sessionService: SessionServiceT
	) {
		super(restServer);
	}
	
	protected async validate(sid: string): Promise<ICommonsSession<SessionT>|undefined> {
		return this.sessionService.validate(sid);
	}
	
	public installSession(
			sessionQuery: string
	): void {
		this.headSessionHandler(
				`${sessionQuery}`,
				async (_req: RequestT, _res: ResponseT): Promise<void> => {
					// nothing to do
				}
		);

		this.deleteSessionHandler(
				`${sessionQuery}`,
				async (req: ICommonsRequestWithSession<SessionT>, _res: ResponseT): Promise<boolean> => {	// eslint-disable-line @typescript-eslint/require-await
					this.sessionService.destroy(req.session);
					return true;
				}
		);
	}
	
	private wrapNoContentHandler(handler: TCommonsSessionNoContentHandler<RequestT, ResponseT, SessionT>): TCommonsNoContentHandler<RequestT, ResponseT> {
		return async (request: RequestT, response: ResponseT): Promise<number|void> => {
			// nasty hack
			const authorization: string|undefined = (request as unknown as ICommonsRequestWithGetMethodTypecast).get('Authorization');
			if (authorization === undefined) return commonsRestApiUnauthorized('No authorization header supplied');
			
			const regex: RegExp = /^session (.+)$/;
			const result: RegExpExecArray|null = regex.exec(authorization);
			if (result === null) return commonsRestApiUnauthorized('Invalid authorization header supplied');

			const session: ICommonsSession<SessionT>|undefined = await this.sessionService.validate(result[1]);
			if (!session) return commonsRestApiForbidden('Session ID is not accepted');
			
			const typecast: RequestT & ICommonsRequestWithSession<SessionT> = request as RequestT & ICommonsRequestWithSession<SessionT>;
			typecast.session = session;
			
			return await handler(
					typecast,
					response
			);
		};
	}
	
	private wrapContentHandler(handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>): TCommonsContentHandler<RequestT, ResponseT> {
		return async (request: RequestT, response: ResponseT): Promise<TCommonsContent|TEncoded> => {
			// nasty hack
			const authorization: string|undefined = (request as unknown as ICommonsRequestWithGetMethodTypecast).get('Authorization');
			if (authorization === undefined) return commonsRestApiUnauthorized('No authorization header supplied');
			
			const regex: RegExp = /^session (.+)$/;
			const result: RegExpExecArray|null = regex.exec(authorization);
			if (result === null) return commonsRestApiUnauthorized('Invalid authorization header supplied');

			const session: ICommonsSession<SessionT>|undefined = await this.sessionService.validate(result[1]);
			if (!session) return commonsRestApiForbidden('Session ID is not accepted');
			
			const typecast: RequestT & ICommonsRequestWithSession<SessionT> = request as RequestT & ICommonsRequestWithSession<SessionT>;
			typecast.session = session;
			
			return await handler(
					typecast,
					response
			);
		};
	}
	
	protected getSessionHandler(
			query: string,
			handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.getHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}

	// insert
	protected postSessionHandler(
			query: string,
			handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.postHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}

	// update
	protected patchSessionHandler(
			query: string,
			handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.patchHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	// upsert
	protected putSessionHandler(
			query: string,
			handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.putHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	protected deleteSessionHandler(
			query: string,
			handler: TCommonsSessionContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.deleteHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	protected headSessionHandler(
			query: string,
			handler: TCommonsSessionNoContentHandler<RequestT, ResponseT, SessionT>
	): void {
		super.headHandler(
				query,
				this.wrapNoContentHandler(handler)
		);
	}
}
