import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyObject,
		commonsTypeDecodePropertyObject,
		commonsObjectStripNulls,
		TEncodedObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';

import { ICommonsRequestWithBodyTypecast, ICommonsRequestWithQueryTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestServer } from '../servers/commons-rest.server';

import {
		CommonsRestApi,
		commonsRestApiBadRequest
} from './commons-rest.api';

type TChallengeResponse = {
		challenge: string;
};

export abstract class CommonsRestWebhookApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
> extends CommonsRestApi<
		RequestT,
		ResponseT
> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			private path: string,
			hook: string,
			namespace?: string
	) {
		super(restServer);
		
		super.getHandler(
				`${this.path}${namespace ? `${namespace}/` : ''}${hook}`,
				async (req: RequestT, _res: ResponseT): Promise<TChallengeResponse> => {	// eslint-disable-line @typescript-eslint/require-await
					// nasty hack
					const queryRequestTypecast: ICommonsRequestWithQueryTypecast = req as unknown as ICommonsRequestWithQueryTypecast;

					if (!commonsTypeHasPropertyString(queryRequestTypecast.query, 'challenge')) {
						return commonsRestApiBadRequest('No challenge supplied');
					}
					
					return {
							challenge: queryRequestTypecast.query.challenge as string
					};
				}
		);
		
		super.postHandler(
				`${this.path}${namespace ? `${namespace}/` : ''}${hook}`,
				async (req: RequestT, _res: ResponseT): Promise<string> => {
					if (!commonsTypeHasPropertyObject(req, 'body')) {
						return commonsRestApiBadRequest('No JSON body supplied');
					}
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					const data: TPropertyObject = commonsObjectStripNulls(commonsTypeDecodePropertyObject(bodyRequestTypecast.body as TEncodedObject));
					
					return await this.handle(data);
				}
		);
	}

	// NB, it is important that this method returns and doesn't throw errors, so it will need to be wrapped in a try-catch
	// The |never allows it to return CommonsRestApi.* errors
	protected abstract handle(data: TPropertyObject): Promise<string|never>;
}
