import { ICommonsSession } from 'tscommons-es-session';
import { ICommonsUser } from 'tscommons-es-session';

import {
		commonsOutputDebug,
		commonsOutputError
} from 'nodecommons-es-cli';
import { CommonsUserSessionService } from 'nodecommons-es-security';
import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestSessionApi } from './commons-rest-session.api';
import {
		commonsRestApiError,
		commonsRestApiNotFound,
		commonsRestApiForbidden
} from './commons-rest.api';

export abstract class CommonsRestUserSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		UserT extends ICommonsUser,
		SessionServiceT extends CommonsUserSessionService<UserT>
> extends CommonsRestSessionApi<
		RequestT,
		ResponseT,
		UserT,
		SessionServiceT
> {
	protected async authenticate(	// eslint-disable-line @typescript-eslint/require-await
			user: UserT,
			_sessionService: SessionServiceT,
			_req: RequestT
	): Promise<ICommonsSession<UserT>|undefined> {
		return this.sessionService.authenticate(user);
	}
	
	public installAuthentication(
			authenticateQuery: string,
			getUserByUsername: (
					username: string,
					req: RequestT
			) => Promise<UserT|undefined>,
			autoRegister?: (
					username: string,
					req: RequestT
			) => Promise<UserT|undefined>
	): void {
		super.postHandler(
				`${authenticateQuery}/:username[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<{ sid: string }> => {
					commonsOutputDebug(`Login requested with username: ${req.strictParams.username as string}`);
					
					let user: UserT|undefined = await getUserByUsername(
							req.strictParams.username as string,
							req
					);
					
					if (!user) {
						commonsOutputDebug('No such user exists');
						if (!autoRegister) return commonsRestApiNotFound('No such user exists.');

						commonsOutputDebug('Attempting auto-register');

						try {
							const registered: UserT|undefined = await autoRegister(
									req.strictParams.username as string,
									req
							);
							if (!registered) {
								commonsOutputDebug('Unable to register a new user for those details.');
								return commonsRestApiError('Unable to register a new user for those details.');
							}
							commonsOutputDebug('Successfully registered new user');
							
							user = registered;
						} catch (e) {
							commonsOutputError((e as Error).message);
							return commonsRestApiError((e as Error).message);
						}
					}
					
					const session: ICommonsSession<UserT>|undefined = await this.authenticate(user, this.sessionService, req);
					if (!session) {
						commonsOutputDebug('Unable to authenticate with those credentials.');
						return commonsRestApiForbidden('Unable to authenticate with those credentials.');
					}
					
					return { sid: session.sid };
				}
		);
	}
}
