import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsSession } from 'tscommons-es-session';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsSimpleSinglePwSessionService } from 'nodecommons-es-security';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestSessionApi } from './commons-rest-session.api';
import {
		commonsRestApiForbidden,
		commonsRestApiBadRequest
} from './commons-rest.api';

type TEmptyObject = { [ key: string ]: never };

export class CommonsRestSimpleSinglePwSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> extends CommonsRestSessionApi<
		RequestT,
		ResponseT,
		TEmptyObject,
		CommonsSimpleSinglePwSessionService
> {
	public installAuthentication(authenticateQuery: string): void {
		super.postHandler(
				`${authenticateQuery}`,
				async (req: RequestT, _res: ResponseT): Promise<{ sid: string }> => {	// eslint-disable-line @typescript-eslint/require-await
					commonsOutputDebug('Login requested with simple password');

					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;
					
					if (!commonsTypeHasPropertyString(bodyRequestTypecast.body, 'pw')) return commonsRestApiBadRequest('No password supplied');

					const session: ICommonsSession<TEmptyObject>|undefined = this.sessionService.authenticate(bodyRequestTypecast.body.pw as string);
					if (!session) {
						commonsOutputDebug('Unable to authenticate with that password.');
						return commonsRestApiForbidden('Unable to authenticate with that password.');
					}
					
					return { sid: session.sid };
				}
		);
	}
}
