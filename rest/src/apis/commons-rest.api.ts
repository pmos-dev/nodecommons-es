import {
		CommonsHttpError,
		CommonsHttpInternalServerError,
		CommonsHttpUnauthorizedError,
		CommonsHttpNotFoundError,
		CommonsHttpConflictError,
		CommonsHttpForbiddenError,
		CommonsHttpBadRequestError,
		CommonsHttpNotImplementedError,
		CommonsHttpServiceUnavailableError
} from 'tscommons-es-http';

import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestServer } from '../servers/commons-rest.server';

import { TCommonsContentHandler } from '../types/tcommons-content-handler';
import { TCommonsNoContentHandler } from '../types/tcommons-no-content-handler';

export function commonsRestApiError(message: string = '', status: number = 500): never {
	if (status === 500) throw new CommonsHttpInternalServerError(message);
	throw new CommonsHttpError(message, status);
}

export function commonsRestApiUnauthorized(message: string = ''): never {
	throw new CommonsHttpUnauthorizedError(message);
}

export function commonsRestApiNotFound(message: string = ''): never {
	throw new CommonsHttpNotFoundError(message);
}

export function commonsRestApiConflict(message: string = ''): never {
	throw new CommonsHttpConflictError(message);
}

export function commonsRestApiForbidden(message: string = ''): never {
	throw new CommonsHttpForbiddenError(message);
}

export function commonsRestApiBadRequest(message: string = ''): never {
	throw new CommonsHttpBadRequestError(message);
}

export function commonsRestApiNotImplemented(message: string = ''): never {
	throw new CommonsHttpNotImplementedError(message);
}

export function commonsRestApiServiceUnavailable(message: string = ''): never {
	throw new CommonsHttpServiceUnavailableError(message);
}

export abstract class CommonsRestApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> {
	
	constructor(
			private restServer: CommonsRestServer<RequestT, ResponseT>
	) {}

	protected getHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.get(query, handler);
	}

	// insert
	protected postHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.post(query, handler);
	}

	// update
	protected patchHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.patch(query, handler);
	}
	
	// upsert
	protected putHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.put(query, handler);
	}
	
	protected deleteHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.delete(query, handler);
	}
	
	protected headHandler(
			query: string,
			handler: TCommonsNoContentHandler<RequestT, ResponseT>
	): void {
		this.restServer.head(query, handler);
	}
}
