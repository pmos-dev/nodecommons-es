import { TEncoded } from 'tscommons-es-core';

import { ICommonsRequestWithGetMethodTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { CommonsRestServer } from '../servers/commons-rest.server';

import { TCommonsContentHandler } from '../types/tcommons-content-handler';
import { TCommonsNoContentHandler } from '../types/tcommons-no-content-handler';
import { TCommonsContent } from '../types/tcommons-content';

import {
		CommonsRestApi,
		commonsRestApiUnauthorized,
		commonsRestApiForbidden
} from './commons-rest.api';

export abstract class CommonsRestKeyApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> extends CommonsRestApi<RequestT, ResponseT> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			private authKey: string
	) {
		super(restServer);
	}
	
	private wrapNoContentHandler(handler: TCommonsNoContentHandler<RequestT, ResponseT>): TCommonsNoContentHandler<RequestT, ResponseT> {
		return async (request: RequestT, response: ResponseT): Promise<number|void> => {
			// nasty hack
			const authorization: string|undefined = (request as unknown as ICommonsRequestWithGetMethodTypecast).get('Authorization');
			if (authorization === undefined) return commonsRestApiUnauthorized('No authorization header supplied');
			
			const regex: RegExp = /^key (.+)$/;
			const result: RegExpExecArray|null = regex.exec(authorization);
			if (result === null) return commonsRestApiUnauthorized('Invalid authorization header supplied');
			
			if (result[1] !== this.authKey) return commonsRestApiForbidden('Key supplied is not accepted');
			
			return await handler(request, response);
		};
	}
	
	private wrapContentHandler(handler: TCommonsContentHandler<RequestT, ResponseT>): TCommonsContentHandler<RequestT, ResponseT> {
		return async (request: RequestT, response: ResponseT): Promise<TCommonsContent|TEncoded> => {
			// nasty hack
			const authorization: string|undefined = (request as unknown as ICommonsRequestWithGetMethodTypecast).get('Authorization');
			if (authorization === undefined) return commonsRestApiUnauthorized('No authorization header supplied');
			
			const regex: RegExp = /^key (.+)$/;
			const result: RegExpExecArray|null = regex.exec(authorization);
			if (result === null) return commonsRestApiUnauthorized('Invalid authorization header supplied');
			
			if (result[1] !== this.authKey) return commonsRestApiForbidden('Key supplied is not accepted');
			
			return await handler(request, response);
		};
	}
	
	protected getHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		super.getHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}

	// insert
	protected postHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		super.postHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}

	// update
	protected patchHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		super.patchHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	// upsert
	protected putHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		super.putHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	protected deleteHandler(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		super.deleteHandler(
				query,
				this.wrapContentHandler(handler)
		);
	}
	
	protected headHandler(
			query: string,
			handler: TCommonsNoContentHandler<RequestT, ResponseT>
	): void {
		super.headHandler(
				query,
				this.wrapNoContentHandler(handler)
		);
	}
}
