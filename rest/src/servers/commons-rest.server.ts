import { TEncoded } from 'tscommons-es-core';
import { CommonsHttpError } from 'tscommons-es-http';

import { CommonsStrictHttpServer, ICommonsStrictParamsRequest, TCommonsHttpResponse, commonsHttpNoCache } from 'nodecommons-es-http';

import { TCommonsContent, isTCommonsContent } from '../types/tcommons-content';
import { TCommonsContentHandler } from '../types/tcommons-content-handler';
import { TCommonsNoContentHandler } from '../types/tcommons-no-content-handler';

export class CommonsRestServer<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> {
	constructor(
			private httpServer: CommonsStrictHttpServer<RequestT, ResponseT>
	) {}
	
	private async handleContent(
			request: RequestT,
			response: ResponseT,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): Promise<void> {
		try {
			let content: TEncoded|undefined;
			let statusCode: number = 200;
			
			const result: TEncoded|TCommonsContent = await handler(request, response);
			if (isTCommonsContent(result)) {
				content = result.content;
				if (result.statusCode) statusCode = result.statusCode;
			} else content = result;
			
			commonsHttpNoCache(response);
			response.setHeader('Content-Type', 'application/json');
			
			if (statusCode === 204) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
				response.status(statusCode).send();
			} else {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
				response.status(statusCode).send(JSON.stringify(content));
			}
		} catch (e) {
			if (e instanceof CommonsHttpError) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
				response.status(e.httpResponseCode).send(e.message);
				return;
			}
			
			console.error(e);
			throw e;
		}
	}
	
	private async handleNoContent(
			request: RequestT,
			response: ResponseT,
			handler: TCommonsNoContentHandler<RequestT, ResponseT>
	): Promise<void> {
		try {
			const statusCode: number|undefined|void = await handler(request, response);
			commonsHttpNoCache(response);
			// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
			response.status(statusCode || 204).send();
		} catch (e) {
			if (e instanceof CommonsHttpError) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
				response.status(e.httpResponseCode).send(e.message);
				return;
			}
			
			console.error(e);
			throw e;
		}
	}

	public head(
			query: string,
			handler: TCommonsNoContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.head(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleNoContent(request, response, handler);
		});
	}

	public get(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.get(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleContent(request, response, handler);
		});
	}

	public post(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.post(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleContent(request, response, handler);
		});
	}

	public put(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.put(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleContent(request, response, handler);
		});
	}

	public patch(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.patch(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleContent(request, response, handler);
		});
	}

	public delete(
			query: string,
			handler: TCommonsContentHandler<RequestT, ResponseT>
	): void {
		this.httpServer.delete(query, async (
				request: RequestT,
				response: ResponseT
		): Promise<void> => {
			await this.handleContent(request, response, handler);
		});
	}
}
