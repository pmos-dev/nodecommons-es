import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { ICommonsRequestWithSession } from '../interfaces/icommons-request-with-session';

export type TCommonsSessionNoContentHandler<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		SessionT
> = (
		request: RequestT & ICommonsRequestWithSession<SessionT>,
		response: ResponseT
) => Promise<number|void>;
