import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

export type TCommonsNoContentHandler<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> = (
		request: RequestT,
		response: ResponseT
) => Promise<number|void>;
