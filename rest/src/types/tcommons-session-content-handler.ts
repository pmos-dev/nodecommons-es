import { TEncoded } from 'tscommons-es-core';

import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { ICommonsRequestWithSession } from '../interfaces/icommons-request-with-session';

import { TCommonsContent } from './tcommons-content';

export type TCommonsSessionContentHandler<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		SessionT
> = (
		request: RequestT & ICommonsRequestWithSession<SessionT>,
		response: ResponseT
) => Promise<TCommonsContent|TEncoded>;
