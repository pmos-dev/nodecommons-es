import { ECommonsHttpMethod } from 'tscommons-es-http';
import { THttpHeaderOrParamObject } from 'tscommons-es-http';

export type TCommonsHttpRequest = {
		protocol: string;
		hostname: string;
		port: number;
		path: string;
		
		method: ECommonsHttpMethod;
		headers: THttpHeaderOrParamObject;
		timeout?: number;
};
