import { TEncoded } from 'tscommons-es-core';

import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { TCommonsContent } from './tcommons-content';

export type TCommonsContentHandler<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse
> = (
		request: RequestT,
		response: ResponseT
) => Promise<TCommonsContent|TEncoded>;
