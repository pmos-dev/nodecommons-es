import {
		commonsTypeIsObject,
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumberOrUndefined,
		TEncoded
} from 'tscommons-es-core';

export type TCommonsContent = {
		statusCode?: number;
		content: TEncoded;
};

export function isTCommonsContent(test: any): test is TCommonsContent {
	if (!commonsTypeIsObject(test)) return false;

	const attempt: TCommonsContent = test as TCommonsContent;
	
	if (!commonsTypeHasProperty(attempt, 'content')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(attempt, 'statusCode')) return false;

	return true;
}
