import { ICommonsSession } from 'tscommons-es-session';

import { ICommonsStrictParamsRequest } from 'nodecommons-es-http';

export interface ICommonsRequestWithSession<SessionT> extends ICommonsStrictParamsRequest {
		session: ICommonsSession<SessionT>;
}
