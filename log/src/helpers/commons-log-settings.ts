import { commonsNumberParseByteSize, commonsTypeIsString } from 'tscommons-es-core';

import { CommonsArgs } from 'nodecommons-es-cli';

import { CommonsStdErrLog } from '../classes/commons-std-err-log';
import { CommonsBoundedFileLog } from '../classes/commons-bounded-file-log';
import { CommonsFileLog } from '../classes/commons-file-log';

import { ICommonsLogConfig, isICommonsLogConfig, TCommonsLogParsedConfig } from '../interfaces/icommons-log-config';
import { CommonsLoggable } from '../interfaces/commons-log';

import { commonsLogReplace } from './commons-log-globals';

export function commonsLogBuildConfig(
		config?: unknown,
		args?: CommonsArgs
): TCommonsLogParsedConfig|undefined {
	if (!isICommonsLogConfig(config)) config = undefined;

	const configTypecast: ICommonsLogConfig|undefined = config as ICommonsLogConfig|undefined;

	let stdErr: boolean = false;
	if (configTypecast && configTypecast.stdErr) stdErr = true;
	if (args && args.hasAttribute('log-stderr')) stdErr = true;

	let logFile: string|undefined;
	if (configTypecast) logFile = configTypecast.file;
	if (args) {
		const attempt: string|undefined = args.getStringOrUndefined('log-file');
		if (attempt) logFile = attempt;
	}

	const built: TCommonsLogParsedConfig = {};
	if (stdErr) built.stdErr = true;

	if (logFile) {
		built.file = logFile;

		let maxSize: string|number|undefined;

		if (configTypecast && configTypecast.maxSize !== undefined) maxSize = configTypecast.maxSize;
		if (args) {
			const attempt: string|undefined = args.getStringOrUndefined('log-file-max-size');
			if (attempt !== undefined && attempt.trim() !== '') maxSize = attempt.trim();
		}

		if (commonsTypeIsString(maxSize)) {
			maxSize = commonsNumberParseByteSize(maxSize);
		}

		if (maxSize !== undefined) {
			built.maxSize = maxSize;

			let maxHistorical: number|undefined;

			if (configTypecast && configTypecast.maxHistorical !== undefined) maxHistorical = configTypecast.maxHistorical;
			if (args) {
				const numberAttempt: number|undefined = args.getNumberOrUndefined('log-file-max-historical');
				if (numberAttempt !== undefined) maxHistorical = numberAttempt;
			}

			if (maxHistorical !== undefined) built.maxHistorical = maxHistorical;
		}
	}

	return built;
}

export function commonsLogAutoReplace(
		context: string,
		config?: unknown,
		args?: CommonsArgs
): void {
	const built: TCommonsLogParsedConfig|undefined = commonsLogBuildConfig(config, args);
	if (!built) return;

	const replacements: CommonsLoggable[] = [];

	if (built.stdErr) replacements.push(new CommonsStdErrLog(context));

	if (built.file) {
		if (built.maxSize !== undefined) {
			replacements.push(
					new CommonsBoundedFileLog(
							built.file,
							built.maxSize,
							context,
							built.maxHistorical
					)
			);
		} else {
			replacements.push(new CommonsFileLog(built.file, context));
		}
	}

	commonsLogReplace(replacements);
}
