import { TPrimative } from 'tscommons-es-core';

import { CommonsLoggable, CommonsSubLoggable } from '../interfaces/commons-log';

import { CommonsMultiLog } from '../classes/commons-multi-log';
import { CommonsStdErrLog } from '../classes/commons-std-err-log';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
global['COMMONS_LOG_DEFAULT'] = global['COMMONS_LOG_DEFAULT'] !== undefined ? global['COMMONS_LOG_DEFAULT'] : undefined;
if (global['COMMONS_LOG_DEFAULT'] === undefined) {
	const stdErrLogger: CommonsStdErrLog = new CommonsStdErrLog('_default');
	stdErrLogger.level = ECommonsLogLevel.NORMAL;

	const multiLogger: CommonsMultiLog = new CommonsMultiLog();
	multiLogger.add(stdErrLogger);

	global['COMMONS_LOG_DEFAULT'] = multiLogger;
}

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
global['COMMONS_LOG_DEFAULT_SUBS'] = global['COMMONS_LOG_DEFAULT_SUBS'] !== undefined ? global['COMMONS_LOG_DEFAULT_SUBS'] : undefined;
if (global['COMMONS_LOG_DEFAULT_SUBS'] === undefined) {
	global['COMMONS_LOG_DEFAULT_SUBS'] = new Map<string, CommonsSubLoggable>();
}

export function commonsLogReplace(loggers: CommonsLoggable[]): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsMultiLog).replace(loggers);
}

export function commonsLogSetLevel(level: ECommonsLogLevel): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).level = level;
}

export function commonsLogInfo(...args: TPrimative[]): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).info(...args);
}

export function commonsLogError(...args: (string | number | boolean)[]): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).error(...args);
}

export function commonsLogAlert(...args: (string | number | boolean)[]): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).alert(...args);
}

export function commonsLogDoing(subContext: string, message: string): CommonsSubLoggable {
	const subLogger: CommonsSubLoggable = (global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).doing(subContext, message);

	(global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).set(subContext, subLogger);

	return subLogger;
}

export function commonsLogDebug(...args: (string | number | boolean)[]): void {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).debug(...args);
}

export function commonsLogDie(message: string, exitCode?: number): never {
	(global['COMMONS_LOG_DEFAULT'] as CommonsLoggable).die(message, exitCode);
	
	process.exit(exitCode);	// never reached, but satisfies the typescript compiler
}

export function commonsLogProgress(subContext: string, progress?: TPrimative): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.progress(progress);
}

export function commonsLogPercent(subContext: string, i: number, length: number): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.percent(i, length);
}

export function commonsLogFound(subContext: string, tally: number, found: number): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.found(tally, found);
}

export function commonsLogSuccess(subContext: string, outcome?: TPrimative): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.success(outcome);

	(global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).delete(subContext);
}

export function commonsLogResult(subContext: string, result: TPrimative, outcome?: TPrimative): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.result(result, outcome);

	(global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).delete(subContext);
}

export function commonsLogFail(subContext: string, outcome?: string): void {
	const subLogger: CommonsSubLoggable|undefined = (global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).get(subContext);
	if (!subLogger) return;

	subLogger.fail(outcome);

	(global['COMMONS_LOG_DEFAULT_SUBS'] as Map<string, CommonsSubLoggable>).delete(subContext);
}
