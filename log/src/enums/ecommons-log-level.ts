export enum ECommonsLogLevel {
		DEBUG = 'debug',
		NORMAL = 'normal',
		SILENT = 'silent'
}
