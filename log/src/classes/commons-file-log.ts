import { TPrimative, commonsDateDateToYmdHis } from 'tscommons-es-core';

import { commonsFileAppendTextFile } from 'nodecommons-es-file';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

import { CommonsLinearWritableLog, CommonsLinearWritableSubLog } from './commons-linear-writable-log';

class FileSubLog extends CommonsLinearWritableSubLog {
	public override progress(_progress?: TPrimative): void {
		// do nothing
		// don't output progress to file logging
	}

	public override success(outcome?: TPrimative): void {
		if (outcome === undefined) {
			this.success('done');
			return;
		}

		this.write(`${this.message} ... ${outcome.toString()}`);
	}

	public override fail(outcome?: string | undefined): void {
		if (outcome === undefined) {
			this.success('failed');
			return;
		}

		this.write(`${this.message} ... ${outcome}`);
	}

	public override debug(...args: (string | number | boolean)[]): void {
		if (this.level !== ECommonsLogLevel.DEBUG) return;

		this.write(`DEBUG:<${this.context}> ${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}
}

export class CommonsFileLog extends CommonsLinearWritableLog<FileSubLog> {
	constructor(
			protected file: string,
			context: string
	) {
		super(
				context,
				(
						parent: CommonsLinearWritableLog<any>,
						subContext: string,
						message: string
				): FileSubLog => new FileSubLog(
						parent,
						subContext,
						message
				)
		);
	}

	public write(line: string): void {
		const now: Date = new Date();
		const millis: string = now.getUTCMilliseconds().toString(10).padEnd(3, '0').substring(0, 3);

		commonsFileAppendTextFile(this.file, `[${commonsDateDateToYmdHis(now)}.${millis}] ${line}\n`);
	}

	public override debug(...args: (string | number | boolean)[]): void {
		if (this.level !== ECommonsLogLevel.DEBUG) return;

		this.write(`DEBUG:<${this.context}> ${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}
}
