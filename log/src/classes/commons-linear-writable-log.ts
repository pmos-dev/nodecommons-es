import { TPrimative, commonsDateDateToYmdHis } from 'tscommons-es-core';

import { CommonsLoggable, CommonsSubLoggable } from '../interfaces/commons-log';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

export class CommonsLinearWritableSubLog implements CommonsSubLoggable {
	private logLevel: ECommonsLogLevel;
	private logContext: string;

	constructor(
			private parent: CommonsLinearWritableLog<any>,
			subContext: string,
			protected message: string
	) {
		this.logLevel = parent.level;
		this.logContext = `${parent.context}.${subContext}`;
	}

	public get level(): ECommonsLogLevel {
		return this.logLevel;
	}

	public get context(): string {
		return this.logContext;
	}

	public write(line: string): void {
		this.parent.write(line);
	}

	public progress(progress?: TPrimative): void {
		if (this.logLevel === ECommonsLogLevel.SILENT) return;

		this.write(`${this.message} ... ${progress === undefined ? '' : progress.toString()}`);
	}

	public percent(i: number, length: number): void {
		const percent: string = Math.round((i / length) * 100).toString();
		this.progress(`${percent}%`);
	}

	public found(tally: number, found: number): void {
		this.progress(`${tally}: ${found}`);
	}

	public success(outcome?: TPrimative): void {
		if (outcome === undefined) {
			this.success('done');
			return;
		}

		this.progress(outcome);
	}

	public result(result: TPrimative, outcome?: TPrimative): void {
		if (outcome === undefined) {
			this.result(result, 'done');
			return;
		}
		
		this.success(`${outcome.toString()} (${result.toString()})`);
	}

	public fail(outcome?: string | undefined): void {
		if (outcome === undefined) {
			this.success('failed');
			return;
		}

		this.progress(outcome);
	}

	public debug(...args: (string | number | boolean)[]): void {
		if (this.logLevel !== ECommonsLogLevel.DEBUG) return;

		const now: Date = new Date();
		const millis: string = now.getUTCMilliseconds().toString(10).padEnd(3, '0').substring(0, 3);

		this.write(`[${commonsDateDateToYmdHis(now)}.${millis}] <${this.context}> ${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}

	public die(message: string, exitCode?: number|'suppress'): never {
		this.fail(message);
		
		if (exitCode === 'suppress') {
			// pseudo return: dangerous!
			return undefined as never;
		}
		
		process.exit(exitCode);
	}
}

export abstract class CommonsLinearWritableLog<SubT extends CommonsLinearWritableSubLog> implements CommonsLoggable {
	private logLevel: ECommonsLogLevel = ECommonsLogLevel.NORMAL;
	private logContext: string;

	constructor(
			context: string,
			private subConstructor: (
					parent: CommonsLinearWritableLog<any>,
					subContext: string,
					message: string
			) => SubT
	) {
		this.logContext = context;
	}

	public abstract write(line: string): void;

	public get level(): ECommonsLogLevel {
		return this.logLevel;
	}
	public set level(level: ECommonsLogLevel) {
		this.logLevel = level;
	}

	public get context(): string {
		return this.logContext;
	}

	public info(...args: TPrimative[]): void {
		if (this.logLevel === ECommonsLogLevel.SILENT) return;

		this.write(`${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}

	public error(...args: TPrimative[]): void {
		if (this.logLevel === ECommonsLogLevel.SILENT) return;

		this.write(`ERROR: ${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}

	public alert(...args: TPrimative[]): void {
		if (this.logLevel === ECommonsLogLevel.SILENT) return;

		this.write(`${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}

	public doing(subContext: string, message: string): CommonsSubLoggable {
		const closure: SubT = this.subConstructor(
				this,
				subContext,
				message
		);
		return closure;
	}

	public debug(...args: (string | number | boolean)[]): void {
		if (this.logLevel !== ECommonsLogLevel.DEBUG) return;
		
		const now: Date = new Date();
		const millis: string = now.getUTCMilliseconds().toString(10).padEnd(3, '0').substring(0, 3);

		this.write(`[${commonsDateDateToYmdHis(now)}.${millis}] DEBUG:<${this.context}> ${args.map((primative: TPrimative): string => primative.toString()).join(' ')}`);
	}

	public die(message: string, exitCode?: number|'suppress'): never {
		this.error(message);
		
		if (exitCode === 'suppress') {
			// pseudo return: dangerous!
			return undefined as never;
		}
		
		process.exit(exitCode);
	}
}
