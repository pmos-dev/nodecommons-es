import * as readline from 'readline';

import { TPrimative, commonsDateDateToYmdHis } from 'tscommons-es-core';

import {
		COMMONS_COLOR_BRIGHT,
		COMMONS_COLOR_DIM,
		COMMONS_COLOR_FOREGROUND_GREEN,
		COMMONS_COLOR_FOREGROUND_RED,
		COMMONS_COLOR_FOREGROUND_WHITE,
		COMMONS_COLOR_FOREGROUND_YELLOW,
		COMMONS_COLOR_RESET
} from 'nodecommons-es-cli';

import { CommonsLoggable, CommonsSubLoggable } from '../interfaces/commons-log';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
global['COMMONS_LOG_STDERR_LAST_PENDING'] = global['COMMONS_LOG_STDERR_LAST_PENDING'] !== undefined ? global['COMMONS_LOG_STDERR_LAST_PENDING'] : undefined;

class CommonsStdErrSubLog implements CommonsSubLoggable {
	private logLevel: ECommonsLogLevel;
	private context: string;

	constructor(
			parent: CommonsStdErrLog,
			subContext: string,
			private message: string
	) {
		this.logLevel = parent.level;
		this.context = `${parent.context}.${subContext}`;

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;

		this.progress();
	}

	private handleOtherPending(): void {
		if (global['COMMONS_LOG_STDERR_LAST_PENDING'] === undefined || global['COMMONS_LOG_STDERR_LAST_PENDING'] === this.context) {
			// no new line required

			if (this.logLevel !== ECommonsLogLevel.SILENT) {
				readline.clearLine(process.stderr, 0);
				readline.cursorTo(process.stderr, 0, undefined);

				process.stderr.write(`${this.message} ...`);
			}
			global['COMMONS_LOG_STDERR_LAST_PENDING'] = this.context;
			return;
		}

		// new line required

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			process.stderr.write('\n');
		}
		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
		this.handleOtherPending();
	}

	public progress(progress?: TPrimative): void {
		this.handleOtherPending();

		if (progress !== undefined) {
			if (this.logLevel !== ECommonsLogLevel.SILENT) {
				process.stderr.write(` ${progress.toString()}`);
			}
		}
	}

	public percent(i: number, length: number): void {
		const percent: string = Math.round((i / length) * 100).toString();
		this.progress(`${percent}%`);
	}

	public found(tally: number, found: number): void {
		this.progress(`${tally}: ${found}`);
	}

	public success(outcome?: TPrimative): void {
		if (outcome === undefined) {
			this.success('done');
			return;
		}

		this.handleOtherPending();

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			this.progress(`${COMMONS_COLOR_FOREGROUND_GREEN}${COMMONS_COLOR_BRIGHT}${outcome.toString()}${COMMONS_COLOR_RESET}`);
			process.stderr.write('\n');
		}

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
	}

	public result(result: TPrimative, outcome?: TPrimative): void {
		if (outcome === undefined) {
			this.result(result, 'done');
			return;
		}
		
		this.success(`${outcome.toString()} (${result.toString()})`);
	}

	public fail(outcome?: string): void {
		if (outcome === undefined) {
			this.success('failed');
			return;
		}

		this.handleOtherPending();

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			this.progress(`${COMMONS_COLOR_FOREGROUND_RED}${COMMONS_COLOR_BRIGHT}${outcome}${COMMONS_COLOR_RESET}`);
			process.stderr.write('\n');
		}

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
	}

	public debug(...args: TPrimative[]): void {
		if (this.logLevel !== ECommonsLogLevel.DEBUG) return;

		if (global['COMMONS_LOG_STDERR_LAST_PENDING'] !== undefined) {
			process.stderr.write('\n');
		}

		readline.clearLine(process.stderr, 0);
		readline.cursorTo(process.stderr, 0, undefined);

		const now: Date = new Date();
		const millis: string = now.getUTCMilliseconds().toString(10).padEnd(3, '0').substring(0, 3);

		process.stderr.write(`${COMMONS_COLOR_FOREGROUND_WHITE}${COMMONS_COLOR_DIM}`);
		console.error(`[${commonsDateDateToYmdHis(now)}.${millis}] <${this.context}>`, ...args);
		process.stderr.write(COMMONS_COLOR_RESET);

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
	}

	public die(message: string, exitCode?: number|'suppress'): never {
		this.fail(message);
		
		if (exitCode === 'suppress') {
			// pseudo return: dangerous!
			return undefined as never;
		}
		
		process.exit(exitCode);
	}
}

export class CommonsStdErrLog implements CommonsLoggable {
	private logLevel: ECommonsLogLevel = ECommonsLogLevel.NORMAL;
	private logContext: string;

	constructor(
			context: string
	) {
		this.logContext = context;
	}

	public get level(): ECommonsLogLevel {
		return this.logLevel;
	}
	public set level(level: ECommonsLogLevel) {
		this.logLevel = level;
	}

	public get context(): string {
		return this.logContext;
	}

	private handleOtherPending(): void {
		if (global['COMMONS_LOG_STDERR_LAST_PENDING'] === undefined) return;

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			process.stderr.write('\n');
		}

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
	}

	public info(...args: TPrimative[]): void {
		this.handleOtherPending();
	
		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			readline.clearLine(process.stderr, 0);
			readline.cursorTo(process.stderr, 0, undefined);
		
			console.error(...args);
		}
	}

	public error(...args: TPrimative[]): void {
		this.handleOtherPending();

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			readline.clearLine(process.stderr, 0);
			readline.cursorTo(process.stderr, 0, undefined);
		
			process.stderr.write(`${COMMONS_COLOR_FOREGROUND_RED}${COMMONS_COLOR_BRIGHT}`);
			console.error(...args);
			process.stderr.write(COMMONS_COLOR_RESET);
		}
	}

	public alert(...args: TPrimative[]): void {
		this.handleOtherPending();

		if (this.logLevel !== ECommonsLogLevel.SILENT) {
			readline.clearLine(process.stderr, 0);
			readline.cursorTo(process.stderr, 0, undefined);
		
			process.stderr.write(`${COMMONS_COLOR_FOREGROUND_YELLOW}${COMMONS_COLOR_BRIGHT}`);
			console.error(...args);
			process.stderr.write(COMMONS_COLOR_RESET);
		}
	}

	public doing(subContext: string, message: string): CommonsSubLoggable {
		this.handleOtherPending();

		const closure: CommonsStdErrSubLog = new CommonsStdErrSubLog(
				this,
				subContext,
				message
		);
		return closure;
	}

	public debug(...args: TPrimative[]): void {
		if (this.logLevel !== ECommonsLogLevel.DEBUG) return;
		
		if (global['COMMONS_LOG_STDERR_LAST_PENDING'] !== undefined) {
			process.stderr.write('\n');
		}

		readline.clearLine(process.stderr, 0);
		readline.cursorTo(process.stderr, 0, undefined);

		const now: Date = new Date();
		const millis: string = now.getUTCMilliseconds().toString(10).padEnd(3, '0').substring(0, 3);

		process.stderr.write(`${COMMONS_COLOR_FOREGROUND_WHITE}${COMMONS_COLOR_DIM}`);
		console.error(`[${commonsDateDateToYmdHis(now)}.${millis}] <${this.context}>`, ...args);
		process.stderr.write(COMMONS_COLOR_RESET);

		global['COMMONS_LOG_STDERR_LAST_PENDING'] = undefined;
	}

	public die(message: string, exitCode?: number|'suppress'): never {
		this.error(message);
				
		if (exitCode === 'suppress') {
			// pseudo return: dangerous!
			return undefined as never;
		}

		process.exit(exitCode);
	}
}
