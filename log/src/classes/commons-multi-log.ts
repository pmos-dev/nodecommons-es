import { TPrimative, commonsArrayRemove } from 'tscommons-es-core';

import { CommonsLoggable, CommonsSubLoggable } from '../interfaces/commons-log';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

class CommonsMultiSubLog implements CommonsSubLoggable {
	constructor(
			private subLoggers: CommonsSubLoggable[]
	) {}

	public progress(progress?: TPrimative): void {
		for (const subLogger of this.subLoggers) subLogger.progress(progress);
	}

	public percent(i: number, length: number): void {
		for (const subLogger of this.subLoggers) subLogger.percent(i, length);
	}

	public found(tally: number, found: number): void {
		for (const subLogger of this.subLoggers) subLogger.found(tally, found);
	}

	public success(outcome?: TPrimative): void {
		for (const subLogger of this.subLoggers) subLogger.success(outcome);
	}

	public result(result: TPrimative, outcome?: TPrimative): void {
		for (const subLogger of this.subLoggers) subLogger.result(result, outcome);
	}

	public fail(outcome?: string): void {
		for (const subLogger of this.subLoggers) subLogger.fail(outcome);
	}

	public debug(...args: TPrimative[]): void {
		for (const subLogger of this.subLoggers) subLogger.debug(...args);
	}

	public die(message: string, exitCode?: number): never {
		// we can't use the subLoggers die, as they would process.exit before the next
		for (const subLogger of this.subLoggers) subLogger.die(message, 'suppress');

		process.exit(exitCode);
	}
}

export class CommonsMultiLog implements CommonsLoggable {
	private loggers: CommonsLoggable[] = [];

	public add(logger: CommonsLoggable): void {
		this.loggers.push(logger);
	}

	public remove(logger: CommonsLoggable): void {
		commonsArrayRemove(this.loggers, logger);
	}

	public replace(loggers: CommonsLoggable[]): void {
		this.loggers = [ ...loggers ];
	}

	public clear(): void {
		this.replace([]);
	}

	public get level(): ECommonsLogLevel {
		throw new Error('CommonsMultiLog does not support a concept of levels itself, as each logger has its own');
	}
	public set level(level: ECommonsLogLevel) {
		for (const logger of this.loggers) logger.level = level;
	}

	public info(...args: TPrimative[]): void {
		for (const logger of this.loggers) logger.info(...args);
	}

	public error(...args: TPrimative[]): void {
		for (const logger of this.loggers) logger.error(...args);
	}

	public alert(...args: TPrimative[]): void {
		for (const logger of this.loggers) logger.alert(...args);
	}

	public doing(subContext: string, message: string): CommonsSubLoggable {
		const subs: CommonsSubLoggable[] = this.loggers
				.map((logger: CommonsLoggable): CommonsSubLoggable => logger.doing(subContext, message));
		
		return new CommonsMultiSubLog(subs);
	}

	public debug(...args: TPrimative[]): void {
		for (const logger of this.loggers) logger.debug(...args);
	}

	public die(message: string, exitCode?: number): never {
		// we can't use the loggers die, as they would process.exit before the next
		for (const logger of this.loggers) logger.die(message, 'suppress');

		process.exit(exitCode);
	}
}
