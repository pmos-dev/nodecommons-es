import { commonsFileExists, commonsFileRename, commonsFileRm, commonsFileSize } from 'nodecommons-es-file';

import { CommonsFileLog } from './commons-file-log';

export class CommonsBoundedFileLog extends CommonsFileLog {
	constructor(
			file: string,
			private maxSizeInBytes: number,
			context: string,
			private maxHistoricalLogs?: number
	) {
		super(
				file,
				context
		);
	}

	private shuntLogs(): void {
		let tail: number = 1;
		while (commonsFileExists(`${this.file}.${tail.toString(10)}`)) tail++;

		if (this.maxHistoricalLogs !== undefined) {
			while (tail > this.maxHistoricalLogs) {
				commonsFileRm(`${this.file}.${(tail - 1).toString(10)}`);
				tail--;
			}
		}

		while (tail > 1) {
			// shunt forward existing logs (do in reverse to prevent shunting the .1 forward all the way to the end!)
			commonsFileRename(
					`${this.file}.${(tail - 1).toString(10)}`,
					`${this.file}.${tail.toString(10)}`
			);
			tail--;
		}

		commonsFileRename(
				this.file,
				`${this.file}.1`
		);
	}

	public override write(line: string): void {
		if (commonsFileExists(this.file)) {
			const existingSize: number = commonsFileSize(this.file);
			if (existingSize > this.maxSizeInBytes) this.shuntLogs();
		}

		super.write(line);
	}
}
