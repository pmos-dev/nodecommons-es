// this is a true interface

import { TPrimative } from 'tscommons-es-core';

import { ECommonsLogLevel } from '../enums/ecommons-log-level';

interface CommonsCommonLoggable {
	debug(...args: TPrimative[]): void;
	die(message: string, exitCode?: number|'suppress'): never;	// use to suppress the process.exit, a nasty way because it isn't then 'never'
}

export interface CommonsSubLoggable extends CommonsCommonLoggable {
	progress(progress?: TPrimative): void;
	percent(i: number, length: number): void;
	found(tally: number, found: number): void;
	success(outcome?: TPrimative): void;
	result(result: TPrimative, outcome?: TPrimative): void;
	fail(outcome?: string): void;
}

export interface CommonsLoggable extends CommonsCommonLoggable {
	get level(): ECommonsLogLevel;
	set level(level: ECommonsLogLevel);

	info(...args: TPrimative[]): void;
	error(...args: TPrimative[]): void;
	alert(...args: TPrimative[]): void;
	doing(subContext: string, message: string): CommonsSubLoggable;
}
