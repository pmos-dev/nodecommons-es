import {
		commonsNumberParseByteSize,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyBooleanOrUndefined,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyNumberOrUndefined,
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringOrUndefined
} from 'tscommons-es-core';

export interface ICommonsLogConfig {
		stdErr?: true;
		file?: string;
		maxSize?: string|number;	// string allows for commonsNumberParseByteSize
		maxHistorical?: number;
}

export function isICommonsLogConfig(test: unknown): test is ICommonsLogConfig {
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'stdErr')) return false;
	if (commonsTypeHasPropertyBoolean(test, 'stdErr') && test.stdErr === false) return false;

	if (!commonsTypeHasPropertyStringOrUndefined(test, 'file')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'maxSize') && !commonsTypeHasPropertyNumberOrUndefined(test, 'maxSize')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'maxHistorical')) return false;

	return true;
}

export type TCommonsLogParsedConfig = Omit<ICommonsLogConfig, 'maxSize'> & {
	maxSize?: number;
}

export function parseCommonsLogConfig(config: ICommonsLogConfig): TCommonsLogParsedConfig {
	const parsed: TCommonsLogParsedConfig = {};
	
	if (config.stdErr) parsed.stdErr = true;
	if (config.file) parsed.file = config.file;

	let size: number|undefined;
	if (commonsTypeHasPropertyNumber(config, 'maxSize')) size = config.maxSize as number;
	if (commonsTypeHasPropertyString(config, 'maxSize')) size = commonsNumberParseByteSize(config.maxSize as string);
	if (size !== undefined) parsed.maxSize = size;
	
	// maxHistorical makes no sense if the maxSize isn't set
	if (parsed.maxSize !== undefined && config.maxHistorical !== undefined) parsed.maxHistorical = config.maxHistorical;

	return parsed;
}
