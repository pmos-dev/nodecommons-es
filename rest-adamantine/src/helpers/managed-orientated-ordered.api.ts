import {
		commonsTypeAttemptBoolean,
		commonsTypeHasPropertyEnum,
		commonsBase62GenerateRandomId
} from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsOrientatedOrdered } from 'tscommons-es-models';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ECommonsMoveDirection, isECommonsMoveDirection, toECommonsMoveDirection } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsUniqueNamed } from 'tscommons-es-models-adamantine';

import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsManagedOrientatedOrdered } from 'nodecommons-es-models-adamantine';
import {
		commonsRestApiError,
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiConflict,
		commonsRestApiNotImplemented
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { ManagedModelApi } from './managed-model.api';

export class ManagedOrientatedOrderedApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ModelT extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		) & ICommonsOrientatedOrdered<ParentT>,
		ParentT extends ICommonsFirstClass & ICommonsUniqueNamed,
		ClassT extends CommonsManagedOrientatedOrdered<ModelT, ParentT>,
		TypeT extends TEncodedObject
> extends ManagedModelApi<
		RequestT,
		ResponseT,
		ModelT,
		ClassT
> {
	private parentModel: CommonsFirstClass<ParentT>;
	
	constructor(
			path: string,
			model: ClassT,
			private encodeData: (m: ModelT) => TypeT,
			private postCreateCallback?: (firstClass: ParentT, inserted: ModelT) => Promise<void>,
			private postUpdateCallback?: (firstClass: ParentT, updated: ModelT) => Promise<void>,
			private postDeleteCallback?: (firstClass: ParentT, existing: ModelT) => Promise<void>,
			private postMoveCallback?: (firstClass: ParentT, moved: ModelT) => Promise<void>,
			private preCreateCallback?: (firstClass: ParentT, inserting: TPropertyObject) => Promise<void|never>,
			private preUpdateCallback?: (firstClass: ParentT, updating: ModelT) => Promise<void|never>,
			private preDeleteCallback?: (firstClass: ParentT, existing: ModelT) => Promise<void|never>,
			private preMoveCallback?: (firstClass: ParentT, moving: ModelT, direction: ECommonsMoveDirection) => Promise<void|never>,
			private altCreateCallback?: (firstClass: ParentT, insert: TPropertyObject) => Promise<ModelT>,
			private altUpdateCallback?: (firstClass: ParentT, update: ModelT) => Promise<void>,
			private altDeleteCallback?: (firstClass: ParentT, existing: ModelT) => Promise<void>,
			private altMoveCallback?: (firstClass: ParentT, move: ModelT, direction: ECommonsMoveDirection) => Promise<ModelT>
	) {
		super(path, model);

		this.parentModel = this.model.getFirstClass();
	}
	
	private async getP(parent: number): Promise<ParentT|undefined> {
		if (this.parentModel instanceof CommonsSecondClass) {
			// have to use getByIdOnly
			return await (this.parentModel as CommonsSecondClass<ParentT, any>).getByIdOnly(parent);
		} else {
			return await this.parentModel.getById(parent);
		}
	}

	public override applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		if (!this.model.isModelManageable()) return;
		
		// list by firstclass
		getHandler(
				`${this.path}data/:parent[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT[]> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');
					
					const items: ModelT[] = await this.model.listOrderedByFirstClass(p);
					
					return items
							.map((m: ModelT): TypeT => this.encodeData(m));
				}
		);
	
		// list IDs by firstclass
		getHandler(
				`${this.path}data/:parent[id]/ids`,
				async (req: RequestT, _res: ResponseT): Promise<number[]> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');
					
					const items: ModelT[] = await this.model.listOrderedByFirstClass(p);
					
					return items
							.map((m: ModelT): number => m.id);
				}
		);

		// list names by firstclass
		getHandler(
				`${this.path}data/:parent[id]/names`,
				async (req: RequestT, _res: ResponseT): Promise<string[]> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');
					
					const items: ModelT[] = await this.model.listOrderedByFirstClass(p);
					
					return items
							.map((m: ModelT): string => m.name);
				}
		);

		// get by firstclass and id
		getHandler(
				`${this.path}data/:parent[id]/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');
					
					const m: ModelT|undefined = await this.model.getByFirstClassAndId(
							p,
							req.strictParams.id as number
					);
					if (!m) return commonsRestApiNotFound('No such model item exists');
					
					return this.encodeData(m);
				}
		);

		// get by firstclass and name
		getHandler(
				`${this.path}data/:parent[id]/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');
					
					const m: ModelT|undefined = await this.model.getByName(
							p,
							req.strictParams.name as string
					);
					if (!m) return commonsRestApiNotFound('No such model item exists');
					
					return this.encodeData(m);
				}
		);

		if (this.model.isUidSupported()) {
			// list UIDs by firstclass
			getHandler(
					`${this.path}data/:parent[id]/uids`,
					async (req: RequestT, _res: ResponseT): Promise<string[]> => {
						const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
						if (!p) return commonsRestApiNotFound('No such parent exists');
						
						const items: ModelT[] = await this.model.listOrderedByFirstClass(p);
						
						return items
								.map((m: ModelT): string => (m as ICommonsUniquelyIdentified).uid);
					}
			);
	
			// get by uid (without a firstclass needed)
			getHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const m: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!m) return commonsRestApiNotFound('No such model item exists');
						
						return this.encodeData(m);
					}
			);
		}
	}
	
	private async doUpdate(
			p: ParentT,
			existing: ModelT,
			data: TPropertyObject,
			skipOmitted: boolean
	): Promise<ModelT> {
		let rebuild: TPropertyObject = {};
		try {
			rebuild = this.attemptBuildData(
					this.model.getManageableFields(),
					data,
					skipOmitted
			);
		} catch (e) {
			if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
			
			return commonsRestApiError((e as Error).message);
		}
		
		const merged: ModelT = {
				...existing,
				...rebuild
		};
		
		// double checks: disallow changing of internal fields
		
		if (merged.id !== existing.id) return commonsRestApiBadRequest('Attempting to reassign a new ID to an existing model item');
		if (merged[this.model.getFirstClassField()] !== existing[this.model.getFirstClassField()]) return commonsRestApiBadRequest('Attempting to reassign a new firstClassField parent to an existing model item');

		if (this.model.isUidSupported()) {
			if (merged['uid'] !== existing['uid']) return commonsRestApiBadRequest('Attempting to reassign a new UID to an existing model item');
		}
		
		if (this.preUpdateCallback) await this.preUpdateCallback(p, merged);

		if (this.altUpdateCallback) {
			await this.altUpdateCallback(p, merged);
		} else {
			await this.model.updateForFirstClass(p, merged);
		}

		if (this.postUpdateCallback) await this.postUpdateCallback(p, merged);
		
		return merged;
	}

	public override applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		if (!this.model.isModelManageable()) return;
		
		// create for firstclass
		postHandler(
				`${this.path}data/:parent[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					let rebuild: TPropertyObject = {};
					try {
						rebuild = this.attemptBuildData(
								this.model.getManageableFields(),
								bodyRequestTypecast.body as TPropertyObject,
								false
						);
					} catch (e) {
						if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
						
						return commonsRestApiError((e as Error).message);
					}

					if (this.model.isUidSupported()) {
						rebuild.uid = commonsBase62GenerateRandomId();
					}

					// check for name conflicts (no need to check if name exists, as it is a mandatory field and manageable)
					const existing: ModelT|undefined = await this.model.getByName(
							p,
							rebuild['name'] as string
					);
					if (existing) return commonsRestApiConflict('An model item for this name already exists');

					if (this.preCreateCallback) await this.preCreateCallback(p, rebuild);
					
					let inserted: ModelT|undefined;
					if (this.altCreateCallback) {
						inserted = await this.altCreateCallback(p, rebuild);
					} else {
						inserted = await this.model.insertForFirstClass(p, rebuild);
					}
					if (!inserted) throw new Error('Failed to create model item');
					
					if (this.postCreateCallback) await this.postCreateCallback(p, inserted);
					
					return this.encodeData(inserted);
				}
		);
		
		// update for firstclass and id
		patchHandler(
				`${this.path}data/:parent[id]/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByFirstClassAndId(
							p,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					const updated: ModelT = await this.doUpdate(
							p,
							existing,
							bodyRequestTypecast.body as TPropertyObject,
							commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
					);
					
					return this.encodeData(updated);
				}
		);
		
		// update for firstclass and name
		patchHandler(
				`${this.path}data/:parent[id]/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByName(
							p,
							req.strictParams.name as string
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					const updated: ModelT = await this.doUpdate(
							p,
							existing,
							bodyRequestTypecast.body as TPropertyObject,
							commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
					);
					
					return this.encodeData(updated);
				}
		);
		
		// move for firstclass and id
		patchHandler(
				`${this.path}data/:parent[id]/ids/:id[id]/move`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByFirstClassAndId(
							p,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
				
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
					const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
					switch (direction) {
						case ECommonsMoveDirection.UP:
						case ECommonsMoveDirection.DOWN:
						case ECommonsMoveDirection.TOP:
						case ECommonsMoveDirection.BOTTOM:
							try {
								if (this.preMoveCallback) await this.preMoveCallback(p, existing, direction);

								let moved: ModelT|undefined;
								if (this.altMoveCallback) {
									moved = await this.altMoveCallback(p, existing, direction);
								} else {
									moved = await this.model.move(existing, direction);
								}
								if (!moved) throw new Error('Failed to move model item');

								if (this.postMoveCallback) await this.postMoveCallback(p, moved);
								
								return this.encodeData(moved);
							} catch (e) {
								if (/^Cannot move /.test((e as Error).message)) {
									return commonsRestApiBadRequest((e as Error).message);
								}
								
								throw e;
							}
							
						default:
							return commonsRestApiBadRequest('Unknown movement direction');
					}
				}
		);
		
		// move for firstclass and name
		patchHandler(
				`${this.path}data/:parent[id]/names/:name[idname]/move`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByName(
							p,
							req.strictParams.name as string
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
					const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
					switch (direction) {
						case ECommonsMoveDirection.UP:
						case ECommonsMoveDirection.DOWN:
						case ECommonsMoveDirection.TOP:
						case ECommonsMoveDirection.BOTTOM:
							try {
								if (this.preMoveCallback) await this.preMoveCallback(p, existing, direction);

								let moved: ModelT|undefined;
								if (this.altMoveCallback) {
									moved = await this.altMoveCallback(p, existing, direction);
								} else {
									moved = await this.model.move(existing, direction);
								}
								if (!moved) throw new Error('Failed to move model item');

								if (this.postMoveCallback) await this.postMoveCallback(p, moved);
								
								return this.encodeData(moved);
							} catch (e) {
								if (/^Cannot move /.test((e as Error).message)) {
									return commonsRestApiBadRequest((e as Error).message);
								}
								
								throw e;
							}
							
						default:
							return commonsRestApiBadRequest('Unknown movement direction');
					}
				}
		);
		
		// delete for firstclass and id
		deleteHandler(
				`${this.path}data/:parent[id]/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<boolean> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByFirstClassAndId(
							p,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					if (this.preDeleteCallback) await this.preDeleteCallback(p, existing);
					
					if (this.altDeleteCallback) {
						await this.altDeleteCallback(p, existing);
					} else {
						await this.model.deleteForFirstClass(p, existing);
					}
					
					if (this.postDeleteCallback) await this.postDeleteCallback(p, existing);
					
					return true;
				}
		);
		
		// delete for firstclass and name
		deleteHandler(
				`${this.path}data/:parent[id]/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<boolean> => {
					const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
					if (!p) return commonsRestApiNotFound('No such parent exists');

					const existing: ModelT|undefined = await this.model.getByName(
							p,
							req.strictParams.name as string
					);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					if (this.preDeleteCallback) await this.preDeleteCallback(p, existing);
					
					if (this.altDeleteCallback) {
						await this.altDeleteCallback(p, existing);
					} else {
						await this.model.deleteForFirstClass(p, existing);
					}
					
					if (this.postDeleteCallback) await this.postDeleteCallback(p, existing);
					
					return true;
				}
		);
		
		if (this.model.isUidSupported()) {
			// create for firstclass with fixed uid
			putHandler(
					`${this.path}data/:parent[id]/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						if (!this.model.isUidSupported()) return commonsRestApiNotImplemented('UID support is not available for this model');
	
						const p: ParentT|undefined = await this.getP(req.strictParams.parent as number);
						if (!p) return commonsRestApiNotFound('No such parent exists');
	
						// deny upsert, even though PUT allows it, as it could be used to break the firstClass relationships
						const existingUid: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (existingUid) return commonsRestApiConflict('An model item for this name already exists');
	
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						let rebuild: TPropertyObject = {
								uid: req.strictParams.uid as string
						};
						try {
							rebuild = this.attemptBuildData(
									this.model.getManageableFields(),
									bodyRequestTypecast.body as TPropertyObject,
									false
							);
						} catch (e) {
							if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
							
							return commonsRestApiError((e as Error).message);
						}
	
						// check for name conflicts (no need to check if name exists, as it is a mandatory field and manageable)
						const existing: ModelT|undefined = await this.model.getByName(
								p,
								rebuild['name'] as string
						);
						if (existing) return commonsRestApiConflict('An model item for this name already exists');
						
						if (this.preCreateCallback) await this.preCreateCallback(p, rebuild);
					
						let inserted: ModelT|undefined;
						if (this.altCreateCallback) {
							inserted = await this.altCreateCallback(p, rebuild);
						} else {
							inserted = await this.model.insertForFirstClass(p, rebuild);
						}
						if (!inserted) throw new Error('Failed to create model item');
						
						if (this.postCreateCallback) await this.postCreateCallback(p, inserted);
						
						return this.encodeData(inserted);
					}
			);

			// update for uid without firstclass
			patchHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
						
						const p: ParentT|undefined = await this.getP(existing[this.model.getFirstClassField()] as number);
						if (!p) return commonsRestApiError('No valid parent exists for this model item. This should not be possible.');
						
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						const updated: ModelT = await this.doUpdate(
								p,
								existing,
								bodyRequestTypecast.body as TPropertyObject,
								commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
						);
						
						return this.encodeData(updated);
					}
			);

			// move for uid without firstclass
			patchHandler(
					`${this.path}data/uids/:uid[base62]/move`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
						
						const p: ParentT|undefined = await this.getP(existing[this.model.getFirstClassField()] as number);
						if (!p) return commonsRestApiError('No valid parent exists for this model item. This should not be possible.');
						
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
						const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
						switch (direction) {
							case ECommonsMoveDirection.UP:
							case ECommonsMoveDirection.DOWN:
							case ECommonsMoveDirection.TOP:
							case ECommonsMoveDirection.BOTTOM:
								try {
									if (this.preMoveCallback) await this.preMoveCallback(p, existing, direction);

									let moved: ModelT|undefined;
									if (this.altMoveCallback) {
										moved = await this.altMoveCallback(p, existing, direction);
									} else {
										moved = await this.model.move(existing, direction);
									}
									if (!moved) throw new Error('Failed to move model item');
	
									if (this.postMoveCallback) await this.postMoveCallback(p, moved);
									
									if (this.postMoveCallback) await this.postMoveCallback(p, moved);
								
									return this.encodeData(moved);
								} catch (e) {
									if (/^Cannot move /.test((e as Error).message)) {
										return commonsRestApiBadRequest((e as Error).message);
									}
									
									throw e;
								}
								
							default:
								return commonsRestApiBadRequest('Unknown movement direction');
						}
					}
			);

			// delete for uid without firstclass
			deleteHandler(
					`${this.path}data/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<boolean> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
						
						const p: ParentT|undefined = await this.getP(existing[this.model.getFirstClassField()] as number);
						if (!p) return commonsRestApiError('No valid parent exists for this model item. This should not be possible.');
						
						if (this.preDeleteCallback) await this.preDeleteCallback(p, existing);
					
						if (this.altDeleteCallback) {
							await this.altDeleteCallback(p, existing);
						} else {
							await this.model.deleteForFirstClass(p, existing);
						}
						
						if (this.postDeleteCallback) await this.postDeleteCallback(p, existing);
						
						return true;
					}
			);
		}
	}
}
