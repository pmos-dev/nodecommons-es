import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelField } from 'tscommons-es-models-adamantine';
import { TCommonsManagedModelMetadata, encodeICommonsManagedModelMetadata } from 'tscommons-es-models-adamantine';

import { CommonsManageable } from 'nodecommons-es-models-adamantine';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { AdamantineModelApi } from './adamantine-model.api';

export abstract class ManagedModelApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ModelT extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		ClassT extends CommonsManageable<ModelT>
> extends AdamantineModelApi<
		RequestT,
		ResponseT,
		ModelT,
		ClassT
> {
	public override applyMetadata(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		getHandler(
				`${this.path}metadata`,
				async (_req: RequestT, _res: ResponseT): Promise<TCommonsManagedModelMetadata> => {	// eslint-disable-line @typescript-eslint/require-await
					const dataFields: ICommonsManagedModelField[] = this.getMetadataFields(
							this.model.getManageableFields()
					);
					
					for (const dataField of dataFields) {
						const description: string|undefined = this.model.getFieldDescription(dataField.name);
						if (description) dataField.description = description;

						const suffix: string|undefined = this.model.getFieldSuffix(dataField.name);
						if (suffix) dataField.suffix = suffix;

						const helper: string|undefined = this.model.getFieldHelper(dataField.name);
						if (helper) dataField.helper = helper;
					}
					
					return encodeICommonsManagedModelMetadata({
							manageableFields: dataFields,
							accessRequiredToManage: this.model.getAccessRequiredToManage(),
							isAliasingSupported: this.model.isAliasingSupported()
					});
				}
		);
	}
}
