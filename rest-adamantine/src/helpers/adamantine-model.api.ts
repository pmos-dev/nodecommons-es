import {
		commonsTypeHasProperty,
		commonsTypeIsPrimative,
		commonsTypeIsStringArray,
		commonsTypeAttemptBoolean,
		commonsDateYmdHisToDate,
		commonsDateYmdToDate,
		commonsDateHisToDate,
		commonsTypeAttemptNumber
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { TPrimative } from 'tscommons-es-core';
import { ICommonsManagedModelField } from 'tscommons-es-models-adamantine';
import { ECommonsAdamantineManagedModelFieldType } from 'tscommons-es-models-adamantine';

import {
		CommonsDatabaseType,
		CommonsDatabaseTypeDate,
		CommonsDatabaseTypeTime,
		CommonsDatabaseTypeDateTime,
		CommonsDatabaseTypeBoolean,
		CommonsDatabaseTypeEmail,
		CommonsDatabaseTypeEnum,
		CommonsDatabaseTypeFloat,
		CommonsDatabaseTypeHexRgb,
		CommonsDatabaseTypeNumber,
		CommonsDatabaseTypeInt,
		CommonsDatabaseTypeSmallInt,
		CommonsDatabaseTypeString,
		CommonsDatabaseTypeText,
		CommonsDatabaseTypeTinyInt,
		CommonsDatabaseTypeUrl,
		CommonsDatabaseTypeStringArray,
		TCommonsDatabaseTypes,
		ECommonsDatabaseTypeNull
} from 'nodecommons-es-database';
import { commonsRestApiNotImplemented } from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { CommonsModelable } from 'nodecommons-es-models';
import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

function deriveType(s: CommonsDatabaseType): ECommonsAdamantineManagedModelFieldType|undefined {
	if (s instanceof CommonsDatabaseTypeBoolean) return ECommonsAdamantineManagedModelFieldType.BOOLEAN;
	if (s instanceof CommonsDatabaseTypeDateTime) return ECommonsAdamantineManagedModelFieldType.DATETIME;
	if (s instanceof CommonsDatabaseTypeDate) return ECommonsAdamantineManagedModelFieldType.DATE;
	if (s instanceof CommonsDatabaseTypeEmail) return ECommonsAdamantineManagedModelFieldType.EMAIL;
	if (s instanceof CommonsDatabaseTypeEnum) return ECommonsAdamantineManagedModelFieldType.ENUM;
	if (s instanceof CommonsDatabaseTypeFloat) return ECommonsAdamantineManagedModelFieldType.FLOAT;
	if (s instanceof CommonsDatabaseTypeHexRgb) return ECommonsAdamantineManagedModelFieldType.HEXRGB;
	if (s instanceof CommonsDatabaseTypeInt) return ECommonsAdamantineManagedModelFieldType.INT;
	if (s instanceof CommonsDatabaseTypeSmallInt) return ECommonsAdamantineManagedModelFieldType.SMALLINT;
	if (s instanceof CommonsDatabaseTypeText) return ECommonsAdamantineManagedModelFieldType.TEXT;
	if (s instanceof CommonsDatabaseTypeTime) return ECommonsAdamantineManagedModelFieldType.TIME;
	if (s instanceof CommonsDatabaseTypeTinyInt) return ECommonsAdamantineManagedModelFieldType.TINYINT;
	if (s instanceof CommonsDatabaseTypeUrl) return ECommonsAdamantineManagedModelFieldType.URL;
	if (s instanceof CommonsDatabaseTypeStringArray) return ECommonsAdamantineManagedModelFieldType.STRINGARRAY;
	
	// this needs to go at the end, as custom ones like Email/URL/HexRgb are subclasses of string, so will be matched if it is checked first
	if (s instanceof CommonsDatabaseTypeString) return ECommonsAdamantineManagedModelFieldType.STRING;

	return undefined;
}

export abstract class AdamantineModelApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ModelT extends TPropertyObject,
		ClassT extends CommonsModelable<ModelT>
> {
	constructor(
			protected path: string,
			protected model: ClassT
	) {}

	protected getMetadataFields(
			fields: string[]
	): ICommonsManagedModelField[] {
		const structure: TCommonsDatabaseTypes = this.model.getStructure();
		
		const dataFields: ICommonsManagedModelField[] = [];
		
		for (const field of fields) {
			const dbType: CommonsDatabaseType|undefined = structure[field];
			if (!dbType) throw new Error('Field does not exist in the database structure. This is likely an error');
			
			const type: ECommonsAdamantineManagedModelFieldType|undefined = deriveType(dbType);
			if (!type) return commonsRestApiNotImplemented(`The field type of ${field} is not implemented as manageable`);
			
			const dataField: ICommonsManagedModelField = {
					name: field,
					type: type,
					optional: dbType.getNotNull() !== ECommonsDatabaseTypeNull.NOT_NULL
			};
			
			const defaultValue: string|number|boolean|undefined = dbType.getDefaultValue();
			if (defaultValue !== undefined) dataField.defaultValue = defaultValue;
			
			if (type === ECommonsAdamantineManagedModelFieldType.ENUM) {
				const typecast: CommonsDatabaseTypeEnum<string> = dbType as CommonsDatabaseTypeEnum<string>;
				dataField.options = typecast.getOptions();
			}
			
			if (type === ECommonsAdamantineManagedModelFieldType.HEXRGB) {
				const typecast: CommonsDatabaseTypeHexRgb = dbType as CommonsDatabaseTypeHexRgb;
				dataField.alpha = typecast.hasAlpha();
			}
			
			dataFields.push(dataField);
		}
		
		return dataFields;
	}

	public applyMetadata(
			_getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		// to be overridden
	}

	public applyReadData(
			_getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		// to be overridden
	}
	
	protected attemptBuildData(
			fields: string[],
			data: TPropertyObject,
			skipOmitted: boolean
	): TPropertyObject {
		const rebuild: TPropertyObject = {};
		const structure: TCommonsDatabaseTypes = this.model.getStructure();

		for (const field of fields) {
			if (skipOmitted && !commonsTypeHasProperty(data, field)) continue;
			
			let value: unknown = data[field];
			
			try {
				if (value === undefined || value === null || value === '') {
					value = undefined;
				} else {
					if (!commonsTypeIsPrimative(value) && !commonsTypeIsStringArray(value)) throw new Error('Value is not a primative or string array.');
				}
				
				const type: CommonsDatabaseType = structure[field];
				
				if (value !== undefined) {
					if (type instanceof CommonsDatabaseTypeBoolean) {
						value = commonsTypeAttemptBoolean(value);
					}
					if (type instanceof CommonsDatabaseTypeDateTime) {
						value = commonsDateYmdHisToDate(value as string);
					}
					if (type instanceof CommonsDatabaseTypeDate) {
						value = commonsDateYmdToDate(value as string);
					}
					if (type instanceof CommonsDatabaseTypeTime) {
						value = commonsDateHisToDate(value as string);
					}
					if (type instanceof CommonsDatabaseTypeNumber) {
						value = commonsTypeAttemptNumber(value);
					}
				}
				
				type.assert(value);
				
				rebuild[field] = value as TPrimative;
			} catch (e) {
				throw new Error(`Invalid data for field ${field}`);
			}
		}
		
		return rebuild;
	}

	public applyWriteData(
			_postHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			_deleteHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		// to be overridden
	}
	
	public apply(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		this.applyMetadata(getHandler);
		
		this.applyReadData(getHandler);
		
		this.applyWriteData(
				postHandler,
				putHandler,
				patchHandler,
				deleteHandler
		);
	}
}
