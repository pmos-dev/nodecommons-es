import {
		commonsTypeAttemptBoolean,
		commonsTypeHasPropertyEnum,
		commonsBase62GenerateRandomId
} from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsOrdered } from 'tscommons-es-models';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ECommonsMoveDirection, isECommonsMoveDirection, toECommonsMoveDirection } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';

import { CommonsManagedOrdered } from 'nodecommons-es-models-adamantine';
import {
		commonsRestApiError,
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiConflict
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { ManagedModelApi } from './managed-model.api';

export class ManagedOrderedApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ModelT extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		) & ICommonsOrdered,
		ClassT extends CommonsManagedOrdered<ModelT>,
		TypeT extends TEncodedObject
> extends ManagedModelApi<
		RequestT,
		ResponseT,
		ModelT,
		ClassT
> {
	constructor(
			path: string,
			model: ClassT,
			private encodeData: (m: ModelT) => TypeT,
			private postCreateCallback?: (inserted: ModelT) => Promise<void>,
			private postUpdateCallback?: (updated: ModelT) => Promise<void>,
			private postDeleteCallback?: (existing: ModelT) => Promise<void>,
			private postMoveCallback?: (moved: ModelT) => Promise<void>,
			private preCreateCallback?: (inserting: TPropertyObject) => Promise<void|never>,
			private preUpdateCallback?: (updating: ModelT) => Promise<void|never>,
			private preDeleteCallback?: (existing: ModelT) => Promise<void|never>,
			private preMoveCallback?: (moving: ModelT, direction: ECommonsMoveDirection) => Promise<void|never>,
			private altCreateCallback?: (insert: TPropertyObject) => Promise<ModelT>,
			private altUpdateCallback?: (update: ModelT) => Promise<void>,
			private altDeleteCallback?: (existing: ModelT) => Promise<void>,
			private altMoveCallback?: (move: ModelT, direction: ECommonsMoveDirection) => Promise<ModelT>
	) {
		super(path, model);
	}

	public override applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		if (!this.model.isModelManageable()) return;
		
		// list all
		getHandler(
				`${this.path}data`,
				async (_req: RequestT, _res: ResponseT): Promise<TypeT[]> => {
					const items: ModelT[] = await this.model.listOrdered();
					
					return items
							.map((m: ModelT): TypeT => this.encodeData(m));
				}
		);
	
		// list IDs
		getHandler(
				`${this.path}data/ids`,
				async (_req: RequestT, _res: ResponseT): Promise<number[]> => {
					const items: ModelT[] = await this.model.listOrdered();
					
					return items
							.map((m: ModelT): number => m.id);
				}
		);

		// list names
		getHandler(
				`${this.path}data/names`,
				async (_req: RequestT, _res: ResponseT): Promise<string[]> => {
					const items: ModelT[] = await this.model.listOrdered();
					
					return items
							.map((m: ModelT): string => m.name);
				}
		);

		// get by id
		getHandler(
				`${this.path}data/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const m: ModelT|undefined = await this.model.getById(
							req.strictParams.id as number
					);
					if (!m) return commonsRestApiNotFound('No such model item exists');
					
					return this.encodeData(m);
				}
		);

		// get by name
		getHandler(
				`${this.path}data/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const m: ModelT|undefined = await this.model.getByName(
							req.strictParams.name as string
					);
					if (!m) return commonsRestApiNotFound('No such model item exists');
					
					return this.encodeData(m);
				}
		);

		if (this.model.isUidSupported()) {
			// list UIDs
			getHandler(
					`${this.path}data/uids`,
					async (_req: RequestT, _res: ResponseT): Promise<string[]> => {
						const items: ModelT[] = await this.model.listOrdered();
						
						return items
								.map((m: ModelT): string => (m as ICommonsUniquelyIdentified).uid);
					}
			);
	
			// get by uid
			getHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const m: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!m) return commonsRestApiNotFound('No such model item exists');
						
						return this.encodeData(m);
					}
			);
		}
	}
	
	private async doUpdate(
			existing: ModelT,
			data: TPropertyObject,
			skipOmitted: boolean
	): Promise<ModelT> {
		let rebuild: TPropertyObject = {};
		try {
			rebuild = this.attemptBuildData(
					this.model.getManageableFields(),
					data,
					skipOmitted
			);
		} catch (e) {
			if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
			
			return commonsRestApiError((e as Error).message);
		}
		
		const merged: ModelT = {
				...existing,
				...rebuild
		};
		
		// double checks: disallow changing of the internal fields
		
		if (merged.id !== existing.id) return commonsRestApiBadRequest('Attempting to reassign a new ID to an existing model item');
		
		if (this.model.isUidSupported()) {
			if (merged['uid'] !== existing['uid']) return commonsRestApiBadRequest('Attempting to reassign a new UID to an existing model item');
		}
		
		if (this.preUpdateCallback) await this.preUpdateCallback(merged);

		if (this.altUpdateCallback) {
			await this.altUpdateCallback(merged);
		} else {
			await this.model.update(merged);
		}

		if (this.postUpdateCallback) await this.postUpdateCallback(merged);
		
		return merged;
	}

	public override applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		if (!this.model.isModelManageable()) return;
		
		// create
		postHandler(
				`${this.path}data`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					let rebuild: TPropertyObject = {};
					try {
						rebuild = this.attemptBuildData(
								this.model.getManageableFields(),
								bodyRequestTypecast.body as TPropertyObject,
								false
						);
					} catch (e) {
						if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
						
						return commonsRestApiError((e as Error).message);
					}

					if (this.model.isUidSupported()) {
						rebuild.uid = commonsBase62GenerateRandomId();
					}

					// check for name conflicts (no need to check if name exists, as it is a mandatory field and manageable)
					const existing: ModelT|undefined = await this.model.getByName(rebuild['name'] as string);
					if (existing) return commonsRestApiConflict('An model item for this name already exists');
					
					if (this.preCreateCallback) await this.preCreateCallback(rebuild);
					
					let inserted: ModelT|undefined;
					if (this.altCreateCallback) {
						inserted = await this.altCreateCallback(rebuild);
					} else {
						inserted = await this.model.insertFirstClass(rebuild);
					}
					if (!inserted) throw new Error('Failed to create model item');
					
					if (this.postCreateCallback) await this.postCreateCallback(inserted);
					
					return this.encodeData(inserted);
				}
		);
		
		// update for id
		patchHandler(
				`${this.path}data/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const existing: ModelT|undefined = await this.model.getById(req.strictParams.id as number);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					const updated: ModelT = await this.doUpdate(
							existing,
							bodyRequestTypecast.body as TPropertyObject,
							commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
					);
					
					return this.encodeData(updated);
				}
		);
		
		// update for name
		patchHandler(
				`${this.path}data/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const existing: ModelT|undefined = await this.model.getByName(req.strictParams.name as string);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					const updated: ModelT = await this.doUpdate(
							existing,
							bodyRequestTypecast.body as TPropertyObject,
							commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
					);
					
					return this.encodeData(updated);
				}
		);

		// move for id
		patchHandler(
				`${this.path}data/ids/:id[id]/move`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const existing: ModelT|undefined = await this.model.getById(req.strictParams.id as number);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
					const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
					switch (direction) {
						case ECommonsMoveDirection.UP:
						case ECommonsMoveDirection.DOWN:
						case ECommonsMoveDirection.TOP:
						case ECommonsMoveDirection.BOTTOM:
							try {
								if (this.preMoveCallback) await this.preMoveCallback(existing, direction);

								let moved: ModelT|undefined;
								if (this.altMoveCallback) {
									moved = await this.altMoveCallback(existing, direction);
								} else {
									moved = await this.model.move(existing, direction);
								}
								if (!moved) throw new Error('Failed to move model item');

								if (this.postMoveCallback) await this.postMoveCallback(moved);
								
								return this.encodeData(moved);
							} catch (e) {
								if (/^Cannot move /.test((e as Error).message)) {
									return commonsRestApiBadRequest((e as Error).message);
								}
								
								throw e;
							}
							
						default:
							return commonsRestApiBadRequest('Unknown movement direction');
					}
				}
		);
		
		// move for name
		patchHandler(
				`${this.path}data/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const existing: ModelT|undefined = await this.model.getByName(req.strictParams.name as string);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
					const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
					switch (direction) {
						case ECommonsMoveDirection.UP:
						case ECommonsMoveDirection.DOWN:
						case ECommonsMoveDirection.TOP:
						case ECommonsMoveDirection.BOTTOM:
							try {
								if (this.preMoveCallback) await this.preMoveCallback(existing, direction);

								let moved: ModelT|undefined;
								if (this.altMoveCallback) {
									moved = await this.altMoveCallback(existing, direction);
								} else {
									moved = await this.model.move(existing, direction);
								}
								if (!moved) throw new Error('Failed to move model item');

								if (this.postMoveCallback) await this.postMoveCallback(moved);

								return this.encodeData(moved);
							} catch (e) {
								if (/^Cannot move /.test((e as Error).message)) {
									return commonsRestApiBadRequest((e as Error).message);
								}
								
								throw e;
							}
							
						default:
							return commonsRestApiBadRequest('Unknown movement direction');
					}
				}
		);
		
		// delete for id
		deleteHandler(
				`${this.path}data/ids/:id[id]`,
				async (req: RequestT, _res: ResponseT): Promise<boolean> => {
					const existing: ModelT|undefined = await this.model.getById(req.strictParams.id as number);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					if (this.preDeleteCallback) await this.preDeleteCallback(existing);
					
					if (this.altDeleteCallback) {
						await this.altDeleteCallback(existing);
					} else {
						await this.model.delete(existing);
					}
					
					if (this.postDeleteCallback) await this.postDeleteCallback(existing);
					
					return true;
				}
		);
		
		// delete for name
		deleteHandler(
				`${this.path}data/names/:name[idname]`,
				async (req: RequestT, _res: ResponseT): Promise<boolean> => {
					const existing: ModelT|undefined = await this.model.getByName(req.strictParams.name as string);
					if (!existing) return commonsRestApiNotFound('No such model item exists');
					
					if (this.preDeleteCallback) await this.preDeleteCallback(existing);
					
					if (this.altDeleteCallback) {
						await this.altDeleteCallback(existing);
					} else {
						await this.model.delete(existing);
					}
					
					if (this.postDeleteCallback) await this.postDeleteCallback(existing);
					
					return true;
				}
		);
		
		if (this.model.isUidSupported()) {
			// create with fixed uid
			putHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						// deny upsert, even though PUT allows it, as it could be used to break the firstClass relationships
						const existingUid: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (existingUid) return commonsRestApiConflict('An model item for this name already exists');

						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						let rebuild: TPropertyObject = {
								uid: req.strictParams.uid as string
						};
						try {
							rebuild = this.attemptBuildData(
									this.model.getManageableFields(),
									bodyRequestTypecast.body as TPropertyObject,
									false
							);
						} catch (e) {
							if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
							
							return commonsRestApiError((e as Error).message);
						}
	
						// check for name conflicts (no need to check if name exists, as it is a mandatory field and manageable)
						const existing: ModelT|undefined = await this.model.getByName(rebuild['name'] as string);
						if (existing) return commonsRestApiConflict('An model item for this name already exists');
						
						if (this.preCreateCallback) await this.preCreateCallback(rebuild);
					
						let inserted: ModelT|undefined;
						if (this.altCreateCallback) {
							inserted = await this.altCreateCallback(rebuild);
						} else {
							inserted = await this.model.insertFirstClass(rebuild);
						}
						if (!inserted) throw new Error('Failed to create model item');
						
						if (this.postCreateCallback) await this.postCreateCallback(inserted);
						
						return this.encodeData(inserted);
					}
			);
			
			// update for uid
			patchHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
					
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						const updated: ModelT = await this.doUpdate(
								existing,
								bodyRequestTypecast.body as TPropertyObject,
								commonsTypeAttemptBoolean(bodyRequestTypecast.body['_skipOmitted']) || false	// eslint-disable-line @typescript-eslint/no-unsafe-member-access
						);
						
						return this.encodeData(updated);
					}
			);
			
			// move for uid
			patchHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
						
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(bodyRequestTypecast.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('No direction specified');
						const direction: ECommonsMoveDirection = toECommonsMoveDirection(bodyRequestTypecast.body['direction'] as string)!;
						switch (direction) {
							case ECommonsMoveDirection.UP:
							case ECommonsMoveDirection.DOWN:
							case ECommonsMoveDirection.TOP:
							case ECommonsMoveDirection.BOTTOM:
								try {
									if (this.preMoveCallback) await this.preMoveCallback(existing, direction);

									let moved: ModelT|undefined;
									if (this.altMoveCallback) {
										moved = await this.altMoveCallback(existing, direction);
									} else {
										moved = await this.model.move(existing, direction);
									}
									if (!moved) throw new Error('Failed to move model item');
	
									if (this.postMoveCallback) await this.postMoveCallback(moved);
									
									return this.encodeData(moved);
								} catch (e) {
									if (/^Cannot move /.test((e as Error).message)) {
										return commonsRestApiBadRequest((e as Error).message);
									}
									
									throw e;
								}
								
							default:
								return commonsRestApiBadRequest('Unknown movement direction');
						}
					}
			);

			// delete for uid
			deleteHandler(
					`${this.path}data/uids/:uid[base62]`,
					async (req: RequestT, _res: ResponseT): Promise<boolean> => {
						const existing: ModelT|undefined = await this.model.getByUid(req.strictParams.uid as string);
						if (!existing) return commonsRestApiNotFound('No such model item exists');
						
						if (this.preDeleteCallback) await this.preDeleteCallback(existing);
					
						if (this.altDeleteCallback) {
							await this.altDeleteCallback(existing);
						} else {
							await this.model.delete(existing);
						}
						
						if (this.postDeleteCallback) await this.postDeleteCallback(existing);
						
						return true;
					}
			);
		}
	}
}
