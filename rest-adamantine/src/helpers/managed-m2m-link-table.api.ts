import { commonsStringPluralise } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { ICommonsUniquelyIdentified } from 'tscommons-es-models';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsManaged } from 'tscommons-es-models-adamantine';
import { ICommonsManagedModelField } from 'tscommons-es-models-adamantine';
import { TCommonsManagedM2MLinkTableModelMetadata, encodeICommonsManagedM2MLinkTableModelMetadata } from 'tscommons-es-models-adamantine';

import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsManagedM2MLinkTable } from 'nodecommons-es-models-adamantine';
import {
		commonsRestApiError,
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiConflict
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

import { AdamantineModelApi } from './adamantine-model.api';

async function getModelItemByIdOnly<AB extends ICommonsManaged>(
		id: number,
		model: CommonsFirstClass<AB>
): Promise<AB|undefined> {
	if (model instanceof CommonsSecondClass) {
		return await (model as CommonsSecondClass<AB, any>).getByIdOnly(id);
	} else {
		return await model.getById(id);
	}
}

export class ManagedM2MLinkTableApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		ModelAT extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		ModelBT extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		ModelLinkT extends ICommonsM2MLink<ModelAT, ModelBT>,
		ClassT extends CommonsManagedM2MLinkTable<ModelLinkT, ModelAT, ModelBT>,
		TypeT extends TEncodedObject
> extends AdamantineModelApi<
		RequestT,
		ResponseT,
		ModelLinkT,
		ClassT
> {
	// fudge method to facilitate the fact that second class getById requires a first-class, but we don't have one
	private aPlural: string;
	private bPlural: string;
	
	constructor(
			path: string,
			model: ClassT,
			private encodeData: (m: ModelLinkT) => TypeT,
			private nonManagedAllowedDirectDataFields: string[] = [],
			private postLinkCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void>,
			private postUnlinkCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void>,
			private postUpdateCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void>,
			private preLinkCallback?: (a: ModelAT, b: ModelBT, row: TPropertyObject) => Promise<void|never>,
			private preUnlinkCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void|never>,
			private preUpdateCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void|never>,
			private altLinkCallback?: (a: ModelAT, b: ModelBT, row: TPropertyObject) => Promise<ModelLinkT>,
			private altUnlinkCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<void>,
			private altUpdateCallback?: (a: ModelAT, b: ModelBT, row: ModelLinkT) => Promise<ModelLinkT>
	) {
		super(path, model);
		
		const aFieldName: string = this.model.getAField();
		const bFieldName: string = this.model.getBField();
		
		this.aPlural = commonsStringPluralise(aFieldName, 2);
		this.bPlural = commonsStringPluralise(bFieldName, 2);
	}

	public override applyMetadata(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		getHandler(
				`${this.path}metadata`,
				async (_req: RequestT, _res: ResponseT): Promise<TCommonsManagedM2MLinkTableModelMetadata> => {	// eslint-disable-line @typescript-eslint/require-await
					const dataFields: ICommonsManagedModelField[] = this.getMetadataFields(
							this.model.getManageableFields()
					);

					for (const dataField of dataFields) {
						const description: string|undefined = this.model.getFieldDescription(dataField.name);
						if (description) dataField.description = description;

						const suffix: string|undefined = this.model.getFieldSuffix(dataField.name);
						if (suffix) dataField.suffix = suffix;

						const helper: string|undefined = this.model.getFieldHelper(dataField.name);
						if (helper) dataField.helper = helper;
					}
					
					return encodeICommonsManagedM2MLinkTableModelMetadata({
							manageableFields: dataFields,
							accessRequiredToManage: this.model.getAccessRequiredToManage(),
							aFieldName: this.model.getAField(),
							bFieldName: this.model.getBField()
					});
				}
		);
	}

	public override applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		// list Bs by A
		getHandler(
				`${this.path}${this.aPlural}/:a[id]/${this.bPlural}`,
				async (req: RequestT, _res: ResponseT): Promise<number[]> => {
					const a: ModelAT|undefined = await getModelItemByIdOnly<ModelAT>(
							req.strictParams.a as number,
							this.model.getAFirstClass()
					);
					if (!a) return commonsRestApiNotFound('No such model item exists');
					
					const bs: ModelBT[] = await this.model.listBsByA(a);
					
					return bs
							.map((b: ModelBT): number => b.id);
				}
		);

		// list As by B
		getHandler(
				`${this.path}${this.bPlural}/:b[id]/${this.aPlural}`,
				async (req: RequestT, _res: ResponseT): Promise<number[]> => {
					const b: ModelBT|undefined = await getModelItemByIdOnly<ModelBT>(
							req.strictParams.b as number,
							this.model.getBFirstClass()
					);
					if (!b) return commonsRestApiNotFound('No such model item exists');
					
					const as: ModelAT[] = await this.model.listAsByB(b);
					
					return as
							.map((a: ModelAT): number => a.id);
				}
		);

		// get link row for A and B
		getHandler(
				`${this.path}${this.aPlural}/:a[id]/${this.bPlural}/:b[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const a: ModelAT|undefined = await getModelItemByIdOnly<ModelAT>(
							req.strictParams.a as number,
							this.model.getAFirstClass()
					);
					if (!a) return commonsRestApiNotFound('No such model item A exists');

					const b: ModelBT|undefined = await getModelItemByIdOnly<ModelBT>(
							req.strictParams.b as number,
							this.model.getBFirstClass()
					);
					if (!b) return commonsRestApiNotFound('No such model item B exists');
					
					if (!(await this.model.isLinked(a, b))) return commonsRestApiNotFound('Model items are not linked');
					
					const m: ModelLinkT = await this.model.getLinkRow(a, b);
					
					return this.encodeData(m);
				}
		);
	}
	
	public override applyWriteData(
			_postHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<RequestT, ResponseT>) => void
	) {
		// link
		putHandler(
				`${this.path}${this.aPlural}/:a[id]/${this.bPlural}/:b[id]`,
				async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
					const a: ModelAT|undefined = await getModelItemByIdOnly<ModelAT>(
							req.strictParams.a as number,
							this.model.getAFirstClass()
					);
					if (!a) return commonsRestApiNotFound('No such model item A exists');

					const b: ModelBT|undefined = await getModelItemByIdOnly<ModelBT>(
							req.strictParams.b as number,
							this.model.getBFirstClass()
					);
					if (!b) return commonsRestApiNotFound('No such model item B exists');
					
					const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

					let rebuild: TPropertyObject = {};
					try {
						rebuild = this.attemptBuildData(
								[
										...this.model.getManageableFields(),
										...this.nonManagedAllowedDirectDataFields
								],
								bodyRequestTypecast.body as TPropertyObject,
								false
						);
					} catch (e) {
						if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
						
						return commonsRestApiError((e as Error).message);
					}

					if (await this.model.isLinked(a, b)) return commonsRestApiConflict('Link row for model items already exists');

					if (this.preLinkCallback) await this.preLinkCallback(a, b, rebuild);

					let inserted: ModelLinkT|undefined;
					if (this.altLinkCallback) {
						inserted = await this.altLinkCallback(a, b, rebuild);
					} else {
						await this.model.link(a, b, rebuild);
						inserted = await this.model.getLinkRow(a, b);
					}
					if (!inserted) throw new Error('Failed to link model items');
					
					if (this.postLinkCallback) await this.postLinkCallback(a, b, inserted);

					return this.encodeData(inserted);
				}
		);

		// only implement PATCH if there is actually link row data that can be modified
		if (this.model.getManageableFields().length > 0) {
			// update
			patchHandler(
					`${this.path}${this.aPlural}/:a[id]/${this.bPlural}/:b[id]`,
					async (req: RequestT, _res: ResponseT): Promise<TypeT> => {
						const a: ModelAT|undefined = await getModelItemByIdOnly<ModelAT>(
								req.strictParams.a as number,
								this.model.getAFirstClass()
						);
						if (!a) return commonsRestApiNotFound('No such model item A exists');
	
						const b: ModelBT|undefined = await getModelItemByIdOnly<ModelBT>(
								req.strictParams.b as number,
								this.model.getBFirstClass()
						);
						if (!b) return commonsRestApiNotFound('No such model item B exists');
						
						if (!(await this.model.isLinked(a, b))) return commonsRestApiNotFound('Model items are not linked');
	
						const existing: ModelLinkT = await this.model.getLinkRow(a, b);
						
						const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

						let rebuild: TPropertyObject = {};
						try {
							rebuild = this.attemptBuildData(
									[
											...this.model.getManageableFields(),
											...this.nonManagedAllowedDirectDataFields
									],
									bodyRequestTypecast.body as TPropertyObject,
									true
							);
						} catch (e) {
							if (/^Invalid data/.test((e as Error).message)) return commonsRestApiBadRequest((e as Error).message);
							
							return commonsRestApiError((e as Error).message);
						}
						
						let merged: ModelLinkT = {
								...existing,
								...rebuild
						};
						
						// double checks: disallow changing of the internal fields
						
						if (merged[this.model.getAField()] !== existing[this.model.getAField()]) return commonsRestApiBadRequest('Attempting to reassign a new A ID to an existing model item');
						if (merged[this.model.getBField()] !== existing[this.model.getBField()]) return commonsRestApiBadRequest('Attempting to reassign a new B ID to an existing model item');

						if (this.preUpdateCallback) await this.preUpdateCallback(a, b, merged);
						
						if (this.altUpdateCallback) {
							merged = await this.altUpdateCallback(a, b, merged);
						} else {
							await this.model.updateLinkRow(merged);
						}
					
						if (this.postUpdateCallback) await this.postUpdateCallback(a, b, merged);
						
						return this.encodeData(merged);
					}
			);
		}
		
		// delete
		deleteHandler(
				`${this.path}${this.aPlural}/:a[id]/${this.bPlural}/:b[id]`,
				async (req: RequestT, _res: ResponseT): Promise<boolean> => {
					const a: ModelAT|undefined = await getModelItemByIdOnly<ModelAT>(
							req.strictParams.a as number,
							this.model.getAFirstClass()
					);
					if (!a) return commonsRestApiNotFound('No such model item A exists');

					const b: ModelBT|undefined = await getModelItemByIdOnly<ModelBT>(
							req.strictParams.b as number,
							this.model.getBFirstClass()
					);
					if (!b) return commonsRestApiNotFound('No such model item B exists');
					
					if (!(await this.model.isLinked(a, b))) return commonsRestApiNotFound('Model items are not linked');

					const existing: ModelLinkT = await this.model.getLinkRow(a, b);

					if (this.preUnlinkCallback) await this.preUnlinkCallback(a, b, existing);

					if (this.altUnlinkCallback) {
						await this.altUnlinkCallback(a, b, existing);
					} else {
						await this.model.unlink(a, b);
					}
					
					if (this.postUnlinkCallback) await this.postUnlinkCallback(a, b, existing);
					
					return true;
				}
		);
	}
}
