import { TEncodedObject, TPropertyObject } from 'tscommons-es-core';

import { CommonsManagedFirstClass } from 'nodecommons-es-models-adamantine';
import { CommonsManagedFirstClassUserModel } from 'nodecommons-es-models-adamantine';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestUserPwHashSessionApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithSession } from 'nodecommons-es-rest';
import { CommonsAdamantineManagedFirstClassUserSessionService } from 'nodecommons-es-security-adamantine';
import { ICommonsAdamantineManagedFirstClassUserWithPwHash } from 'nodecommons-es-security-adamantine';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

export abstract class CommonsRestAdamantineManagedFirstClassUserModelSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		UserT extends ICommonsAdamantineManagedFirstClassUserWithPwHash,
		SessionServiceT extends CommonsAdamantineManagedFirstClassUserSessionService<UserT>
> extends CommonsRestUserPwHashSessionApi<
		RequestT,
		ResponseT,
		UserT,
		SessionServiceT
> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			private model: CommonsManagedFirstClass<UserT> & CommonsManagedFirstClassUserModel<UserT>,
			sessionService: SessionServiceT
	) {
		super(restServer, sessionService);
	}
	
	private async autoRegister(
			username: string,
			req: RequestT,
			buildAutoRegisterUser: (
					username: string,
					data: TPropertyObject
			) => TPropertyObject|undefined
	): Promise<UserT|undefined> {
		const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;
		
		const user: TPropertyObject|undefined = buildAutoRegisterUser(username, bodyRequestTypecast.body as TPropertyObject);
		if (!user) return undefined;
		
		return this.model.insertFirstClass(user);
	}
	
	public installUserModelAuthentication(
			authenticateQuery: string,
			encodeUser: (user: UserT) => TEncodedObject,
			buildAutoRegisterUser?: (
					username: string,
					data: TPropertyObject
			) => TPropertyObject|undefined
	): void {
		super.installAuthentication(
				authenticateQuery,
				async (	// getUserByUsername
						username: string,
						_req: RequestT
				): Promise<UserT|undefined> => await this.model.getUserByUsername(username),
				buildAutoRegisterUser ? ((	// autoregister
						username: string,
						req: RequestT
				): Promise<UserT|undefined> => this.autoRegister(
						username,
						req,
						buildAutoRegisterUser
				))
					: undefined
		);

		super.getSessionHandler(
				authenticateQuery,
				async (req: ICommonsRequestWithSession<UserT>, _res: unknown): Promise<TEncodedObject> => encodeUser(req.session.data)	// eslint-disable-line @typescript-eslint/require-await
		);
	}
}
