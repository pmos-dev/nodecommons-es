import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';
import { TEncoded } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';
import { ECommonsAdamantineAccess, computeECommonsAdamantineAccess } from 'tscommons-es-models-adamantine';

import { CommonsAdamantineManagedSecondClassUserSessionService } from 'nodecommons-es-security-adamantine';
import { ICommonsAdamantineManagedSecondClassUserWithPwHash } from 'nodecommons-es-security-adamantine';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestUserPwHashSessionApi } from 'nodecommons-es-rest';
import {
		commonsRestApiError,
		commonsRestApiForbidden
} from 'nodecommons-es-rest';
import { ICommonsRequestWithSession } from 'nodecommons-es-rest';
import { TCommonsSessionContentHandler } from 'nodecommons-es-rest';
import { TCommonsSessionNoContentHandler } from 'nodecommons-es-rest';
import { TCommonsContent } from 'nodecommons-es-rest';
import { CommonsFirstClass } from 'nodecommons-es-models';
import { ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

export class CommonsRestAdamantineRequiredAccessApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		UserT extends ICommonsAdamantineManagedSecondClassUserWithPwHash<ParentT>,
		ParentT extends ICommonsFirstClass,
		FirstClassT extends CommonsFirstClass<ParentT>,
		SessionServiceT extends CommonsAdamantineManagedSecondClassUserSessionService<UserT, ParentT>
> extends CommonsRestUserPwHashSessionApi<
		RequestT,
		ResponseT,
		UserT,
		SessionServiceT
> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			sessionService: SessionServiceT,
			private parentModel: FirstClassT,
			private parentField: string
	) {
		super(restServer, sessionService);
	}
	
	private async getSessionParent(
			req: ICommonsRequestWithSession<UserT>
	): Promise<ParentT> {
		if (!req.session || !req.session.data || !commonsTypeHasPropertyNumber(req.session.data, this.parentField)) return commonsRestApiError('Unable to derive namespace for session');
		
		const parent: ParentT|undefined = await this.parentModel.getById(req.session.data[this.parentField] as number);
		if (!parent) return commonsRestApiError('Unable to find parent for session');
		
		return parent;
	}
	
	private wrapAccessNoContentHandler(
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionNoContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): TCommonsSessionNoContentHandler<
			RequestT,
			ResponseT,
			UserT
	> {
		return async (req: RequestT & ICommonsRequestWithSession<UserT>, response: ResponseT): Promise<number|void> => {
			if (!computeECommonsAdamantineAccess(req.session.data.access, required)) return commonsRestApiForbidden('Access denied to this method.');

			req.session[this.parentField] = await this.getSessionParent(req);

			return await handler(req, response);
		};
	}
	
	private wrapAccessContentHandler(
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): TCommonsSessionContentHandler<
			RequestT,
			ResponseT,
			UserT
	> {
		return async (req: RequestT & ICommonsRequestWithSession<UserT>, response: ResponseT): Promise<TCommonsContent|TEncoded> => {
			if (!computeECommonsAdamantineAccess(req.session.data.access, required)) return commonsRestApiForbidden('Access denied to this method.');

			req.session[this.parentField] = await this.getSessionParent(req);

			return await handler(req, response);
		};
	}
	
	protected getAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.getSessionHandler(
				query,
				this.wrapAccessContentHandler(required, handler)
		);
	}

	// insert
	protected postAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.postSessionHandler(
				query,
				this.wrapAccessContentHandler(required, handler)
		);
	}

	// update
	protected patchAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.patchSessionHandler(
				query,
				this.wrapAccessContentHandler(required, handler)
		);
	}
	
	// upsert
	protected putAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.putSessionHandler(
				query,
				this.wrapAccessContentHandler(required, handler)
		);
	}
	
	protected deleteAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.deleteSessionHandler(
				query,
				this.wrapAccessContentHandler(required, handler)
		);
	}
	
	protected headAccessHandler(
			query: string,
			required: ECommonsAdamantineAccess,
			handler: TCommonsSessionNoContentHandler<
					RequestT,
					ResponseT,
					UserT
			>
	): void {
		super.headSessionHandler(
				query,
				this.wrapAccessNoContentHandler(required, handler)
		);
	}
}
