import {
		commonsTypeHasProperty,
		commonsTypeAttemptNumber,
		TPropertyObject
} from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { ICommonsFirstClass } from 'tscommons-es-models';

import { CommonsFirstClass } from 'nodecommons-es-models';
import { CommonsManagedSecondClass } from 'nodecommons-es-models-adamantine';
import { CommonsManagedSecondClassUserModel } from 'nodecommons-es-models-adamantine';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestUserPwHashSessionApi } from 'nodecommons-es-rest';
import { ICommonsRequestWithSession } from 'nodecommons-es-rest';
import { CommonsAdamantineManagedSecondClassUserSessionService } from 'nodecommons-es-security-adamantine';
import { ICommonsAdamantineManagedSecondClassUserWithPwHash } from 'nodecommons-es-security-adamantine';
import { ICommonsRequestWithBodyTypecast, ICommonsStrictParamsRequest, TCommonsHttpResponse } from 'nodecommons-es-http';

export abstract class CommonsRestAdamantineManagedSecondClassUserModelSessionApi<
		RequestT extends ICommonsStrictParamsRequest,
		ResponseT extends TCommonsHttpResponse,
		UserT extends ICommonsAdamantineManagedSecondClassUserWithPwHash<FirstClassT>,
		FirstClassT extends ICommonsFirstClass,
		SessionServiceT extends CommonsAdamantineManagedSecondClassUserSessionService<UserT, FirstClassT>
> extends CommonsRestUserPwHashSessionApi<
		RequestT,
		ResponseT,
		UserT,
		SessionServiceT
> {
	constructor(
			restServer: CommonsRestServer<RequestT, ResponseT>,
			private model: CommonsManagedSecondClass<UserT, FirstClassT> & CommonsManagedSecondClassUserModel<UserT, FirstClassT>,
			sessionService: SessionServiceT
	) {
		super(restServer, sessionService);
	}
	
	private async getFirstClass(req: RequestT): Promise<FirstClassT|undefined> {
		const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;

		if (!commonsTypeHasProperty(bodyRequestTypecast.body, this.model.getFirstClassField())) return undefined;
		const id: number|undefined = commonsTypeAttemptNumber(bodyRequestTypecast.body[this.model.getFirstClassField()]);
		if (!id) return undefined;
		
		const firstClass: CommonsFirstClass<FirstClassT> = this.model.getFirstClass();
		return await firstClass.getById(id);
	}
	
	private async autoRegister(
			username: string,
			firstClass: FirstClassT,
			req: RequestT,
			buildAutoRegisterUser: (
					username: string,
					firstClass: FirstClassT,
					data: TPropertyObject
			) => TPropertyObject|undefined
	): Promise<UserT|undefined> {
		const bodyRequestTypecast: ICommonsRequestWithBodyTypecast = req as unknown as ICommonsRequestWithBodyTypecast;
		
		const user: TPropertyObject|undefined = buildAutoRegisterUser(username, firstClass, bodyRequestTypecast.body as TPropertyObject);
		if (!user) return undefined;
		
		return this.model.insertForFirstClass(firstClass, user);
	}
	
	public installUserModelAuthentication(
			authenticateQuery: string,
			encodeUser: (user: UserT) => TEncodedObject,
			buildAutoRegisterUser?: (
					username: string,
					firstClass: FirstClassT,
					data: TPropertyObject
			) => TPropertyObject|undefined
	): void {
		super.installAuthentication(
				authenticateQuery,
				async (	// getUserByUsername
						username: string,
						req: RequestT
				): Promise<UserT|undefined> => {
					const firstClass: FirstClassT|undefined = await this.getFirstClass(req);
					if (!firstClass) return undefined;
					
					return await this.model.getUserByUsername(firstClass, username);
				},
				buildAutoRegisterUser ? (async (	// autoregister
						username: string,
						req: RequestT
				): Promise<UserT|undefined> => {
					const firstClass: FirstClassT|undefined = await this.getFirstClass(req);
					if (!firstClass) return undefined;
					
					return this.autoRegister(
							username,
							firstClass,
							req,
							buildAutoRegisterUser
					);
				})
					: undefined
		);

		super.getSessionHandler(
				authenticateQuery,
				async (req: ICommonsRequestWithSession<UserT>, _res: unknown): Promise<TEncodedObject> => encodeUser(req.session.data)	// eslint-disable-line @typescript-eslint/require-await
		);
	}
}
