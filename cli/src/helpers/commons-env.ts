import { commonsTypeHasProperty } from 'tscommons-es-core';

export function commonsEnvHasVariable(key: string): boolean {
	return commonsTypeHasProperty(process.env, key);
}

export function commonsEnvGetString(key: string): string {
	if (!commonsEnvHasVariable(key)) throw new Error(`No such key: ${key}`);
	return process.env[key]!.toString();
}

export function commonsEnvGetStringOrUndefined(key: string): string|undefined {
	if (!commonsEnvHasVariable(key)) return undefined;
	return commonsEnvGetString(key);
}

export function commonsEnvGetNumber(key: string): number {
	if (!commonsEnvHasVariable(key)) throw new Error(`No such key: ${key}`);
	return parseInt(process.env[key]!, 10);
}

export function commonsEnvGetNumberOrUndefined(key: string): number|undefined {
	if (!commonsEnvHasVariable(key)) return undefined;
	return commonsEnvGetNumber(key);
}

export function commonsEnvGetDate(key: string): Date {
	return new Date(Date.parse(commonsEnvGetString(key)));
}

export function commonsEnvGetDateOrUndefined(key: string): Date|undefined {
	if (!commonsEnvHasVariable(key)) return undefined;

	return commonsEnvGetDate(key);
}
