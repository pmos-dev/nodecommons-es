import * as readline from 'readline';
import { commonsArrayEmpty } from 'tscommons-es-core/dist';

import { commonsOutputDebug } from './commons-output';

// we have to use var rather than let so that these are global in case different node_modules libraries are involved
/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call */
global['COMMONS_KEY_PRESS_IS_ACTIVE'] = global['COMMONS_KEY_PRESS_IS_ACTIVE'] !== undefined ? global['COMMONS_KEY_PRESS_IS_ACTIVE'] : false;
global['COMMONS_KEY_PRESS_HANDLERS'] = global['COMMONS_KEY_PRESS_HANDLERS'] !== undefined ? global['COMMONS_KEY_PRESS_HANDLERS'] : [];
global['COMMONS_KEY_PRESS_SIGINT_HANDLER'] = global['COMMONS_KEY_PRESS_SIGINT_HANDLER'] !== undefined ? global['COMMONS_KEY_PRESS_SIGINT_HANDLER'] : (): void => {
	setTimeout(
			(): void => {
				process.exit(1);
			},
			100
	);
};

export function replaceCommonsKeyPressSigIntHandler(
		callback: () => void
): void {
	global['COMMONS_KEY_PRESS_SIGINT_HANDLER'] = callback;
}

export type TCommonsKeyPress = {
		sequence: string;
		name: string;
		ctrl: boolean;
		meta: boolean;
		shift: boolean;
};

export type TCommonsKeyPressHandler = (
		str: string,
		key: TCommonsKeyPress
) => void;

type TKeyHandler = {
		match: Partial<Pick<TCommonsKeyPress, 'name' | 'ctrl' | 'meta' | 'shift'>>;
		handler: () => void;
};

export class CommonsKeyPressHandler {
	private handlers: TCommonsKeyPressHandler[] = [];
	private keyHandlers: TKeyHandler[] = [];

	constructor() {
		global['COMMONS_KEY_PRESS_HANDLERS'].push(this);

		this.addKeyPressHandler(
				(_str: string, key: TCommonsKeyPress): void => {
					for (const kh of this.keyHandlers) {
						if (kh.match.name !== undefined && kh.match.name !== key.name) continue;
						if (kh.match.ctrl !== undefined && kh.match.ctrl !== key.ctrl) continue;
						if (kh.match.meta !== undefined && kh.match.meta !== key.meta) continue;
						if (kh.match.shift !== undefined && kh.match.shift !== key.shift) continue;
						kh.handler();
					}
				}
		);
	}

	public initOnce(): void {
		if (!global['COMMONS_KEY_PRESS_IS_ACTIVE']) {
			global['COMMONS_KEY_PRESS_IS_ACTIVE'] = true;
			this.init();
		}
	}

	private init(): void {
		readline.emitKeypressEvents(process.stdin);
		process.stdin.setRawMode(true);
		process.stdin.resume();	// seems to be necessary if CommonsInput has previously been used

		process.stdin.on(
				'keypress',
				(str: string, key: TCommonsKeyPress) => {
					if (key.ctrl && key.name === 'c') {
						commonsOutputDebug('SIGINT generated whilst CommonsKeyPressHandler active. This isn\'t caught. Calling any registered replacement handler.');
						global['COMMONS_KEY_PRESS_SIGINT_HANDLER']();
						return;
					}

					for (const handlers of global['COMMONS_KEY_PRESS_HANDLERS']) {
						handlers._handle(str, key);	// eslint-disable-line no-underscore-dangle
					}
				}
		);
	}

	public addKeyPressHandler(
			callback: (str: string, key: TCommonsKeyPress) => void
	): void {
		this.handlers.push(callback);
	}

	public addKeyHandler(
			match: Partial<Pick<TCommonsKeyPress, 'name' | 'ctrl' | 'meta' | 'shift'>>,
			callback: () => void
	): void {
		this.keyHandlers.push({
				match: match,
				handler: callback
		});
	}

	public clear(): void {
		commonsArrayEmpty(this.handlers);
	}

	public _handle(str: string, key: TCommonsKeyPress): void {
		for (const handler of this.handlers) {
			handler(str, key);
		}
	}
}

// const test: CommonsKeyPressHandler = new CommonsKeyPressHandler();
// test.addKeyHandler(
// 		{},
// 		(): void => {
// 			console.log('Got here (all matches)');
// 		}
// );
// test.addKeyHandler(
// 		{
// 				name: 'a'
// 		},
// 		(): void => {
// 			console.log('Got here a or A');
// 		}
// );
// test.addKeyHandler(
// 		{
// 				name: 'b',
// 				shift: false
// 		},
// 		(): void => {
// 			console.log('Got here b only');
// 		}
// );
// test.addKeyHandler(
// 		{
// 				name: 'o',
// 				ctrl: true
// 		},
// 		(): void => {
// 			console.log('Got here Ctrl+O');
// 		}
// );

// test.initOnce();
// console.log('a');
