import * as readline from 'readline';

import { commonsOutputError } from './commons-output';

export function commonsInputReadAllStdIn(): Promise<string[]> {
	return new Promise((resolve: (_: string[]) => void, _: (reason: any) => void): void => {
		const data: any[] = [];
		
		process.stdin
				.on('data', (chunk: any): void => {
					data.push(chunk);
				})
				.on('end', (): void => {
					const result: Buffer = Buffer.concat(data);
					resolve(result.toString().split('\n'));
				});
	});
}

export async function commonsInputConfirm(
		message: string,
		defaultAnswer?: boolean
): Promise<boolean> {
	if (defaultAnswer !== undefined) message = `${message} \x1b[2m[${defaultAnswer ? 'yes' : 'no' }]\x1b[1m`;

	return new Promise((resolve: (outcome: boolean) => void, _: (reason: any) => void): void => {
		const rl = readline.createInterface({
				input: process.stdin,
				output: process.stderr
		});

		rl.question(
				`\x1b[37m\x1b[1m${message}?\x1b[0m `,
				(answer: string|undefined): void => {	// needed to satisfy eslint not liking the Promise<void>
					void (async (): Promise<void> => {
						rl.close();
						
						if (answer === undefined || answer === '') {
							if (defaultAnswer !== undefined) return resolve(defaultAnswer);
							commonsOutputError('You must specify an answer!');
							resolve(await commonsInputConfirm(message, defaultAnswer));
							return;
						}
						
						if (/^y(es)?$/i.test(answer)) return resolve(true);
						if (/^n(o)?$/i.test(answer)) return resolve(false);

						commonsOutputError('You must specify either yes or no!');
						
						resolve(await commonsInputConfirm(message, defaultAnswer));
					})();
				}
		);
	});
}

export async function commonsInputString(
		message: string,
		defaultAnswer?: string,
		regex?: RegExp
): Promise<string> {
	if (defaultAnswer !== undefined) message = `${message} \x1b[2m[${defaultAnswer}]\x1b[1m`;

	return new Promise((resolve: (value: string) => void, _: (reason: any) => void): void => {
		const rl = readline.createInterface({
				input: process.stdin,
				output: process.stderr
		});

		rl.question(
				`\x1b[37m\x1b[1m${message}?\x1b[0m `,
				(answer: string|undefined): void => {	// needed to satisfy eslint not liking the Promise<void>
					void (async (): Promise<void> => {
						rl.close();
						
						if (answer === undefined || answer === '') {
							if (defaultAnswer !== undefined) return resolve(defaultAnswer);

							commonsOutputError('You must enter a value!');
							resolve(await commonsInputString(
									message,
									defaultAnswer,
									regex
							));
							return;
						}

						if (regex && !regex.test(answer)) {
							commonsOutputError('Invalid value.');
							resolve(await commonsInputString(
									message,
									defaultAnswer,
									regex
							));
						}
						
						return resolve(answer);
					})();
				}
		);
	});
}
