import {
		commonsTypeHasProperty,
		commonsTypeAttemptNumber,
		commonsNumberParsePeriod,
		TEncodedObject
} from 'tscommons-es-core';

export class CommonsArgs {
	private args: TEncodedObject;
	private params: string[];
	
	constructor(src?: string[]) {
		if (!src) src = process.argv;
		if (!src) throw new Error('Unable to identify arguments to parser');
		
		this.args = {};
		
		for (const arg of src) {
			const result: RegExpExecArray|null = /^--([^=]+)(?:=(.*))?$/.exec(arg);
			if (!result) continue;
			
			const key: string = result[1];
			const value: string|undefined = result[2];
			
			if (value === undefined) {
				this.args[key] = true;
				continue;
			}

			const asNumber: number|undefined = commonsTypeAttemptNumber(value);
			if (asNumber !== undefined && asNumber.toString(10) === value) {
				this.args[key] = asNumber;
				continue;
			}
			
			this.args[key] = value;
		}
		
		this.params = src
				.slice(2)	// remove the node binary and script
				.filter((arg: string): boolean => !/^--/.test(arg));
	}
	
	public getParam(index: number): string|undefined {
		if (index > this.params.length) return undefined;
		
		return this.params[index];
	}
	
	public listParams(): string[] {
		return this.params;
	}
	
	public getParamsCount(): number {
		return this.params.length;
	}
	
	public hasProperty(key: string, type?: string): boolean {
		if (!commonsTypeHasProperty(this.args, key)) return false;
		
		if (!type) return true;
		
		const data: unknown = this.args[key];
		return typeof data === type;
	}
	
	public getString(key: string): string {
		if (!this.hasProperty(key)) throw new Error(`No such key: ${key}`);
		return this.args[key]!.toString();
	}
	
	public getStringOrUndefined(key: string): string|undefined {
		if (!this.hasProperty(key)) return undefined;
		return this.getString(key);
	}
	
	public getNumber(key: string): number {
		if (!this.hasProperty(key)) throw new Error(`No such key: ${key}`);
		if (!this.hasProperty(key, 'number')) throw new Error(`Key is not a number: ${key}`);
		return this.args[key] as number;
	}
	
	public getNumberOrUndefined(key: string): number|undefined {
		if (!this.hasProperty(key)) return undefined;
		return this.getNumber(key);
	}
	
	public getDate(key: string): Date {
		return new Date(Date.parse(this.getString(key)));
	}
	
	public getDateOrUndefined(key: string): Date|undefined {
		if (!this.hasProperty(key)) return undefined;

		return this.getDate(key);
	}
	
	public getPeriod(key: string): number {
		if (!this.hasProperty(key)) throw new Error(`No such key: ${key}`);
		
		if (this.hasProperty(key, 'number')) return this.getNumber(key);
		if (this.hasProperty(key, 'string')) {
			const attempt: number = commonsNumberParsePeriod(this.getString(key));
			if (isNaN(attempt)) throw new Error(`Key is not a parsable period number: ${key}`);
			
			return attempt;
		}
		
		throw new Error(`Key is not a known type: ${key}`);
	}
	
	public getPeriodOrUndefined(key: string): number|undefined {
		if (!this.hasProperty(key)) return undefined;
		
		return this.getPeriod(key);
	}
	
	public hasAttribute(key: string): boolean {
		if (!this.hasProperty(key, 'boolean')) return false;
		if (!this.args[key]) return false;
		
		return true;
	}
}
