import * as readline from 'readline';

import { commonsDateDateToYmdHis } from 'tscommons-es-core';

export const COMMONS_COLOR_RESET = '\x1b[0m';
export const COMMONS_COLOR_BRIGHT = '\x1b[1m';
export const COMMONS_COLOR_DIM = '\x1b[2m';
export const COMMONS_COLOR_UNDERSCORE = '\x1b[4m';
export const COMMONS_COLOR_BLINK = '\x1b[5m';
export const COMMONS_COLOR_REVERSE = '\x1b[7m';
export const COMMONS_COLOR_HIDDEN = '\x1b[8m';
	
export const COMMONS_COLOR_FOREGROUND_BLACK = '\x1b[30m';
export const COMMONS_COLOR_FOREGROUND_RED = '\x1b[31m';
export const COMMONS_COLOR_FOREGROUND_GREEN = '\x1b[32m';
export const COMMONS_COLOR_FOREGROUND_YELLOW = '\x1b[33m';
export const COMMONS_COLOR_FOREGROUND_BLUE = '\x1b[34m';
export const COMMONS_COLOR_FOREGROUND_MAGENTA = '\x1b[35m';
export const COMMONS_COLOR_FOREGROUND_CYAN = '\x1b[36m';
export const COMMONS_COLOR_FOREGROUND_WHITE = '\x1b[37m';
	
export const COMMONS_COLOR_BACKGROUND_BLACK = '\x1b[40m';
export const COMMONS_COLOR_BACKGROUND_RED = '\x1b[41m';
export const COMMONS_COLOR_BACKGROUND_GREEN = '\x1b[42m';
export const COMMONS_COLOR_BACKGROUND_YELLOW = '\x1b[43m';
export const COMMONS_COLOR_BACKGROUND_BLUE = '\x1b[44m';
export const COMMONS_COLOR_BACKGROUND_MAGENTA = '\x1b[45m';
export const COMMONS_COLOR_BACKGROUND_CYAN = '\x1b[46m';
export const COMMONS_COLOR_BACKGROUND_WHITE = '\x1b[47m';

// we have to use var rather than let so that these are global in case different node_modules libraries are involved
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
global['COMMONS_OUTPUT_IS_DEBUGGING'] = global['COMMONS_OUTPUT_IS_DEBUGGING'] !== undefined ? global['COMMONS_OUTPUT_IS_DEBUGGING'] : false;
global['COMMONS_OUTPUT_IS_DAEMON'] = global['COMMONS_OUTPUT_IS_DAEMON']! !== undefined ? global['COMMONS_OUTPUT_IS_DAEMON'] : false;
global['COMMONS_OUTPUT_DOING_MESSAGE'] = global['COMMONS_OUTPUT_DOING_MESSAGE']! !== undefined ? global['COMMONS_OUTPUT_DOING_MESSAGE'] : undefined;
global['COMMONS_OUTPUT_POLLING'] = global['COMMONS_OUTPUT_POLLING']! !== undefined ? global['COMMONS_OUTPUT_POLLING'] : false;
/* eslint-enable @typescript-eslint/no-unsafe-assignment */

type TAcceptableValues = string|number|boolean;

export function commonsOutputInfo(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);
	
	console.error(...args);
}

export function commonsOutputError(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_RED}${COMMONS_COLOR_BRIGHT}`);
	console.error(...args);
	process.stderr.write(COMMONS_COLOR_RESET);
}

export function commonsOutputAlert(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_YELLOW}${COMMONS_COLOR_BRIGHT}`);
	console.error(...args);
	process.stderr.write(COMMONS_COLOR_RESET);
}

export function commonsOutputStarting(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_WHITE}${COMMONS_COLOR_BRIGHT}`);
	console.error(...args);
	process.stderr.write(COMMONS_COLOR_RESET);
}

export function commonsOutputCompleted(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_GREEN}${COMMONS_COLOR_BRIGHT}`);
	console.error(...args);
	process.stderr.write(COMMONS_COLOR_RESET);
}

export function commonsOutputSetDebugging(state: boolean): void {
	global['COMMONS_OUTPUT_IS_DEBUGGING'] = state;
}

export function commonsOutputSetDaemon(state: boolean): void {
	global['COMMONS_OUTPUT_IS_DAEMON'] = state;
}

export function commonsOutputDebug(...args: TAcceptableValues[]): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	if (!global['COMMONS_OUTPUT_IS_DEBUGGING']) return;
	
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_WHITE}${COMMONS_COLOR_DIM}`);
	console.error(`[${commonsDateDateToYmdHis(new Date())}] `, ...args);
	process.stderr.write(COMMONS_COLOR_RESET);
}

export function commonsOutputProgress(progress?: string|number|boolean): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] === undefined) return;
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}

	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);
	
	process.stderr.write(`${global['COMMONS_OUTPUT_DOING_MESSAGE'] as string} ...`);
	if (progress !== undefined) process.stderr.write(` ${progress.toString()}`);
}

export function commonsOutputDoing(message: string): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	global['COMMONS_OUTPUT_DOING_MESSAGE'] = message;
	commonsOutputProgress();
}

export function commonsOutputPercent(i: number, length: number): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	const percent: string = Math.round((i / length) * 100).toString();
	commonsOutputProgress(`${percent}%`);
}

export function commonsOutputFound(tally: number, found: number): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	commonsOutputProgress(`${tally}: ${found}`);
}

export function commonsOutputSuccess(outcome?: TAcceptableValues): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (outcome === undefined) {
		commonsOutputSuccess('done');
		return;
	}
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	
	commonsOutputProgress(`${COMMONS_COLOR_FOREGROUND_GREEN}${COMMONS_COLOR_BRIGHT}${outcome.toString()}${COMMONS_COLOR_RESET}`);
	process.stderr.write('\n');
	
	global['COMMONS_OUTPUT_DOING_MESSAGE'] = undefined;
}

export function commonsOutputResult(result: TAcceptableValues, outcome?: TAcceptableValues): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	if (outcome === undefined) {
		commonsOutputResult(result, 'done');
		return;
	}
	
	commonsOutputSuccess(`${outcome.toString()} (${result.toString()})`);
}

export function commonsOutputFail(outcome?: string): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_POLLING']) {
		global['COMMONS_OUTPUT_POLLING'] = false;
		process.stderr.write('\n');
	}
	if (outcome === undefined) {
		commonsOutputFail('failed');
		return;
	}
	
	commonsOutputProgress(`${COMMONS_COLOR_FOREGROUND_RED}${COMMONS_COLOR_BRIGHT}${outcome}${COMMONS_COLOR_RESET}`);
	process.stderr.write('\n');
	
	global['COMMONS_OUTPUT_DOING_MESSAGE'] = undefined;
}

export function commonsOutputPoll(status: string): void {
	if (global['COMMONS_OUTPUT_IS_DAEMON']) return;
	
	if (global['COMMONS_OUTPUT_DOING_MESSAGE'] !== undefined) process.stderr.write('\n');
	
	readline.clearLine(process.stderr, 0);
	readline.cursorTo(process.stderr, 0, undefined);

	process.stderr.write(`${COMMONS_COLOR_FOREGROUND_WHITE}${COMMONS_COLOR_DIM}${status}${COMMONS_COLOR_RESET}`);
	
	global['COMMONS_OUTPUT_POLLING'] = true;
}

export function commonsOutputDie(message: string, error: boolean = true, exitCode: number = 1): never {
	if (error) commonsOutputError(message);
	else commonsOutputAlert(message);
	
	process.exit(exitCode);
}

// hides the process.exit so that if (true as unknown) isn't constantly required else where
export function commonsHiddenExit(...args: TAcceptableValues[]): void {
	if (args && args.length > 0) commonsOutputDebug(...args);
	if (true as unknown) process.exit(1);
}
