import { ICommonsFirstClass } from 'tscommons-es-models';
import { ICommonsAdamantineManagedSecondClassUser, isICommonsAdamantineManagedSecondClassUser } from 'tscommons-es-models-adamantine';

import { ICommonsUserPwHash, isICommonsUserPwHash } from 'nodecommons-es-security';

export interface ICommonsAdamantineManagedSecondClassUserWithPwHash<
		P extends ICommonsFirstClass
> extends ICommonsAdamantineManagedSecondClassUser<P>, ICommonsUserPwHash {}

export function isICommonsAdamantineManagedSecondClassUserWithPwHash<
		P extends ICommonsFirstClass
>(
		test: unknown,
		firstClassField: string
): test is ICommonsAdamantineManagedSecondClassUserWithPwHash<P> {
	if (!isICommonsUserPwHash(test)) return false;
	if (!isICommonsAdamantineManagedSecondClassUser<P>(test, firstClassField)) return false;

	return true;
}
