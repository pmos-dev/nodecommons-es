import { ICommonsAdamantineManagedFirstClassUser, isICommonsAdamantineManagedFirstClassUser } from 'tscommons-es-models-adamantine';

import { ICommonsUserPwHash, isICommonsUserPwHash } from 'nodecommons-es-security';

export interface ICommonsAdamantineManagedFirstClassUserWithPwHash extends ICommonsAdamantineManagedFirstClassUser, ICommonsUserPwHash {}

export function isICommonsAdamantineManagedFirstClassUserWithPwHash(
		test: unknown
): test is ICommonsAdamantineManagedFirstClassUserWithPwHash {
	if (!isICommonsUserPwHash(test)) return false;
	if (!isICommonsAdamantineManagedFirstClassUser(test)) return false;
	
	return true;
}
