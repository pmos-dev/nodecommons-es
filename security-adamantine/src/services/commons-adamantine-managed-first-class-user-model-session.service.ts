import { CommonsUserPwHashSessionService } from 'nodecommons-es-security';
import { CommonsManagedFirstClass } from 'nodecommons-es-models-adamantine';
import { CommonsManagedFirstClassUserModel } from 'nodecommons-es-models-adamantine';

import { ICommonsAdamantineManagedFirstClassUserWithPwHash } from '../interfaces/icommons-adamantine-managed-first-class-user-with-pw-hash';

export class CommonsAdamantineManagedFirstClassUserSessionService<
		M extends ICommonsAdamantineManagedFirstClassUserWithPwHash
> extends CommonsUserPwHashSessionService<M> {
	constructor(
			_model: CommonsManagedFirstClass<M> & CommonsManagedFirstClassUserModel<M>,
			pepper?: string,
			allowMultipleSessionsForSameUser: boolean = false
	) {
		super(pepper, allowMultipleSessionsForSameUser);
	}
	
	protected override isEqual(a: M, b: M): boolean {
		if (!super.isEqual(a, b)) return false;
		
		if (a.id !== b.id) return false;
		
		return true;
	}
}
