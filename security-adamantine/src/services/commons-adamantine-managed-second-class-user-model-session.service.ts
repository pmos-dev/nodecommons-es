import { ICommonsFirstClass } from 'tscommons-es-models';

import { CommonsUserPwHashSessionService } from 'nodecommons-es-security';
import { CommonsManagedSecondClass } from 'nodecommons-es-models-adamantine';
import { CommonsManagedSecondClassUserModel } from 'nodecommons-es-models-adamantine';

import { ICommonsAdamantineManagedSecondClassUserWithPwHash } from '../interfaces/icommons-adamantine-managed-second-class-user-with-pw-hash';

export class CommonsAdamantineManagedSecondClassUserSessionService<
		M extends ICommonsAdamantineManagedSecondClassUserWithPwHash<P>,
		P extends ICommonsFirstClass
> extends CommonsUserPwHashSessionService<M> {
	constructor(
			private model: CommonsManagedSecondClass<M, P> & CommonsManagedSecondClassUserModel<M, P>,
			pepper?: string,
			allowMultipleSessionsForSameUser: boolean = false
	) {
		super(pepper, allowMultipleSessionsForSameUser);
	}
	
	protected override isEqual(a: M, b: M): boolean {
		if (!super.isEqual(a, b)) return false;
		
		if (a.id !== b.id) return false;
		
		const firstClassField: string = this.model.getFirstClassField();
		if (a[firstClassField] !== b[firstClassField]) return false;
		
		return true;
	}
}
