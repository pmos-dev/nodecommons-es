import * as tfcore from '@tensorflow/tfjs-core';

import { TCommonsAiEpochTrainingStats, TCommonsAiGenericTrainingData, TCommonsAiQuality } from 'tscommons-es-ai';

import { assertSet } from 'nodecommons-es-app';
import { commonsOutputDebug } from 'nodecommons-es-cli';
import { commonsFileExists } from 'nodecommons-es-file';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { CommonsAiTensorflowSequentialOnehotEnumsTraining, CommonsAiTensorflowSimpleEarlyStopping } from 'nodecommons-es-ai-tensorflow';
import { asyncConvertJimpToTensor3D } from 'nodecommons-es-ai-tensorflow-graphics';

import { CommonsAiTrainingTrainer } from './trainer';

export abstract class CommonsAiTrainingClassifierTrainer<E extends string> extends CommonsAiTensorflowSequentialOnehotEnumsTraining<
		CommonsJimp,
		E,
		tfcore.Rank.R3
> implements CommonsAiTrainingTrainer<CommonsJimp, E> {
	constructor(
			values: E[],
			private shape: [ number, number, 1|3 ],
			training: Map<E, CommonsJimp[]>,
			private _epochs: number,
			validation?: Map<E, CommonsJimp[]>,
			learningRate: number = 0.001,
			private blockPeriod: number = 5,
			private reductionFactor: number = 1.5,
			batchSize: number = 16,
			private noise?: number,
			private optimised: boolean = false
	) {
		super(
				values,
				training,
				_epochs,
				validation,
				new CommonsAiTensorflowSimpleEarlyStopping({
						maximumRelativeEpochs: blockPeriod
				}),
				true,
				'categoricalCrossentropy',
				tfcore.train.adam(learningRate),
				[ 'accuracy' ],
				true,
				batchSize
		);
	}

	private internalK: number|undefined;
	public get k(): number | undefined {
		return this.internalK;
	}
	public set k(k: number | undefined) {
		this.internalK = k;
	}

	private exportCallback: ((
			k: number,
			exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
	) => Promise<void>)|undefined;
	public set doExportBuiltCallback(callback: (
			k: number,
			exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
	) => Promise<void>) {
		this.exportCallback = callback;
	}

	public getOptimizer(): tfcore.AdamOptimizer {
		return this.optimizer as tfcore.AdamOptimizer;
	}

	protected override async asyncConvertInputToX(input: CommonsJimp): Promise<tfcore.Tensor3D> {
		return await asyncConvertJimpToTensor3D(
				input,
				this.shape,
				this.noise,
				this.optimised
		);
	}

	public override async train(): Promise<TCommonsAiEpochTrainingStats> {
		if (!this.model) throw new Error('Model has not been initialised');

		if (this.exportCallback) {
			const k: number|undefined = this.k;
			if (k === undefined) throw new Error('k has not been set for trainer model');

			const exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]|undefined = this.exportBuiltTrainingAndValidationXyTensors();
			if (!exported) throw new Error('Tensors cannot be exported yet');

			await this.exportCallback(
					k,
					exported
			);

			return {
					epochs: -1
			};
		}

		let lastStats: TCommonsAiEpochTrainingStats|undefined;

		for (let i = 1; i <= this._epochs; i += this.blockPeriod) {	// eslint-disable-line no-underscore-dangle
			commonsOutputDebug(`Running for epoch ${i}`);

			(this.earlyStopping! as CommonsAiTensorflowSimpleEarlyStopping).setInitialEpoch(i);
			
			if (i === 1) {
				lastStats = await super.train();
			} else {
				lastStats = await super.resume();
			}

			const typecast: { learningRate: number } = this.getOptimizer() as unknown as { learningRate: number };

			const existing: number = typecast.learningRate;
			const updated: number = existing / this.reductionFactor;

			typecast.learningRate = updated;

			commonsOutputDebug(`Decreasing learning rate by factor of ${this.reductionFactor} from ${existing} to ${updated}`);
		}
		if (!lastStats) throw new Error('No last stats');

		return lastStats;
	}

	public override async test(testData: TCommonsAiGenericTrainingData<CommonsJimp, E>[]): Promise<TCommonsAiQuality> {
		if (this.exportCallback) {
			// hasn't been built, so skip this
			return {
					positive: -1,
					negative: -1
			};
		}

		return await super.test(testData);
	}

	public async save(path: string): Promise<void> {
		assertSet(this.model, 'model');

		if (commonsFileExists(path)) throw new Error(`Path ${path} already exists. Cannot save into it.`);

		await this.model.save(`file://${path}`);
	}
}
