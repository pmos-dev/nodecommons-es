import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { TXy } from 'tscommons-es-graphics';

import { TCommonsAiEpochTrainingStats, TCommonsAiGenericTrainingData, TCommonsAiOutputWithCertainty, TCommonsAiQuality } from 'tscommons-es-ai';

import { assertSet } from 'nodecommons-es-app';
import { commonsFileExists } from 'nodecommons-es-file';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { CommonsAiTensorflowSequentialTraining, CommonsAiTensorflowSimpleEarlyStopping } from 'nodecommons-es-ai-tensorflow';
import { commonsOutputDebug } from 'nodecommons-es-cli';
import { asyncConvertJimpToTensor3D } from 'nodecommons-es-ai-tensorflow-graphics';

import { CommonsAiTrainingTrainer } from './trainer';

export abstract class CommonsAiTrainingXyTrainer extends CommonsAiTensorflowSequentialTraining<
		CommonsJimp,
		TXy,
		tfcore.Rank.R3,
		tfcore.Rank.R1
> implements CommonsAiTrainingTrainer<CommonsJimp, TXy> {
	constructor(
			private shape: [ number, number, 1|3 ],
			training: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[],
			private _epochs: number,
			validation?: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[],
			learningRate: number = 0.01,
			private blockPeriod: number = 5,
			private reductionFactor: number = 2,
			batchSize: number = 16,
			private noise?: number,
			private optimised: boolean = false
	) {
		super(
				training,
				_epochs,
				validation,
				new CommonsAiTensorflowSimpleEarlyStopping({
						maximumRelativeEpochs: blockPeriod
				}),
				true,
				false,
				'meanSquaredError',
				tfcore.train.adam(learningRate),
				[ tflayers.metrics.meanSquaredError ],
				true,
				batchSize
		);
	}

	private internalK: number|undefined;
	public get k(): number | undefined {
		return this.internalK;
	}
	public set k(k: number | undefined) {
		this.internalK = k;
	}

	private exportCallback: ((
			k: number,
			exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
	) => Promise<void>)|undefined;
	public set doExportBuiltCallback(callback: (
			k: number,
			exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
	) => Promise<void>) {
		this.exportCallback = callback;
	}

	public getOptimizer(): tfcore.AdamOptimizer {
		return this.optimizer as tfcore.AdamOptimizer;
	}

	protected abstract addInnerLayers(inputShape: number[]): void;

	protected addLayers(inputShape: number[]): void {
		if (!this.model) throw new Error('Model has not bee initialised');

		this.addInnerLayers(inputShape);

		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: 2,
				activation: 'sigmoid'
		}));
	}

	protected override async asyncConvertInputToX(input: CommonsJimp): Promise<tfcore.Tensor3D> {
		return await asyncConvertJimpToTensor3D(
				input,
				this.shape,
				this.noise,
				this.optimised
		);
	}

	protected override async asyncBuildExampleToXY(example: TCommonsAiGenericTrainingData<CommonsJimp, TXy>): Promise<[tfcore.Tensor3D, tfcore.Tensor1D]> {
		const xs: tfcore.Tensor3D = await this.asyncConvertInputToX(example.input);

		return [
				xs,
				tfcore.tensor1d([ example.output.x, example.output.y ])
		];
	}

	protected override convertPredictionToOutput(prediction: tfcore.Tensor1D): TCommonsAiOutputWithCertainty<TXy>|undefined {
		const sync: Float32Array = prediction.dataSync() as Float32Array;

		return {
				output: { x: sync[0], y: sync[1] },
				certainty: -1
		};
	}

	protected calculateQuality(actual: TCommonsAiOutputWithCertainty<TXy>, expected: TXy): [ number, number ] {
		const dx: number = Math.abs(actual.output.x - expected.x);
		const dy: number = Math.abs(actual.output.y - expected.y);
		const pyth: number = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

		return [ Math.max(0, 1 - pyth), 0 ];
	}

	public override async train(): Promise<TCommonsAiEpochTrainingStats> {
		if (!this.model) throw new Error('Model has not been initialised');

		if (this.exportCallback) {
			const k: number|undefined = this.k;
			if (k === undefined) throw new Error('k has not been set for trainer model');

			const exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]|undefined = this.exportBuiltTrainingAndValidationXyTensors();
			if (!exported) throw new Error('Tensors cannot be exported yet');

			await this.exportCallback(
					k,
					exported
			);

			return {
					epochs: -1
			};
		}

		let lastStats: TCommonsAiEpochTrainingStats|undefined;

		for (let i = 1; i <= this._epochs; i += this.blockPeriod) {	// eslint-disable-line no-underscore-dangle
			commonsOutputDebug(`Running for epoch ${i}`);

			(this.earlyStopping! as CommonsAiTensorflowSimpleEarlyStopping).setInitialEpoch(i);
			
			if (i === 1) {
				lastStats = await super.train();
			} else {
				lastStats = await super.resume();
			}

			const typecast: { learningRate: number } = this.getOptimizer() as unknown as { learningRate: number };

			const existing: number = typecast.learningRate;
			const updated: number = existing / this.reductionFactor;

			typecast.learningRate = updated;

			commonsOutputDebug(`Decreasing learning rate by factor of ${this.reductionFactor} from ${existing} to ${updated}`);
		}
		if (!lastStats) throw new Error('No last stats');

		return lastStats;
	}

	public override async test(testData: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[]): Promise<TCommonsAiQuality> {
		if (this.exportCallback) {
			// hasn't been built, so skip this
			return {
					positive: -1,
					negative: -1
			};
		}

		return await super.test(testData);
	}

	public async save(path: string): Promise<void> {
		assertSet(this.model, 'model');

		if (commonsFileExists(path)) throw new Error(`Path ${path} already exists. Cannot save into it.`);

		await this.model.save(`file://${path}`);
	}
}
