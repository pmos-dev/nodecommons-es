// this is a true interface

import * as tfcore from '@tensorflow/tfjs-core';

import { ICommonsAiModelTrainable, TCommonsAiEpochTrainingStats } from 'tscommons-es-ai';

import { ICommonsAiCrossTrainKable } from 'nodecommons-es-ai';
import { ICommonsAiTensorflowExportable } from 'nodecommons-es-ai-tensorflow';

export interface CommonsAiTrainingTrainer<
		InputT,
		OutputT
> extends ICommonsAiModelTrainable<InputT, OutputT, TCommonsAiEpochTrainingStats>, ICommonsAiTensorflowExportable, ICommonsAiCrossTrainKable {
	save(path: string): Promise<void>;
	getOptimizer(): tfcore.AdamOptimizer;

	set doExportBuiltCallback(callback: (
			k: number,
			exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
	) => Promise<void>);
}
