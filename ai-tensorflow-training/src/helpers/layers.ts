import * as tflayers from '@tensorflow/tfjs-layers';

import { TCommonsAiTrainingLayerSpec } from '../types/tlayer-spec';

import { ECommonsAiTrainingLayer } from '../enums/elayer';

export function commonsAiTrainingBuildLayers(
		inputShape: number[],
		specs: TCommonsAiTrainingLayerSpec[]
): tflayers.layers.Layer[] {
	let activeInputShape: number[]|undefined = inputShape;

	return specs
			.map((spec: TCommonsAiTrainingLayerSpec): tflayers.layers.Layer => {
				if (activeInputShape) {
					spec.args.inputShape = activeInputShape;
					activeInputShape = undefined;
				}

				switch (spec.type) {
					case ECommonsAiTrainingLayer.DENSE:
						return tflayers.layers.dense(spec.args);
					case ECommonsAiTrainingLayer.CONV1D:
						return tflayers.layers.conv1d(spec.args);
					case ECommonsAiTrainingLayer.CONV2D:
						return tflayers.layers.conv2d(spec.args);
					case ECommonsAiTrainingLayer.CONV2DTRANSPOSE:
						return tflayers.layers.conv2dTranspose(spec.args);
					case ECommonsAiTrainingLayer.CONV3D:
						return tflayers.layers.conv3d(spec.args);
					case ECommonsAiTrainingLayer.CONV3DTRANSPOSE:
						return tflayers.layers.conv3dTranspose(spec.args);
					case ECommonsAiTrainingLayer.DROPOUT:
						return tflayers.layers.dropout(spec.args);
					case ECommonsAiTrainingLayer.FLATTEN:
						return tflayers.layers.flatten(spec.args);
					case ECommonsAiTrainingLayer.RESHAPE:
						return tflayers.layers.reshape(spec.args);
					case ECommonsAiTrainingLayer.MAXPOOLING2D:
						return tflayers.layers.maxPooling2d(spec.args);
					case ECommonsAiTrainingLayer.AVGPOOLING2D:
						return tflayers.layers.avgPooling2d(spec.args);
					case ECommonsAiTrainingLayer.BATCHNORMALIZATION:
						return tflayers.layers.batchNormalization(spec.args);
				}

				throw new Error('Unknown layer');
			});
}
