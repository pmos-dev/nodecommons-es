import { commonsTypeHasPropertyNumber, commonsTypeIsTKeyObject } from 'tscommons-es-core';
import { TBoundary } from 'tscommons-es-graphics';

function isTBoundary(test: unknown): test is TBoundary {
	if (!commonsTypeHasPropertyNumber(test, 'x')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'y')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'width')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'height')) return false;

	return true;
}

export type TCommonsAiTrainingBoundaryDb = {
		[ file: string ]: TBoundary;
};
export function isTCommonsAiTrainingBoundaryDb(test: unknown): test is TCommonsAiTrainingBoundaryDb {
	return commonsTypeIsTKeyObject<TBoundary>(test, isTBoundary);
}
