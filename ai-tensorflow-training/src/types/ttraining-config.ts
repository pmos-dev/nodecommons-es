import { commonsTypeHasPropertyNumberOrUndefined, commonsTypeHasPropertyString } from 'tscommons-es-core';
import { commonsTypeIsTKeyObject, TKeyObject } from 'tscommons-es-core';

export type TCommonsAiTrainingSetConfig = {
		poolPath: string;
		ignoredsFile: string;
		noise?: number;
};
export function isTCommonsAiTrainingSetConfig(test: unknown): test is TCommonsAiTrainingSetConfig {
	if (!commonsTypeHasPropertyString(test, 'poolPath')) return false;
	if (!commonsTypeHasPropertyString(test, 'ignoredsFile')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'noise')) return false;

	return true;
}

export type TCommonsAiTrainingConfig = TKeyObject<TCommonsAiTrainingSetConfig>;
export function isTCommonsAiTrainingConfig(test: unknown): test is TCommonsAiTrainingConfig {
	if (!commonsTypeIsTKeyObject<TCommonsAiTrainingSetConfig>(test, isTCommonsAiTrainingSetConfig)) return false;
	
	return true;
}

export type TCommonsAiTrainingClassifiedSetConfig = TCommonsAiTrainingSetConfig & {
		classifiedPath: string;
};
export function isTCommonsAiTrainingClassifiedSetConfig(test: unknown): test is TCommonsAiTrainingClassifiedSetConfig {
	if (!isTCommonsAiTrainingSetConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'classifiedPath')) return false;

	return true;
}

export type TCommonsAiTrainingXySetConfig = TCommonsAiTrainingSetConfig & {
		xyDbFile: string;
};
export function isTCommonsAiTrainingXySetConfig(test: unknown): test is TCommonsAiTrainingXySetConfig {
	if (!isTCommonsAiTrainingSetConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'xyDbFile')) return false;

	return true;
}

export type TCommonsAiTrainingBoundarySetConfig = TCommonsAiTrainingSetConfig & {
		boundaryDbFile: string;
};
export function isTCommonsAiTrainingBoundarySetConfig(test: unknown): test is TCommonsAiTrainingBoundarySetConfig {
	if (!isTCommonsAiTrainingSetConfig(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'boundaryDbFile')) return false;

	return true;
}
