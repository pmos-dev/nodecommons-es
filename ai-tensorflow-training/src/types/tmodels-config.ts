import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TCommonsAiTrainingModelsConfig = {
		path: string;
};

export function isTCommonsAiTrainingModelsConfig(test: unknown): test is TCommonsAiTrainingModelsConfig {
	if (!commonsTypeHasPropertyString(test, 'path')) return false;
	
	return true;
}
