import { TXy } from 'tscommons-es-graphics';

import { TCommonsAiEOrUnclassified, TCommonsAiPrediction } from 'tscommons-es-ai';

import { TCommonsAiTrainingImage } from './timage';

export type TCommonsAiPredictionWithImage<T> = TCommonsAiPrediction<T> & {
		image: TCommonsAiTrainingImage;
};

export type TCommonsAiClassifiedPredictionWithImage<E extends string> = TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>;

export type TCommonsAiXyPredictionWithImage = TCommonsAiPredictionWithImage<TXy>;
