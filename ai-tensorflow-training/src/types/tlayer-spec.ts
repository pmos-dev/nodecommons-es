import { DenseLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/core';
import { DropoutLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/core';
import { FlattenLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/core';
import { ReshapeLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/core';
import { ConvLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/convolutional';
import { Pooling2DLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/pooling';
import { BatchNormalizationLayerArgs } from '@tensorflow/tfjs-layers/dist/layers/normalization';

import { ECommonsAiTrainingLayer } from '../enums/elayer';

type TGenericLayer = {
		type: ECommonsAiTrainingLayer;
};

type TDenseLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.DENSE;
		args: DenseLayerArgs;
};

type TConv1DLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.CONV1D;
		args: ConvLayerArgs;
};

type TConv2DLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.CONV2D;
		args: ConvLayerArgs;
};

type TConv2DTransposeLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.CONV2DTRANSPOSE;
		args: ConvLayerArgs;
};

type TConv3DLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.CONV3D;
		args: ConvLayerArgs;
};

type TConv3DTransposeLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.CONV3DTRANSPOSE;
		args: ConvLayerArgs;
};

type TDropoutLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.DROPOUT;
		args: DropoutLayerArgs;
};

type TFlattenLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.FLATTEN;
		args: FlattenLayerArgs;
};

type TReshapeLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.RESHAPE;
		args: ReshapeLayerArgs;
};

type TMaxPooling2DLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.MAXPOOLING2D;
		args: Pooling2DLayerArgs;
};

type TAvgPooling2DLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.AVGPOOLING2D;
		args: Pooling2DLayerArgs;
};

type TBatchNormalizationLayer = TGenericLayer & {
		type: ECommonsAiTrainingLayer.BATCHNORMALIZATION;
		args: BatchNormalizationLayerArgs;
};

export type TCommonsAiTrainingLayerSpec = TDenseLayer
		| TConv1DLayer
		| TConv2DLayer | TConv2DTransposeLayer
		| TConv3DLayer | TConv3DTransposeLayer
		| TDropoutLayer
		| TFlattenLayer
		| TReshapeLayer
		| TMaxPooling2DLayer | TAvgPooling2DLayer
		| TBatchNormalizationLayer;
