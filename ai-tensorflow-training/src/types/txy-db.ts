import { commonsTypeHasPropertyNumber, commonsTypeIsTKeyObject } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';

function isTXy(test: unknown): test is TXy {
	if (!commonsTypeHasPropertyNumber(test, 'x')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'y')) return false;

	return true;
}

export type TCommonsAiTrainingXyDb = {
		[ file: string ]: TXy;
};
export function isTCommonsAiTrainingXyDb(test: unknown): test is TCommonsAiTrainingXyDb {
	return commonsTypeIsTKeyObject<TXy>(test, isTXy);
}
