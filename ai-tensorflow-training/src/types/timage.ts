import { CommonsJimp } from 'nodecommons-es-graphics';

export type TCommonsAiTrainingImage = {
		file: string;
		image: CommonsJimp;
};
