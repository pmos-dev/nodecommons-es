import * as express from 'express';

import * as tfcore from '@tensorflow/tfjs-core';

import { commonsMapToObject, commonsTypeAttemptNumber } from 'tscommons-es-core';
import { commonsArrayRemove, commonsArrayShuffle } from 'tscommons-es-core';
import {
		TCommonsAiFileAndPrediction,
		ECommonsAiUnclassified,
		TCommonsAiClassifierStats,
		TCommonsAiPrediction,
		TCommonsAiEOrUnclassified,
		TCommonsAiCrossValidationK,
		TCommonsAiGenericTrainingData,
		TCommonsAiModelWithQuality,
		TCommonsAiOutputWithCertainty,
		commonsAiCrossValidationBuildEnumKs,
		TCommonsAiEpochTrainingStats
} from 'tscommons-es-ai';

import { commonsOutputDebug, commonsOutputDie, commonsOutputDoing, commonsOutputProgress, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { commonsFileExists, commonsFileMoveAsync } from 'nodecommons-es-file';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { commonsAiCrossValidationTrain, ICommonsAiEarlyStopping } from 'nodecommons-es-ai';
import { assertSet } from 'nodecommons-es-app';
import { CommonsAiJimpClassifier } from 'nodecommons-es-ai-tensorflow-graphics';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsAiTensorflowPythonBridge } from 'nodecommons-es-ai-tensorflow';

import { CommonsAiTrainingClassifyApi } from '../apis/classify.api';

import { CommonsAiTrainingClassifierTrainer } from '../classes/classifier-trainer';

import { TCommonsAiTrainingImage } from '../types/timage';
import { TCommonsAiPredictionWithImage } from '../types/tprediction-with-image';

import { CommonsAiTrainingApp } from './training.app';

function unflipData<E extends string>(
		values: E[],
		data: TCommonsAiGenericTrainingData<CommonsJimp, E>[]
): Map<E, CommonsJimp[]> {
	const map: Map<E, CommonsJimp[]> = new Map<E, CommonsJimp[]>();
	for (const value of values) map.set(value, []);

	for (const d of data) {
		map.get(d.output)!.push(d.input);
	}

	return map;
}

export abstract class CommonsAiTrainingClassifierApp<
		E extends string,
		TrainerT extends CommonsAiTrainingClassifierTrainer<E> = CommonsAiTrainingClassifierTrainer<E>,
		DetectorT extends CommonsAiJimpClassifier<E> = CommonsAiJimpClassifier<E>
> extends CommonsAiTrainingApp<
		E,
		TCommonsAiClassifierStats<E>,
		TrainerT,
		DetectorT
> {
	protected classifieds: Map<E, TCommonsAiTrainingImage[]> = new Map<E, TCommonsAiTrainingImage[]>();

	protected poolPath: string|undefined;	// this has to be set so we can do the save file move
	protected classifiedPath: string|undefined;

	constructor(
			name: string,
			private values: E[],
			private constructClassifierCallback: (
					training: Map<E, CommonsJimp[]>,
					validation: Map<E, CommonsJimp[]>
			) => TrainerT,
			private constructClassifyApiCallback: (
					restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>,
					restPath: string,
					predict: () => Promise<Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>>>,
					listRandoms: (limit: number) => Promise<TCommonsAiFileAndPrediction<E>[]>,
					getImage: (file: string) => TCommonsAiTrainingImage|undefined,
					setImage: (file: string, value: E) => boolean,
					ignore: (file: string) => void,
					classifierTrain: () => Promise<number|undefined>,
					classifierSave: () => Promise<void>,
					getStats: () => TCommonsAiClassifierStats<E>
			) => CommonsAiTrainingClassifyApi<E>,
			private crossTrainInterations: number = 4,
			private maxValidationRatio: number = 0.2,
			private testingRatio: number = 0.15,
			private appendExtension?: string
	) {
		super(name);

		for (const value of this.values) this.classifieds.set(value, []);
	}

	protected abstract listClassifiedFiles(value: E): string[];
	protected abstract loadClassifiedFile(value: E, file: string): Promise<CommonsJimp>;

	protected override async init(): Promise<void> {
		this.install(
				(restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>, path: string): void => {
					const api: CommonsAiTrainingClassifyApi<E> = this.constructClassifyApiCallback(
							restServer,
							path,
							(): Promise<Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>>> => this.listPredictions(),
							(limit: number): Promise<TCommonsAiFileAndPrediction<E>[]> => this.listRandoms(limit),
							(file: string): TCommonsAiTrainingImage|undefined => this.getImage(file),
							(file: string, value: E): boolean => this.setImage(file, value, true),
							(file: string): void => this.ignore(file),
							(): Promise<number|undefined> => this.train(),
							(): Promise<void> => this.save(),
							(): TCommonsAiClassifierStats<E> => this.getStats()
					);
					api.installImageHandler(this.getHttpServer());
				}
		);

		await super.init();

		commonsOutputDoing('Loading already classified');

		for (const value of this.values) {
			const files: string[] = this.listClassifiedFiles(value);
			commonsArrayShuffle(files);

			for (const file of files) {
				commonsOutputProgress(`${value}: ${this.classifieds.get(value)!.length}`);
	
				const jimp: CommonsJimp = await this.loadClassifiedFile(value, file);
	
				this.classifieds.get(value)!.push({
						file: file,
						image: jimp
				});
			}
		}

		commonsOutputSuccess();
	}

	protected hasEnoughToTrain(): boolean {
		for (const value of this.values) {
			if (this.classifieds.get(value)!.length === 0) return false;
		}
		return true;
	}

	protected async retrain(): Promise<void> {
		const images: Map<E, CommonsJimp[]> = new Map<E, CommonsJimp[]>();
		for (const value of this.values) {
			images.set(
					value,
					(this.classifieds.get(value) || [])
							.map((image: TCommonsAiTrainingImage): CommonsJimp => image.image)
			);
		}

		const fixClassifiedsLengthParam: string|undefined = this.getArgs().getStringOrUndefined('fix-classifieds-length');
		const minClassifiedsLengthParam: number|undefined = this.getArgs().getNumberOrUndefined('min-classifieds-length');

		if (fixClassifiedsLengthParam && this.getArgs().hasAttribute('pre-test')) {
			let fixClassifiedsLength: number|undefined;
			if (fixClassifiedsLengthParam === 'max') {
				fixClassifiedsLength = 0;
				for (const value of this.values) {
					const item: CommonsJimp[] = images.get(value)!;
					fixClassifiedsLength = Math.max(fixClassifiedsLength, item.length);
				}
			} else if (fixClassifiedsLengthParam) {
				fixClassifiedsLength = commonsTypeAttemptNumber(fixClassifiedsLengthParam);
				if (!fixClassifiedsLength) commonsOutputDie('Fixed length cannot be zero or a non-number, other than "max"');
			}
			if (!fixClassifiedsLength) throw new Error('Unable to parse fixClassifiedsLengthParam');
	
			for (const value of this.values) {
				commonsOutputDebug(`Pre-test fixing length of ${value} from ${images.get(value)!.length} to ${fixClassifiedsLength}`);
				const item: CommonsJimp[] = images.get(value)!;

				if (item.length > fixClassifiedsLength) {
					commonsArrayShuffle(item);
					while (item.length > fixClassifiedsLength) item.pop();
				}

				while (item.length < fixClassifiedsLength) {
					const index: number = Math.floor(Math.random() * item.length);
					item.push(item[index]);
				}

				commonsOutputDebug(`Length is now ${images.get(value)!.length}`);
			}
		}
		if (minClassifiedsLengthParam && this.getArgs().hasAttribute('pre-test')) {
			for (const value of this.values) {
				if (images.get(value)!.length < minClassifiedsLengthParam) {
					commonsOutputDebug(`Pre-test exanding length of ${value} from ${images.get(value)!.length} to ${minClassifiedsLengthParam}`);
					const item: CommonsJimp[] = images.get(value)!;

					while (item.length < minClassifiedsLengthParam) {
						const index: number = Math.floor(Math.random() * item.length);
						item.push(item[index]);
					}

					commonsOutputDebug(`Length is now ${images.get(value)!.length}`);
				}
			}
		}

		const testing: TCommonsAiGenericTrainingData<CommonsJimp, E>[] = [];

		for (const value of this.values) {
			const qty: number = Math.floor(images.get(value)!.length * this.testingRatio);
			if (qty < 1) throw new Error('Not enough data to create test set');

			commonsArrayShuffle(images.get(value)!);
			testing.push(
					...images.get(value)!.splice(0, qty)
							.map((image: CommonsJimp): TCommonsAiGenericTrainingData<CommonsJimp, E> => ({
									input: image,
									output: value
							}))
			);
		}

		if (fixClassifiedsLengthParam && !this.getArgs().hasAttribute('pre-test')) {
			let fixClassifiedsLength: number|undefined;
			if (fixClassifiedsLengthParam === 'max') {
				fixClassifiedsLength = 0;
				for (const value of this.values) {
					const item: CommonsJimp[] = images.get(value)!;
					fixClassifiedsLength = Math.max(fixClassifiedsLength, item.length);
				}
			} else if (fixClassifiedsLengthParam) {
				fixClassifiedsLength = commonsTypeAttemptNumber(fixClassifiedsLengthParam);
				if (!fixClassifiedsLength) commonsOutputDie('Fixed length cannot be zero or a non-number, other than "max"');
			}
			if (!fixClassifiedsLength) throw new Error('Unable to parse fixClassifiedsLengthParam');

			for (const value of this.values) {
				commonsOutputDebug(`Post-test fixing length of ${value} from ${images.get(value)!.length} to ${fixClassifiedsLength}`);
				const item: CommonsJimp[] = images.get(value)!;

				if (item.length > fixClassifiedsLength) {
					commonsArrayShuffle(item);
					while (item.length > fixClassifiedsLength) item.pop();
				}

				while (item.length < fixClassifiedsLength) {
					const index: number = Math.floor(Math.random() * item.length);
					item.push(item[index]);
				}

				commonsOutputDebug(`Length is now ${images.get(value)!.length}`);
			}
		}
		if (minClassifiedsLengthParam && !this.getArgs().hasAttribute('pre-test')) {
			for (const value of this.values) {
				if (images.get(value)!.length < minClassifiedsLengthParam) {
					commonsOutputDebug(`Post-test exanding length of ${value} from ${images.get(value)!.length} to ${minClassifiedsLengthParam}`);
					const item: CommonsJimp[] = images.get(value)!;

					while (item.length < minClassifiedsLengthParam) {
						const index: number = Math.floor(Math.random() * item.length);
						item.push(item[index]);
					}

					commonsOutputDebug(`Length is now ${images.get(value)!.length}`);
				}
			}
		}

		const ks: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<CommonsJimp, E>>[] = commonsAiCrossValidationBuildEnumKs<CommonsJimp, E>(
				this.values,
				images,
				this.crossTrainInterations,
				this.maxValidationRatio
		);

		const best: TCommonsAiModelWithQuality<CommonsJimp, E, TrainerT> = await commonsAiCrossValidationTrain<CommonsJimp, E, TrainerT, TCommonsAiEpochTrainingStats>(
				ks,
				testing,
				async (
						training: TCommonsAiGenericTrainingData<CommonsJimp, E>[],
						_epochs: number,
						validation: TCommonsAiGenericTrainingData<CommonsJimp, E>[]|undefined,
						_earlyStopping: ICommonsAiEarlyStopping|undefined
				): Promise<TrainerT> => {
					const constructed: TrainerT = this.constructClassifierCallback(
							unflipData<E>(this.values, training),
							unflipData<E>(this.values, validation || [])
					);

					if (this.getArgs().hasAttribute('export-built')) {
						const path: string = this.getArgs().getString('export-path');
						const prefix: string = this.getArgs().getString('export-prefix');

						constructed.doExportBuiltCallback = async (
								k: number,
								exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
						): Promise<void> => {
							const exporter: CommonsAiTensorflowPythonBridge = new CommonsAiTensorflowPythonBridge(prefix, path);
							const promises: Promise<void>[] = [
									exporter.write(`k-${k}-training-xs`, exported[0]),
									exporter.write(`k-${k}-training-ys`, exported[1])
							];
							if (exported[2]) promises.push(exporter.write(`k-${k}-validation-xs`, exported[2]));
							if (exported[3]) promises.push(exporter.write(`k-${k}-validation-ys`, exported[3]));

							if (k === 1) {
								// also export the testing data at this time

								const testingTensors: [ tfcore.Tensor, tfcore.Tensor ] = await constructed.buildXYFromExamples(testing);
								promises.push(exporter.write('testing-xs', testingTensors[0]));
								promises.push(exporter.write('testing-ys', testingTensors[1]));
							}
				
							await Promise.all(promises);
						};
					}

					await constructed.init();

					return constructed;
				},
				-1,	// na
				undefined,
				this.dumpAttemptQualitiesCallback
		);

		if (this.trainer) this.trainer.dispose();
		this.trainer = best.model;
		this.detector = undefined;
	}

	protected async attemptPredict(image: CommonsJimp): Promise<TCommonsAiOutputWithCertainty<E>|undefined> {
		if (!this.trainer && !this.detector) return undefined;

		return (this.trainer || this.detector)!.predict(image);
	}

	private async listPredictions(): Promise<Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>>> {
		commonsOutputDoing('Generating prediction samples');

		const map: Map<TCommonsAiEOrUnclassified<E>, TCommonsAiPredictionWithImage<E>[]> = new Map<TCommonsAiEOrUnclassified<E>, TCommonsAiPredictionWithImage<E>[]>();
		for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ] ) map.set(value, []);

		if (this.trainer || this.detector) {
			const available: TCommonsAiTrainingImage[] = this.pool.slice();
			commonsArrayShuffle(available);

			for (const image of available) {
				const tallies: string[] = [];
				for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ]) tallies.push(`${value}[${map.get(value)!.length}]`);

				commonsOutputProgress(tallies.join(' '));

				const output: TCommonsAiOutputWithCertainty<E>|undefined = await (this.trainer || this.detector)!.predict(image.image);

				let prediction: TCommonsAiPrediction<TCommonsAiEOrUnclassified<E>>|undefined;
				if (output) {
					prediction = {
							prediction: output.output,
							certainty: output.certainty
					};
				}
				
				let ref: TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>[]|undefined;

				if (!prediction) {
					ref = map.get(ECommonsAiUnclassified.UNCLASSIFIED)!;
					prediction = {
							prediction: ECommonsAiUnclassified.UNCLASSIFIED,
							certainty: 1
					};
				} else {
					ref = map.get(prediction.prediction)!;
				}

				ref.push({
						...prediction,
						image: image
				});
			}
		}

		const sorteds: Map<TCommonsAiEOrUnclassified<E>, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>[]> = new Map<TCommonsAiEOrUnclassified<E>, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>[]>();

		for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ]) {
			const matches: TCommonsAiPredictionWithImage<E>[] = map.get(value)!
					.sort((a: TCommonsAiPredictionWithImage<E>, b: TCommonsAiPredictionWithImage<E>): number => {
						if (a.certainty < b.certainty) return -1;
						if (a.certainty > b.certainty) return 1;
						return 0;
					});
			
			sorteds.set(value, matches);
		}

		const polariseds: Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>> = new Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>>();

		for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ]) {
			const map2: Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined> = new Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>();

			if (sorteds.get(value)!.length === 0) {
				map2.set(true, undefined);
				map2.set(false, undefined);
			} else if (sorteds.get(value)!.length === 1) {
				map2.set(true, sorteds.get(value)![0]);
				map2.set(false, undefined);
			} else {
				map2.set(true, sorteds.get(value)![0]);
				map2.set(false, sorteds.get(value)![sorteds.get(value)!.length - 1]);
			}

			polariseds.set(value, map2);
		}

		commonsOutputSuccess();

		return polariseds;
	}

	protected setImage(file: string, value: E, allowOverwrite: boolean = true): boolean {
		commonsOutputDebug(`Marking ${file} as ${value}`);

		const match: TCommonsAiTrainingImage|undefined = this.getImage(file);
		
		if (!match) {	// not in pool, may be already classified
			if (!allowOverwrite) return false;

			commonsOutputDebug('Attempting overwrite if already set');

			for (const s of this.values) {
				const existing: TCommonsAiTrainingImage|undefined = this.classifieds.get(s)!
						.find((image: TCommonsAiTrainingImage): boolean => image.file === file);
				if (!existing) continue;

				commonsArrayRemove(
						this.classifieds.get(s)!,
						existing,
						(a: TCommonsAiTrainingImage, b: TCommonsAiTrainingImage): boolean => a.file === b.file
				);
				this.pool.push(existing);
			}

			return this.setImage(file, value, false);
		}
		
		this.classifieds.get(value)!.push(match);
		
		commonsArrayRemove(this.pool, match);

		return true;
	}

	protected getTrainingLoss(): number|undefined {
		if (!this.trainer) return undefined;

		return this.trainer.getTrainingStats().loss;
	}

	protected override async save(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		assertSet(this.poolPath, 'poolPath');
		assertSet(this.classifiedPath, 'classifiedPath');

		await super.save();

		commonsOutputDoing('Moving existing classified files out of pool');

		for (const value of this.values) {
			for (const image of this.classifieds.get(value)!) {
				let src: string = `${this.poolPath}/${image.file}`;
				if (this.appendExtension) src = src + `${this.appendExtension.startsWith('.') ? '' : '.'}${this.appendExtension}`;

				let dest: string = `${this.classifiedPath}/${value}/${image.file}`;
				if (this.appendExtension) dest = dest + `${this.appendExtension.startsWith('.') ? '' : '.'}${this.appendExtension}`;

				if (!commonsFileExists(src)) continue;	// doesn't exist for some reason
				if (commonsFileExists(dest)) continue;	// already moved

				commonsOutputDebug(`Moving ${image.file} to ${value}`);
				await commonsFileMoveAsync(
						src,
						dest
				);
			}
		}

		commonsOutputSuccess();
	}

	protected getStats(): TCommonsAiClassifierStats<E> {
		const map: Map<E, number> = new Map<E, number>();
		for (const value of this.values) map.set(value, this.classifieds.get(value)!.length);

		return {
				pool: this.pool.length,
				classified: commonsMapToObject<E, number>(map),
				bestLoss: (!this.trainer || this.trainer.getTrainingStats().loss === undefined) ? null : this.trainer.getTrainingStats().loss
		};
	}
}
