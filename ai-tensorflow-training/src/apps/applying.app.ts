import * as tfcore from '@tensorflow/tfjs-core';

import { commonsArrayShuffle } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsAiFileAndPrediction, TCommonsAiOutputWithCertainty, TCommonsAiPrediction } from 'tscommons-es-ai';

import { commonsOutputDoing, commonsOutputProgress, commonsOutputResult } from 'nodecommons-es-cli';
import { assertSet, CommonsApp } from 'nodecommons-es-app';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { CommonsAiTensorflowSequential } from 'nodecommons-es-ai-tensorflow';

import { TCommonsAiTrainingImage } from '../types/timage';

export abstract class CommonsAiApplyingApp<
		OutputT extends string|TEncodedObject|number,
		ModelT extends CommonsAiTensorflowSequential<
				CommonsJimp,
				OutputT,
				tfcore.Rank.R3,
				tfcore.Rank.R1
		>
> extends CommonsApp {
	protected pool: TCommonsAiTrainingImage[] = [];
	protected model: ModelT|undefined;
	protected poolPath: string|undefined;
	protected modelPath: string|undefined;

	protected abstract listPoolFiles(): string[];
	protected abstract loadPoolFile(file: string): Promise<CommonsJimp>;

	protected abstract loadModel(): Promise<ModelT>;

	protected override async init(): Promise<void> {
		assertSet(this.modelPath, 'modelPath');
		this.model = await this.loadModel();
		
		const poolLimit: number|undefined = this.getArgs().getNumberOrUndefined('limit');

		assertSet(this.poolPath, 'poolPath');

		commonsOutputDoing('Loading pool');

		const files: string[] = this.listPoolFiles();
		commonsArrayShuffle(files);

		for (const file of files) {
			commonsOutputProgress(this.pool.length);
			if (poolLimit !== undefined && this.pool.length > poolLimit) break;

			const jimp: CommonsJimp = await this.loadPoolFile(file);

			this.pool.push({
					file: file,
					image: jimp
			});
		}
		commonsOutputResult(this.pool.length);

		await super.init();
	}

	protected abstract handlePredictions(results: TCommonsAiFileAndPrediction<OutputT>[]): Promise<void>;

	protected async attemptPredict(image: CommonsJimp): Promise<TCommonsAiOutputWithCertainty<OutputT>|undefined> {
		assertSet(this.model, 'model');

		return await this.model.predict(image);
	}

	protected override async run(): Promise<void> {
		commonsOutputDoing('Generating predictions for pool');

		const results: TCommonsAiFileAndPrediction<OutputT>[] = [];
		let i: number = 0;
		for (const image of this.pool) {
			commonsOutputProgress(++i);

			let classification: TCommonsAiPrediction<OutputT>|undefined;

			const output: TCommonsAiOutputWithCertainty<OutputT>|undefined = await this.attemptPredict(image.image);
			if (output) {
				classification = {
						prediction: output.output,
						certainty: output.certainty
				};
			}

			results.push({
					file: image.file,
					prediction: classification ? classification : null
			});
		}

		commonsOutputResult(results.length);

		await this.handlePredictions(results);
	}
}
