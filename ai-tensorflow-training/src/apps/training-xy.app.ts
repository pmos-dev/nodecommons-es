import * as express from 'express';

import * as tfcore from '@tensorflow/tfjs-core';

import { commonsArrayRemoveAll, commonsTypeHasProperty } from 'tscommons-es-core';
import { commonsArrayShuffle, commonsArrayRemove } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';
import {
		TCommonsAiXyFileAndPrediction,
		commonsAiCrossValidationBuildKs,
		TCommonsAiCrossValidationK,
		TCommonsAiEpochTrainingStats,
		TCommonsAiGenericTrainingData,
		TCommonsAiModelWithQuality,
		TCommonsAiOutputWithCertainty,
		TCommonsAiXyStats
} from 'tscommons-es-ai';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputInfo, commonsOutputProgress, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { commonsFileReadJsonFileAsync, commonsFileWriteJsonFileAsync } from 'nodecommons-es-file';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { commonsAiCrossValidationTrain, ICommonsAiEarlyStopping } from 'nodecommons-es-ai';
import { assertSet } from 'nodecommons-es-app';
import { CommonsAiJimpXy } from 'nodecommons-es-ai-tensorflow-graphics';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsAiTensorflowPythonBridge } from 'nodecommons-es-ai-tensorflow';

import { CommonsAiTrainingXyApi } from '../apis/xy.api';

import { CommonsAiTrainingXyTrainer } from '../classes/xy-trainer';

import { TCommonsAiTrainingImage } from '../types/timage';
import { isTCommonsAiTrainingXyDb, TCommonsAiTrainingXyDb } from '../types/txy-db';

import { CommonsAiTrainingApp } from './training.app';

type TDone = TCommonsAiTrainingImage & {
		xy: TXy;
};

function clampXy(xy: TXy): void {
	xy.x = Math.max(0, Math.min(1, xy.x));
	xy.y = Math.max(0, Math.min(1, xy.y));
}

export abstract class CommonsAiTrainingXyApp<
		TrainerT extends CommonsAiTrainingXyTrainer = CommonsAiTrainingXyTrainer,
		DetectorT extends CommonsAiJimpXy = CommonsAiJimpXy
> extends CommonsAiTrainingApp<
		TXy,
		TCommonsAiXyStats,
		TrainerT,
		DetectorT
> {
	protected xyDbFile: string|undefined;

	protected done: TDone[] = [];
	protected xyDb: TCommonsAiTrainingXyDb = {};

	constructor(
			name: string,
			private constructXyCallback: (
					training: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[],
					validation: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[]
			) => TrainerT,
			private crossTrainInterations: number = 4,
			private maxValidationRatio: number = 0.2,
			private testingRatio: number = 0.15
	) {
		super(name);
	}

	protected override async init(): Promise<void> {
		this.install(
				(restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>, path: string): void => {
					const api: CommonsAiTrainingXyApi = new CommonsAiTrainingXyApi(
							restServer,
							path,
							(limit: number): Promise<TCommonsAiXyFileAndPrediction[]> => this.listRandoms(limit),
							(file: string): TCommonsAiTrainingImage|undefined => this.getImage(file),
							(file: string, value: TXy): boolean => this.setImage(file, value, true),
							(file: string): void => this.ignore(file),
							(): Promise<number|undefined> => this.train(),
							(): Promise<void> => this.save(),
							(): TCommonsAiXyStats => this.getStats()
					);
					api.installImageHandler(this.getHttpServer());
				}
		);

		await super.init();

		assertSet(this.xyDbFile, 'xyDbFile');

		const xyDb: unknown = await commonsFileReadJsonFileAsync(this.xyDbFile);
		if (!isTCommonsAiTrainingXyDb(xyDb)) throw new Error('Invalid XyDb');

		this.xyDb = xyDb;

		this.done = [];
		for (const item of this.pool) {
			if (commonsTypeHasProperty(this.xyDb, item.file)) {
				const xy: TXy = this.xyDb[item.file];
				clampXy(xy);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes

				this.done.push({
						...item,
						xy: { ...xy }
				});
			}
		}
		
		commonsArrayRemoveAll(
				this.pool,
				this.done,
				undefined,
				(a: TCommonsAiTrainingImage, b: TCommonsAiTrainingImage): boolean => a.file === b.file
		);

		commonsOutputDoing('Loading any non-pool from xydb');

		const files: string[] = Object.keys(this.xyDb);

		let i: number = 0;
		for (const file of files) {
			const existing: TDone|undefined = this.done
					.find((d: TDone): boolean => d.file === file);
			if (existing) continue;
			
			commonsOutputProgress(++i);

			const jimp: CommonsJimp = await this.loadPoolFile(file);

			const xy: TXy = this.xyDb[file];
			clampXy(xy);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes

			this.done.push({
					file: file,
					image: jimp,
					xy: { ...xy }
			});
		}

		commonsOutputSuccess();

		commonsOutputInfo(`${this.done.length} items have been done.`);
	}

	protected hasEnoughToTrain(): boolean {
		return this.done.length > 3;
	}

	protected async retrain(): Promise<void> {
		const images: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[] = this.done
				.map((done: TDone): TCommonsAiGenericTrainingData<CommonsJimp, TXy> => ({
						input: done.image,
						output: done.xy
				}));
		
		commonsArrayShuffle(images);

		const qty: number = Math.floor(images.length * this.testingRatio);
		if (qty < 1) throw new Error('Not enough data to create test set');

		const testing: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[] = images
				.splice(0, qty);

		const ks: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<CommonsJimp, TXy>>[] = commonsAiCrossValidationBuildKs<CommonsJimp, TXy>(
				images,
				this.crossTrainInterations,
				this.maxValidationRatio
		);

		const best: TCommonsAiModelWithQuality<CommonsJimp, TXy, TrainerT> = await commonsAiCrossValidationTrain<CommonsJimp, TXy, TrainerT, TCommonsAiEpochTrainingStats>(
				ks,
				testing,
				async (
						training: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[],
						_epochs: number,
						validation: TCommonsAiGenericTrainingData<CommonsJimp, TXy>[]|undefined,
						_earlyStopping: ICommonsAiEarlyStopping|undefined
				): Promise<TrainerT> => {
					const constructed: TrainerT = this.constructXyCallback(
							training,
							validation || []
					);

					if (this.getArgs().hasAttribute('export-built')) {
						const path: string = this.getArgs().getString('export-path');
						const prefix: string = this.getArgs().getString('export-prefix');

						constructed.doExportBuiltCallback = async (
								k: number,
								exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
						): Promise<void> => {
							const exporter: CommonsAiTensorflowPythonBridge = new CommonsAiTensorflowPythonBridge(prefix, path);
							const promises: Promise<void>[] = [
									exporter.write(`k-${k}-training-xs`, exported[0]),
									exporter.write(`k-${k}-training-ys`, exported[1])
							];
							if (exported[2]) promises.push(exporter.write(`k-${k}-validation-xs`, exported[2]));
							if (exported[3]) promises.push(exporter.write(`k-${k}-validation-ys`, exported[3]));
				
							if (k === 1) {
								// also export the testing data at this time

								const testingTensors: [ tfcore.Tensor, tfcore.Tensor ] = await constructed.buildXYFromExamples(testing);
								promises.push(exporter.write('testing-xs', testingTensors[0]));
								promises.push(exporter.write('testing-ys', testingTensors[1]));
							}

							await Promise.all(promises);
						};
					}

					await constructed.init();

					return constructed;
				},
				-1,	// na
				undefined,
				this.dumpAttemptQualitiesCallback
		);

		if (this.trainer) this.trainer.dispose();
		this.trainer = best.model;
		this.detector = undefined;
	}

	protected async attemptPredict(image: CommonsJimp): Promise<TCommonsAiOutputWithCertainty<TXy>|undefined> {
		if (!this.trainer && !this.detector) return undefined;

		const prediction: TCommonsAiOutputWithCertainty<TXy>|undefined = await (this.trainer || this.detector)!.predict(image);
		return prediction;
	}

	protected setImage(file: string, xy: TXy, allowOverwrite: boolean = true): boolean {
		clampXy(xy);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes

		commonsOutputDebug(`Marking ${file} as ${xy.x},${xy.y}`);

		const match: TCommonsAiTrainingImage|undefined = this.getImage(file);
		
		if (!match) {	// not in pool, may be already classified
			if (!allowOverwrite) return false;

			commonsOutputDebug('Attempting overwrite if already set');

			const match2: TDone|undefined = this.done
					.find((d: TDone): boolean => d.file === file);
			if (!match2) throw new Error('Image is not in pool or dones');

			match2.xy = { ...xy };
		} else {
			this.done.push({
					...match,
					xy: { ...xy }
			});
		}
		
		commonsArrayRemove(this.pool, match);

		return true;
	}

	protected getTrainingLoss(): number|undefined {
		if (!this.trainer) return undefined;

		return this.trainer.getTrainingStats().loss;
	}

	protected override async save(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		assertSet(this.xyDbFile, 'xyDbFile');

		await super.save();

		commonsOutputDoing('Updating XyDb');

		const xyDb: TCommonsAiTrainingXyDb = {};

		for (const d of this.done) {
			xyDb[d.file] = d.xy;
		}

		await commonsFileWriteJsonFileAsync(this.xyDbFile, xyDb);

		commonsOutputSuccess();
	}

	protected getStats(): TCommonsAiXyStats {
		return {
				pool: this.pool.length,
				done: this.done.length,
				bestLoss: (!this.trainer || this.trainer.getTrainingStats().loss === undefined) ? null : this.trainer.getTrainingStats().loss
		};
	}
}
