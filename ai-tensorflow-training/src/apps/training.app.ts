import * as fs from 'fs';

import * as express from 'express';

import * as tfcore from '@tensorflow/tfjs-core';

import { TPropertyObject, commonsArrayRemove, commonsArrayShuffle, commonsTypeIsStringArray } from 'tscommons-es-core';
import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsAiFileAndPrediction, TCommonsAiOutputWithCertainty, TCommonsAiPrediction } from 'tscommons-es-ai';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputInfo, commonsOutputProgress, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { assertSet } from 'nodecommons-es-app';
import { CommonsRestApp } from 'nodecommons-es-app-rest';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { commonsFileReadJsonFileAsync, commonsFileRm, commonsFileWriteJsonFileAsync } from 'nodecommons-es-file';
import { CommonsAiTensorflowSequential } from 'nodecommons-es-ai-tensorflow';
import { CommonsStrictExpressServer, ICommonsRequestWithStrictParams, commonsExpressBuildDefaultServer, isICommonsExpressConfig } from 'nodecommons-es-express';

import { CommonsAiTrainingTrainer } from '../classes/trainer';

import { TCommonsAiTrainingImage } from '../types/timage';

export abstract class CommonsAiTrainingApp<
		OutputT extends string|TEncodedObject|number,
		StatsT extends TEncodedObject,
		TrainerT extends CommonsAiTrainingTrainer<CommonsJimp, OutputT>,
		DetectorT extends CommonsAiTensorflowSequential<
				CommonsJimp,
				OutputT,
				tfcore.Rank.R3,
				tfcore.Rank.R1
		> = CommonsAiTensorflowSequential<
				CommonsJimp,
				OutputT,
				tfcore.Rank.R3,
				tfcore.Rank.R1
		>
> extends CommonsRestApp<
		ICommonsRequestWithStrictParams,
		express.Response,
		CommonsStrictExpressServer
> {
	protected pool: TCommonsAiTrainingImage[] = [];
	protected trainer: TrainerT|undefined;
	protected detector: DetectorT|undefined;
	protected modelPath: string|undefined;
	protected ignoredsFile: string|undefined;

	protected ignoreds: string[] = [];

	protected abstract listPoolFiles(): string[];
	protected abstract loadPoolFile(file: string): Promise<CommonsJimp>;

	protected dumpAttemptQualitiesCallback: ((qualities: number[]) => void)|undefined;

	constructor(
			name: string
	) {
		super(name, 'express');
	}

	protected override async init(): Promise<void> {
		assertSet(this.ignoredsFile, 'ignoreFile');

		const ignoreds: unknown = await commonsFileReadJsonFileAsync(this.ignoredsFile);
		if (!commonsTypeIsStringArray(ignoreds)) throw new Error('Invalid ignored');
		this.ignoreds = ignoreds;
		commonsOutputDebug(`Ignored list contains ${ignoreds.length} items`);

		const requirePrefix: string|undefined = this.getArgs().getStringOrUndefined('require-prefix');
		if (requirePrefix) commonsOutputDebug(`Prefix ${requirePrefix} is required for suggestions from pool`);

		if (!this.getArgs().hasAttribute('non-rest')) {
			const poolLimit: number|undefined = this.getArgs().getNumberOrUndefined('limit');

			commonsOutputDoing('Loading pool');

			const files: string[] = this.listPoolFiles();
			commonsArrayShuffle(files);

			for (const file of files) {
				if (ignoreds.includes(file)) continue;
				if (requirePrefix && !file.startsWith(requirePrefix)) continue;

				commonsOutputProgress(this.pool.length);
				if (poolLimit !== undefined && this.pool.length > poolLimit) break;

				const jimp: CommonsJimp = await this.loadPoolFile(file);

				this.pool.push({
						file: file,
						image: jimp
				});
			}
			commonsOutputResult(this.pool.length);
		}

		await super.init();
	}

	protected override buildHttpServer(): CommonsStrictExpressServer {
		const expressConfig: TPropertyObject = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');

		return commonsExpressBuildDefaultServer(
				this.httpConfig.port,
				expressConfig.bodyParserLimit
		);
	}

	public setDumpAttemptQualitiesCallback(callback: (qualities: number[]) => void): void {
		this.dumpAttemptQualitiesCallback = callback;
	}

	// override if desired
	protected async loadDetector(_path: string): Promise<DetectorT|undefined> {	// eslint-disable-line @typescript-eslint/require-await
		return undefined;
	}

	protected override async run(): Promise<void> {
		if (this.getArgs().hasAttribute('non-rest')) {
			if (!this.hasEnoughToTrain()) throw new Error('Not enough to train');

			await this.attemptRetrain();

			if (this.getArgs().hasAttribute('export-built')) {
				commonsOutputInfo('Refusing to save when using export-built');
			} else {
				await this.save();
			}
			return;
		}

		if (this.getArgs().hasAttribute('use-existing-model')) {
			assertSet(this.modelPath, 'modelPath');
	
			commonsOutputDoing(`Loading detector from ${this.modelPath}`);
			const detector: DetectorT|undefined = await this.loadDetector(this.modelPath);
			if (!detector) throw new Error('Existing model loading is not implemented');

			await detector.init();

			this.detector = detector;
			commonsOutputSuccess();
		}
		
		await super.run();
	}

	protected abstract hasEnoughToTrain(): boolean;

	protected abstract retrain(): Promise<void>;

	private async attemptRetrain(): Promise<void> {
		if (!this.hasEnoughToTrain()) return;

		await this.retrain();
	}

	protected abstract attemptPredict(image: CommonsJimp): Promise<TCommonsAiOutputWithCertainty<OutputT>|undefined>;

	protected async listRandoms(limit: number): Promise<TCommonsAiFileAndPrediction<OutputT>[]> {
		const pool: TCommonsAiTrainingImage[] = this.pool.slice();
		commonsArrayShuffle(pool);

		commonsOutputDoing('Generating random samples');

		const clone: TCommonsAiTrainingImage[] = [ ...pool ];
		const samples: TCommonsAiTrainingImage[] = [];
		while (samples.length < limit && clone.length > 0) {
			const next: TCommonsAiTrainingImage|undefined = clone.shift();
			if (!next) break;

			if (this.ignoreds.includes(next.file)) continue;

			samples.push(next);
		}

		const results: TCommonsAiFileAndPrediction<OutputT>[] = [];
		for (const image of samples) {
			let classification: TCommonsAiPrediction<OutputT>|undefined;

			const output: TCommonsAiOutputWithCertainty<OutputT>|undefined = await this.attemptPredict(image.image);
			if (output) {
				classification = {
						prediction: output.output,
						certainty: output.certainty
				};
			}

			results.push({
					file: image.file,
					prediction: classification ? classification : null
			});
		}

		commonsOutputResult(results.length);

		return results;
	}

	protected getImage(file: string): TCommonsAiTrainingImage|undefined {
		return this.pool
				.find((image: TCommonsAiTrainingImage): boolean => image.file === file);
	}

	protected abstract setImage(file: string, value: OutputT, allowOverwrite: boolean): boolean;

	protected ignore(file: string): void {
		commonsOutputDebug(`Marking ${file} as ignored`);
		
		if (this.ignoreds.includes(file)) return;
		
		this.ignoreds.push(file);

		const match: TCommonsAiTrainingImage|undefined = this.getImage(file);
		if (match) commonsArrayRemove(this.pool, match);
	}

	protected abstract getTrainingLoss(): number|undefined;

	protected async train(): Promise<number|undefined> {
		// NB, this method doesn't appear to be used by anything? More specifically, the cross trainer doesn't call it

		await this.attemptRetrain();

		return this.getTrainingLoss();
	}

	protected async save(): Promise<void> {
		assertSet(this.modelPath, 'modelPath');
		assertSet(this.ignoredsFile, 'ignoredsFile');

		if (this.trainer) {
			try {
				commonsFileRm(`${this.modelPath}/model.json`);
				commonsFileRm(`${this.modelPath}/weights.bin`);
				fs.rmdirSync(this.modelPath);
			} catch (e) {
				// ignore
			}

			commonsOutputDoing(`Saving to ${this.modelPath}`);
			await this.trainer.save(this.modelPath);
			commonsOutputSuccess();
		}

		commonsOutputDoing('Updating ignores');

		await commonsFileWriteJsonFileAsync(this.ignoredsFile, this.ignoreds);

		commonsOutputSuccess();
	}

	protected abstract getStats(): StatsT;
}
