import * as express from 'express';

import * as tfcore from '@tensorflow/tfjs-core';

import { commonsArrayRemoveAll, commonsTypeHasProperty } from 'tscommons-es-core';
import { commonsArrayShuffle, commonsArrayRemove } from 'tscommons-es-core';
import { TBoundary } from 'tscommons-es-graphics';
import {
		TCommonsAiBoundaryFileAndPrediction,
		commonsAiCrossValidationBuildKs,
		TCommonsAiBoundaryStats,
		TCommonsAiCrossValidationK,
		TCommonsAiEpochTrainingStats,
		TCommonsAiGenericTrainingData,
		TCommonsAiModelWithQuality,
		TCommonsAiOutputWithCertainty
} from 'tscommons-es-ai';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputInfo, commonsOutputProgress, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { commonsFileReadJsonFileAsync, commonsFileWriteJsonFileAsync } from 'nodecommons-es-file';
import { CommonsJimp } from 'nodecommons-es-graphics';
import { commonsAiCrossValidationTrain, ICommonsAiEarlyStopping } from 'nodecommons-es-ai';
import { assertSet } from 'nodecommons-es-app';
import { CommonsAiJimpBoundary } from 'nodecommons-es-ai-tensorflow-graphics';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsAiTensorflowPythonBridge } from 'nodecommons-es-ai-tensorflow';

import { CommonsAiTrainingBoundaryApi } from '../apis/boundary.api';

import { CommonsAiTrainingBoundaryTrainer } from '../classes/boundary-trainer';

import { TCommonsAiTrainingImage } from '../types/timage';
import { isTCommonsAiTrainingBoundaryDb, TCommonsAiTrainingBoundaryDb } from '../types/tboundary-db';

import { CommonsAiTrainingApp } from './training.app';

type TDone = TCommonsAiTrainingImage & {
		boundary: TBoundary;
};

function clampBoundary(xy: TBoundary): void {
	xy.x = Math.max(0, Math.min(1, xy.x));
	xy.y = Math.max(0, Math.min(1, xy.y));
	xy.width = Math.max(0, Math.min(1, xy.width));
	xy.height = Math.max(0, Math.min(1, xy.height));

	if ((xy.x + xy.width) > 1) xy.width = 1 - xy.x;
	if ((xy.y + xy.height) > 1) xy.height = 1 - xy.y;
}

export abstract class CommonsAiTrainingBoundaryApp<
		TrainerT extends CommonsAiTrainingBoundaryTrainer = CommonsAiTrainingBoundaryTrainer,
		DetectorT extends CommonsAiJimpBoundary = CommonsAiJimpBoundary
> extends CommonsAiTrainingApp<
		TBoundary,
		TCommonsAiBoundaryStats,
		TrainerT,
		DetectorT
> {
	protected boundaryDbFile: string|undefined;

	protected done: TDone[] = [];
	protected boundaryDb: TCommonsAiTrainingBoundaryDb = {};

	constructor(
			name: string,
			private constructBoundaryCallback: (
					training: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[],
					validation: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[]
			) => TrainerT,
			private crossTrainInterations: number = 4,
			private maxValidationRatio: number = 0.2,
			private testingRatio: number = 0.15
	) {
		super(name);
	}

	protected override async init(): Promise<void> {
		this.install(
				(restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>, path: string): void => {
					const api: CommonsAiTrainingBoundaryApi = new CommonsAiTrainingBoundaryApi(
							restServer,
							path,
							(limit: number): Promise<TCommonsAiBoundaryFileAndPrediction[]> => this.listRandoms(limit),
							(file: string): TCommonsAiTrainingImage|undefined => this.getImage(file),
							(file: string, value: TBoundary): boolean => this.setImage(file, value, true),
							(file: string): void => this.ignore(file),
							(): Promise<number|undefined> => this.train(),
							(): Promise<void> => this.save(),
							(): TCommonsAiBoundaryStats => this.getStats()
					);
					api.installImageHandler(this.getHttpServer());
				}
		);

		await super.init();

		assertSet(this.boundaryDbFile, 'boundaryDbFile');

		const boundaryDb: unknown = await commonsFileReadJsonFileAsync(this.boundaryDbFile);
		if (!isTCommonsAiTrainingBoundaryDb(boundaryDb)) throw new Error('Invalid BoundaryDb');

		this.boundaryDb = boundaryDb;

		this.done = [];
		for (const item of this.pool) {
			if (commonsTypeHasProperty(this.boundaryDb, item.file)) {
				const boundary: TBoundary = this.boundaryDb[item.file];
				clampBoundary(boundary);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes

				this.done.push({
						...item,
						boundary: { ...boundary }
				});
			}
		}
		
		commonsArrayRemoveAll(
				this.pool,
				this.done,
				undefined,
				(a: TCommonsAiTrainingImage, b: TCommonsAiTrainingImage): boolean => a.file === b.file
		);

		commonsOutputDoing('Loading any non-pool from boundarydb');

		const files: string[] = Object.keys(this.boundaryDb);

		let i: number = 0;
		for (const file of files) {
			const existing: TDone|undefined = this.done
					.find((d: TDone): boolean => d.file === file);
			if (existing) continue;

			commonsOutputProgress(++i);

			const jimp: CommonsJimp = await this.loadPoolFile(file);

			const boundary: TBoundary = this.boundaryDb[file];
			clampBoundary(boundary);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes
			
			this.done.push({
					file: file,
					image: jimp,
					boundary: { ...boundary }
			});
		}

		commonsOutputSuccess();

		commonsOutputInfo(`${this.done.length} items have been done.`);
	}

	protected hasEnoughToTrain(): boolean {
		return this.done.length > 3;
	}

	protected async retrain(): Promise<void> {
		const images: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[] = this.done
				.map((done: TDone): TCommonsAiGenericTrainingData<CommonsJimp, TBoundary> => ({
						input: done.image,
						output: done.boundary
				}));
		
		commonsArrayShuffle(images);

		const qty: number = Math.floor(images.length * this.testingRatio);
		if (qty < 1) throw new Error('Not enough data to create test set');

		const testing: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[] = images
				.splice(0, qty);

		const ks: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>>[] = commonsAiCrossValidationBuildKs<CommonsJimp, TBoundary>(
				images,
				this.crossTrainInterations,
				this.maxValidationRatio
		);

		const best: TCommonsAiModelWithQuality<CommonsJimp, TBoundary, TrainerT> = await commonsAiCrossValidationTrain<CommonsJimp, TBoundary, TrainerT, TCommonsAiEpochTrainingStats>(
				ks,
				testing,
				async (
						training: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[],
						_epochs: number,
						validation: TCommonsAiGenericTrainingData<CommonsJimp, TBoundary>[]|undefined,
						_earlyStopping: ICommonsAiEarlyStopping|undefined
				): Promise<TrainerT> => {
					const constructed: TrainerT = this.constructBoundaryCallback(
							training,
							validation || []
					);

					if (this.getArgs().hasAttribute('export-built')) {
						const path: string = this.getArgs().getString('export-path');
						const prefix: string = this.getArgs().getString('export-prefix');

						constructed.doExportBuiltCallback = async (
								k: number,
								exported: [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ]
						): Promise<void> => {
							const exporter: CommonsAiTensorflowPythonBridge = new CommonsAiTensorflowPythonBridge(prefix, path);
							const promises: Promise<void>[] = [
									exporter.write(`k-${k}-training-xs`, exported[0]),
									exporter.write(`k-${k}-training-ys`, exported[1])
							];
							if (exported[2]) promises.push(exporter.write(`k-${k}-validation-xs`, exported[2]));
							if (exported[3]) promises.push(exporter.write(`k-${k}-validation-ys`, exported[3]));
				
							if (k === 1) {
								// also export the testing data at this time

								const testingTensors: [ tfcore.Tensor, tfcore.Tensor ] = await constructed.buildXYFromExamples(testing);
								promises.push(exporter.write('testing-xs', testingTensors[0]));
								promises.push(exporter.write('testing-ys', testingTensors[1]));
							}

							await Promise.all(promises);
						};
					}

					await constructed.init();

					return constructed;
				},
				-1,	// na
				undefined,
				this.dumpAttemptQualitiesCallback
		);

		if (this.trainer) this.trainer.dispose();
		this.trainer = best.model;
		this.detector = undefined;
	}

	protected async attemptPredict(image: CommonsJimp): Promise<TCommonsAiOutputWithCertainty<TBoundary>|undefined> {
		if (!this.trainer && !this.detector) return undefined;

		const prediction: TCommonsAiOutputWithCertainty<TBoundary>|undefined = await (this.trainer || this.detector)!.predict(image);
		return prediction;
	}

	protected setImage(file: string, boundary: TBoundary, allowOverwrite: boolean = true): boolean {
		clampBoundary(boundary);	// some of the training from the UI browser offset calcs appears to be <0 or >1 sometimes

		commonsOutputDebug(`Marking ${file} as ${boundary.x},${boundary.y},${boundary.width},${boundary.height}`);

		const match: TCommonsAiTrainingImage|undefined = this.getImage(file);
		
		if (!match) {	// not in pool, may be already classified
			if (!allowOverwrite) return false;

			commonsOutputDebug('Attempting overwrite if already set');

			const match2: TDone|undefined = this.done
					.find((d: TDone): boolean => d.file === file);
			if (!match2) throw new Error('Image is not in pool or dones');

			match2.boundary = { ...boundary };
		} else {
			this.done.push({
					...match,
					boundary: { ...boundary }
			});
		}
		
		commonsArrayRemove(this.pool, match);

		return true;
	}

	protected getTrainingLoss(): number|undefined {
		if (!this.trainer) return undefined;

		return this.trainer.getTrainingStats().loss;
	}

	protected override async save(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		assertSet(this.boundaryDbFile, 'boundaryDbFile');

		await super.save();

		commonsOutputDoing('Updating BoundaryDb');

		const boundaryDb: TCommonsAiTrainingBoundaryDb = {};

		for (const d of this.done) {
			boundaryDb[d.file] = d.boundary;
		}

		await commonsFileWriteJsonFileAsync(this.boundaryDbFile, boundaryDb);

		commonsOutputSuccess();
	}

	protected getStats(): TCommonsAiBoundaryStats {
		return {
				pool: this.pool.length,
				done: this.done.length,
				bestLoss: (!this.trainer || this.trainer.getTrainingStats().loss === undefined) ? null : this.trainer.getTrainingStats().loss
		};
	}
}
