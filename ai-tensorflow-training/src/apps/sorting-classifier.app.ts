import * as tfcore from '@tensorflow/tfjs-core';

import { TCommonsAiFileAndPrediction } from 'tscommons-es-ai';

import { CommonsJimp } from 'nodecommons-es-graphics';
import { CommonsAiTensorflowSequential } from 'nodecommons-es-ai-tensorflow';

import { CommonsAiApplyingApp } from './applying.app';

export abstract class CommonsAiSortingClassifierApp<
		E extends string,
		ModelT extends CommonsAiTensorflowSequential<
				CommonsJimp,
				E,
				tfcore.Rank.R3,
				tfcore.Rank.R1
		>
> extends CommonsAiApplyingApp<
		E,
		ModelT
> {
	constructor(
			name: string,
			private includeUnmatched: boolean = true,
			private minThreshold: number = 0
	) {
		super(name);
	}

	protected abstract handleMatch(file: string, value: E, certainty: number): Promise<void>;
	protected async handleNoMatchOrBelowThreshold(_file: string, _value: E|undefined, _certainty?: number): Promise<void> {
		// do nothing, can be overridden in a subclass
	}

	protected async handlePredictions(results: TCommonsAiFileAndPrediction<E>[]): Promise<void> {
		for (const result of results) {
			if (!this.includeUnmatched && (result.prediction === undefined || result.prediction === null || result.prediction.certainty < this.minThreshold)) continue;

			if (!result.prediction) {
				await this.handleNoMatchOrBelowThreshold(result.file, undefined, undefined);
			} else {
				await this.handleMatch(result.file, result.prediction.prediction, result.prediction.certainty);
			}
		}
	}
}
