export enum ECommonsAiTrainingLayer {
		DENSE = 'dense',
		CONV1D = 'conv1d',
		CONV2D = 'conv2d',
		CONV2DTRANSPOSE = 'conv2dtranspose',
		CONV3D = 'conv3d',
		CONV3DTRANSPOSE = 'conv3dtranspose',
		DROPOUT = 'dropout',
		FLATTEN = 'flatten',
		RESHAPE = 'reshape',
		MAXPOOLING2D = 'maxpooling2d',
		AVGPOOLING2D = 'avgpooling2d',
		BATCHNORMALIZATION = 'batchnormalization'
}
