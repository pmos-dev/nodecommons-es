import * as express from 'express';

import { TCommonsAiFileAndPrediction, ECommonsAiUnclassified, TCommonsAiClassifierStats, TCommonsAiEOrUnclassified } from 'tscommons-es-ai';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { commonsRestApiBadRequest } from 'nodecommons-es-rest';
import { CommonsRestServer } from 'nodecommons-es-rest';

import { TCommonsAiTrainingImage } from '../types/timage';
import { TCommonsAiPredictionWithImage } from '../types/tprediction-with-image';

import { CommonsAiTrainingApi } from './api.api';

type TConverted<E extends string> = { [ key in TCommonsAiEOrUnclassified<E> ]: {
		'true': TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null;
		'false': TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null;
} };

export abstract class CommonsAiTrainingClassifyApi<E extends string> extends CommonsAiTrainingApi<TCommonsAiEOrUnclassified<E>, TCommonsAiClassifierStats<E>> {
	constructor(
			private values: E[],
			private toE: (value: string) => E|undefined,
			restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>,
			path: string,
			predict: () => Promise<Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>>>,
			listRandoms: (limit: number) => Promise<TCommonsAiFileAndPrediction<E>[]>,
			getImage: (file: string) => TCommonsAiTrainingImage|undefined,
			setImage: (file: string, value: E) => boolean,
			ignore: (file: string) => void,
			train: () => Promise<number|undefined>,
			save: () => Promise<void>,
			getStats: () => TCommonsAiClassifierStats<E>
	) {
		super(
				restServer,
				path,
				listRandoms,
				getImage,
				ignore,
				train,
				save,
				getStats
		);

		super.getHandler(
				`${path}images/prediction`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TConverted<E>> => {
					commonsOutputDebug('Request for prediction');

					const predictions: Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined>> = await predict();

					const files: Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null>> = new Map<TCommonsAiEOrUnclassified<E>, Map<boolean, TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null>>();
					for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ]) {
						const map: Map<boolean, TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null> = new Map<boolean, TCommonsAiFileAndPrediction<TCommonsAiEOrUnclassified<E>>|null>();

						for (const state of [ true, false ]) {
							if (!predictions.has(value) || !predictions.get(value)!.has(state)) {
								map.set(state, null);
							} else {
								const prediction: TCommonsAiPredictionWithImage<TCommonsAiEOrUnclassified<E>>|undefined = predictions.get(value)!.get(state);

								if (!prediction) {
									map.set(state, null);
								} else {
									map.set(
											state,
											{
													file: prediction.image.file,
													prediction: {
															prediction: prediction.prediction,
															certainty: prediction.certainty
													}
											}
									);
								}
							}
						}

						files.set(value, map);
					}

					const converted: TConverted<E> = {} as TConverted<E>;	// eslint-disable-line @typescript-eslint/consistent-type-assertions
					for (const value of [ ...this.values, ECommonsAiUnclassified.UNCLASSIFIED ]) {
						converted[value] = {
								true: files.get(value)!.get(true) || null,
								false: files.get(value)!.get(false) || null
						};
					}

					return converted;
				}
		);

		super.putHandler(
				`${path}images/:file/:e[idname]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<boolean> => {	// eslint-disable-line @typescript-eslint/require-await
					const file: string = req.strictParams.file as string;
					const value: E|undefined = this.toE(req.strictParams.e as string);
					if (!value) return commonsRestApiBadRequest('Invalid value');

					return setImage(file, value);
				}
		);
	}
}
