import * as express from 'express';

import { commonsTypeAttemptNumber } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';
import { TCommonsAiXyFileAndPrediction, TCommonsAiXyStats } from 'tscommons-es-ai';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';

import { TCommonsAiTrainingImage } from '../types/timage';

import { CommonsAiTrainingApi } from './api.api';

export class CommonsAiTrainingXyApi extends CommonsAiTrainingApi<TXy, TCommonsAiXyStats> {
	constructor(
			restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>,
			path: string,
			listRandoms: (limit: number) => Promise<TCommonsAiXyFileAndPrediction[]>,
			getImage: (file: string) => TCommonsAiTrainingImage|undefined,
			setImage: (file: string, xy: TXy) => boolean,
			ignore: (file: string) => void,
			train: () => Promise<number|undefined>,
			save: () => Promise<void>,
			getStats: () => TCommonsAiXyStats
	) {
		super(
				restServer,
				path,
				listRandoms,
				getImage,
				ignore,
				train,
				save,
				getStats
		);

		super.putHandler(
				`${path}images/:file/:x/:y`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<boolean> => setImage(	// eslint-disable-line @typescript-eslint/require-await
					req.strictParams.file as string,
					{
							x: commonsTypeAttemptNumber(req.strictParams.x) || 0,
							y: commonsTypeAttemptNumber(req.strictParams.y) || 0
					}
				)
		);
	}
}
