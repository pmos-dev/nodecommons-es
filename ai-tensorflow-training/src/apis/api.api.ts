import * as express from 'express';

import { TEncodedObject } from 'tscommons-es-core';
import { TCommonsAiFileAndPrediction } from 'tscommons-es-ai';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestApi } from 'nodecommons-es-rest';
import { commonsRestApiNotFound } from 'nodecommons-es-rest';
import { CommonsRestServer } from 'nodecommons-es-rest';

import { TCommonsAiTrainingImage } from '../types/timage';

export abstract class CommonsAiTrainingApi<R extends string|TEncodedObject|number, S extends TEncodedObject> extends CommonsRestApi<ICommonsRequestWithStrictParams, express.Response> {
	constructor(
			restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>,
			private path: string,
			listRandoms: (limit: number) => Promise<TCommonsAiFileAndPrediction<R>[]>,
			private getImage: (file: string) => TCommonsAiTrainingImage|undefined,
			ignore: (file: string) => void,
			train: () => Promise<number|undefined>,
			save: () => Promise<void>,
			getStats: () => S
	) {
		super(restServer);

		super.getHandler(
				`${path}images/random/:limit[int]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TCommonsAiFileAndPrediction<R>[]> => {
					commonsOutputDebug('Request for random');

					const randoms: TCommonsAiFileAndPrediction<R>[] = await listRandoms(req.strictParams.limit as number);

					return randoms;
				}
		);

		super.getHandler(
				`${path}trainer/stats`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<S> => {	// eslint-disable-line @typescript-eslint/require-await, arrow-body-style
					return getStats();
				}
		);

		super.getHandler(
				`${path}trainer/train`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<number> => {	// eslint-disable-line @typescript-eslint/require-await
					const bestLoss: number|undefined = await train();

					if (bestLoss === undefined) return -1;
					return bestLoss;
				}
		);

		super.postHandler(
				`${path}trainer/save`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<true> => {	// eslint-disable-line @typescript-eslint/require-await
					await save();

					return true;
				}
		);

		super.patchHandler(
				`${path}ignore/:file`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<true> => {	// eslint-disable-line @typescript-eslint/require-await
					ignore(req.strictParams.file as string);

					return true;
				}
		);
	}

	public installImageHandler(
			expressServer: CommonsStrictExpressServer
	): void {
		expressServer.get(
				`${this.path}images/:file/image`,
				async (req: ICommonsRequestWithStrictParams, res: express.Response): Promise<void> => {
					console.log('Image request');
					const image: TCommonsAiTrainingImage|undefined = this.getImage(req.strictParams.file as string);
					if (!image) return commonsRestApiNotFound('No such image exists');

					const buffer: Buffer = await image.image.getRawJimpImage().getBufferAsync('image/jpeg');

					res.setHeader('Content-Type', 'image/jpeg');
					res.status(200).send(buffer);
				}
		);
	}
}
