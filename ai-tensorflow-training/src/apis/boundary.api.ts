import * as express from 'express';

import { commonsTypeAttemptNumber } from 'tscommons-es-core';
import { TBoundary } from 'tscommons-es-graphics';
import { TCommonsAiBoundaryFileAndPrediction, TCommonsAiBoundaryStats } from 'tscommons-es-ai';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';

import { TCommonsAiTrainingImage } from '../types/timage';

import { CommonsAiTrainingApi } from './api.api';

export class CommonsAiTrainingBoundaryApi extends CommonsAiTrainingApi<TBoundary, TCommonsAiBoundaryStats> {
	constructor(
			restServer: CommonsRestServer<ICommonsRequestWithStrictParams, express.Response>,
			path: string,
			listRandoms: (limit: number) => Promise<TCommonsAiBoundaryFileAndPrediction[]>,
			getImage: (file: string) => TCommonsAiTrainingImage|undefined,
			setImage: (file: string, xy: TBoundary) => boolean,
			ignore: (file: string) => void,
			train: () => Promise<number|undefined>,
			save: () => Promise<void>,
			getStats: () => TCommonsAiBoundaryStats
	) {
		super(
				restServer,
				path,
				listRandoms,
				getImage,
				ignore,
				train,
				save,
				getStats
		);

		super.putHandler(
				`${path}images/:file/:x/:y/:width/:height`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<boolean> => setImage(	// eslint-disable-line @typescript-eslint/require-await
					req.strictParams.file as string,
					{
							x: commonsTypeAttemptNumber(req.strictParams.x) || 0,
							y: commonsTypeAttemptNumber(req.strictParams.y) || 0,
							width: commonsTypeAttemptNumber(req.strictParams.width) || 0,
							height: commonsTypeAttemptNumber(req.strictParams.height) || 0
					}
				)
		);
	}
}
