import { commonsTypeHasProperty } from 'tscommons-es-core';

import { TCommonsMongoId, isTCommonsMongoId } from '../types/tcommons-mongo-id';

export interface ICommonsMongoIdRow {
		readonly _id: TCommonsMongoId;
}

export function isICommonsMongoIdRow(test: unknown): test is ICommonsMongoIdRow {
	if (!commonsTypeHasProperty(test, '_id')) return false;
	if (!isTCommonsMongoId(test['_id'])) return false;
	
	return true;
}

export function removeICommonsMongoIdRow<T>(src: T & ICommonsMongoIdRow): T {
	const withoutId: Omit<ICommonsMongoIdRow, '_id'> & T = src;
	delete(withoutId['_id']);
	
	return withoutId;
}
