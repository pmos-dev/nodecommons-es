import { ObjectId } from 'bson';

import {
		commonsTypeIsString,
		commonsTypeIsNumber,
		commonsTypeIsObject,
		commonsTypeIsPropertyObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';

export type TCommonsMongoId = ObjectId|number|string|TPropertyObject;

export function isTCommonsMongoId(test: unknown): test is TCommonsMongoId {
	// we can't use ObjectId.isValid, as sometimes group._id etc. is returning custom data rather than a true id
	if (commonsTypeIsString(test)) return true;
	if (commonsTypeIsNumber(test)) return true;
	if (commonsTypeIsObject(test) && ObjectId.isValid(test as unknown as ObjectId)) return true;
	if (commonsTypeIsPropertyObject(test)) return true;

	return false;
}

export function fromTCommonsMongoId(id: TCommonsMongoId): string {
	if (commonsTypeIsString(id)) return id;
	if (commonsTypeIsNumber(id)) return id.toString(16);
	if (commonsTypeIsObject(id) && ObjectId.isValid(id as unknown as ObjectId)) return (id as unknown as ObjectId).toHexString() as string;	// eslint-disable-line @typescript-eslint/no-unnecessary-type-assertion
	
	throw new Error('Unable to convert TCommonsMongoId to string as it doesn\'t match any known ObjectId or primative data types');
}
