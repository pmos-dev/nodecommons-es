import mongodb from 'mongodb';

import { CommonsNosqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';

import { ICommonsMongoIdRow, isICommonsMongoIdRow, removeICommonsMongoIdRow } from '../interfaces/icommons-mongo-id-row';

import { fromTCommonsMongoId } from '../types/tcommons-mongo-id';

export abstract class CommonsMongodbService extends CommonsNosqlDatabaseService<ICommonsCredentials> {
	private client: mongodb.MongoClient|undefined;
	protected database: mongodb.Db|undefined;
	
	protected async init(): Promise<void> {
		const connectionStringComponents: string[] = [ 'mongodb://' ];
		
		if (this.credentials.user && this.credentials.user !== '') {
			const up: string[] = [ this.credentials.user ];
			if (this.credentials.password && this.credentials.password !== '') {
				up.push(this.credentials.password);
			}
			
			connectionStringComponents.push(`${up.join(':')}@`);
		}
		
		connectionStringComponents.push(this.credentials.host || 'localhost');
		connectionStringComponents.push(`:${this.credentials.port || 27017}/`);
		
		if (this.credentials.authSource) {
			connectionStringComponents.push(`?authSource=${this.credentials.authSource}`);
		}

		const connectionString: string = connectionStringComponents.join('');

		this.client = await mongodb.MongoClient.connect(connectionString);
		this.database = this.client.db(this.credentials.name);
	}
	
	public async ensureCollection<T extends mongodb.Document>(
			name: string,
			options?: mongodb.CreateCollectionOptions
	): Promise<mongodb.Collection<T>> {
		// because the mongodb methods return Collection<any>, it matches Collection<T>
		// however be aware it isn't actually being checked (nor could it)

		const existing: mongodb.Collection<any>|undefined = (await this.getRawDatabase().collections())
				.find((collection: mongodb.Collection<any>): boolean => collection.collectionName === name);
		
		if (existing) return existing;
		
		return await this.getRawDatabase().createCollection(
				name,
				options
		);
	}
	
	public async close(): Promise<void> {
		if (this.client) await this.client.close();
	}
	
	protected getRawDatabase(): mongodb.Db {
		if (!this.database) throw new Error('No client is initialised yet');
		return this.database;
	}

	public async forEachQueryResults<T>(
			results: mongodb.AggregationCursor<T>|mongodb.FindCursor<T>,
			isT: (t: unknown) => t is T,
			callback: (t: T) => Promise<boolean|void>
	): Promise<boolean> {
		try {
			while (await results.hasNext()) {
				const next: unknown = await results.next();
				if (!next) break;
				
				if (!isT(next)) throw new Error('Invalid row returned from database');

				const outcome: boolean|void = await callback(next);
				if (outcome === false) return false;
			}
			
			return true;
		} finally {
			await results.close();
		}
	}

	public async listQueryResults<T>(
			results: mongodb.AggregationCursor<T>|mongodb.FindCursor<T>,
			isT: (t: unknown) => t is T
	): Promise<T[]> {
		const items: T[] = [];
		
		await this.forEachQueryResults(
				results,
				isT,
				async (t: T): Promise<void> => {	// eslint-disable-line @typescript-eslint/require-await
					items.push(t);
				}
		);
		
		return items;
	}

	public async listFirstQueryResult<T>(
			results: mongodb.AggregationCursor<T>|mongodb.FindCursor<T>,
			isT: (t: unknown) => t is T
	): Promise<T|undefined> {
		const all: T[] = await this.listQueryResults<T>(results, isT);

		if (all.length === 0) return undefined;
		return all[0];
	}

	public async listQueryResultsWithoutIds<T>(
			results: mongodb.AggregationCursor<T & ICommonsMongoIdRow>|mongodb.FindCursor<T & ICommonsMongoIdRow>,
			isT: (t: unknown) => t is T
	): Promise<T[]> {
		const items: T[] = [];
		
		await this.forEachQueryResults(
				results,
				isT,
				async (t: T): Promise<void> => {	// eslint-disable-line @typescript-eslint/require-await
					if (!isICommonsMongoIdRow(t)) throw new Error('Returned row lacked an _id field');
					items.push(removeICommonsMongoIdRow(t));
				}
		);
		
		return items;
	}

	public async listFirstQueryResultWithoutId<T>(
			results: mongodb.AggregationCursor<T & ICommonsMongoIdRow>|mongodb.FindCursor<T & ICommonsMongoIdRow>,
			isT: (t: unknown) => t is T
	): Promise<T|undefined> {
		const all: T[] = await this.listQueryResultsWithoutIds<T>(results, isT);

		if (all.length === 0) return undefined;
		return all[0];
	}

	public async listQueryResultsAsIdKeyObject<T>(
			results: mongodb.AggregationCursor<ICommonsMongoIdRow & T>|mongodb.FindCursor<ICommonsMongoIdRow & T>,
			isT: (t: unknown) => t is T
	): Promise<{ [ _id: string ]: T }> {
		const items: (ICommonsMongoIdRow & T)[] = await this.listQueryResults<ICommonsMongoIdRow & T>(
				results,
				(t: unknown): t is (ICommonsMongoIdRow & T) => {
					if (!isICommonsMongoIdRow(t)) return false;
					if (!isT(t)) return false;
					
					return true;
				}
		);
		
		const object: { [ _id: string ]: T } = {};
		
		for (const item of items) {
			const id: string = fromTCommonsMongoId(item._id);	// eslint-disable-line no-underscore-dangle
			object[id] = removeICommonsMongoIdRow<T>(item);
		}
		
		return object;
	}
}
