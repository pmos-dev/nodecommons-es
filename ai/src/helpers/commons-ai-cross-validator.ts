import { commonsOutputDebug, commonsOutputDoing, commonsOutputInfo, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';

import {
		ICommonsAiModelTrainable,
		TCommonsAiCrossValidationK,
		TCommonsAiGenericTrainingData,
		TCommonsAiModelWithQuality,
		TCommonsAiQuality,
		TCommonsAiTrainingStats
} from 'tscommons-es-ai';

import { ICommonsAiEarlyStopping } from '../interfaces/icommons-ai-early-stopping';
import { ICommonsAiCrossTrainKable } from '../interfaces/icommons-ai-cross-train-kable';

export async function commonsAiCrossValidationTrain<
		I,
		O,
		M extends ICommonsAiModelTrainable<I, O, S> & ICommonsAiCrossTrainKable,
		S extends TCommonsAiTrainingStats
>(
		crossValidationKs: TCommonsAiCrossValidationK<TCommonsAiGenericTrainingData<I, O>>[],
		testingData: TCommonsAiGenericTrainingData<I, O>[],
		modelConstructorCallback: (
				training: TCommonsAiGenericTrainingData<I, O>[],
				epochs: number,
				validation: TCommonsAiGenericTrainingData<I, O>[]|undefined,
				earlyStopping: ICommonsAiEarlyStopping|undefined
		) => Promise<M>,
		epochs: number,
		earlyStopping?: ICommonsAiEarlyStopping,
		dumpAttemptQualitiesCallback?: (qualities: number[]) => void
): Promise<TCommonsAiModelWithQuality<I, O, M>> {
	const iterations: number = crossValidationKs.length;

	type TAttempt = {
			model: M;
			quality: TCommonsAiQuality;
			delta: number;
	};

	const attempts: TAttempt[] = [];
	for (const crossValidationK of crossValidationKs) {
		if (crossValidationK.training.length === 0) throw new Error(`Not enough data to train for ${crossValidationKs.length} Ks.`);
		if (crossValidationK.validation.length === 0) throw new Error(`Not enough data to validate for ${crossValidationKs.length} Ks.`);
		
		commonsOutputInfo(`K iteration ${attempts.length + 1} of ${iterations}`);

		const model: M = await modelConstructorCallback(
				crossValidationK.training,
				epochs,
				crossValidationK.validation,
				earlyStopping
		);
		model.k = attempts.length + 1;

		commonsOutputDoing('Building data');
		await model.build();
		commonsOutputSuccess();

		commonsOutputDoing('Training model');
		const stats: S|undefined = await model.train();
		if (stats) {
			commonsOutputResult(`loss=${stats.loss}, acc=${stats.acc}, epochs=${stats.epochs}`);
		} else {
			commonsOutputSuccess();
		}

		commonsOutputDoing('Testing model quality using testing data');
		const quality: TCommonsAiQuality = await model.test(testingData);
		const delta: number = quality.positive - quality.negative;
		commonsOutputResult(`${quality.positive} positive, ${quality.negative} negative, overall ${delta}`);

		attempts.push({
				model: model,
				quality: quality,
				delta: delta
		});
	}

	attempts
			.sort((a: TAttempt, b: TAttempt): number => {
				if (a.delta < b.delta) return -1;
				if (b.delta > a.delta) return 1;
				return 0;
			})
			.reverse();

	const qualities: number[] = attempts
			.map((attempt: TAttempt): number => attempt.delta);
	commonsOutputDebug(`Attempt delta qualities are ${qualities.join(', ')}`);

	if (dumpAttemptQualitiesCallback) dumpAttemptQualitiesCallback(qualities);

	return {
			model: attempts[0].model,
			quality: attempts[0].quality
	};
}
