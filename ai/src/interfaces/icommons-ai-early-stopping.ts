// this is a true interface

import { TCommonsAiTrainingStats } from 'tscommons-es-ai';

export interface ICommonsAiEarlyStopping {
	consider(stats: TCommonsAiTrainingStats): Promise<boolean>;
	getCompletedEpochs(): number;
}
