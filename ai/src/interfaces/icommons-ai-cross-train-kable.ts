// this is a true interface

export interface ICommonsAiCrossTrainKable {
	get k(): number|undefined;
	set k(k: number|undefined);
}
