import * as tfcore from '@tensorflow/tfjs-core';

import { CommonsJimp } from 'nodecommons-es-graphics';
import { commonsAiJimpToGreyscaleNumbers, commonsAiJimpToGreyscaleNumbersOptimised, commonsAiJimpToRgbNumbersOptimised } from 'nodecommons-es-ai-graphics';

export async function asyncConvertJimpToTensor3D(
		input: CommonsJimp,
		shape: [ number, number, 1 | 3 ],
		noise?: number,
		optimised: boolean = false
): Promise<tfcore.Tensor3D> {
	const clone: CommonsJimp = input.clone();
	
	if (clone.getWidth() !== shape[0] || clone.getHeight() !== shape[1]) await clone.resize(shape[0], shape[1]);

	const ps: number[] = [];

	if (shape[2] === 1) {
		// monochrome

		if (optimised) {
			ps.push(...commonsAiJimpToGreyscaleNumbersOptimised(clone));
		} else {
			ps.push(...await commonsAiJimpToGreyscaleNumbers(clone));
		}
	} else {
		// rgb

		if (optimised) {
			ps.push(...commonsAiJimpToRgbNumbersOptimised(clone));
		} else {
			ps.push(...await commonsAiJimpToGreyscaleNumbers(clone));
		}
	}

	if (noise) {
		for (let i = ps.length; i-- > 0;) {
			ps[i] = Math.max(0, Math.min(1, ps[i] + (((Math.random() * 2) - 1) * noise)));
		}
	}

	return tfcore.tensor3d(ps, shape);
}
