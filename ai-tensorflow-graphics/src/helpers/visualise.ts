import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { IRgba } from 'tscommons-es-graphics';

import { CommonsJimp } from 'nodecommons-es-graphics';

import { asyncConvertJimpToTensor3D } from './graphics';

function normaliseBlock(block: number[]): void {
	let min: number|undefined;
	let max: number|undefined;
	// eslint-disable-next-line @typescript-eslint/prefer-for-of
	for (let i = block.length; i-- > 0;) {
		if (min === undefined || block[i] < min) min = block[i];
		if (max === undefined || block[i] > max) max = block[i];
	}

	if (min === undefined || max === undefined) return;

	const d: number = max - min;
	for (let i = block.length; i-- > 0;) {
		block[i] = Math.max(0, Math.min(1, (block[i] - min) / d));
	}
}

export async function commonsAiVisualiseLayer(
		model: tflayers.LayersModel,
		layerName: string,
		spacing: number = 2
): Promise<CommonsJimp> {
	const layer: tflayers.layers.Layer|undefined = model.getLayer(layerName);
	if (!layer) throw new Error('No such layer exists');

	const weights: tfcore.Tensor<tfcore.Rank>[] = layer.getWeights();
	const shape: number[] = weights[0].shape;

	if (shape.length !== 4) throw new Error('Only shapes of [x,y,rgb,filter] are supported at the moment');
	if (shape[2] !== 3) throw new Error('Shape dimension [2] was expected to be 3 (i.e. rgb)');

	const filters: number = weights[0].shape[weights[0].shape.length - 1];

	const normalised: number[] = Array.from(weights[0].dataSync() as Float32Array);
	normaliseBlock(normalised);

	const gridSize: number = Math.ceil(Math.sqrt(filters));
	const cellSize: number = Math.max(shape[0], shape[1]) + spacing;

	const visualisation: CommonsJimp = new CommonsJimp();
	await visualisation.createNew(gridSize * cellSize, gridSize * cellSize, 0xffffffff);

	let gx: number = 0;
	let gy: number = 0;

	for (let f = 0; f < filters; f++) {
		let i: number = f;
		const block: number[] = [];

		for (let x = 0; x < shape[0]; x++) {
			for (let y = 0; y < shape[1]; y++) {
				for (let c = 0; c < shape[2]; c++) {
					block.push(Math.round(normalised[i] * 255));
					i += filters;
				}
			}
		}

		const rgbs: IRgba[] = [];
		for (let i2 = 0; i2 < block.length; i2 += 3) {
			rgbs.push({
					red: block[i2],
					green: block[i2 + 1],
					blue: block[i2 + 2],
					alpha: 255
			});
		}

		const jimp: CommonsJimp = new CommonsJimp();
		await jimp.createNew(shape[0], shape[1]);
		await jimp.fromRgba(rgbs);

		await visualisation.blit(
				jimp,
				(gx * cellSize) + Math.floor(spacing / 2),
				(gy * cellSize) + Math.floor(spacing / 2),
				0,
				0,
				jimp.getWidth(),
				jimp.getHeight()
		);

		gx++;
		if (gx >= gridSize) {
			gx = 0;
			gy++;
		}
	}

	return visualisation;
}

export type TCommonsAiFeatureMap = {
		name: string;
		visualisation: CommonsJimp;
};

export async function commonsAiVisualiseFeatureMaps(
		model: tflayers.LayersModel,
		layerName: string|undefined,
		size: [ number, number, 1|3 ],
		image: CommonsJimp,
		noise?: number,
		optimised: boolean = false,
		normalise: boolean = true,	// non-normalised may not work, as some weights might be negative etc.
		spacing: number = 2
): Promise<TCommonsAiFeatureMap[]> {
	const input: tfcore.Tensor3D = await asyncConvertJimpToTensor3D(
			image,
			size,
			noise,
			optimised
	);

	const layers: tflayers.layers.Layer[] = [];

	if (layerName) {
		const layer: tflayers.layers.Layer|undefined = model.getLayer(layerName);
		if (!layer) throw new Error('No such layer exists');
	} else {
		for (const l of model.layers) {
			if (!/^(conv2d_|batch_normalization_|layer_normalization_|average_pooling2d_|max_pooling2d_)/.test(l.name)) continue;

			layers.push(l);
		}
	}

	const featureMaps: TCommonsAiFeatureMap[] = [];

	for (const layer of layers) {
		const remodel: tflayers.LayersModel = tflayers.model({
				inputs: model.inputs,
				outputs: layer.output
		});

		const out: tfcore.Tensor2D = remodel.predict(input) as tfcore.Tensor2D;
		const shape: number[] = out.shape;

		const filters: number = shape[shape.length - 1];
		const data: number[] = Array.from(out.dataSync());

		const gridSize: number = Math.ceil(Math.sqrt(filters));
		const cellSize: number = Math.max(shape[1], shape[2]) + spacing;

		const visualisation: CommonsJimp = new CommonsJimp();
		await visualisation.createNew(gridSize * cellSize, gridSize * cellSize, 0xffffffff);

		let gx: number = 0;
		let gy: number = 0;

		for (let f = 0; f < filters; f++) {
			let i: number = f;
			const block: number[] = [];

			for (let x = 0; x < shape[1]; x++) {
				for (let y = 0; y < shape[2]; y++) {
					block.push(data[i]);
					i += filters;
				}
			}

			if (normalise) normaliseBlock(block);

			const rgbs: IRgba[] = [];
			// eslint-disable-next-line @typescript-eslint/prefer-for-of
			for (let i2 = 0; i2 < block.length; i2++) {
				rgbs.push({
						red: Math.max(0, Math.min(255, Math.round(block[i2] * 255))),
						green: Math.max(0, Math.min(255, Math.round(block[i2] * 255))),
						blue: Math.max(0, Math.min(255, Math.round(block[i2] * 255))),
						alpha: 255
				});
			}

			const jimp: CommonsJimp = new CommonsJimp();
			await jimp.createNew(shape[1], shape[2]);
			await jimp.fromRgba(rgbs);

			await visualisation.blit(
					jimp,
					(gx * cellSize) + Math.floor(spacing / 2),
					(gy * cellSize) + Math.floor(spacing / 2),
					0,
					0,
					jimp.getWidth(),
					jimp.getHeight()
			);

			gx++;
			if (gx >= gridSize) {
				gx = 0;
				gy++;
			}
		}

		featureMaps.push({
				name: layer.name,
				visualisation: visualisation
		});
	}

	return featureMaps;
}
