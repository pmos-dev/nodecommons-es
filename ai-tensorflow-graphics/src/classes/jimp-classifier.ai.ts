import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { CommonsJimp } from 'nodecommons-es-graphics';

import { CommonsAiTensorflowSequentialOnehotEnums } from 'nodecommons-es-ai-tensorflow';

import { asyncConvertJimpToTensor3D } from '../helpers/graphics';

export abstract class CommonsAiJimpClassifier<E extends string> extends CommonsAiTensorflowSequentialOnehotEnums<
		CommonsJimp,
		E,
		tfcore.Rank.R3
> {
	constructor(
			values: E[],
			private shape: [ number, number, 1|3 ],
			private optimised: boolean = false
	) {
		super(
				values,
				true
		);
	}

	// this renaming is just to make it clearer that we are loading rather than constructing
	protected abstract loadModel(): Promise<tflayers.LayersModel>
	protected async constructSequential(): Promise<tflayers.LayersModel> {
		return this.loadModel();
	}

	protected override async asyncConvertInputToX(input: CommonsJimp): Promise<tfcore.Tensor3D> {
		return await asyncConvertJimpToTensor3D(
				input,
				this.shape,
				undefined,
				this.optimised
		);
	}
}
