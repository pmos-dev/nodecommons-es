import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { TBoundary } from 'tscommons-es-graphics';
import { TCommonsAiOutputWithCertainty } from 'tscommons-es-ai';

import { CommonsJimp } from 'nodecommons-es-graphics';
import { CommonsAiTensorflowSequential } from 'nodecommons-es-ai-tensorflow';

import { asyncConvertJimpToTensor3D } from '../helpers/graphics';

export abstract class CommonsAiJimpBoundary extends CommonsAiTensorflowSequential<
		CommonsJimp,
		TBoundary,
		tfcore.Rank.R3,
		tfcore.Rank.R1
> {
	constructor(
			private shape: [ number, number, 1|3 ],
			private optimised: boolean = false,
			private dualXyApproach: boolean = false
	) {
		super(true, false);
	}

	// this renaming is just to make it clearer that we are loading rather than constructing
	protected abstract loadModel(): Promise<tflayers.LayersModel>
	protected async constructSequential(): Promise<tflayers.LayersModel> {
		return this.loadModel();
	}

	protected override async asyncConvertInputToX(input: CommonsJimp): Promise<tfcore.Tensor3D> {
		return await asyncConvertJimpToTensor3D(
				input,
				this.shape,
				undefined,
				this.optimised
		);
	}

	protected override convertPredictionToOutput(prediction: tfcore.Tensor1D): TCommonsAiOutputWithCertainty<TBoundary>|undefined {
		const sync: Float32Array = prediction.dataSync() as Float32Array;

		if (this.dualXyApproach) {
			const w: number = Math.max(0, sync[2] - sync[0]);
			const h: number = Math.max(0, sync[3] - sync[1]);
			return {
					output: { x: sync[0], y: sync[1], width: w, height: h },
					certainty: -1
			};
		} else {
			return {
					output: { x: sync[0], y: sync[1], width: sync[2], height: sync[3] },
					certainty: -1
			};
		}
	}
}
