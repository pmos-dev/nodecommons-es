import * as http from 'http';
import * as https from 'https';
import * as path from 'path';

import * as express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { commonsTypeHasProperty, commonsTypeIsString } from 'tscommons-es-core';
import { ECommonsHttpMethod, fromECommonsHttpMethod } from 'tscommons-es-http';

import {
		commonsOutputDoing,
		commonsOutputSuccess
} from 'nodecommons-es-cli';
import { commonsFileRootRelativePath } from 'nodecommons-es-file';
import { CommonsStrictHttpServer, TCommonsHttpStrictParam, TCommonsStrictParamPropertyObject, TCommonsStrictParamResult, commonsBuildStrictsFromPathDefinition, commonsRemoveStrictsFromPathDefinition, commonsValueToStrictParamResult } from 'nodecommons-es-http';

import {
		commonsExpressCache,
		commonsExpressNoCache,
		commonsExpressUseCacheIfUnchanged,
		commonsExpressAutoNpm1985TimestampFile,
		commonsExpressSetLastModified,
		commonsExpressConsiderMultiPart
} from '../helpers/commons-express';

import { ICommonsRequestWithStrictParams } from '../interfaces/icommons-request-with-strict-params';

import { TCommonsExpressHandler } from '../types/tcommons-express-handler';

import { COMMONS_ANGULAR_EXTS_CACHE, COMMONS_ANGULAR_EXTS_NOCACHE, COMMONS_ANGULAR_EXTS_USE_CACHE_IF_UNCHANGED } from '../consts/commons-angular-exts';

function paramMismatchPage(
		method: ECommonsHttpMethod,
		url: string,
		response: express.Response
): void {
	response.status(400).send(`
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Error</title>
</head>
<body>
<pre>Bad parameters supplied for ${fromECommonsHttpMethod(method).toUpperCase()} ${url}</pre>
</body>
</html>
	`.trim());
}

async function handle(
		method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>,
		request: express.Request,
		response: express.Response,
		stricts: TCommonsHttpStrictParam[],
		handler: TCommonsExpressHandler
): Promise<void> {
	const strictParams: TCommonsStrictParamPropertyObject = {};
	
	for (const param of stricts) {
		if (!commonsTypeHasProperty(request.params, param.variable)) {
			paramMismatchPage(method, request.originalUrl, response);
			return;
		}

		const value: string|string[] = request.params[param.variable];
		
		if (param.type === undefined) {
			strictParams[param.variable] = value;
		} else {
			const attempt: TCommonsStrictParamResult|TCommonsStrictParamResult[]|undefined = commonsValueToStrictParamResult(
					value,
					param.type,
					param.plurality,
					param.options
			);
			if (attempt === undefined) {
				paramMismatchPage(method, request.originalUrl, response);
				return;
			}
			
			if (param.options !== undefined) {
				if (!commonsTypeIsString(attempt)) {
					paramMismatchPage(method, request.originalUrl, response);
					return;
				}
				
				if (!param.options.includes(attempt)) {
					paramMismatchPage(method, request.originalUrl, response);
					return;
				}
			}
			
			strictParams[param.variable] = attempt;
		}
	}
	
	// hack to get the strict params into the request object, as you can't assign to replace params
	request['strictParams'] = strictParams;

	await handler(request as ICommonsRequestWithStrictParams, response);
}

/*
Note 1
There is an issue where the root URL (e.g. /angular/) is served by static rather than the Angular code.
This is possibly due to a mildly incorrect usage of static etc. Angular, i.e. static servicing files that it finds regardless of Angular routing
e.g. if a file is found it is assumed to be static, if it isn't it is assumed to be a route, so a route which is also an actual file won't be recognised and instead the file will be returned
This isn't good and probably a bug, but is hard to fix whilst maintaining Angular's assets folder etc.
That means that /angular/ will be served by static, and the noCache call isn't made. So we need to put one into serveStatic manually to compensate.

Note 2
npm locks the timestamp for package contents at 26 October 1985 for some internal reason to do with shasum or something.
This causes the Angular files being served (or indeed any from npm install) to have that as their timestamp, regardless of when they were last changed.
The only way around that is to hard-code manual current time Last-Modified dates into the detection, which is extremely non-ideal, but there isn't a way round it
*/

export class CommonsStrictExpressServer implements CommonsStrictHttpServer<
		ICommonsRequestWithStrictParams,
		express.Response
> {
	// See note 2 at top
	private usingNpmResources: boolean = false;
	private nodeStartTime: Date;

	constructor(
			private ex: express.Express,
			private server: http.Server|https.Server,
			private port: number,
			private bodyParserLimit: number|string = '100kb'
	) {
		this.nodeStartTime = new Date();
	}

	// See note 2 at top
	public setUsingNpmResources(state: boolean = true): void {
		this.usingNpmResources = state;
	}
	
	protected getExpress(): express.Express {
		return this.ex;
	}
	
	// needs to be public so SocketIO can access it
	public getServer(): http.Server|https.Server {
		return this.server;
	}
	
	public init() {
		this.ex.set('etag', false);
		
		const limitParams: bodyParser.OptionsJson = {};
		if (this.bodyParserLimit) limitParams.limit = this.bodyParserLimit;
	
		this.ex.use(bodyParser.json(limitParams));
		this.ex.use(bodyParser.urlencoded({
				...limitParams,
				extended: true
		}));

		this.ex.use(cors());
	}
	
	public async listen(): Promise<void> {
		return new Promise<void>((resolve: () => void, _): void => {
			commonsOutputDoing(`Starting Express server on port ${this.port}`);
			
			this.server.listen(this.port, (): void => {
				commonsOutputSuccess();
				resolve();
			});
		});
	}
	
	public close() {
		this.server.close();
	}
	
	// See note 1 at top
	public serveStatic(
			srcDirPath: string,
			virtualPath: string,
			cacheMaxAgeSeconds: number = 60 * 60 * 24	// 24 hours
	): void {
		let absolute: string = srcDirPath;
		if (!(/^\//.test(srcDirPath))) absolute = commonsFileRootRelativePath(srcDirPath);
		
		this.ex.use(
				virtualPath,
				express.static(
						absolute,
						{
								setHeaders: (
										res: express.Response,
										p: string,	// "path" is shadowed
										_stat: unknown
								): void => {
									if (/\/index\.html$/.test(p)) {
										// See note 1 at top
										commonsExpressUseCacheIfUnchanged(res);
										
										// See note 2 at top
										if (this.usingNpmResources) {
											commonsExpressAutoNpm1985TimestampFile(p, res, this.nodeStartTime);
										}
									} else {
										const ext: RegExpMatchArray|null = p.match(/\.([a-z0-9]+)$/);
										
										if (ext) {
											if (COMMONS_ANGULAR_EXTS_NOCACHE.includes(ext[1])) {
												commonsExpressNoCache(res);
												
												// See note 2 at top
												if (this.usingNpmResources) {
													commonsExpressAutoNpm1985TimestampFile(p, res, new Date());
												}
											}
											if (COMMONS_ANGULAR_EXTS_CACHE.includes(ext[1])) {
												commonsExpressCache(res, cacheMaxAgeSeconds);
											}
											if (COMMONS_ANGULAR_EXTS_USE_CACHE_IF_UNCHANGED.includes(ext[1])) {
												commonsExpressUseCacheIfUnchanged(res);
												
												// See note 2 at top
												if (this.usingNpmResources) {
													commonsExpressAutoNpm1985TimestampFile(p, res, this.nodeStartTime);
												}
											}
										}
									}
								}
						}
				)
		);
	}

	// See note 1 at top
	public angular(
			src: string = 'angular',
			url: string = '/angular/',
			cacheMaxAgeSeconds: number = 60 * 60 * 24	// 24 hours
	): void {
		// See note 2 at top
		this.setUsingNpmResources(true);
		
		this.serveStatic(src, url, cacheMaxAgeSeconds);
		
		let absolute: string = src;
		if (!(/^\//.test(src))) absolute = commonsFileRootRelativePath(src);
		
		this.ex.get(`${url}*`, (_: express.Request, res: express.Response): void => {
			commonsExpressUseCacheIfUnchanged(res);
			
			// See note 2 at top
			commonsExpressSetLastModified(res, this.nodeStartTime);

			res.sendFile(path.join(absolute, '/index.html'));
		});
	}

	public head(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.head(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => void handle(
						ECommonsHttpMethod.HEAD,
						request,
						response,
						stricts,
						handler
				)
		);
	}

	public get(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.get(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => void handle(
						ECommonsHttpMethod.GET,
						request,
						response,
						stricts,
						handler
				)
		);
	}

	public post(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.post(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => {
					void (async (): Promise<void> => {
						await commonsExpressConsiderMultiPart(request);
						await handle(
								ECommonsHttpMethod.POST,
								request,
								response,
								stricts,
								handler
						);
					})();
				}
		);
	}

	public put(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.put(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => {
					void (async (): Promise<void> => {
						await commonsExpressConsiderMultiPart(request);
						await handle(
								ECommonsHttpMethod.PUT,
								request,
								response,
								stricts,
								handler
						);
					})();
				}
		);
	}

	public patch(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.patch(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => {
					void (async (): Promise<void> => {
						await commonsExpressConsiderMultiPart(request);
						await handle(
								ECommonsHttpMethod.PATCH,
								request,
								response,
								stricts,
								handler
						);
					})();
				}
		);
	}

	public delete(
			query: string,
			handler: TCommonsExpressHandler
	): void {
		const reduced: string = commonsRemoveStrictsFromPathDefinition(query);
		const stricts: TCommonsHttpStrictParam[] = commonsBuildStrictsFromPathDefinition(query);

		this.ex.delete(
				reduced,
				(
						request: express.Request,
						response: express.Response
				): void => void handle(
						ECommonsHttpMethod.DELETE,
						request,
						response,
						stricts,
						handler
				)
		);
	}
}
