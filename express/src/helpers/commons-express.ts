import * as express from 'express';
import formidable, { Fields, Files } from 'formidable';
import IncomingForm from 'formidable/Formidable';

import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyDate,
		commonsTypeHasPropertyObject,
		commonsDateYmdHisToDate,
		TPropertyObject,
		commonsTypeIsArray
} from 'tscommons-es-core';
import { TDateRange } from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_DATETIME_YMDHIS } from 'tscommons-es-core';

import {
		commonsFileIsFile,
		commonsFileLastModified,
		commonsFileRm
} from 'nodecommons-es-file';
import { commonsHttpCache, commonsHttpNoCache, commonsHttpSetLastModified } from 'nodecommons-es-http';

import { ICommonsRequestWithStrictParams } from '../interfaces/icommons-request-with-strict-params';

export function commonsExpressNoCache(res: express.Response): void {
	commonsHttpNoCache(res);
}

export function commonsExpressCache(res: express.Response, seconds: number): void {
	commonsHttpCache(res, seconds);
}

export function commonsExpressUseCacheIfUnchanged(res: express.Response): void {
	// see "Requiring revalidation": https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
	// Makes browsers check for a new version, but not download if it isn't changed
	commonsExpressUseCacheIfUnchanged(res);
}

function isValidIp(value: any): boolean {
	if (value === undefined || value === null) return false;
	if ('string' !== typeof value) return false;

	// ipv4
	if (/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}/.test(value)) return true;
	
	// ipv6
	if (/^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/.test(value)) return true;

	return false;
}

export function commonsExpressGetIpAddress(req: express.Request, real: boolean = false): string|undefined {
	if (!real) {
		for (const header of 'X-Client-IP,X-Forwarded-For,CF-Connecting-IP,True-Client-Ip,X-Real-IP,X-Cluster-Client-IP,X-Forwarded,Forwarded-For,Forwarded'.split(',')) {
			const value: string|null|undefined = req.get(header);
			if (value === undefined || value === null || value === '' || 'string' !== typeof value) continue;
			
			// check for multiple
			const ips: string[] = value
					.split(/[,; ]/)
					.map((raw: string): string => raw.trim().toLowerCase())
					.filter((test: string): boolean => isValidIp(test));
			
			if (ips.length > 0) return ips[0];
		}
	}

	if (req.socket && isValidIp(req.socket.remoteAddress)) return req.socket.remoteAddress;
	if (req['info'] && isValidIp((req['info'] as TPropertyObject)['remoteAddress'])) return (req['info'] as TPropertyObject)['remoteAddress'] as string;
	
	return undefined;
}

export function commonsExpressGetUserAgent(req: express.Request): string|undefined {
	const value: any = req.get('User-Agent');
	if (value === undefined || value === null || value === '' || 'string' !== typeof value) return undefined;
	
	return value;
}

export function commonsExpressGetStrictParamsDateRange(
		request: ICommonsRequestWithStrictParams,
		fromKey: string = 'from',
		toKey: string = 'to'
): TDateRange|undefined {
	if (!commonsTypeHasPropertyDate(request, fromKey)) return undefined;
	if (!commonsTypeHasPropertyDate(request, toKey)) return undefined;

	return {
			from: request.strictParams[fromKey] as Date,
			to: request.strictParams[toKey] as Date
	};
}

function getRequestAspectDateRange(
		request: express.Request,
		aspect: string,
		fromKey: string = 'from',
		toKey: string = 'to'
): TDateRange|undefined {
	if (!request[aspect]) return undefined;
	
	if (!commonsTypeHasPropertyString(request[aspect], fromKey)) return undefined;
	const fromRange: string = (request[aspect] as TPropertyObject)[fromKey] as string;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(fromRange)) return undefined;
	
	if (!commonsTypeHasPropertyString(request[aspect], toKey)) return undefined;
	const toRange: string = (request[aspect] as TPropertyObject)[toKey] as string;
	if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(toRange)) return undefined;
	
	return {
			from: commonsDateYmdHisToDate(fromRange),
			to: commonsDateYmdHisToDate(toRange)
	};
}

export function commonsExpressGetQueryDateRange(
		request: express.Request,
		fromKey: string = 'from',
		toKey: string = 'to'
): TDateRange|undefined {
	return getRequestAspectDateRange(
			request,
			'query',
			fromKey,
			toKey
	);
}

export function commonsExpressGetBodyDateRange(
		request: express.Request,
		fromKey: string = 'from',
		toKey: string = 'to'
): TDateRange|undefined {
	return getRequestAspectDateRange(
			request,
			'body',
			fromKey,
			toKey
	);
}

// See note 2 at top of CommonsExpressServer
export function commonsExpressIsNpm1985TimestampFile(file: string): boolean {
	if (!commonsFileIsFile(file)) return false;
	
	const modified: Date = commonsFileLastModified(file);
	return modified.getFullYear() === 1985;
}

// See note 2 at top of CommonsExpressServer
export function commonsExpressSetLastModified(res: express.Response, timestamp: Date): void {
	commonsHttpSetLastModified(res, timestamp);
}

// See note 2 at top of CommonsExpressServer
export function commonsExpressAutoNpm1985TimestampFile(file: string, res: express.Response, timestamp: Date): void {
	if (!commonsExpressIsNpm1985TimestampFile(file)) return;
	commonsExpressSetLastModified(res, timestamp);
}

function isFile(test: unknown): test is formidable.File {
	if (!commonsTypeHasPropertyNumber(test, 'size')) return false;
	if (!commonsTypeHasPropertyString(test, 'path')) return false;
	
	// all other fields are optional
	
	return true;
}

export function commonsExpressCleanupMultiPartFiles(request: express.Request): void {
	if (!commonsTypeHasPropertyObject(request, 'files')) return;
	
	const files: { [key: string]: unknown } = request['files'] as TPropertyObject;
	
	for (const key of Object.keys(files)) {
		const file: unknown = files[key];
		if (commonsTypeIsArray(file)) {
			for (const f of file) {
				if (!isFile(f)) continue;
			
				try {
					commonsFileRm(f.filepath);
				} catch (e) {
					// ignore
				}
			}
		} else {
			if (!isFile(file)) continue;
			
			try {
				commonsFileRm(file.filepath);
			} catch (e) {
				// ignore
			}
		}
	}
}

// Version 3 of Formidable contains a breaking change whereby ALL fields are string arrays, so need to compensate for this
function pre3Behaviour<T>(key: string, values: T[]|undefined): T|T[]|undefined {
	if (!values) return undefined;
	if (values.length > 1) return values;

	if (key.endsWith('[]')) return values;

	if (values.length === 0) return undefined;
	return values[0];
}

export function commonsExpressConsiderMultiPart(request: express.Request): Promise<void> {
	return new Promise((resolve: () => void, reject: (error: Error) => void): void => {
		const contentType: string|undefined = request.header('content-type');
		if (!contentType || !/^multipart\/form-data;/.test(contentType)) {
			resolve();
			return;
		}
	
		const form: IncomingForm = new formidable.IncomingForm({ multiples: true });
		form.parse(
				request,
				(
						err: Error,
						fields: Fields,
						files: Files
				): void => {
					if (err) {
						reject(err);
						return;
					}
					
					for (const key of Object.keys(fields)) {
						const value: string|string[]|undefined = pre3Behaviour(key, fields[key]);
						if (value !== undefined) (request.body as TPropertyObject)[key] = value;
					}

					request['files'] = {};
					for (const key of Object.keys(files)) {
						const value: formidable.File|formidable.File[]|undefined = pre3Behaviour(key, files[key]);
						(request['files'] as TPropertyObject)[key] = value;
					}
					
					resolve();
				}
		);
	});
}
