import * as http from 'http';

import express from 'express';

import { CommonsStrictExpressServer } from '../servers/commons-strict-express.server';

export function commonsExpressBuildDefaultServer(
		port: number,
		bodyParserLimit?: number|string
): CommonsStrictExpressServer {
	const ex: express.Express = express();
	const server: http.Server = http.createServer(ex);

	return new CommonsStrictExpressServer(
			ex,
			server,
			port,
			bodyParserLimit
	);

}
