export const COMMONS_ANGULAR_EXTS_CACHE: string[] = [
		'js',
		'ico',
		'css',
		'woff2',
		'woff',
		'ttf'
];

export const COMMONS_ANGULAR_EXTS_NOCACHE: string[] = [
		'json'
];

export const COMMONS_ANGULAR_EXTS_USE_CACHE_IF_UNCHANGED: string[] = [
		'png',
		'jpg',
		'svg'
];
