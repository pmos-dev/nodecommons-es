import {
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyNumberOrUndefined
} from 'tscommons-es-core';

import { ICommonsHttpConfig, isICommonsHttpConfig } from 'nodecommons-es-http';

export interface ICommonsExpressConfig extends ICommonsHttpConfig {
		bodyParserLimit?: string|number;
}

export function isICommonsExpressConfig(test: any): test is ICommonsExpressConfig {
	if (!isICommonsHttpConfig(test)) return false;

	if (
		!commonsTypeHasPropertyStringOrUndefined(test, 'bodyParserLimit')
		&& !commonsTypeHasPropertyNumberOrUndefined(test, 'bodyParserLimit')
	) return false;

	return true;
}
