import * as express from 'express';

import { ICommonsStrictParamsRequest } from 'nodecommons-es-http';

export interface ICommonsRequestWithStrictParams extends express.Request, ICommonsStrictParamsRequest {}
