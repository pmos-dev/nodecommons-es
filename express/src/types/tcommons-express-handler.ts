import * as express from 'express';

import { TCommonsHttpHandler } from 'nodecommons-es-http';

import { ICommonsRequestWithStrictParams } from '../interfaces/icommons-request-with-strict-params';

export type TCommonsExpressHandler = TCommonsHttpHandler<
		ICommonsRequestWithStrictParams,
		express.Response
>;
