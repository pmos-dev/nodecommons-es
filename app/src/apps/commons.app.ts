import * as os from 'os';
import * as nodePath from 'path';

import {
		commonsTypeIsObject,
		commonsStringSnakeCase,
		TEncodedObject
} from 'tscommons-es-core';
import { TPropertyObject } from 'tscommons-es-core';
import { commonsStringKebabCase } from 'tscommons-es-core';
import { CommonsConfig } from 'tscommons-es-config';

import { CommonsConfigFile } from 'nodecommons-es-config';
import { CommonsArgs } from 'nodecommons-es-cli';
import {
		commonsEnvGetStringOrUndefined,
		commonsOutputDebug,
		commonsOutputAlert,
		commonsOutputDoing,
		commonsOutputSuccess,
		commonsOutputFail,
		commonsOutputError
} from 'nodecommons-es-cli';
import {
		commonsFileIsFile,
		commonsFileCwdRelativeOrAbsolutePath,
		commonsFileIsDirectory,
		commonsFileRootRelativePath,
		commonsFileReadJsonFile
} from 'nodecommons-es-file';
import {
		commonsSystemdInstall,
		commonsSystemdUninstall
} from 'nodecommons-es-process';

import { ICommonsPackageJson, isICommonsPackageJson } from '../interfaces/icommons-package-json';

import { CommonsBareApp } from './commons-bare.app';

// we're doing this in here rather than in a nodecommons-process or similar, as it's very bespoke to this code
enum ESimplifiedPlatform {
		POSIX = 'posix',
		WIN32 = 'win32'
}

// we're doing this in here rather than in a nodecommons-process or similar, as it's very bespoke to this code
function simplifiedPlatform(): ESimplifiedPlatform {
	switch (os.platform()) {
		case 'aix':
		case 'android':
		case 'darwin':
		case 'freebsd':
		case 'linux':
		case 'netbsd':
		case 'openbsd':
		case 'sunos':
			return ESimplifiedPlatform.POSIX;
		case 'win32':
		case 'cygwin':
			return ESimplifiedPlatform.WIN32;
		default:
			throw new Error('OS platform not supported for CommonsFile');
	}
}

export class CommonsApp extends CommonsBareApp {
	public static getConfigPaths(
			name?: string,
			args?: CommonsArgs,
			argsKey: string = 'config-path',
			envVar?: string,
			configFile?: string
	): string[] {
		const attempts: string[] = [];
		
		if (args) {
			const path: string|undefined = args.getStringOrUndefined(argsKey);
			if (path !== undefined) attempts.push(path);
		}
		
		if (!envVar && name) {
			envVar = `${commonsStringSnakeCase(name).toUpperCase()}_CONFIG`;
			commonsOutputDebug(`Calculated environment variable for config is ${envVar}`);
		}
		
		if (envVar) {
			const env: string|undefined = commonsEnvGetStringOrUndefined(envVar);
			if (env !== undefined) attempts.push(env);
		}
		
		const relative: string = commonsFileCwdRelativeOrAbsolutePath('config');
		if (commonsFileIsDirectory(relative)) attempts.push(relative);

		if (name) {
			switch (simplifiedPlatform()) {
				case ESimplifiedPlatform.POSIX:
					const etc: string = commonsFileCwdRelativeOrAbsolutePath(`/etc/${name}`);
					commonsOutputDebug(`Calculated /etc path for config is ${etc}`);
					if (commonsFileIsDirectory(etc)) attempts.push(etc);

					const home: string = nodePath.join(os.homedir(), `.${name}`);
					commonsOutputDebug(`Calculated $HOME path for config is ${home}`);
					if (commonsFileIsDirectory(home)) attempts.push(home);

					break;
				case ESimplifiedPlatform.WIN32:
					// not implemented for win32 yet
					break;
			}
		}

		if (configFile) {
			// derive the configPath from the configFile absolute path
			const parts: string[] = configFile.split(nodePath.sep);
			parts.pop();
			const result: string = parts.join(nodePath.sep);
			if (commonsFileIsDirectory(result)) attempts.push(result);
		}

		if (name) {
			const localNodeModulesConfig: string = commonsFileRootRelativePath(nodePath.join('node_modules', name, 'config'));
			commonsOutputDebug(`Calculated cwd node_modules path for config is ${localNodeModulesConfig}`);
			if (commonsFileIsDirectory(localNodeModulesConfig)) attempts.push(localNodeModulesConfig);

			const globalNodeModulesConfig: string = commonsFileRootRelativePath('config');
			commonsOutputDebug(`Calculated global node_modules path for config is ${globalNodeModulesConfig}`);
			if (commonsFileIsDirectory(globalNodeModulesConfig)) attempts.push(globalNodeModulesConfig);
		}
		
		commonsOutputDebug(`Searching for config in paths: ${attempts.join('; ')}`);
		
		return attempts;
	}
	
	protected config: CommonsConfig | CommonsConfigFile;

	private configPaths: string[];
	
	constructor(
			private name: string,
			protected configFile?: string,
			private configPath?: string,	// NB, a direct path, not the name of the args parameter
			envVar?: string,
			argsKey: string = 'config-path',
			allowNoConfig: boolean = false
	) {
		super();

		if (this.getArgs().hasProperty('config', 'string')) {
			this.configFile = this.getArgs().getString('config');
		}
		
		if (!this.configFile) {
			this.configFile = `${commonsStringKebabCase(this.name)}.json`;
		}

		if (this.configPath) {
			this.configPaths = [ this.configPath ];
		} else {
			this.configPaths = CommonsApp.getConfigPaths(
					this.name,
					this.getArgs(),
					argsKey,
					envVar,
					this.configFile
			);
			if (this.configPaths.length === 0 && !allowNoConfig) throw new Error('Unable to derive config path');
		}

		if (this.configPaths.length === 0 && allowNoConfig) {
			this.config = new CommonsConfig({});
		} else {
			this.config = this.loadConfigFile(this.configFile);
		}
	}
	
	public getConfigArea(area: string, optional: boolean = false): TPropertyObject {
		const result: unknown = this.config.getObject(area, optional ? {} : undefined);
		if (!commonsTypeIsObject(result)) throw new Error('Invalid config area');
		
		return result;
	}
	
	public setConfigArea<T extends TEncodedObject>(area: string, config: T): void {
		this.config.setObject(area, config);
	}
	
	protected locateConfigFile(file: string): string {
		const asAbsolute: string = commonsFileCwdRelativeOrAbsolutePath(file);
		if (file === asAbsolute) return file;
		
		for (const path of this.configPaths) {
			//const p: string = /^\//.test(path) ? path : commonsFileRootRelativePath(path);
			const attempt: string = nodePath.join(path, file);
			commonsOutputDebug(`Looking for config file at ${attempt}`);
			if (commonsFileIsFile(attempt)) {
				commonsOutputDebug('Found config file.');
				return attempt;
			}
		}

		throw new Error(`Unable to locate config file ${file} at any config path location`);
	}
	
	public loadRawJsonConfigFile(file: string): unknown {
		const location: string = this.locateConfigFile(file);
		
		return commonsFileReadJsonFile(location);
	}
	
	public loadConfigFile(file: string): CommonsConfigFile {
		const location: string = this.locateConfigFile(file);
		
		return new CommonsConfigFile(location);
	}
	
	public saveConfig(): void {
		if (!(this.config instanceof CommonsConfigFile)) throw new Error('Cannot save to a non-file based config');
		this.config.save();
	}
	
	public getPackageJson(): ICommonsPackageJson|undefined {
		const attempts: string[] = [];
		
		const relative: string = commonsFileCwdRelativeOrAbsolutePath('package.json');
		if (commonsFileIsFile(relative)) attempts.push(relative);

		const localNodeModulesPackage: string = commonsFileRootRelativePath(nodePath.join('node_modules', this.name, 'package.json'));
		commonsOutputDebug(`Calculated cwd node_modules path for package.json is ${localNodeModulesPackage}`);
		if (commonsFileIsFile(localNodeModulesPackage)) attempts.push(localNodeModulesPackage);

		const globalNodeModulesPackage: string = commonsFileRootRelativePath('package.json');
		commonsOutputDebug(`Calculated global node_modules path for package.json is ${globalNodeModulesPackage}`);
		if (commonsFileIsFile(globalNodeModulesPackage)) attempts.push(globalNodeModulesPackage);
		
		if (attempts.length === 0) {
			commonsOutputAlert('package.json could not be located');
			return undefined;
		}
		
		const first: string = attempts.shift()!;
		commonsOutputDebug(`Found package.json in path: ${first}`);

		const attempt: unknown = commonsFileReadJsonFile<ICommonsPackageJson>(first);
		if (!isICommonsPackageJson(attempt)) return undefined;
		
		return attempt;
	}
	
	public installSystemd(
			command: string,
			description: string,
			group?: string,
			autoJoinGroup: boolean = false,
			restart: string = 'always',
			restartSec: number = 5,
			autoEnable: boolean = true,
			autoStart: boolean = false,
			systemdServiceName?: string
	): void {
		switch (simplifiedPlatform()) {
			case ESimplifiedPlatform.POSIX:
				const etcConfig: CommonsConfigFile = new CommonsConfigFile(`/etc/${this.name}/${this.name}.json`);
				etcConfig.cloneConfig(this.config);
				
				commonsSystemdInstall(
						systemdServiceName === undefined ? this.name : systemdServiceName,
						command,
						description,
						etcConfig,
						undefined,
						undefined,
						group,
						autoJoinGroup,
						restart,
						restartSec,
						autoEnable,
						autoStart
				);
				break;
			case ESimplifiedPlatform.WIN32:
				throw new Error('Systemd not supported under win32');
		}
	}
	
	public uninstallSystemd(
			group?: string,
			systemdServiceName?: string
	): void {
		switch (simplifiedPlatform()) {
			case ESimplifiedPlatform.POSIX:
				commonsSystemdUninstall(
						systemdServiceName === undefined ? this.name : systemdServiceName,
						group
				);
				break;
			case ESimplifiedPlatform.WIN32:
				throw new Error('Systemd not supported under win32');
		}
	}
	
	public autoSystemd(
			command: string,
			description: string,
			group?: string,
			autoJoinGroup: boolean = false,
			restart: string = 'always',
			restartSec: number = 5,
			autoEnable: boolean = true,
			autoStart: boolean = false,
			systemdServiceName?: string
	): void|never {
		if (!this.getArgs().hasProperty('systemd', 'string')) return;
		
		const direction: string = this.getArgs().getString('systemd');
		
		switch (direction) {
			case 'install':
				commonsOutputDoing('Installing systemd service');
				
				try {
					this.installSystemd(
							command,
							description,
							group,
							autoJoinGroup,
							restart,
							restartSec,
							autoEnable,
							autoStart,
							systemdServiceName
					);
					commonsOutputSuccess();
				} catch (e) {
					commonsOutputFail((e as Error).message);
					commonsOutputError((e as Error).message);
				}
				break;
			case 'uninstall':
				commonsOutputDoing('Uninstalling systemd service');
				
				try {
					this.uninstallSystemd();
					commonsOutputSuccess();
				} catch (e) {
					commonsOutputFail((e as Error).message);
					commonsOutputError((e as Error).message);
				}
				break;
			default:
				commonsOutputError('Unknown command for --systemd. Use either install or uninstall');
				process.exit(1);
		}
		
		process.exit(0);
	}
}
