import { ICommonsStateful, ECommonsRunState } from 'tscommons-es-async';

import { CommonsArgs } from 'nodecommons-es-cli';
import {
		commonsOutputAlert,
		commonsOutputDebug,
		commonsOutputError
} from 'nodecommons-es-cli';
import {
		commonsGracefulAbortAddCallback,
		commonsGracefulAbortRemoveCallback
} from 'nodecommons-es-process';

export function assertSet(
		property: unknown,
		name: string
): asserts property {
	if (!property) throw new Error(`ASSERTION FAIL: ${name} has not been set.`);
}

export class CommonsBareApp implements ICommonsStateful {
	private args: CommonsArgs;

	private state: ECommonsRunState;
	private aborted: boolean = false;
	private error: Error|undefined;
	
	constructor() {
		this.args = new CommonsArgs();

		this.state = ECommonsRunState.PRE;
	}
	
	protected getArgs(): CommonsArgs {
		return this.args;
	}
	
	public isAborted(): boolean {
		return this.aborted;
	}
	
	public abort(): void {
		this.aborted = true;
	}
	
	public getState(): ECommonsRunState {
		return this.state;
	}
	
	public getError(): Error|undefined {
		return this.error;
	}
	
	protected async init(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		const gracefulAbortId: string = commonsGracefulAbortAddCallback((): void => {
			commonsGracefulAbortRemoveCallback(gracefulAbortId);

			commonsOutputAlert('SIGINT abort flag is set. Aborting application.');
			this.abort();
		});
	}
	
	protected async run(): Promise<void> {
		// to be overridden if desired
	}
	protected async shutdown(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		commonsOutputDebug('Shutting down CommonsAppBare');
		
		// to be overridden if desired
	}
	
	public async start(): Promise<void> {
		try {
			await this.init();
			
			this.state = ECommonsRunState.RUNNING;
			await this.run();
			
			if (this.isAborted()) this.state = ECommonsRunState.ABORTED;
			else this.state = ECommonsRunState.COMPLETED;
			
			await this.shutdown();
		} catch (e) {
			this.state = ECommonsRunState.ERROR;
			this.error = e as Error;
			commonsOutputError(`Application error: ${this.error.message}`);
		}
	}
}
