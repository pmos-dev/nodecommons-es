import {
		commonsTypeHasPropertyString,
		commonsTypeHasPropertyStringOrUndefined,
		commonsTypeHasPropertyTOrUndefined,
		commonsTypeIsStringKeyObject
} from 'tscommons-es-core';

export type TCommonsDependencies = {
	[ name: string ]: string;
};

export function isTCommonsDependencies(test: any): test is TCommonsDependencies {
	return commonsTypeIsStringKeyObject(test);
}

export interface ICommonsPackageJson {
	name: string;
	version: string;
	description?: string;
	dependencies?: TCommonsDependencies;
	devDependencies?: TCommonsDependencies;
}

export function isICommonsPackageJson(test: any): test is ICommonsPackageJson {
	if (!commonsTypeHasPropertyString(test, 'name')) return false;
	if (!commonsTypeHasPropertyString(test, 'version')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'description')) return false;
	if (!commonsTypeHasPropertyTOrUndefined<TCommonsDependencies>(test, 'dependencies', isTCommonsDependencies)) return false;
	if (!commonsTypeHasPropertyTOrUndefined<TCommonsDependencies>(test, 'devDependencies', isTCommonsDependencies)) return false;

	return true;
}
