import * as mysql from 'mysql2/promise';

import { TPropertyObject, commonsObjectMapObject } from 'tscommons-es-core';

import { ECommonsDatabaseEngine } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';

import { ICommonsMysqlCredentials } from '../interfaces/icommons-mysql-credentials';

export class CommonsMysqlService extends CommonsSqlDatabaseService<ICommonsMysqlCredentials> {
	private database?: mysql.Connection|mysql.Pool;
	
	constructor(
			credentials: ICommonsMysqlCredentials,
			private pool: boolean = false,
			private poolSize: number = 5,
			private queueLimit: number = 5
	) {
		super(
				ECommonsDatabaseEngine.MYSQL,
				credentials
		);
	}

	public async connect(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		const internalCredentials: TPropertyObject<string|boolean|number|undefined> = {
				host: this.credentials.host,
				user: this.credentials.user,
				password: this.credentials.password,
				database: this.credentials.name
		};

		if (this.pool) {
			internalCredentials['waitForConnections'] = true;
			internalCredentials['connectionLimit'] = this.poolSize;
			internalCredentials['queueLimit'] = this.queueLimit;
			internalCredentials['namedPlaceholders'] = true;

			this.database = mysql.createPool(internalCredentials);
		} else {
			this.database = await mysql.createConnection(internalCredentials);
			this.database.config.namedPlaceholders = true;
		}
		if (this.database === undefined) throw new Error('Unable to create a single or pool connection database');
	}
	
	public async doesTableExist(name: string): Promise<boolean> {
		return (await this.internalSelect(
				`SHOW TABLES LIKE ${this.table(name)}`
		)).length > 0;
	}
	
	public async internalSelect(
			sql: string,
			params?: TPropertyObject
	): Promise<TPropertyObject[]> {
		if (!this.database) throw new Error('No connection to database');

		try {
			let rows: { [key: string]: unknown }[] = [];

			if (params) {
				const internal: [ TPropertyObject<unknown>[], unknown ] = (await this.database.execute(sql, params)) as [ TPropertyObject<unknown>[], unknown ];
				rows = internal[0];
			} else {
				const internal: [ TPropertyObject<unknown>[], unknown ] = (await this.database.execute(sql)) as [ TPropertyObject<unknown>[], unknown ];
				rows = internal[0];
			}

			// get rid of the BinaryRow objects and turn them back into normal objects
			return rows
					.map((row: TPropertyObject): TPropertyObject => commonsObjectMapObject(
							row,
							(value: unknown): unknown => value
					));
		} catch (e) {
			throw e;
		}
	}

	public async internalNone(
			sql: string,
			params?: TPropertyObject
	): Promise<void> {
		if (!this.database) throw new Error('No connection to database');

		try {
			if (params) {
				await this.database.execute(sql, params);
			} else {
				await this.database.execute(sql);
			}
		} catch (e) {
			console.log(e);
			throw e;
		}
	}

	protected async internalTransactionBegin(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
	
	protected async internalTransactionCommit(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
	
	protected async internalTransactionRollback(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.database) throw new Error('No connection to database');

		// not implemented yet
	}
}
