import { commonsTypeHasPropertyString } from 'tscommons-es-core';

import { ICommonsCredentials, isICommonsCredentials } from 'nodecommons-es-database';

export interface ICommonsMysqlCredentials extends ICommonsCredentials {
		user: string;
		password: string;
}

export function isICommonsMysqlCredentials(test: any): test is ICommonsMysqlCredentials {
	if (!isICommonsCredentials(test)) return false;

	if (!commonsTypeHasPropertyString(test, 'user')) return false;
	if (!commonsTypeHasPropertyString(test, 'password')) return false;

	return true;
}
