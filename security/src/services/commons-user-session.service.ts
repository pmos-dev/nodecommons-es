import { ICommonsSession } from 'tscommons-es-session';
import { ICommonsUser } from 'tscommons-es-session';

import { CommonsSessionService } from './commons-session.service';

export abstract class CommonsUserSessionService<T extends ICommonsUser> extends CommonsSessionService<T> {
	constructor(
			allowMultipleSessionsForSameUser: boolean = false
	) {
		super(allowMultipleSessionsForSameUser);
	}

	protected isEqual(a: T, b: T): boolean {
		if (a.username !== b.username) return false;
		
		return true;
	}
	
	protected async isStillValid(user: T): Promise<boolean> {	// eslint-disable-line @typescript-eslint/require-await
		if (!user.enabled) return false;
		
		return true;
	}
	
	public authenticate(user: T): ICommonsSession<T>|undefined {
		if (!user.enabled) {
			// if an account is disabled, abort any existing sessions
			
			this.abort(user);
			return undefined;
		}
		
		return this.create(user);
	}
}
