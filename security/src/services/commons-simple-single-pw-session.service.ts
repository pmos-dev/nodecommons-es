import { ICommonsSession } from 'tscommons-es-session';

import { CommonsSessionService } from './commons-session.service';

type TEmptyObject = { [ key: string ]: never };

export class CommonsSimpleSinglePwSessionService extends CommonsSessionService<TEmptyObject> {
	constructor(
			private password: string
	) {
		super(true);
	}

	protected isEqual(_a: TEmptyObject, _b: TEmptyObject): boolean {
		return true;
	}
	
	protected async isStillValid(_user: TEmptyObject): Promise<boolean> {	// eslint-disable-line @typescript-eslint/require-await
		return true;
	}
	
	public authenticate(password: string): ICommonsSession<TEmptyObject>|undefined {
		if (password !== this.password) return undefined;
		
		return this.create({});
	}
}
