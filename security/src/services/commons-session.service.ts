import { ICommonsSession } from 'tscommons-es-session';

import { commonsHashRandomSha256 } from '../helpers/commons-hash';

export abstract class CommonsSessionService<T> {
	private sessions: ICommonsSession<T>[] = [];
	
	constructor(
			private allowMultipleSessionsForSameUser: boolean = false
	) {}
	
	protected abstract isEqual(a: T, b: T): boolean;
	protected abstract isStillValid(data: T): Promise<boolean>;
	
	public create(data: T): ICommonsSession<T> {
		if (!this.allowMultipleSessionsForSameUser) this.abort(data);	// clear out any existing sessions for the same user

		const session: ICommonsSession<T> = {
				sid: commonsHashRandomSha256(),
				start: new Date(),
				last: new Date(),
				data: data
		};
		
		this.sessions.push(session);
		
		return session;
	}
	
	public destroy(session: ICommonsSession<T>): boolean {
		const match: ICommonsSession<T>|undefined = this.sessions
				.find((s: ICommonsSession<T>): boolean => s.sid === session.sid);
		if (!match) return false;
		
		this.sessions = this.sessions
				.filter((s: ICommonsSession<T>): boolean => s.sid !== session.sid);
		
		return true;
	}
	
	public abort(data: T): boolean {
		const existing: ICommonsSession<T>|undefined = this.sessions
				.find((s: ICommonsSession<T>): boolean => this.isEqual(data, s.data));
		if (!existing) return false;
		
		this.destroy(existing);
		return true;
	}
	
	public async validate(sid: string): Promise<ICommonsSession<T>|undefined> {
		const session: ICommonsSession<T>|undefined = this.sessions
				.find((s: ICommonsSession<T>): boolean => s.sid === sid);
				
		if (!session) return undefined;
		
		if (!(await this.isStillValid(session.data))) {
			this.destroy(session);
			return undefined;
		}
		
		return session;
	}
}
