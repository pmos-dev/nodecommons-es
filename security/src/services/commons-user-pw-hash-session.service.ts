import { ICommonsSession } from 'tscommons-es-session';

import { commonsHashSha256 } from '../helpers/commons-hash';

import { ICommonsUserPwHash } from '../interfaces/icommons-user-pw-hash';

import { CommonsUserSessionService } from './commons-user-session.service';

export class CommonsUserPwHashSessionService<T extends ICommonsUserPwHash> extends CommonsUserSessionService<T> {
	constructor(
			private pepper?: string,
			allowMultipleSessionsForSameUser: boolean = false
	) {
		super(allowMultipleSessionsForSameUser);
	}

	public generatePwHash(pw: string, salt: string): string {
		const components: string[] = [
				salt,
				this.pepper || '',
				pw
		];
		
		return commonsHashSha256(components.join(''));
	}

	public authenticateWithPwHash(user: T, pw: string): ICommonsSession<T>|undefined {
		const compare: string = this.generatePwHash(pw, user.salt);
		
		if (user.pwHash !== compare) {
			// don't abort existing sessions for subsequent failed logins
			return undefined;
		}
		
		return this.authenticate(user);
	}
}
