import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ICommonsUser, isICommonsUser } from 'tscommons-es-session';

export interface ICommonsUserPwHash extends ICommonsUser {
		pwHash: string;
		salt: string;
}

export function isICommonsUserPwHash(
		test: unknown
): test is ICommonsUserPwHash {
	if (!isICommonsUser(test)) return false;
	
	if (!commonsTypeHasPropertyString(test, 'pwHash')) return false;
	if (!commonsTypeHasPropertyString(test, 'salt')) return false;
	
	return true;
}
