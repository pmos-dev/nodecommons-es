import * as crypto from 'crypto';

export function commonsHashMd5(data: string|Buffer): string {
	return crypto.createHash('md5').update(data).digest('hex');
}

export function commonsHashSha1(data: string|Buffer): string {
	return crypto.createHash('sha1').update(data).digest('hex');
}

export function commonsHashSha256(data: string|Buffer): string {
	return crypto.createHash('sha256').update(data).digest('hex');
}

function generateRandomData(iterations: number = 10000): string {
	const rands: number[] = [ new Date().getTime() ];
	for (let i = iterations; i-- > 0;) {
		rands.push(Math.random());
	}
	
	return rands
			.map((rand: number): string => rand.toString(36))
			.join('');
}

export function commonsHashRandomMd5(iterations: number = 10000): string {
	return commonsHashMd5(generateRandomData(iterations));
}

export function commonsHashRandomSha1(iterations: number = 10000): string {
	return commonsHashSha1(generateRandomData(iterations));
}

export function commonsHashRandomSha256(iterations: number = 10000): string {
	return commonsHashSha256(generateRandomData(iterations));
}
