// This is a true interface

import * as tfcore from '@tensorflow/tfjs-core';

export interface ICommonsAiTensorflowExportable {
	exportBuiltTrainingAndValidationXyTensors(): [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ];
}
