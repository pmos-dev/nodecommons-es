import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { ICommonsAiEarlyStopping } from 'nodecommons-es-ai';

import { TLeNetData } from '../types/tlet-net-data';

import { CommonsAiTensorflowSequentialOnehotEnumsTraining } from './tensorflow-sequential-onehot-enums';

export abstract class CommonsAiTensorflowLeNetTraining<E extends string> extends CommonsAiTensorflowSequentialOnehotEnumsTraining<
		TLeNetData,
		E,
		tfcore.Rank.R3
> {
	constructor(
			values: E[],
			training: Map<E, TLeNetData[]>,
			epochs: number,
			validation?: Map<E, TLeNetData[]>,
			earlyStopping?: ICommonsAiEarlyStopping,
			learningRate?: number,
			shuffle: boolean = true,
			batchSize?: number
	) {
		super(
				values,
				training,
				epochs,
				validation,
				earlyStopping,
				true,
				'categoricalCrossentropy',
				tfcore.train.adam(learningRate),
				[ 'accuracy' ],
				shuffle,
				batchSize
		);

	}

	public getOptimizer(): tfcore.AdamOptimizer {
		return this.optimizer as tfcore.AdamOptimizer;
	}

	protected override async asyncConvertInputToX(input: TLeNetData): Promise<tfcore.Tensor3D> {	// eslint-disable-line @typescript-eslint/require-await
		if (input.length !== (32 * 32)) throw new Error('LeNet requires 32x32 linear number array');

		return tfcore.tensor3d(input, [ 32, 32, 1 ]);
	}

	protected addInnerLayers(inputShape: number[]): void {
		if (!this.model) throw new Error('Model has not bee initialised');

		if (inputShape[0] !== 32 || inputShape[1] !== 32) throw new Error('LeNet requires 32x32 input layers');

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				inputShape: inputShape,
				kernelSize: [ 3, 3 ],
				filters: 6,
				strides: [ 1, 1 ],
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				kernelSize: [ 3, 3 ],
				filters: 16,
				strides: [ 1, 1 ],
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));

		(this.model as tflayers.Sequential).add(tflayers.layers.flatten());

		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: 120,
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: 84,
				activation: 'relu'
		}));
	}
}
