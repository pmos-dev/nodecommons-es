import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';
import * as tftypes from '@tensorflow/tfjs-layers/dist/types';

import { commonsArrayBestItem } from 'tscommons-es-core';
import { TCommonsAiGenericTrainingData, TCommonsAiOutputWithCertainty } from 'tscommons-es-ai';

import { ICommonsAiEarlyStopping } from 'nodecommons-es-ai';

import { CommonsAiTensorflowSequential, CommonsAiTensorflowSequentialTraining } from './tensorflow-sequential';

function flipMapData<
		InputT,
		E extends string
>(
		values: E[],
		data: Map<E, InputT[]>
): TCommonsAiGenericTrainingData<InputT, E>[] {
	const flipped: TCommonsAiGenericTrainingData<InputT, E>[] = [];

	for (const value of values) {
		for (const item of data.get(value) || []) {
			flipped.push({
					input: item,
					output: value
			});
		}
	}

	return flipped;
}

function convertPredictionToOutput<E extends string>(
		values: E[],
		prediction: tfcore.Tensor<tfcore.Rank.R1>,
		permitted?: E[]
): TCommonsAiOutputWithCertainty<E>|undefined {
	const array: number[] = Array.from(prediction.dataSync());

	type TMapping = { certainty: number; value: E };

	const mapping: TMapping[] = array
			.map((certainty: number, index: number): TMapping => ({
					certainty: certainty,
					value: values[index]
			}))
			.filter((map: TMapping): boolean => !permitted || permitted.includes(map.value));

	const best: TMapping|undefined = commonsArrayBestItem<TMapping>(
			mapping,
			(a: TMapping, b: TMapping): number => {
				if (a.certainty < b.certainty) return 1;
				if (a.certainty > b.certainty) return -1;
				return 0;
			}
	);
	if (!best) return undefined;

	return {
			output: best.value,
			certainty: best.certainty
	};
}

export abstract class CommonsAiTensorflowSequentialOnehotEnums<
		InputT,
		E extends string,
		XRankT extends tfcore.Rank
> extends CommonsAiTensorflowSequential<
		InputT,
		E,
		XRankT,
		tfcore.Rank.R1
> {
	private permitted: E[]|undefined;

	constructor(
			protected values: E[],
			asyncInput: boolean = false
	) {
		super(
				asyncInput,
				false
		);
	}

	public setPermittedPredictionValues(permitted?: E[]): void {
		this.permitted = permitted;
	}

	protected override convertPredictionToOutput(prediction: tfcore.Tensor<tfcore.Rank.R1>): TCommonsAiOutputWithCertainty<E>|undefined {
		return convertPredictionToOutput<E>(this.values, prediction, this.permitted);
	}
}

export abstract class CommonsAiTensorflowSequentialOnehotEnumsTraining<
		InputT,
		E extends string,
		XRankT extends tfcore.Rank
> extends CommonsAiTensorflowSequentialTraining<
		InputT,
		E,
		XRankT,
		tfcore.Rank.R1
> {
	private permitted: E[]|undefined;

	constructor(
			protected values: E[],
			training: Map<E, InputT[]>,
			epochs: number,
			validation?: Map<E, InputT[]>,
			earlyStopping?: ICommonsAiEarlyStopping,
			asyncInput: boolean = false,
			lossFunction: string = 'categoricalCrossentropy',
			optimizer: string|tfcore.Optimizer = tfcore.train.adam(0.01),
			metrics: (string|tftypes.LossOrMetricFn)[] = [ 'accuracy' ],
			shuffle: boolean = true,
			batchSize?: number
	) {
		super(
				flipMapData<InputT, E>(values, training),
				epochs,
				validation ? flipMapData<InputT, E>(values, validation) : undefined,
				earlyStopping,
				asyncInput,
				false,
				lossFunction,
				optimizer,
				metrics,
				shuffle,
				batchSize
		);
	}

	public setPermittedPredictionValues(permitted?: E[]): void {
		this.permitted = permitted;
	}

	protected abstract addInnerLayers(inputShape: number[]): void;

	protected addLayers(inputShape: number[]): void {
		if (!this.model) throw new Error('Model has not bee initialised');

		this.addInnerLayers(inputShape);

		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: this.values.length,
				activation: 'softmax'
		}));
	}

	protected override buildExampleToXY(example: TCommonsAiGenericTrainingData<InputT, E>): [tfcore.Tensor<XRankT>, tfcore.Tensor<tfcore.Rank.R1>] {
		const xs: tfcore.Tensor<XRankT> = this.convertInputToX(example.input);

		const index: number = this.values.indexOf(example.output);
		if (index === -1) throw new Error('Example output is not in the enum values');

		return [
				xs,
				tfcore.oneHot(index, this.values.length) as tfcore.Tensor1D
		];
	}

	protected override async asyncBuildExampleToXY(example: TCommonsAiGenericTrainingData<InputT, E>): Promise<[tfcore.Tensor<XRankT>, tfcore.Tensor<tfcore.Rank.R1>]> {
		const xs: tfcore.Tensor<XRankT> = await this.asyncConvertInputToX(example.input);

		const index: number = this.values.indexOf(example.output);
		if (index === -1) throw new Error('Example output is not in the enum values');

		return [
				xs,
				tfcore.oneHot(index, this.values.length) as tfcore.Tensor1D
		];
	}

	protected override convertPredictionToOutput(prediction: tfcore.Tensor<tfcore.Rank.R1>): TCommonsAiOutputWithCertainty<E>|undefined {
		return convertPredictionToOutput(this.values, prediction, this.permitted);
	}

	protected calculateQuality(actual: TCommonsAiOutputWithCertainty<E>, expected: E): [ number, number ] {
		if (actual.output === expected) {
			return [ actual.certainty, 0 ];
		} else {
			return [ 0, actual.certainty ];
		}
	}
}
