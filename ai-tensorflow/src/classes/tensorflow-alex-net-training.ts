import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';

import { ICommonsAiEarlyStopping } from 'nodecommons-es-ai';

import { CommonsAiTensorflowSequentialOnehotEnumsTraining } from './tensorflow-sequential-onehot-enums';

// 217x217x3 is too big to meaningfully write a tuple type
export type TAlexNetData = number[];

export abstract class CommonsAiTensorflowAlexNetTraining<E extends string> extends CommonsAiTensorflowSequentialOnehotEnumsTraining<
		TAlexNetData,
		E,
		tfcore.Rank.R3
> {
	constructor(
			values: E[],
			training: Map<E, TAlexNetData[]>,
			epochs: number,
			validation?: Map<E, TAlexNetData[]>,
			earlyStopping?: ICommonsAiEarlyStopping,
			learningRate?: number,
			shuffle: boolean = true,
			batchSize?: number
	) {
		super(
				values,
				training,
				epochs,
				validation,
				earlyStopping,
				true,
				'categoricalCrossentropy',
				tfcore.train.adam(learningRate),
				[ 'accuracy' ],
				shuffle,
				batchSize
		);
	}

	public getOptimizer(): tfcore.AdamOptimizer {
		return this.optimizer as tfcore.AdamOptimizer;
	}

	protected override async asyncConvertInputToX(input: TAlexNetData): Promise<tfcore.Tensor3D> {	// eslint-disable-line @typescript-eslint/require-await
		if (input.length !== (217 * 217 * 3)) throw new Error('AlexNet requires 217x217x3 linear number array');

		return tfcore.tensor3d(input, [ 217, 217, 3 ]);
	}

	protected addInnerLayers(_inputShape: number[]): void {
		if (!this.model) throw new Error('Model has not bee initialised');

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				inputShape: [ 217, 217, 3 ],
				kernelSize: [ 11, 11 ],
				filters: 96,
				strides: [ 4, 4 ],
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.batchNormalization());
		(this.model as tflayers.Sequential).add(tflayers.layers.maxPooling2d({poolSize: [ 3, 3 ], strides: [2, 2]}));

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				kernelSize: [ 5, 5 ],
				filters: 256,
				strides: [ 1, 1 ],
				activation: 'relu',
				padding: 'same'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.batchNormalization());
		(this.model as tflayers.Sequential).add(tflayers.layers.maxPooling2d({poolSize: [ 3, 3 ], strides: [2, 2]}));

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				kernelSize: [ 3, 3 ],
				filters: 384,
				strides: [ 1, 1 ],
				activation: 'relu',
				padding: 'same'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.batchNormalization());

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				kernelSize: [ 3, 3 ],
				filters: 384,
				strides: [ 1, 1 ],
				activation: 'relu',
				padding: 'same'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.batchNormalization());

		(this.model as tflayers.Sequential).add(tflayers.layers.conv2d({
				kernelSize: [ 3, 3 ],
				filters: 256,
				strides: [ 1, 1 ],
				activation: 'relu',
				padding: 'same'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.batchNormalization());

		(this.model as tflayers.Sequential).add(tflayers.layers.flatten());

		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: 4096,
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.dropout({rate: 0.5 }));
		
		(this.model as tflayers.Sequential).add(tflayers.layers.dense({
				units: 4096,
				activation: 'relu'
		}));
		(this.model as tflayers.Sequential).add(tflayers.layers.dropout({rate: 0.5 }));
	}
}
