import * as tfcore from '@tensorflow/tfjs-core';

import { CommonsShm } from 'nodecommons-es-process';

type TTensorMetadata = {
		shape: number[];
		dtype: string;
		size: number;
		// strides: number[];
		rank: number;
};

export class CommonsAiTensorflowPythonBridge {
	private shm: CommonsShm;

	constructor(
			context: string,
			shmPath?: string
	) {
		this.shm = new CommonsShm(context, shmPath);
	}

	private async read(id: string): Promise<[ TTensorMetadata, tfcore.Tensor ]|undefined> {
		const ms: string|undefined = await this.shm.readString(`${id}-metadata`);
		if (!ms) return undefined;

		const metadata: TTensorMetadata = JSON.parse(ms) as TTensorMetadata;

		switch (metadata.dtype) {
			case 'float32': {
				const data: Float32Array|undefined = await this.shm.readFloat32s(`${id}-data`);
				if (!data) return undefined;

				return [ metadata, tfcore.tensor(data) ];
			}
			case 'int32': {
				const data: Int32Array|undefined = await this.shm.readInt32s(`${id}-data`);
				if (!data) return undefined;
		
				return [ metadata, tfcore.tensor(data) ];
			}
			case 'uint8': {
				const data: Uint8Array|undefined = await this.shm.readUint8s(`${id}-data`);
				if (!data) return undefined;
		
				return [ metadata, tfcore.tensor(data) ];
			}
			default:
				throw new Error('Unknown tensor dtype');
		}
	}

	public async read1d(id: string): Promise<tfcore.Tensor1D|undefined> {
		const read: [TTensorMetadata, tfcore.Tensor ]|undefined = await this.read(id);
		if (!read) return undefined;

		const metadata: TTensorMetadata = read[0];
		if (metadata.shape.length !== 1) throw new Error('Tensor is not 1d');

		const tensor: tfcore.Tensor = read[1];

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
		const reshaped: tfcore.Tensor1D = (tensor as any).reshape(metadata.shape) as tfcore.Tensor1D;

		return reshaped;
	}

	public async read2d(id: string): Promise<tfcore.Tensor2D|undefined> {
		const read: [TTensorMetadata, tfcore.Tensor ]|undefined = await this.read(id);
		if (!read) return undefined;

		const metadata: TTensorMetadata = read[0];
		if (metadata.shape.length !== 2) throw new Error('Tensor is not 2d');
		
		const tensor: tfcore.Tensor = read[1];

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
		const reshaped: tfcore.Tensor2D = (tensor as any).reshape(metadata.shape) as tfcore.Tensor2D;

		return reshaped;
	}

	public async read3d(id: string): Promise<tfcore.Tensor3D|undefined> {
		const read: [TTensorMetadata, tfcore.Tensor ]|undefined = await this.read(id);
		if (!read) return undefined;

		const metadata: TTensorMetadata = read[0];
		if (metadata.shape.length !== 3) throw new Error('Tensor is not 3d');
		
		const tensor: tfcore.Tensor = read[1];

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
		const reshaped: tfcore.Tensor3D = (tensor as any).reshape(metadata.shape) as tfcore.Tensor3D;

		return reshaped;
	}

	public async read4d(id: string): Promise<tfcore.Tensor4D|undefined> {
		const read: [TTensorMetadata, tfcore.Tensor ]|undefined = await this.read(id);
		if (!read) return undefined;

		const metadata: TTensorMetadata = read[0];
		if (metadata.shape.length !== 4) throw new Error('Tensor is not 4d');
		
		const tensor: tfcore.Tensor = read[1];

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
		const reshaped: tfcore.Tensor4D = (tensor as any).reshape(metadata.shape) as tfcore.Tensor4D;

		return reshaped;
	}

	public async write(
			id: string,
			tensor: tfcore.Tensor
	): Promise<void> {
		const metadata: TTensorMetadata = {
				shape: [ ...tensor.shape ],
				dtype: tensor.dtype,
				size: tensor.size,
				rank: tensor.rank
		};

		await this.shm.writeString(
				`${id}-metadata`,
				JSON.stringify(metadata)
		);

		switch (metadata.dtype) {
			case 'float32':
				await this.shm.writeFloat32s(
						`${id}-data`,
						await tensor.data() as Float32Array
				);
				break;
			case 'int32':
				await this.shm.writeInt32s(
						`${id}-data`,
						await tensor.data() as Int32Array
				);
				break;
			case 'float32':
				await this.shm.writeUint8s(
						`${id}-data`,
						await tensor.data() as Uint8Array
				);
				break;
			default:
				throw new Error('Unknown tensor dtype');
		}
	}

	public free(id: string): void {
		this.shm.free(`${id}-metadata`);
		this.shm.free(`${id}-data`);
	}
}
