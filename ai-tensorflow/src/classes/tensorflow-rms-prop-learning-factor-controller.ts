import * as tfcore from '@tensorflow/tfjs-core';

import { TCommonsAiTrainingStats } from 'tscommons-es-ai';

import { CommonsAiTensorflowEarlyStopping } from './tensorflow-early-stopping';

// This is a true interface
export interface ILearningFactorControllable {
	getOptimizer(): tfcore.RMSPropOptimizer;
}

export class CommonsAiTensorflowRMSPropLearningFactorController extends CommonsAiTensorflowEarlyStopping {
	private controllable: ILearningFactorControllable|undefined;

	constructor(
			private period: number = 5,
			private reductionFactor: number = 2.5,
			private displayCallback?: (
					stats: TCommonsAiTrainingStats,
					factor: number,
					existing: number,
					updated: number
			) => void
	) {
		super();
	}

	public setControllable(controllable: ILearningFactorControllable): void {
		this.controllable = controllable;
	}

	public async consider(stats: TCommonsAiTrainingStats): Promise<boolean> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.controllable) return true;	// abort if the merger doesn't exist for some reason

		if (stats.epochs > 0 && (stats.epochs % this.period) === 0) {
			const typecast: { learningRate: number } = this.controllable.getOptimizer() as unknown as { learningRate: number };

			const factor: number = this.reductionFactor;
			const existing: number = typecast.learningRate;
			const updated: number = existing / factor;

			typecast.learningRate = updated;

			if (this.displayCallback) {
				this.displayCallback(
						stats,
						factor,
						existing,
						updated
				);
			}
		}

		return false;
	}
}
