import * as tflayers from '@tensorflow/tfjs-layers';

import { TCommonsAiTrainingStats } from 'tscommons-es-ai';

import { ICommonsAiEarlyStopping } from 'nodecommons-es-ai';

export abstract class CommonsAiTensorflowEarlyStopping extends tflayers.Callback implements ICommonsAiEarlyStopping {
	private epochs: number = 0;

	public abstract consider(stats: TCommonsAiTrainingStats): Promise<boolean>;

	public getCompletedEpochs(): number {
		return this.epochs;
	}

	// eslint-disable-next-line @typescript-eslint/require-await
	public override async onTrainBegin(_logs?: tflayers.Logs): Promise<void> {
		this.epochs = 0;
	}

	// eslint-disable-next-line @typescript-eslint/require-await
	public override async onEpochEnd(epoch: number, logs?: tflayers.Logs): Promise<void> {
		this.epochs = epoch + 1;	// confusingly tfjs displays "Epoch [1...] of n" rather than "Epoch [0...] of n"

		if (!logs) return;

		const stats: TCommonsAiTrainingStats = {
				epochs: this.epochs,
				loss: logs.loss,
				acc: logs.acc
		};
		if (logs.val_loss !== undefined) stats.val_loss = logs.val_loss;
		if (logs.val_acc !== undefined) stats.val_acc = logs.val_acc;

		if (await this.consider(stats)) this.model.stopTraining = true;
	}
}
