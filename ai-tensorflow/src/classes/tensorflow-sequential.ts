import * as tfcore from '@tensorflow/tfjs-core';
import * as tflayers from '@tensorflow/tfjs-layers';
import * as tftypes from '@tensorflow/tfjs-layers/dist/types';

import { commonsArrayShuffle, commonsTypeHasPropertyArray } from 'tscommons-es-core';
import {
		ICommonsAiModelPredictable,
		ICommonsAiModelTrainable,
		TCommonsAiEpochTrainingStats,
		TCommonsAiGenericTrainingData,
		TCommonsAiOutputWithCertainty,
		TCommonsAiQuality
} from 'tscommons-es-ai';

import { ICommonsAiEarlyStopping } from 'nodecommons-es-ai';

import { ICommonsAiTensorflowExportable } from '../interfaces/icommons-ai-tensorflow-exportable';

export abstract class CommonsAiTensorflowSequential<
		InputT,
		OutputT,
		XRankT extends tfcore.Rank,
		YRankT extends tfcore.Rank
> implements ICommonsAiModelPredictable<InputT, OutputT> {
	protected model: tflayers.LayersModel|undefined;

	constructor(
			protected asyncInput: boolean = false,
			protected asyncOutput: boolean = false
	) {}

	public async init(): Promise<void> {
		this.model = await this.constructSequential();
	}

	protected abstract constructSequential(): Promise<tflayers.LayersModel>;

	protected asyncConvertInputToX(_value: InputT): Promise<tfcore.Tensor<XRankT>> {
		throw new Error('asyncConvertInputToX has not been implemented');
	}
	protected asyncConvertPredictionToOutput(_prediction: tfcore.Tensor<YRankT>): Promise<TCommonsAiOutputWithCertainty<OutputT>|undefined> {
		throw new Error('asyncConvertPredictionToOutput has not been implemented');
	}

	protected convertInputToX(_value: InputT): tfcore.Tensor<XRankT> {
		throw new Error('convertInputToX has not been implemented');
	}
	protected convertPredictionToOutput(_prediction: tfcore.Tensor<YRankT>): TCommonsAiOutputWithCertainty<OutputT>|undefined {
		throw new Error('convertPredictionToOutput has not been implemented');
	}

	public async predict(input: InputT): Promise<TCommonsAiOutputWithCertainty<OutputT>|undefined> {
		if (!this.model) throw new Error('Model has not been initialised');

		const tensor: tfcore.Tensor<XRankT> = this.asyncInput ? (await this.asyncConvertInputToX(input)) : this.convertInputToX(input);

		const result: tfcore.Tensor<YRankT> = this.model.predict(tfcore.stack([ tensor ])) as tfcore.Tensor<YRankT>;

		return this.asyncOutput ? (await this.asyncConvertPredictionToOutput(result)) : this.convertPredictionToOutput(result);
	}

	public syncPredict(input: InputT): TCommonsAiOutputWithCertainty<OutputT>|undefined {
		if (!this.model) throw new Error('Model has not been initialised');
		
		if (this.asyncInput || this.asyncOutput) throw new Error('Cannot use syncPredict on a model with async input or output');

		const tensor: tfcore.Tensor<XRankT> = this.convertInputToX(input);

		const result: tfcore.Tensor<YRankT> = this.model.predict(tfcore.stack([ tensor ])) as tfcore.Tensor<YRankT>;

		return this.convertPredictionToOutput(result);
	}

	public dispose(): void {
		if (this.model) this.model.dispose();
	}
}

export abstract class CommonsAiTensorflowSequentialTraining<
		InputT,
		OutputT,
		XRankT extends tfcore.Rank,
		YRankT extends tfcore.Rank
> extends CommonsAiTensorflowSequential<
		InputT,
		OutputT,
		XRankT,
		YRankT
> implements ICommonsAiModelTrainable<
		InputT,
		OutputT,
		TCommonsAiEpochTrainingStats
>, ICommonsAiTensorflowExportable {
	private trainTensorsXs: tfcore.Tensor<tfcore.Rank>|undefined;
	private trainTensorsYs: tfcore.Tensor<tfcore.Rank>|undefined;
	private valTensorsXs: tfcore.Tensor<tfcore.Rank>|undefined;
	private valTensorsYs: tfcore.Tensor<tfcore.Rank>|undefined;

	private trainingStats: TCommonsAiEpochTrainingStats|undefined;
	private initialEpoch: number = 0;

	private verbosity: number = 1;

	constructor(
			private training: TCommonsAiGenericTrainingData<InputT, OutputT>[],
			private epochs: number,
			private validation?: TCommonsAiGenericTrainingData<InputT, OutputT>[],
			protected earlyStopping?: ICommonsAiEarlyStopping,
			asyncInput: boolean = false,
			asyncOutput: boolean = false,
			private lossFunction: string = 'categoricalCrossentropy',
			protected optimizer: string|tfcore.Optimizer = tfcore.train.adam(0.01),
			private metrics: (string|tftypes.LossOrMetricFn)[] = [ 'accuracy' ],
			private shuffle: boolean = true,
			private batchSize?: number
	) {
		super(asyncInput, asyncOutput);

		if (this.earlyStopping && !(this.earlyStopping instanceof tflayers.Callback)) throw new Error('Early stopping is not an instance of tf.Callback');
	}

	public setSuppressVerbose(verbosity: number = 0): void {
		this.verbosity = verbosity;
	}

	protected abstract addLayers(inputShape: number[]): void;

	protected asyncBuildExampleToXY(_example: TCommonsAiGenericTrainingData<InputT, OutputT>): Promise<[ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ]> {
		throw new Error('asyncBuildExampleToXY has not been implemented');
	}

	protected buildExampleToXY(_example: TCommonsAiGenericTrainingData<InputT, OutputT>): [ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ] {
		throw new Error('buildExampleToXY has not been implemented');
	}

	// helper method to allow things like manually converting test data for export etc
	public async buildXYFromExamples(
			examples: TCommonsAiGenericTrainingData<InputT, OutputT>[]
	): Promise<[ tfcore.Tensor<tfcore.Rank>, tfcore.Tensor<tfcore.Rank> ]> {
		const converted: [ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ][] = (this.asyncInput || this.asyncOutput)
			? (await Promise.all(examples
					.map((example: TCommonsAiGenericTrainingData<InputT, OutputT>): Promise<[ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ]> => this.asyncBuildExampleToXY(example))
			)) : (examples
					.map((example: TCommonsAiGenericTrainingData<InputT, OutputT>): [ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ] => this.buildExampleToXY(example))
			);
		
		const xs: tfcore.Tensor<XRankT>[] = converted
				.map((item: [ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ]): tfcore.Tensor<XRankT> => item[0]);
		const ys: tfcore.Tensor<YRankT>[] = converted
				.map((item: [ tfcore.Tensor<XRankT>, tfcore.Tensor<YRankT> ]): tfcore.Tensor<YRankT> => item[1]);

		return [
				tfcore.stack(xs),
				tfcore.stack(ys)
		];
	}

	private async buildXYs(
			examples: TCommonsAiGenericTrainingData<InputT, OutputT>[]
	): Promise<[ tfcore.Tensor<tfcore.Rank>, tfcore.Tensor<tfcore.Rank> ]> {
		if (this.shuffle) {
			const shuffled: TCommonsAiGenericTrainingData<InputT, OutputT>[] = [ ...examples ];
			commonsArrayShuffle(shuffled);
			examples = shuffled;
		}

		return await this.buildXYFromExamples(examples);
	}

	public async build(): Promise<void> {
		[ this.trainTensorsXs, this.trainTensorsYs ] = await this.buildXYs(this.training);
		if (this.validation && this.validation.length > 0) [ this.valTensorsXs, this.valTensorsYs ] = await this.buildXYs(this.validation);
	}

	private assertBuilt(): void {
		if (
			!this.trainTensorsXs
			|| !this.trainTensorsYs
			|| (
				this.validation !== undefined
				&& this.validation.length > 0
				&& (
					!this.valTensorsXs
					|| !this.valTensorsYs
				)
			)
		) throw new Error('Building has not been performed');

		return;
	}

	public exportBuiltTrainingAndValidationXyTensors(): [ tfcore.Tensor, tfcore.Tensor, tfcore.Tensor|undefined, tfcore.Tensor|undefined ] {
		this.assertBuilt();

		return [
				this.trainTensorsXs!,
				this.trainTensorsYs!,
				this.valTensorsXs,
				this.valTensorsYs
		];
	}

	protected builtInputShape(): number[] {
		this.assertBuilt();

		return [ ...this.trainTensorsXs!.shape ].slice(1);
	}

	public async train(): Promise<TCommonsAiEpochTrainingStats> {
		if (!this.model) throw new Error('Model has not been initialised');
		
		this.assertBuilt();
		
		this.addLayers(this.builtInputShape());
		if (this.verbosity > 0) this.model.summary();
		
		this.model.compile({
				optimizer: this.optimizer,
				loss: this.lossFunction,
				metrics: [ ...this.metrics ]
		});

		this.initialEpoch = 0;

		return await this.resume();
	}

	public async resume(): Promise<TCommonsAiEpochTrainingStats> {
		if (!this.model) throw new Error('Model has not been initialised');
		
		this.assertBuilt();

		const callbacks: tflayers.CustomCallbackArgs[] = [];
		if (this.earlyStopping) callbacks.push(this.earlyStopping as unknown as tflayers.Callback);
 
		const history: tflayers.History = await this.model.fit(
				this.trainTensorsXs!,
				this.trainTensorsYs!,
				{
						epochs: this.epochs,
						validationData: (this.validation && this.validation.length > 0)  ? [ this.valTensorsXs!, this.valTensorsYs! ] : undefined,
						callbacks: callbacks,
						initialEpoch: this.initialEpoch,
						batchSize: this.batchSize,
						verbose: this.verbosity
				}
		);

		const stats: TCommonsAiEpochTrainingStats = {
				epochs: this.earlyStopping ? this.earlyStopping.getCompletedEpochs() : this.epochs
		};

		for (const key of Object.keys(history.history)) {
			if (!commonsTypeHasPropertyArray(history.history, key)) continue;
			const value: number|undefined = history.history[key].pop() as (number|undefined);

			stats[key] = value || -1;
		}
		
		this.trainingStats = stats;

		this.initialEpoch = this.trainingStats.epochs;

		return stats;
	}

	public getTrainingStats(): TCommonsAiEpochTrainingStats {
		if (!this.trainingStats) throw new Error('Training has not been performed yet');

		return this.trainingStats;
	}

	protected abstract calculateQuality(actual: TCommonsAiOutputWithCertainty<OutputT>, expected: OutputT): [ number, number ];

	public async test(testData: TCommonsAiGenericTrainingData<InputT, OutputT>[]): Promise<TCommonsAiQuality> {
		if (testData.length === 0) throw new Error('No data to test on');
		
		let positiveQuality: number = 0;
		let negativeQuality: number = 0;
		for (const test of testData) {
			const prediction: TCommonsAiOutputWithCertainty<OutputT>|undefined = await this.predict(test.input);

			if (!prediction) {
				negativeQuality++;
			} else {
				const [ positive, negative ]: [ number, number ] = this.calculateQuality(prediction, test.output);

				positiveQuality += positive;
				negativeQuality += negative;
			}
		}

		return {
				positive: positiveQuality / testData.length,
				negative: negativeQuality / testData.length
		};
	}
}
