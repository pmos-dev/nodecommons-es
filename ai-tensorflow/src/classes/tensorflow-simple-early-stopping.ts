import { TCommonsAiTrainingStats } from 'tscommons-es-ai';

import { CommonsAiTensorflowEarlyStopping } from './tensorflow-early-stopping';

export enum ECommonsAiTensorflowSimpleEarlyStoppingLogic {
		AND = 'and',
		OR = 'or'
}

export type TCommonsAiTensorflowSimpleEarlyStoppingOptions = {
		acceptableLossThresh?: number;
		acceptableAccThresh?: number;
		acceptableValLossThresh?: number;
		acceptableValAccThresh?: number;
		acceptableCombinedLossThresh?: number;
		acceptableCombinedAccThresh?: number;
		acceptableCombinedValWeight?: number;
		acceptableLogic?: ECommonsAiTensorflowSimpleEarlyStoppingLogic;
		deadSameLossQuantity?: number;
		deadLossDecimalPlaces?: number;
		maximumEpochs?: number;	// this is sometimes useful for mid-way pause/resume
		maximumRelativeEpochs?: number;	// this is sometimes useful for mid-way pause/resume
};

const DEFAULT_EARLY_STOPPING_OPTIONS: TCommonsAiTensorflowSimpleEarlyStoppingOptions = {
		acceptableCombinedValWeight: 0.65,
		acceptableLogic: ECommonsAiTensorflowSimpleEarlyStoppingLogic.OR,
		deadLossDecimalPlaces: 3
};

export class CommonsAiTensorflowSimpleEarlyStopping extends CommonsAiTensorflowEarlyStopping {
	private options: TCommonsAiTensorflowSimpleEarlyStoppingOptions;
	private lossStack: string[] = [];
	private initialEpoch: number = 0;

	constructor(
			options: TCommonsAiTensorflowSimpleEarlyStoppingOptions
	) {
		super();

		this.options = {
				...DEFAULT_EARLY_STOPPING_OPTIONS,
				...options
		};

		if (this.options.deadSameLossQuantity !== undefined) {
			if (this.options.deadSameLossQuantity < 2) throw new Error('Dead same loss can only be calculated for 2 or more epochs');
			if (this.options.deadLossDecimalPlaces === undefined) throw new Error('Dead same loss decimal places cannot be undefined if used');
		}

		if (this.options.acceptableCombinedLossThresh !== undefined || this.options.acceptableCombinedAccThresh !== undefined) {
			if (this.options.acceptableCombinedValWeight === undefined) throw new Error('A val_* weight must be specified for combined thresholds');
		}

		if (
			this.options.acceptableLossThresh !== undefined
			|| this.options.acceptableAccThresh !== undefined
			|| this.options.acceptableValLossThresh !== undefined
			|| this.options.acceptableValAccThresh !== undefined
			|| this.options.acceptableCombinedLossThresh !== undefined
			|| this.options.acceptableCombinedAccThresh !== undefined
		) {
			if (this.options.acceptableLogic === undefined) throw new Error('A logic must be specified for acceptable thresholds');
		}
	}

	public setInitialEpoch(epoch: number): void {
		this.initialEpoch = epoch;
	}

	public async consider(stats: TCommonsAiTrainingStats): Promise<boolean> {	// eslint-disable-line @typescript-eslint/require-await
		let matches: number = 0;
		let matchables: number = 0;

		if (this.options.maximumEpochs !== undefined && stats.epochs >= this.options.maximumEpochs) {
			matchables++;
			matches++;
		}

		if (this.options.maximumRelativeEpochs !== undefined && (stats.epochs - (this.initialEpoch - 1)) >= this.options.maximumRelativeEpochs) {
			matchables++;
			matches++;
		}

		if (this.options.acceptableLossThresh !== undefined) {
			matchables++;
			if (stats.loss < this.options.acceptableLossThresh) matches++;
		}

		if (this.options.acceptableValLossThresh !== undefined && stats.val_loss !== undefined) {
			matchables++;
			if (stats.val_loss < this.options.acceptableValLossThresh) matches++;
		}

		if (this.options.acceptableCombinedLossThresh !== undefined && stats.val_loss !== undefined) {
			matchables++;

			const combined: number = (stats.loss * (1 - this.options.acceptableCombinedValWeight!)) + (stats.val_loss * this.options.acceptableCombinedValWeight!);
			if (combined < this.options.acceptableCombinedLossThresh) matches++;
		}

		if (this.options.acceptableAccThresh !== undefined) {
			matchables++;
			if (stats.acc > this.options.acceptableAccThresh) matches++;
		}

		if (this.options.acceptableValAccThresh !== undefined && stats.val_acc !== undefined) {
			matchables++;
			if (stats.val_acc > this.options.acceptableValAccThresh) matches++;
		}

		if (this.options.acceptableCombinedAccThresh !== undefined && stats.val_acc !== undefined) {
			matchables++;

			const combined: number = (stats.acc * (1 - this.options.acceptableCombinedValWeight!)) + (stats.val_acc * this.options.acceptableCombinedValWeight!);
			if (combined > this.options.acceptableCombinedAccThresh) matches++;
		}

		if (matchables > 0) {
			switch (this.options.acceptableLogic) {
				case ECommonsAiTensorflowSimpleEarlyStoppingLogic.OR:
					if (matches > 0) return true;
					break;
				case ECommonsAiTensorflowSimpleEarlyStoppingLogic.AND:
					if (matches === matchables) return true;
					break;
				default:
					throw new Error('Invalid logic specification');
			}
		}

		if (this.options.deadSameLossQuantity !== undefined) {
			this.lossStack.push(stats.loss.toFixed(this.options.deadLossDecimalPlaces));
			if (this.lossStack.length > this.options.deadSameLossQuantity) this.lossStack.shift();

			const first: string = this.lossStack[0];
			let sameTally: number = 1;
			for (let i = this.lossStack.length; i-- > 1;) {
				if (this.lossStack[i] === first) sameTally++;
			}

			if (this.options.deadSameLossQuantity === sameTally) return true;
		}

		return false;
	}
}
