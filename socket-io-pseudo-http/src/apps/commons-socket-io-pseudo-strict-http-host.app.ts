import { Socket, ServerOptions } from 'socket.io';

import { commonsBase62HasPropertyId, commonsTypeHasPropertyString } from 'tscommons-es-core';
import { ECommonsHttpMethod } from 'tscommons-es-http';
import { isTCommonsSocketIoPseudoQuery, isTCommonsSocketIoPseudoResponse } from 'tscommons-es-socket-io-pseudo-http';

import { CommonsStrictExpressServer, ICommonsExpressConfig, commonsExpressBuildDefaultServer, isICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsAppSocketIoServer, CommonsSocketIoApp } from 'nodecommons-es-app-socket-io';
import { commonsOutputDebug, commonsOutputError, commonsOutputInfo } from 'nodecommons-es-cli';

type TIdWithAuthority = {
		id: string;
		authorityKey: string;
};

class SocketIoServer extends CommonsAppSocketIoServer {
	private pseudoDomains: Map<string, TIdWithAuthority> = new Map<string, TIdWithAuthority>();

	constructor(
			private registerAuthKey: string,
			expressServer: CommonsStrictExpressServer,
			expressConfig: ICommonsExpressConfig,
			options?: Partial<ServerOptions>
	) {
		super(expressServer, expressConfig, true, options);

		this.on(
				'http/register',
				(data: unknown): void => this.handleRegister(data)
		);

		this.on(
				'http/request/head',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.HEAD, data)
		);
		this.on(
				'http/request/get',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.GET, data)
		);
		this.on(
				'http/request/post',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.POST, data)
		);
		this.on(
				'http/request/put',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.PUT, data)
		);
		this.on(
				'http/request/patch',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.PATCH, data)
		);
		this.on(
				'http/request/delete',
				(data: unknown): void => this.handleRequest(ECommonsHttpMethod.DELETE, data)
		);

		this.on(
				'http/response',
				(data: unknown): void => this.handleResponse(data)
		);
	}

	private handleRegister(data: unknown): void {
		commonsOutputDebug('Handling incoming registration');

		if (!commonsTypeHasPropertyString(data, 'registerAuthKey')) return;
		const registerAuthKey: string = data.registerAuthKey as string;
		if (registerAuthKey !== this.registerAuthKey) {
			commonsOutputDebug('Invalid registerAuthKey supplied. Ignoring');
			return;
		}

		if (!commonsTypeHasPropertyString(data, 'pseudoDomain')) return;
		const pseudoDomain: string = data.pseudoDomain as string;

		if (!commonsTypeHasPropertyString(data, 'authorityKey')) return;
		const authorityKey: string = data.authorityKey as string;

		const existing: TIdWithAuthority|undefined = this.pseudoDomains.get(pseudoDomain);
		if (existing && existing.authorityKey !== authorityKey) {
			// Refuse to replace a server without a corresponding authorityKey
			// This isn't the best security, but prevents trivial 'replacement' of a handler, i.e. first one gets protected
			// To replace it, have to manually restart the host server
			commonsOutputDebug('Pseudo domain already exists, and authorityKey does not match. Ignoring');
			return;
		}

		if (!commonsBase62HasPropertyId(data, 'id')) return;
		const id: string = data.id as string;

		commonsOutputInfo(`Registering ${pseudoDomain} as handled by id ${id}`);

		this.pseudoDomains.set(
				pseudoDomain,
				{
						id: id,
						authorityKey: authorityKey
				}
		);
	}

	private handleRequest(method: ECommonsHttpMethod, data: unknown): void {
		commonsOutputDebug(`Handling request for ${method}`);

		if (!isTCommonsSocketIoPseudoQuery(data)) {
			commonsOutputError('Invalid request');
			return;
		}

		const serverId: TIdWithAuthority|undefined = this.pseudoDomains.get(data.pseudoDomain);
		if (!serverId) {
			commonsOutputError('No such server is registered');
			return;
		}
		commonsOutputDebug(`Identified server for ${data.pseudoDomain} to be ${serverId.id}`);

		const socket: Socket|undefined = this.getSocketById(serverId.id);
		if (!socket) {
			commonsOutputError('Unable to identify server socket. Maybe it has disconnected?');
			return;
		}

		void this.direct(socket, `http/request/${method}`, data);
	}

	private handleResponse(data: unknown): void {
		if (!isTCommonsSocketIoPseudoResponse(data)) {
			commonsOutputError('Invalid response');
			return;
		}

		const socket: Socket|undefined = this.getSocketById(data.clientId);
		if (!socket) {
			commonsOutputError('Unable to identify requester socket. Maybe it has disconnected?');
			return;
		}

		commonsOutputDebug(`Handling response for client ${data.clientId}, query ${data.queryUid}`);
		void this.direct(socket, 'http/response', data);
	}
}

export class CommonsSocketIoPseudoStrictHttpHostApp extends CommonsSocketIoApp<SocketIoServer> {
	private registerAuthKey: string|undefined;

	public setRegisterAuthKey(key: string): void {
		this.registerAuthKey = key;
	}

	protected override buildSocketIoServer(_expressServer: CommonsStrictExpressServer, expressConfig: ICommonsExpressConfig): SocketIoServer {
		if (!this.registerAuthKey) throw new Error('registerAuthKey has not been set');

		return new SocketIoServer(
				this.registerAuthKey,
				this.getHttpServer(),
				expressConfig
		);
	}

	protected override buildHttpServer(): CommonsStrictExpressServer {
		const expressConfig: unknown = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Invalid express config');

		return commonsExpressBuildDefaultServer(this.httpConfig.port, expressConfig.bodyParserLimit);
	}
}
