import { TEncodedObject, TKeyObject, TPrimativeObject, commonsBase62GenerateRandomId, commonsTypeAttemptString, commonsTypeHasPropertyString, commonsTypeIsStringArray } from 'tscommons-es-core';
import { commonsAsyncAbortTimeout, commonsAsyncTimeout } from 'tscommons-es-async';
import { CommonsHttpError, ECommonsHttpContentType, ECommonsHttpMethod, ECommonsHttpResponseDataType, ICommonsHttpClientImplementation, TCommonsHttpInternalRequestOptions, THttpHeaderOrParamObject } from 'tscommons-es-http';
import { TCommonsSocketIoPseudoDecodedResponse, isTCommonsSocketIoPseudoResponse, isTCommonsSocketIoPseudoDecodedResponse, TCommonsSocketIoPseudoQuery } from 'tscommons-es-socket-io-pseudo-http';

import { CommonsSocketIoClientService } from 'nodecommons-es-socket-io';
import { commonsOutputDebug } from 'nodecommons-es-cli';

const PSEUDO_URL_REGEX: RegExp = /^pseudo:\/\/([-a-z0-9.]+)(?:\/([^?]*))(?:[?](.*))?$/;

type TPseudoUrlParse = {
		pseudoDomain: string;
		url: string;
		query: TKeyObject<string>|undefined;
};

function parsePseudoUrl(url: string): TPseudoUrlParse {
	const regex: RegExpExecArray|null = PSEUDO_URL_REGEX.exec(url);
	if (!regex) throw new Error('Invalid pseudo URL');

	const parsed: TPseudoUrlParse = {
			pseudoDomain: regex[1],
			url: `/${regex[2]}`,
			query: undefined
	};

	if (regex[3] && regex[3] !== '') {
		const pairs: string[][] = regex[3]
				.split('&')
				.map((s: string): string[] => s.split('='))
				.filter((pair: string[]): boolean => pair.length === 2);
		
		parsed.query = {};
		for (const pair of pairs) {
			parsed.query[pair[0]] = pair[1];
		}
	}

	return parsed;
}

function reduceAnyQueryArray(query: THttpHeaderOrParamObject): TKeyObject<string> {
	const rebuild: TKeyObject<string> = {};

	for (const key of Object.keys(query)) {
		const value: string|string[] = query[key];

		if (commonsTypeIsStringArray(value)) {
			// just use the first for now. Not ideal.
			if (value.length > 0) rebuild[key] = value[0];
		} else {
			rebuild[key] = value;
		}
	}

	return rebuild;
}

export class CommonsSocketIoPseudoStrictHttpClientImplementation extends CommonsSocketIoClientService implements ICommonsHttpClientImplementation {
	private pendings: Map<string, (data: TCommonsSocketIoPseudoDecodedResponse) => void> = new Map<string, (data: TCommonsSocketIoPseudoDecodedResponse) => void>();
	private selfId: string|undefined;

	protected override setupOns(): void {
		this.on(
				'http/response',
				(data: unknown): void => {
					if (!isTCommonsSocketIoPseudoResponse(data)) {
						commonsOutputDebug('Invalid response data');
						return;
					}

					if (data.clientId !== this.selfId) {
						// shouldn't be possible due to the direct sockets, but in case
						commonsOutputDebug('Socket clientId/selfId mismatch. This shouldn\'t be possible.');
						return;
					}

					const pending: undefined|((data: TCommonsSocketIoPseudoDecodedResponse) => void) = this.pendings.get(data.queryUid);
					if (!pending) {
						commonsOutputDebug('No such uid exists. May have timed out');
						return;
					}

					const decoded: unknown = JSON.parse(data.result);
					if (!isTCommonsSocketIoPseudoDecodedResponse(decoded)) {
						commonsOutputDebug('Invalid TDecodedResponse data');
						return;
					}

					pending(decoded);
				}
		);
	}

	public async init(): Promise<void> {
		this.setEnabled(true);
		this.connect();

		await commonsAsyncTimeout(500);
		
		const id: string|undefined = await this.getId();
		if (!id) throw new Error('Unable to fetch self-id');

		this.selfId = id;
		commonsOutputDebug(`Self-id is ${this.selfId}`);
	}

	private request(
			method: ECommonsHttpMethod,
			pseudoDomain: string,
			url: string,
			query?: TPrimativeObject,
			headers?: TKeyObject<string>,
			body?: TEncodedObject,
			timeout: number = 5000
	): Promise<string|undefined> {
		if (!this.selfId) throw new Error('Self-id hasn\'t been fetched yet.');

		const queryUid: string = commonsBase62GenerateRandomId();

		const pseudoQuery: TCommonsSocketIoPseudoQuery = {
				pseudoDomain: pseudoDomain,
				clientId: this.selfId,
				queryUid: queryUid,
				url: url,
				query: query || {},
				headers: headers || {},
				body: body
		};

		return new Promise((resolve: (body?: string) => void, reject: (e: Error) => void): void => {
			this.pendings.set(
					queryUid,
					(data: TCommonsSocketIoPseudoDecodedResponse): void => {
						commonsAsyncAbortTimeout(`commonsSocketIoPseudoStrictHttpClientService_${queryUid}`);
						this.pendings.delete(queryUid);

						if (data.statusCode === 204) {
							resolve();
							return;
						}

						let responseBody: string|undefined;
						if (commonsTypeHasPropertyString(data, 'body')) {
							const typecast: { body: string } = data as { body: string };

							responseBody = commonsTypeAttemptString(JSON.parse(typecast.body));
						}

						if (data.statusCode >= 400) {
							reject(new CommonsHttpError(responseBody || '', data.statusCode));
							return;
						}

						resolve(responseBody || '');
					}
			);

			void (async (): Promise<void> => {
				try {
					await this.emit(
							`http/request/${method}`,
							pseudoQuery
					);

					try {
						await commonsAsyncTimeout(timeout, `commonsSocketIoPseudoStrictHttpClientService_${queryUid}`);
						reject(new Error('Timeout'));
					} catch (e) {
						// do nothing
					} finally {
						this.pendings.delete(queryUid);
					}
				} catch (e) {
					commonsAsyncAbortTimeout(`commonsSocketIoPseudoStrictHttpClientService_${queryUid}`);
					reject(e as Error);
				}
			})();
		});
	}

	public async internalHead(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			options: TCommonsHttpInternalRequestOptions
	): Promise<void> {
		const parsed: TPseudoUrlParse = parsePseudoUrl(url);

		await this.request(
				ECommonsHttpMethod.HEAD,
				parsed.pseudoDomain,
				parsed.url,
				{ ...parsed.query, ...reduceAnyQueryArray(params) },
				reduceAnyQueryArray(headers),
				undefined,
				options.timeout
		);
	}

	private async responseRequest(
			method: ECommonsHttpMethod.GET|ECommonsHttpMethod.POST|ECommonsHttpMethod.PUT|ECommonsHttpMethod.PATCH|ECommonsHttpMethod.DELETE,
			url: string,
			body: TEncodedObject|undefined,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType|undefined,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		const parsed: TPseudoUrlParse = parsePseudoUrl(url);

		if (bodyDataEncoding) {
			switch (bodyDataEncoding) {
				case ECommonsHttpContentType.JSON:
					break;
				default:
					throw new Error('Only JSON encoding is supported');
			}
		}

		let response: string|undefined = await this.request(
				method,
				parsed.pseudoDomain,
				parsed.url,
				{ ...parsed.query, ...reduceAnyQueryArray(params) },
				reduceAnyQueryArray(headers),
				body,
				options.timeout
		);
		if (!response) response = '';

		switch (responseDataType) {
			case ECommonsHttpResponseDataType.STRING:
				return response;
			case ECommonsHttpResponseDataType.UINT8ARRAY:
				return new TextEncoder().encode(response);
		}
	}

	public async internalGet(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string | Uint8Array> {
		return this.responseRequest(
				ECommonsHttpMethod.GET,
				url,
				undefined,
				params,
				headers,
				undefined,
				responseDataType,
				options
		);
	}

	private async bodyRequest<B extends TEncodedObject = TEncodedObject>(
			method: ECommonsHttpMethod.POST|ECommonsHttpMethod.PUT|ECommonsHttpMethod.PATCH,
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return this.responseRequest(
				method,
				url,
				body,
				params,
				headers,
				bodyDataEncoding,
				responseDataType,
				options
		);
	}

	public async internalPost<B extends TEncodedObject = TEncodedObject>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string | Uint8Array> {
		return this.bodyRequest(
				ECommonsHttpMethod.POST,
				url,
				body,
				params,
				headers,
				bodyDataEncoding,
				responseDataType,
				options
		);
	}

	public async internalPut<B extends TEncodedObject = TEncodedObject>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string | Uint8Array> {
		return this.bodyRequest(
				ECommonsHttpMethod.PUT,
				url,
				body,
				params,
				headers,
				bodyDataEncoding,
				responseDataType,
				options
		);
	}

	public async internalPatch<B extends TEncodedObject = TEncodedObject>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string | Uint8Array> {
		return this.bodyRequest(
				ECommonsHttpMethod.PATCH,
				url,
				body,
				params,
				headers,
				bodyDataEncoding,
				responseDataType,
				options
		);
	}

	public async internalDelete(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string | Uint8Array> {
		return this.responseRequest(
				ECommonsHttpMethod.DELETE,
				url,
				undefined,
				params,
				headers,
				undefined,
				responseDataType,
				options
		);
	}
}
