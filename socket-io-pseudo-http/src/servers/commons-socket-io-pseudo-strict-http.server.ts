import { ManagerOptions } from 'socket.io-client';

import {
		TEncodedObject,
		commonsTypeHasPropertyString,
		commonsTypeAttemptString,
		TPrimativeObject,
		commonsBase62HasPropertyId,
		TKeyObject
} from 'tscommons-es-core';
import { CommonsHttpError, ECommonsHttpMethod } from 'tscommons-es-http';
import { commonsAsyncTimeout, commonsPromiseTimeout } from 'tscommons-es-async';
import { TCommonsSocketIoPseudoDecodedResponse, TCommonsSocketIoPseudoQuery, isTCommonsSocketIoPseudoQuery, TCommonsSocketIoPseudoResponse } from 'tscommons-es-socket-io-pseudo-http';

import { commonsOutputDebug, commonsOutputDoing, commonsOutputError, commonsOutputFail, commonsOutputResult, commonsOutputSuccess } from 'nodecommons-es-cli';
import { CommonsStrictHttpServer, ICommonsStrictParamsRequest, TCommonsHttpHandler, TCommonsHttpResponse, TCommonsStrictParamPropertyObject, commonsExtractStrictParamsFromUrlPath } from 'nodecommons-es-http';
import { CommonsSocketIoClientService } from 'nodecommons-es-socket-io';

export class CommonsSocketIoPseudoStrictParamsRequest implements ICommonsStrictParamsRequest {
	public params: TPrimativeObject;
	public strictParams: TCommonsStrictParamPropertyObject;
	public query: TPrimativeObject;
	public headers: TKeyObject<string>;
	public body: TEncodedObject = {};

	constructor(
			params: TPrimativeObject,
			strictParams: TCommonsStrictParamPropertyObject,
			query: TPrimativeObject,
			headers: TKeyObject<string>
	) {
		this.params = params;
		this.strictParams = strictParams;
		this.query = query;
		this.headers = headers;
	}

	public internalSetBody(body: TEncodedObject): void {
		this.body = body;
	}

	public get(field: string): string|undefined {
		return commonsTypeAttemptString(this.headers[field]);
	}
}

export class CommonsSocketIoPseudoPseudoResponse implements TCommonsHttpResponse {
	private headers: TKeyObject<string> = {};
	private statusCode: number|undefined;
	private body: string|number|boolean|object|undefined;

	public setHeader(key: string, value: string) {
		this.headers[key] = value;

		return this;
	}

	public status(statusCode: number) {
		this.statusCode = statusCode;
		
		return this;
	}

	public send(body?: string | number | boolean | object | Buffer | undefined) {
		if (Buffer.isBuffer(body)) throw new Error('Buffer return types are not supported for PseudoResponse');

		this.body = body;

		if (!this.statusCode) {
			if (this.body === undefined) {
				this.status(204);
			} else {
				this.status(200);
			}
		}

		return this;
	}

	public encode(): string {
		const response: TCommonsSocketIoPseudoDecodedResponse = this.body === undefined ? {
				isEmpty: true,
				headers: this.headers,
				statusCode: this.statusCode || 500
		} : {
				headers: this.headers,
				statusCode: this.statusCode || 500,
				body: JSON.stringify(this.body)
		};

		return JSON.stringify(response);
	}
}

export type TRequestHandler = TCommonsHttpHandler<
		CommonsSocketIoPseudoStrictParamsRequest,
		CommonsSocketIoPseudoPseudoResponse
>;

type THandler = {
		method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>;
		definition: string;
		handler: (
				request: CommonsSocketIoPseudoStrictParamsRequest,
				response: CommonsSocketIoPseudoPseudoResponse
		) => Promise<void>;
};

class SocketIoClientService extends CommonsSocketIoClientService {
	private internalIsRegistered: boolean = false;
	public get isRegistered(): boolean {
		return this.internalIsRegistered;
	}

	constructor(
			private registerAuthKey: string,
			private pseudoDomain: string,
			private authorityKey: string,
			private handler: (
					method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>,
					data: TCommonsSocketIoPseudoQuery
			) => Promise<CommonsSocketIoPseudoPseudoResponse>,
			url: string,
			options?: Partial<ManagerOptions>
	) {
		super(
				url,
				true,
				options
		);

		this.addConnectCallback(
				(): void => {
					commonsOutputDebug('Socket.io re/connected');
					void (async (): Promise<void> => {
						commonsOutputDoing('Attempting to acquire internal socket.io id from upwards server');
						const id: string|undefined = await this.getId();
						if (id) {
							commonsOutputResult(id);
				
							commonsOutputDoing(`Registering pseudoDomain ${this.pseudoDomain} for id ${id}`);
							await this.registerPseudoDomain(id);
							commonsOutputSuccess();

							// assume it succeeded as there isn't really a way of checking
							this.internalIsRegistered = true;
						} else {
							commonsOutputFail('unobtainable. Pseudo server will probably not work');
							this.internalIsRegistered = false;
						}
					})();
				}
		);

		this.addDisconnectCallback(
				(): void => {
					commonsOutputDebug('Socket.io disconnected');
					this.internalIsRegistered = false;
				}
		);
	}

	protected setupOns(): void {
		this.on(
				'http/request/head',
				(data: unknown): void => this.handle(ECommonsHttpMethod.HEAD, data)
		);
		this.on(
				'http/request/get',
				(data: unknown): void => this.handle(ECommonsHttpMethod.GET, data)
		);
		this.on(
				'http/request/post',
				(data: unknown): void => this.handle(ECommonsHttpMethod.POST, data)
		);
		this.on(
				'http/request/put',
				(data: unknown): void => this.handle(ECommonsHttpMethod.PUT, data)
		);
		this.on(
				'http/request/patch',
				(data: unknown): void => this.handle(ECommonsHttpMethod.PATCH, data)
		);
		this.on(
				'http/request/delete',
				(data: unknown): void => this.handle(ECommonsHttpMethod.DELETE, data)
		);
	}

	private handle(
			method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>,
			data: unknown
	): void {
		if (!commonsTypeHasPropertyString(data, 'pseudoDomain')) return;	// cannot continue;
		if (data.pseudoDomain !== this.pseudoDomain) return;	// for another

		commonsOutputDebug(`Received request to handle a ${method} query`);

		void (async (): Promise<void> => {
			if (!commonsBase62HasPropertyId(data, 'clientId')) return;	// cannot continue;
			commonsOutputDebug(`Self ID is ${data.clientId as string}`);

			if (!commonsBase62HasPropertyId(data, 'queryUid')) return;	// cannot continue;
			commonsOutputDebug(`Query UID is ${data.queryUid as string}`);

			let response: CommonsSocketIoPseudoPseudoResponse = new CommonsSocketIoPseudoPseudoResponse();

			if (!isTCommonsSocketIoPseudoQuery(data)) {
				response.status(500);
				response.send('Invalid pseudo query data');
			} else {
				response = await this.handler(method, data);
			}

			await this.emit(
					'http/response',
					{
							clientId: (data as TCommonsSocketIoPseudoQuery).clientId,
							queryUid: (data as TCommonsSocketIoPseudoQuery).queryUid,
							result: response.encode()
					} as TCommonsSocketIoPseudoResponse
			);
		})();
	}

	public async registerPseudoDomain(id: string): Promise<void> {
		await this.emit('http/register', {
				registerAuthKey: this.registerAuthKey,
				pseudoDomain: this.pseudoDomain,
				authorityKey: this.authorityKey,
				id: id
		});
	}
}

export class CommonsSocketIoPseudoStrictHttpServer implements CommonsStrictHttpServer<
		ICommonsStrictParamsRequest,
		TCommonsHttpResponse
> {
	private client: SocketIoClientService|undefined;
	private handlers: THandler[] = [];

	constructor(
			private registerAuthKey: string,
			private pseudoDomain: string,
			private authorityKey: string,
			private socketIoServerUrl: string,
			private options?: Partial<ManagerOptions>
	) {}

	public init() {
		this.client = new SocketIoClientService(
				this.registerAuthKey,
				this.pseudoDomain,
				this.authorityKey,
				(
						method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>,
						data: TCommonsSocketIoPseudoQuery
				): Promise<CommonsSocketIoPseudoPseudoResponse> => this.handle(method, data),
				this.socketIoServerUrl,
				this.options
		);
	}

	private async handle(
			method: Exclude<ECommonsHttpMethod, ECommonsHttpMethod.OPTIONS>,
			data: TCommonsSocketIoPseudoQuery
	): Promise<CommonsSocketIoPseudoPseudoResponse> {
		commonsOutputDebug(`Received a pseudo request of ${method}: ${data.url}`);
		
		const response: CommonsSocketIoPseudoPseudoResponse = new CommonsSocketIoPseudoPseudoResponse();

		try {
			let success: boolean = false;

			for (const handler of this.handlers) {
				if (handler.method !== method) continue;
	
				const paramsMatch: [ TKeyObject<string>, TCommonsStrictParamPropertyObject ]|false = commonsExtractStrictParamsFromUrlPath(handler.definition, data.url);
				if (!paramsMatch) continue;

				commonsOutputDebug(`Found a matching handler; the strict params resolved as: ${JSON.stringify(paramsMatch)}`);

				const request: CommonsSocketIoPseudoStrictParamsRequest = new CommonsSocketIoPseudoStrictParamsRequest(
						paramsMatch[0],
						paramsMatch[1],
						{ ...data.query },
						{ ...data.headers }
				);

				if ([ ECommonsHttpMethod.POST, ECommonsHttpMethod.PUT, ECommonsHttpMethod.PATCH ].includes(method)) {
					request.internalSetBody({ ...data.body || {} });
				}

				success = true;

				await handler.handler(
						request,
						response
				);

				break;
			}

			if (!success) {
				commonsOutputDebug('No matching handler could be found');
				throw new Error('No such method');
			}
		} catch (e) {
			commonsOutputDebug(`Error response: ${JSON.stringify(e)}`);
			if (e instanceof CommonsHttpError) {
				response.status(e.httpResponseCode);
			} else {
				response.status(500);
			}

			response.send((e as Error).message);
		}

		return response;
	}

	public async listen(): Promise<void> {	// eslint-disable-line @typescript-eslint/require-await
		if (!this.client) throw new Error('Client has not been initialised');

		commonsOutputDoing('Starting SocketIO based pseudo HTTP server');
		this.client.connect();
		commonsOutputSuccess();

		commonsOutputDebug('Waiting for registration to complete');
		let abortDueToTimeout: boolean = false;
		try {
			await commonsPromiseTimeout(
					async (): Promise<void> => {
						let ttl: number = 1000;
						while (true) {
							if (abortDueToTimeout) break;
							if (ttl-- < 0) break;
							if (this.client!.isRegistered) break;

							await commonsAsyncTimeout(100);
						}
					},
					5000
			);
			commonsOutputDebug('Registration successful');
		} catch (e) {
			if ((e as Error).message === 'Timeout') {
				abortDueToTimeout = true;
				commonsOutputError('Timeout waiting for registration');
			} else {
				throw e;
			}
		}
	}
	
	public close() {
		if (!this.client) throw new Error('Client has not been initialised');

		this.client.disconnect();
	}
	
	public head(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.HEAD,
				definition: query,
				handler: handler
		});
	}

	public get(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.GET,
				definition: query,
				handler: handler
		});
	}

	public post(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.POST,
				definition: query,
				handler: handler
		});
	}

	public put(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.PUT,
				definition: query,
				handler: handler
		});
	}

	public patch(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.PATCH,
				definition: query,
				handler: handler
		});
	}

	public delete(
			query: string,
			handler: TRequestHandler
	): void {
		this.handlers.push({
				method: ECommonsHttpMethod.DELETE,
				definition: query,
				handler: handler
		});
	}
}
